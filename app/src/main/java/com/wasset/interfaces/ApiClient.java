package com.wasset.interfaces;

import com.google.gson.Gson;
import com.wasset.BuildConfig;
import com.wasset.models.Login;
import com.wasset.utility.LoggingInterceptor;
import com.wasset.utility.Utils;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by AHMED-PC on 12/6/2016.
 */

public class ApiClient {
    public static final String BASE_URL = "http://hwaseet.com.sa/api/v2/";
    public static final String STAGE_URL = "http://371587a3.ngrok.io//api/v2/";
    private static Retrofit retrofit = null;
    private static OkHttpClient okHttpClient;


    public static OkHttpClient.Builder okHttpClientDebugBuilder = new OkHttpClient.Builder()
            .readTimeout(30, TimeUnit.SECONDS)
            .connectTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .addInterceptor(new LoggingInterceptor())
            .addInterceptor(chain -> {
                Request.Builder builder = chain.request().newBuilder();
                builder.addHeader("Accept", "application/json");
                String lang = Locale.getDefault().getDisplayLanguage();
                builder.addHeader("Lang", "ar");
                Login.DataBean login = Utils.IsUserLogin();
                if (login != null && login.getToken() != null) {
                    builder.addHeader("Token", login.getToken());
                }
                Request request = builder.build();
                return chain.proceed(request);
            });

    public static OkHttpClient.Builder okHttpClientReleaseBuilder = new OkHttpClient.Builder()
            .readTimeout(30, TimeUnit.SECONDS)
            .connectTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .addInterceptor(chain -> {
                Request.Builder builder = chain.request().newBuilder();
                builder.addHeader("Accept", "application/json");
                String lang = Locale.getDefault().getDisplayLanguage();
                builder.addHeader("Lang", "ar");
                Login.DataBean login = Utils.IsUserLogin();
                if (login != null && login.getToken() != null) {
                    builder.addHeader("Token", login.getToken());
                }
                Request request = builder.build();
                return chain.proceed(request);
            });

    public static Retrofit getClient() {
        if (BuildConfig.DEBUG) {
            okHttpClient = okHttpClientDebugBuilder.build();
        } else {
            okHttpClient = okHttpClientReleaseBuilder.build();
        }

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create(new Gson()))
                    .build();
        }
        return retrofit;
    }
}
