package com.wasset.interfaces;

import android.view.View;

public interface OnAttrItemSelectListener {
    void onAttrItemSelect(String key, String value);
    void onAttrMultiItemSelect(String key1, String value1,String key2, String value2);
    void onAttrItemSelectText(String valueTitle);
}