package com.wasset.interfaces;

import com.wasset.models.CategoryProducts;
import com.wasset.models.Comments;
import com.wasset.models.Config;
import com.wasset.models.FormalResponse;
import com.wasset.models.Home;
import com.wasset.models.Login;
import com.wasset.models.Models;
import com.wasset.models.MyPosts;
import com.wasset.models.Notifications;
import com.wasset.models.PostSteps;
import com.wasset.models.categoryList;
import com.wasset.models.favorite;
import com.wasset.models.postDetails;
import com.wasset.models.publisherProfile;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

public interface ApiInterface {


    // AUTH PART

    @FormUrlEncoded
    @POST("auth/login")
    Call<Login> login(@Field("username") String username,
                      @Field("password") String password,
                      @Field("device_id") String device_id,
                      @Field("device_type") String device_type,
                      @Field("fcm") String fcm);


    @POST("auth/logout")
    Call<FormalResponse> logout();

    @FormUrlEncoded
    @POST("auth/fcm")
    Call<FormalResponse> SendFCMToken(@Field("device_id") String device_id,
                                      @Field("device_type") String device_type,
                                      @Field("fcm") String fcm);


    @FormUrlEncoded
    @POST("user/registration")
    Call<Login> registration(
            @Field("username") String username,
            @Field("email") String email,
            @Field("password") String password,
            @Field("device_id") String device_id,
            @Field("city") String city,
            @Field("device_type") String device_type,
            @Field("fcm") String fcm,
            @Field("mobile") String mobile);

    @FormUrlEncoded
    @POST("user/update")
    Call<Login> updateProfileWithPassword(@Field("username") String username,
                                          @Field("email") String email,
                                          @Field("mobile") String mobile,
                                          @Field("old_password") String current_password,
                                          @Field("password") String password,
                                          @Field("device_id") String device_id,
                                          @Field("device_type") String device_type,
                                          @Field("city") String city,
                                          @Field("fcm") String fcm);

    @FormUrlEncoded
    @POST("user/update")
    Call<Login> updateProfileWithOutPassword(@Field("username") String username,
                                             @Field("email") String email,
                                             @Field("device_id") String device_id,
                                             @Field("device_type") String device_type,
                                             @Field("city") String city,
                                             @Field("fcm") String fcm,
                                             @Field("mobile") String mobile);

    @Multipart
    @POST("user/update")
    Call<Login> updateProfile(@Part MultipartBody.Part image,
                              @PartMap() Map<String, RequestBody> partMap);

    @POST("user/info")
    Call<Login> getUserInfo();


    @POST("post/main")
    Call<Home> getMainList();

    @FormUrlEncoded
    @POST("category/list")
    Call<categoryList> getCategoryList(@Field("post_id") String post_id);


    @FormUrlEncoded
    @POST("post/details")
    Call<postDetails> getPostDetails(@Field("post_id") String post_id);


    @FormUrlEncoded
    @POST("favorite/add")
    Call<FormalResponse> addToFavorites(@Field("post_id") String id);

    @FormUrlEncoded
    @POST("favorite/list")
    Call<favorite> getFavoriteList(@Field("page_id") int page_id);

    @FormUrlEncoded
    @POST("user/posts")
    Call<MyPosts> getMyPostsList(@Field("page_id") int page_id);

    @FormUrlEncoded
    @POST("post/search")
    Call<MyPosts> search(@Field("search_text") String search_text,
                         @Field("page_id") int page_id,
                         @Field("category_id") String category_id,
                         @Field("country_id") String country_id,
                         @Field("city_id") String city_id);


    @FormUrlEncoded
    @POST("auth/recover_password")
    Call<FormalResponse> forgotPassword(@Field("email") String email);

    @POST("app/info")
    Call<Config> getConfig();


    @FormUrlEncoded
    @POST("post/list")
    Call<CategoryProducts> getCategoryPosts(@Field("category_id") String category_id, @Field("page_id") int page_id);

    @FormUrlEncoded
    @POST("contact/add")
    Call<FormalResponse> contactUs(@Field("name") String name,
                                   @Field("email") String email,
                                   @Field("mobile") String phone,
                                   @Field("title") String title,
                                   @Field("details") String details);


    @FormUrlEncoded
    @POST("user/profile")
    Call<publisherProfile> getPublisherProfile(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("user/profile")
    Call<publisherProfile> getPublisherProfileWithSharedCategory(@Field("user_id") String user_id, @Field("category_id") String categoryId, @Field("page_id") int page);


    @FormUrlEncoded
    @POST("user/profile")
    Call<publisherProfile> getPublisherProfileWithSharedCategory(@Field("user_id") String user_id, @Field("category_id") String categoryId);

    @FormUrlEncoded
    @POST("post/delete")
    Call<FormalResponse> DeletePost(@Field("post_id") String post_id);

    @FormUrlEncoded
    @POST("post/hide")
    Call<FormalResponse> HidePost(@Field("post_id") String post_id);

    @FormUrlEncoded
    @POST("post/show")
    Call<FormalResponse> ShowPost(@Field("post_id") String post_id);


    @FormUrlEncoded
    @POST("post/steps4update")
    Call<PostSteps> getPostSteps(@Field("category_id") int category_id, @Field("post_id") String post_id);


    @FormUrlEncoded
    @POST("post/models")
    Call<Models> getModels(@Field("brand_id") String brand_id, @Field("post_id") String post_id);


    @FormUrlEncoded
    @POST("post/remove_photo")
    Call<FormalResponse> removePhoto(@Field("photo_id") String photo_id);

    @Multipart
    @POST("post/add")
    Call<FormalResponse> addPost(@PartMap() Map<String, RequestBody> partMap, @Part MultipartBody.Part[] surveyImage);

    @Multipart
    @POST("post/update")
    Call<FormalResponse> editPost(@PartMap() Map<String, RequestBody> partMap, @Part MultipartBody.Part[] surveyImage);


    @FormUrlEncoded
    @POST("report/add")
    Call<FormalResponse> createAdReport(@Field("post_id") String postId, @Field("body") String body);

    @FormUrlEncoded
    @POST("comment/list")
    Call<Comments> getPostComments(@Field("post_id") String post_id);

    @FormUrlEncoded
    @POST("comment/add")
    Call<Comments> SendComment(@Field("post_id") String post_id, @Field("comment_text") String comment);

    @FormUrlEncoded
    @POST("comment/update")
    Call<FormalResponse> updateComment(@Field("comment_id") String commentId, @Field("comment_text") String commentText);

    @FormUrlEncoded
    @POST("comment/delete")
    Call<FormalResponse> deleteComment(@Field("comment_id") String commentId);

    @Multipart
    @POST("loginTwitter")
    Call<Login> loginByTwitter(@PartMap() Map<String, RequestBody> partMap);


    @FormUrlEncoded
    @POST("user/check")
    Call<Login> CheckUser(@Field("email") String email,
                          @Field("device_id") String device_id,
                          @Field("device_type") String device_type,
                          @Field("fcm") String fcm);


    @FormUrlEncoded
    @POST("post/notifications")
    Call<Notifications> getNotifications(@Field("page_id") int page_id);


}

