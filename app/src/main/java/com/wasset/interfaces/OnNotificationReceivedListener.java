package com.wasset.interfaces;

/**
 * Created by macbook on 9/4/18.
 */

public interface OnNotificationReceivedListener {
    void onNotificationReceived();

}
