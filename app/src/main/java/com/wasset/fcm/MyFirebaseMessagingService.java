package com.wasset.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.wasset.R;
import com.wasset.ui.PostDetailsActivity;
import com.wasset.ui.splahactivity.SplashActivity;
import com.wasset.interfaces.ApiClient;
import com.wasset.interfaces.ApiInterface;
import com.wasset.models.FormalResponse;
import com.wasset.ui.CommentsActivity;
import com.wasset.utility.PreferenceEditor;
import com.wasset.utility.QuickstartPreferences;

import java.util.Map;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        System.out.println("From: " + remoteMessage.getFrom());

        // Check if message contains a notification payload.

        PreferenceEditor pf = PreferenceEditor.getInstance();
        int mNotificationItemCount = pf.getIntegerPreference(QuickstartPreferences.BADGE_COUNT);
        mNotificationItemCount++;
        //Utils.setBadge(getApplicationContext(), mNotificationItemCount);
        pf.setIntegerPreference(QuickstartPreferences.BADGE_COUNT, mNotificationItemCount);

        Intent i = new Intent("NewNotification");
        sendBroadcast(i);


        /*
       if (remoteMessage.getNotification() != null) {
            System.out.println("Notificationnnnnnnn Body: " + remoteMessage.getNotification().getBody());
            //handleNotification(remoteMessage.getNotification());
        }
        */


        if (remoteMessage.getData().size() > 0) {
            System.out.println("Message data payload: " + remoteMessage.getData());
            try {
                handleDataMessage(remoteMessage.getData());
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Exception: " + e.getMessage());
            }

        }
    }

    private void handleNotification(RemoteMessage.Notification notification) {
        //sendNotification(notification.getTitle(), notification.getBody());

    }




   /* private void handleNotification(String message) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            System.out.println("message" + message);

            Intent pushNotification = new Intent(NotificationUtils.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();
        } else {
            // If the app is in background, firebase itself handles the notification
        }
    }*/


    private void handleDataMessage(Map<String, String> json) {
        Log.e(TAG, "push json: " + json.toString());

        try {
            String type = (String) json.get("type");
            System.out.println("type" + type);

            String title = (String) json.get("title");
            System.out.println("title" + title);
            String body = (String) json.get("body");
            System.out.println("body" + body);


            sendNotification(title, body, json);


        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
            e.printStackTrace();
        }


    }


    private void sendNotification(String title, String messageBody, Map<String, String> json) {

        int NOTIFICATION_ID = new Random().nextInt(50) + 1;
        final String NOTIFICATION_CHANNEL_ID = getString(R.string.default_notification_channel_id);
        final String NOTIFICATION_NAME = getString(R.string.app_name);
        final String NOTIFICATION_DESC = getString(R.string.notifications);
        ;

        String type = null;
        try {
            type = (String) json.get("type");

            Intent intent = null;
            if (type != null && type.equalsIgnoreCase("comment")) {
                intent = new Intent(this, CommentsActivity.class);
                String postId = json.get("post_id");
                intent.putExtra("Id", postId);
            } else if (type != null && (type.equalsIgnoreCase("ads") || type.equalsIgnoreCase("favorite"))) {
                String postId = json.get("post_id");

                intent = new Intent(this, PostDetailsActivity.class);
                intent.putExtra("Id", postId);
            } else {
                intent = new Intent(this, SplashActivity.class);
            }

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                notificationBuilder.setSmallIcon(R.drawable.ic_stat);
                notificationBuilder.setColor(getResources().getColor(R.color.colorPrimary));
            } else {
                notificationBuilder.setSmallIcon(R.drawable.ic_stat);
            }

            //notificationBuilder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_stat));
            notificationBuilder.setContentTitle(title);
            notificationBuilder.setContentText(messageBody);
            notificationBuilder.setAutoCancel(true);
            notificationBuilder.setSound(defaultSoundUri);
            notificationBuilder.setContentIntent(pendingIntent);

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            // Since android Oreo notification channel is needed.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID,
                        NOTIFICATION_NAME,
                        NotificationManager.IMPORTANCE_DEFAULT);
                notificationChannel.setDescription(NOTIFICATION_DESC);
                notificationChannel.enableLights(true);
                notificationChannel.canShowBadge();
                notificationChannel.setShowBadge(true);
                notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
                notificationChannel.enableVibration(true);


                if (notificationManager != null) {
                    notificationManager.createNotificationChannel(notificationChannel);
                }
            }

            if (notificationManager != null) {
                notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
            }


        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
            e.printStackTrace();
        }
    }


    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);


        String ANDROID_ID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.d(TAG, "Refreshed token: " + token);
        sendToken(token,ANDROID_ID);
    }

    private void sendToken(String token,String ANDROID_ID) {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<FormalResponse> call = apiService.SendFCMToken(ANDROID_ID, "android",token);
        call.enqueue(new Callback<FormalResponse>() {
            @Override
            public void onResponse(Call<FormalResponse> call, final Response<FormalResponse> response) {

                try {
                    FormalResponse formalResponse = response.body();
                    if (formalResponse != null && formalResponse.isStatus() && formalResponse.getCode() == QuickstartPreferences.OK_STATUS_CODE) {


                    }

                } catch (Exception e) {
                    e.printStackTrace();

                }
            }

            @Override
            public void onFailure(Call<FormalResponse> call, Throwable t) {

                t.printStackTrace();

            }
        });
    }
}