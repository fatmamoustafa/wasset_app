package com.wasset.ui

import android.content.Context
import android.content.Intent
import android.content.Intent.getIntent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.media.session.MediaButtonReceiver.handleIntent
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import butterknife.BindView
import com.facebook.FacebookSdk.getApplicationContext

import com.wasset.R
import com.wasset.adapter.AdapterCategoryTitle
import com.wasset.adapter.AdapterPostDetails
import com.wasset.databinding.ActivityPublisherProfileBinding
import com.wasset.databinding.FragmentPublisherProfileBinding
import com.wasset.interfaces.ApiClient
import com.wasset.interfaces.ApiInterface
import com.wasset.interfaces.OnItemClickListener
import com.wasset.models.*
import com.wasset.utility.EndlessRecyclerViewScrollListener
import com.wasset.utility.QuickstartPreferences
import com.wasset.utility.QuickstartPreferences.UNAUTHORIZED_STATUS_CODE
import com.wasset.utility.Utils
import kotlinx.android.synthetic.main.fragment_publisher_profile.*
import kotlinx.android.synthetic.main.no_internet_connection_layout.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "Id"
private const val ARG_PARAM2 = "intent"
private const val ARG_PARAM3 = "from"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [PublisherProfileFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [PublisherProfileFragment] factory method to
 * create an instance of this fragment.
 */
class PublisherProfileFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener {
    private val STARTING_PAGE_INDEX = 1

    private var UserId: String? = null
    private var call: Call<publisherProfile>? = null
    private var handlers: MyHandlers? = null
    private var binding: FragmentPublisherProfileBinding? = null
    private var userDetailsData: publisherProfile.DataBean? = null
    private var endlessScrollListener: EndlessRecyclerViewScrollListener? = null
    internal var userUrl: String? = null
    private var isThereMore = true
    internal var isFromDeepLink = false


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
//
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_publisher_profile, container, false)
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handlers = MyHandlers(context!!)
//        setToolbar()
        swipe.setOnRefreshListener(this)

        try {

            UserId = (arguments!!.getParcelable<Intent>(ARG_PARAM2) as Intent).getStringExtra("Id")
            if (UserId != null && !UserId!!.isEmpty()) {
                getUserDetails()
            } else {
                handleIntent(activity!!.intent)
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }

        share_profile!!.setOnClickListener(View.OnClickListener {
            val link = "http://hwaseet.com.sa/user/profile/$UserId"
            share(link)
        })
        if (arguments!!.get(ARG_PARAM2) != null) {
            handleIntent(arguments!!.get(ARG_PARAM2) as Intent)
        }
    }

    companion object {

        @JvmStatic
        fun newInstance(param2: Intent, param3: String) =
                PublisherProfileFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(ARG_PARAM2, param2)
                        putString(ARG_PARAM3, param3)
                    }
                }

        @JvmStatic
        fun newInstance(param1: String) =
                PublisherProfileFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                    }
                }
    }

    private fun getUserDetails() {

        if (!Utils.isNetworkAvailable()) {
            ShowErrorLayout(R.string.no_internet_connection)
            return
        }
        val apiService = ApiClient.getClient().create(ApiInterface::class.java)
        swipe.setRefreshing(true)
        call = apiService.getPublisherProfile(UserId)
        call!!.enqueue(object : Callback<publisherProfile> {
            override fun onResponse(call: Call<publisherProfile>, response: Response<publisherProfile>) {
                println("raw" + response.raw())

                swipe.setRefreshing(false)


                try {
                    if (response.body()!!.isStatus && response.body()!!.code == QuickstartPreferences.OK_STATUS_CODE) {
                        if (response.body()!!.data != null) {
                            showContent()

                            userDetailsData = response.body()!!.data
                            binding!!.setUserProfile(userDetailsData)
                            binding!!.setHandlersListener(handlers)
                            if (userDetailsData!!.getUser_info().getUser_special()!!) {
                                verified_badge.setVisibility(View.VISIBLE)
                                share_profile.setVisibility(View.VISIBLE)
                                iv_publisher_name.setText(userDetailsData!!.getUser_info().getUsername())
                                iv_publisher_desc.setVisibility(View.VISIBLE)
                            } else {
                                verified_badge.setVisibility(View.GONE)
                                share_profile.setVisibility(View.GONE)
                                iv_publisher_name.setText(userDetailsData!!.getUser_info().getUsername())
                                iv_publisher_desc.setVisibility(View.GONE)
                            }
                            //setUpViewPager(response.body().getData().getPhotos());
                            if (response.body()!!.data.user_categories != null) {
                                userUrl = response.body()!!.data.user_url
                                setupPublisherPostsList(response.body()!!.data.user_categories)
                            } else if (response.body()!!.data.posts != null) {
                                setupNonVerifiedPublisherPostsList(response.body()!!.data.posts, true)
                            } else {
                                isThereMore = false

                            }
                            //SetupPostAttribute(response.body().getData().getAttributes());

                        } else {
                            ShowErrorLayout(R.string.err_msg_server)
                        }

                    } else if (response.body()!!.code == UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(getApplicationContext(), R.string.Unauthorized, Toast.LENGTH_LONG).show()
                        Utils.LunchLogin(getApplicationContext())
                    } else {
                        ShowErrorLayout(R.string.err_msg_server)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    ShowErrorLayout(R.string.err_msg_server)
                }

            }

            override fun onFailure(call: Call<publisherProfile>, t: Throwable) {
                if (!call.isCanceled && "Canceled" != t.message) {

                    swipe.setRefreshing(false)
                    t.printStackTrace()
                    ShowErrorLayout(R.string.err_msg_server)
                }
            }
        })

    }

    internal var linearLayoutManager: LinearLayoutManager? = null
    internal var adapterPostDetails: AdapterPostDetails? = null

    private fun setupNonVerifiedPublisherPostsList(posts: List<PostDataBean>, shouldClearPreviousPosts: Boolean) {
        if (linearLayoutManager == null)
            linearLayoutManager = LinearLayoutManager(context)
        if (endlessScrollListener == null)
            endlessScrollListener = object : EndlessRecyclerViewScrollListener(linearLayoutManager, STARTING_PAGE_INDEX) {
                override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
                    if (Utils.isNetworkAvailable() && isThereMore) {
                        getUserDetailsWithSharedCategory(UserId!!, categoryId, page)
                    }
                }
            }

        if (adapterPostDetails == null) {
            adapterPostDetails = AdapterPostDetails(posts, true)
            rv_publisher_posts.setLayoutManager(linearLayoutManager)
            rv_publisher_posts.addOnScrollListener(endlessScrollListener!!)
            rv_publisher_posts.setAdapter(adapterPostDetails)
            adapterPostDetails!!.setListener { itemView, position ->
                val PostId = posts[position].id
                val name = posts[position].title
//                val i = Intent(context, CategoryPostsActivity::class.java)
//                                    Intent i = new Intent(getApplicationContext(), PostDetailsActivity.class);
//                i.putExtra("Id", PostId)
//                startActivity(i)
                try {
                    activity!!.supportFragmentManager.beginTransaction().replace(R.id.pu_nav_host_fragment, CategoryPostsActivity.newInstance(PostId, name,"publish")).setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out).commit()
                } catch (e: Exception) {

                }

            }
        } else {

            adapterPostDetails!!.addNewPosts(posts, shouldClearPreviousPosts)
            adapterPostDetails!!.notifyDataSetChanged()
        }


    }


    private fun getUserDetailsWithSharedCategory(userId: String, categoryId: String, page: Int) {

        if (!Utils.isNetworkAvailable()) {
            ShowErrorLayout(R.string.no_internet_connection)
            return
        }

        val apiService = ApiClient.getClient().create(ApiInterface::class.java)
        swipe.setRefreshing(true)

        call = apiService.getPublisherProfileWithSharedCategory(userId, categoryId, page)

        call!!.enqueue(object : Callback<publisherProfile> {
            override fun onResponse(call: Call<publisherProfile>, response: Response<publisherProfile>) {
                swipe.setRefreshing(false)
                try {
                    if (response.body()!!.isStatus && response.body()!!.code == QuickstartPreferences.OK_STATUS_CODE) {
                        if (response.body()!!.data != null) {
                            if (response.body()!!.data.posts.size > 0) {
                                showContent()

                                userDetailsData = response.body()!!.data
                                binding!!.setUserProfile(userDetailsData)
                                binding!!.setHandlersListener(handlers)
                                if (userDetailsData!!.getUser_info().getUser_special()!!) {
                                    verified_badge.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.verified_badge))
                                    verified_badge.setVisibility(View.VISIBLE)
                                    share_profile.setVisibility(View.VISIBLE)
                                    iv_publisher_name.setText(userDetailsData!!.getUser_info().getUsername() + " أحد الداعمين بمجهوداته لتطبيق وسيط")
                                    if (response.body()!!.data.posts != null)
                                        setupNonVerifiedPublisherPostsList(response.body()!!.data.posts, false)
                                } else {
                                    verified_badge.setVisibility(View.GONE)
                                    share_profile.setVisibility(View.GONE)
                                    iv_publisher_name.setText(userDetailsData!!.getUser_info().getUsername())
                                    if (response.body()!!.data.posts != null)
                                        setupNonVerifiedPublisherPostsList(response.body()!!.data.posts, false)
                                }
                            } else
                                isThereMore = false

                        } else {
                            ShowErrorLayout(R.string.err_msg_server)
                        }

                    } else if (response.body()!!.code == UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(context, R.string.Unauthorized, Toast.LENGTH_LONG).show()
                        Utils.LunchLogin(context)
                    } else {
                        ShowErrorLayout(R.string.err_msg_server)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    swipe.setRefreshing(false)
                    ShowErrorLayout(R.string.err_msg_server)
                }

            }

            override fun onFailure(call: Call<publisherProfile>, t: Throwable) {
                if (!call.isCanceled && "Canceled" != t.message) {

                    swipe.setRefreshing(false)
                    t.printStackTrace()
                    ShowErrorLayout(R.string.err_msg_server)
                }
            }
        })

    }


    private fun showContent() {
        try {
            content.setVisibility(View.VISIBLE)
            rl_no_internet_connection.setVisibility(View.GONE)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun ShowErrorLayout(txt: Int) {
        try {
            content.setVisibility(View.GONE)
            rl_no_internet_connection.setVisibility(View.VISIBLE)
            tv_error.setText(txt)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    private fun setupPublisherPostsList(postDataBeanList: List<Home.DataBean>) {
        val userPostsCategories = ArrayList<UserPostsCategories>()
        for (i in postDataBeanList.indices) {
            val userPosts = ArrayList<UserPosts>()
            for (p in 0 until postDataBeanList[i].posts.size) {
                val postDataBean = postDataBeanList[i].posts[p]
                val userPost = UserPosts()
                userPost.added_date = postDataBean.added_date
                userPost.attributes = postDataBean.attributes
                userPost.category_id = postDataBean.category_id
                userPost.city = postDataBean.location
                userPost.comments_count = postDataBean.comments_count
                userPost.details = postDataBean.details
                userPost.id = postDataBean.id
                userPost.is_favorite = postDataBean.is_favorite
                userPost.mobile_number = postDataBean.mobile_number
                userPost.photo = postDataBean.photo
                userPost.photos = postDataBean.photos
                userPost.post_url = postDataBean.post_url
                userPost.price = postDataBean.price
                userPost.related = postDataBean.related
                userPost.title = postDataBean.title
                userPost.user = postDataBean.user
                userPost.user_id = postDataBean.user_id
                userPost.views_count = postDataBean.views_count
                userPosts.add(userPost)
            }
            val expandableHolders = ArrayList<ExpandableHolder>()
            val expandableHolder = ExpandableHolder()
            expandableHolder.userPosts = userPosts
            expandableHolders.add(expandableHolder)
            val userPostsCategory = UserPostsCategories(postDataBeanList[i].name, expandableHolders)
            userPostsCategory.category_id = postDataBeanList[i].category_id
            userPostsCategory.userUrl = userUrl
            userPostsCategories.add(userPostsCategory)
        }
        val linearLayoutManager = LinearLayoutManager(context)
        val adapterPostDetails = AdapterCategoryTitle(userPostsCategories, userDetailsData!!.getUser_info().getId())

        rv_publisher_posts.setLayoutManager(linearLayoutManager)
        rv_publisher_posts.setAdapter(adapterPostDetails)
        // rvPublisherPosts.setNestedScrollingEnabled(false);

        adapterPostDetails.setOnItemClickListener { itemView, position ->
            val item = postDataBeanList[position]
            if (itemView.id == R.id.tv_category_name) {
                try {
                    activity!!.getSupportFragmentManager().beginTransaction().replace(R.id.pu_nav_host_fragment, CategoryPostsActivity.newInstance(item.category_id, item.name, userDetailsData!!.getUser_info().getId(), true)).setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out).commit()
                } catch (e: Exception) {

                }

                //                Intent i = new Intent(PublisherProfileActivity.this, CategoryPostsActivity.class);
                //                i.putExtra("Id", item.getCategory_id());
                //                i.putExtra("name", item.getName());
                //                i.putExtra("fromProfile", true);
                //                i.putExtra("userId", userDetailsData.getUser_info().getId());
                //                startActivity(i);
            } else if (itemView.id == R.id.iv_share) {
                if (userPostsCategories[position] != null) {
                    val link = userPostsCategories[position].userUrl + "/" + userPostsCategories[position].category_id
                    share(link)
                }
            }
        }
    }

    internal fun share(link: String?) {
        if (link != null && !link.isEmpty()) {
            val i = Intent(Intent.ACTION_SEND)
            i.type = "text/plain"
            i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name))
            i.putExtra(Intent.EXTRA_TEXT, link)
            startActivity(Intent.createChooser(i, getString(R.string.share)))
        } else {
            Toast.makeText(context, R.string.invalid_post_link, Toast.LENGTH_LONG).show()
        }
    }

    inner class MyHandlers internal constructor(internal var context: Context) {

        fun onFollowClicked(view: View, profile: publisherProfile.DataBean) {

        }


        fun onChatClicked(view: View, profile: publisherProfile.DataBean) {

        }

    }


    internal var categoryId = ""

    private fun handleIntent(intent: Intent?) {
        if (intent != null) {
            isFromDeepLink = true
            val appLinkAction = intent.action
            val appLinkData = intent.data
            if (Intent.ACTION_VIEW == appLinkAction && appLinkData != null) {
                val segments = appLinkData.pathSegments
                if (segments.size == 4) {

                    UserId = segments[segments.size - 2]
                    categoryId = segments[segments.size - 1]
                    getUserDetailsWithSharedCategory(segments[segments.size - 2], segments[segments.size - 1], STARTING_PAGE_INDEX)
                } else {
                    UserId = segments[segments.size - 1]
                    getUserDetails()

                }
            }
        }
    }

    override fun onRefresh() {
        isThereMore = true

        endlessScrollListener?.resetState()
        if (!isFromDeepLink) {
            getUserDetails()
        } else {
            handleIntent(activity!!.intent)
        }
    }

}
