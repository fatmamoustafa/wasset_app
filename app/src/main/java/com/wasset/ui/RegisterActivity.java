package com.wasset.ui;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatTextView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.wasset.R;
import com.wasset.adapter.AdapterCountries;
import com.wasset.interfaces.ApiClient;
import com.wasset.interfaces.ApiInterface;
import com.wasset.models.Config;
import com.wasset.models.Login;
import com.wasset.models.ValidationBean;
import com.wasset.utility.PreferenceEditor;
import com.wasset.utility.QuickstartPreferences;
import com.wasset.utility.Utils;
import com.wasset.views.AVLoadingIndicatorDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cn.refactor.lib.colordialog.PromptDialog.DIALOG_TYPE_WRONG;

public class RegisterActivity extends BaseActivity {

    @BindView(R.id.et_username)
    EditText etUserName;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.btn_register)
    Button btnRegister;
    @BindView(R.id.sp_countries)
    Spinner spCountries;
    @BindView(R.id.tv_err_countries)
    TextView tvErrCountries;
    private Call<Login> call;
    private PreferenceEditor pf;
    private AVLoadingIndicatorDialog indicatorView;

    private String CityId = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        pf = PreferenceEditor.getInstance();
        final List<Config.DataBean.CountriesBean> countriesBeanList = new ArrayList<>();

        Config.DataBean cfg = new Gson().fromJson(pf.getStringPreference(QuickstartPreferences.CONFIG), Config.DataBean.class);


        if (getIntent() != null && getIntent().getExtras() != null) {
            String user = (String) getIntent().getExtras().get("username");
            String mail = (String) getIntent().getExtras().get("email");
            etUserName.setText(user);
            etEmail.setText(mail);

        }


        countriesBeanList.addAll(cfg.getCountries());
        countriesBeanList.remove(0);

       /* List<String> spinnerList = new ArrayList<>();
        for (int i = 0; i < countriesBeanList.size(); i++) {
            Config.DataBean.CountriesBean item = countriesBeanList.get(i);
            spinnerList.add(item.getName());
        }*/

        //ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, spinnerList);

        AdapterCountries adapter = new AdapterCountries(RegisterActivity.this, R.layout.row_countries_spinner_item, R.id.tvName, countriesBeanList);
        //adapter.setDropDownViewResource(R.layout.row_countries_spinner_item);
        spCountries.setAdapter(adapter);

        spCountries.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {

                System.out.println(countriesBeanList.get(pos).getName());
                System.out.println(countriesBeanList.get(pos).getId());
                CityId = countriesBeanList.get(pos).getId();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }


    private void checkForm() {
        String username = etUserName.getText().toString().trim();
        String email = etEmail.getText().toString().trim();
        String phone = etPhone.getText().toString().trim();
        String password = etPassword.getText().toString().trim();


        if (username.isEmpty()) {
            Utils.ShowDialog(RegisterActivity.this, getString(R.string.err), getString(R.string.err_msg_username), DIALOG_TYPE_WRONG);
            return;
        }

        if (email.isEmpty()) {
            Utils.ShowDialog(RegisterActivity.this, getString(R.string.err), getString(R.string.err_msg_email), DIALOG_TYPE_WRONG);
            return;
        }

        if (!Utils.isValidMail(email)) {
            Utils.ShowDialog(RegisterActivity.this, getString(R.string.err), getString(R.string.err_msg_valid_email), DIALOG_TYPE_WRONG);

            return;
        }

        if (phone.isEmpty()) {
            Utils.ShowDialog(RegisterActivity.this, getString(R.string.err), getString(R.string.err_msg_phone_number), DIALOG_TYPE_WRONG);
            return;
        }


        if (password.isEmpty()) {
            Utils.ShowDialog(RegisterActivity.this, getString(R.string.err), getString(R.string.err_msg_password), DIALOG_TYPE_WRONG);
            return;
        }

        if (CityId == null) {
            Utils.ShowDialog(RegisterActivity.this, getString(R.string.err), getString(R.string.err_msg_city), DIALOG_TYPE_WRONG);
            return;
        }

        requestRegister();

    }

    private void requestRegister() {
        String username = etUserName.getText().toString().trim();
        String email = etEmail.getText().toString().trim();
        String phone = etPhone.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        //String city = "city";
        String token = FirebaseInstanceId.getInstance().getToken();
        String ANDROID_ID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        String device_type = "android";


        if (!Utils.isNetworkAvailable()) {
            Utils.ShowNoInternetConnectionDialog(RegisterActivity.this);
            return;
        }

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Utils.HideKeyBord(RegisterActivity.this);
        showProgress();
        call = apiService.registration(username, email, password, ANDROID_ID, CityId, device_type, token, phone);
        call.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(@NonNull Call<Login> call, @NonNull final Response<Login> response) {

                hideProgress();
                try {
                    Login login = response.body();
                    if (login.isStatus() && login.getCode() == QuickstartPreferences.OK_STATUS_CODE) {
                        Toast.makeText(RegisterActivity.this, login.getMessage(), Toast.LENGTH_SHORT).show();
                        pf.setStringPreference(QuickstartPreferences.USER_INFO, new Gson().toJson(login.getData()));
                        goToHomeActivity();
                        finish();

                    } else if (!login.isStatus() && login.getCode() == QuickstartPreferences.VALIDATION_ERROR_CODE) {

                        ValidationBean validation = login.getValidation();

                        if (validation.getUsername() != null && !validation.getUsername().isEmpty()) {
                            etUserName.setError(validation.getUsername());
                        }

                        if (validation.getEmail() != null && !validation.getEmail().isEmpty()) {
                            etEmail.setError(validation.getEmail());
                        }

                        if (validation.getPassword() != null && !validation.getPassword().isEmpty()) {
                            etPassword.setError(validation.getPassword());
                        }

                        if (validation.getCity() != null && !validation.getCity().isEmpty()) {
                            tvErrCountries.setVisibility(View.VISIBLE);
                            tvErrCountries.setText(validation.getUsername());
                        }else {
                            tvErrCountries.setVisibility(View.GONE);
                        }


                    } else {
                        Utils.ShowDialog(RegisterActivity.this, getString(R.string.err), login.getMessage(), DIALOG_TYPE_WRONG);

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.ShowServerDialog(RegisterActivity.this);
                }
            }


            @Override
            public void onFailure(@NonNull Call<Login> call, Throwable t) {
                if (!call.isCanceled() && !"Canceled".equals(t.getMessage())) {

                    hideProgress();
                    t.printStackTrace();
                    Utils.ShowServerDialog(RegisterActivity.this);
                }
            }
        });
    }

    private void goToHomeActivity() {
        Intent i = new Intent(getApplicationContext(), HomeActivity.class);
        finish();
        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(i);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }


    private void hideProgress() {
        if (indicatorView != null && indicatorView.isShowing()) {
            indicatorView.dismiss();
        }
    }

    private void showProgress() {

        if (indicatorView == null) {
            indicatorView = new AVLoadingIndicatorDialog(RegisterActivity.this);
            //indicatorView.setMessage(msg);
            indicatorView.setCancelable(false);
        }
        indicatorView.show();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (call != null) {
            call.cancel();
        }

        hideProgress();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    @OnClick(R.id.btn_register)
    public void onViewClicked() {
        checkForm();
    }
}
