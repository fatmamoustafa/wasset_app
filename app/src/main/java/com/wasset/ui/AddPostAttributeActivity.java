package com.wasset.ui;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.wasset.R;
import com.wasset.adapter.AdapterPostAttribute;
import com.wasset.interfaces.ApiClient;
import com.wasset.interfaces.ApiInterface;
import com.wasset.interfaces.OnAttrItemSelectListener;
import com.wasset.models.Models;
import com.wasset.models.PostSteps;
import com.wasset.utility.QuickstartPreferences;
import com.wasset.utility.Utils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import static com.wasset.utility.QuickstartPreferences.UNAUTHORIZED_STATUS_CODE;

public class AddPostAttributeActivity extends BaseActivity implements OnAttrItemSelectListener {

    @BindView(R.id.tv_toolbar_title)
    AppCompatTextView tvToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rv_post_item_attributes)
    RecyclerView rvPostItemAttributes;
    @BindView(R.id.iv_empty_icon)
    AppCompatImageView ivEmptyIcon;
    @BindView(R.id.tv_empty_text)
    AppCompatTextView tvEmptyText;
    @BindView(R.id.rl_empty_layout)
    LinearLayoutCompat rlEmptyLayout;
    @BindView(R.id.tv_error)
    AppCompatTextView tvError;
    @BindView(R.id.btn_refresh)
    AppCompatButton btnRefresh;
    @BindView(R.id.rl_no_internet_connection)
    RelativeLayout rlNoInternetConnection;
    private Call<PostSteps> PostStepsCall;
    private List<PostSteps.DataBean> postStepsList;
    private int CurrentPosition = 0;
    private HashMap<String, String> map = new HashMap<>();
    private HashMap<String, String> textMap = new HashMap<>();
    private int categoryId;
    private String post_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_post_attribute);
        ButterKnife.bind(this);
        setToolbar();
        postStepsList = new ArrayList<>();

        try {
            categoryId = (int) getIntent().getExtras().get("categoryId");
            post_id = (String) getIntent().getExtras().get("post_id");
            getPostSteps();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getPostSteps() {

        if (!Utils.isNetworkAvailable()) {
            ShowErrorLayout(R.string.no_internet_connection);
            return;
        }

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        PostStepsCall = apiService.getPostSteps(categoryId, post_id);
        PostStepsCall.enqueue(new Callback<PostSteps>() {
            @Override
            public void onResponse(Call<PostSteps> call, final Response<PostSteps> response) {
                System.out.println("raw" + response.raw());

                try {
                    if (response.body().isStatus() && response.body().getCode() == QuickstartPreferences.OK_STATUS_CODE) {
                        if (response.body().getData() != null) {
                            if (response.body().getData().size() > 0) {
                                showContent();
                                postStepsList.clear();
                                postStepsList.addAll(response.body().getData());
                                setupList(CurrentPosition);
                            } else {
                                FinishSteps();
                            }
                        } else {
                            ShowErrorLayout(R.string.err_msg_server);
                        }

                    } else if (response.body().getCode() == UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(getApplicationContext(), R.string.Unauthorized, Toast.LENGTH_LONG).show();
                        Utils.LunchLogin(getApplicationContext());
                    } else {
                        ShowErrorLayout(R.string.err_msg_server);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    ShowErrorLayout(R.string.err_msg_server);
                }
            }


            @Override
            public void onFailure(Call<PostSteps> call, Throwable t) {
                if (!call.isCanceled() && !"Canceled".equals(t.getMessage())) {
                    t.printStackTrace();
                    ShowErrorLayout(R.string.err_msg_server);
                }
            }
        });
    }


    private void setupList(int position) {

        final PostSteps.DataBean item = postStepsList.get(position);
        tvToolbarTitle.setText(item.getTitle());

        if (item.getField().getAttribute().equalsIgnoreCase("model_id")) {
            requestModelsList(map.get("brand_id"), item.getField());
            return;
        }


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        final AdapterPostAttribute adapterPostAttribute = new AdapterPostAttribute(AddPostAttributeActivity.this, item.getField());
        rvPostItemAttributes.setLayoutManager(linearLayoutManager);
        rvPostItemAttributes.setAdapter(adapterPostAttribute);
        rvPostItemAttributes.setNestedScrollingEnabled(true);
        adapterPostAttribute.setOnAttrItemSelectListener(this);

    }


    private void requestModelsList(String brandId, final PostSteps.DataBean.FieldBean item) {
        if (!Utils.isNetworkAvailable()) {
            ShowErrorLayout(R.string.no_internet_connection);
            return;
        }

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Models> call = apiService.getModels(brandId, post_id);
        call.enqueue(new Callback<Models>() {
            @Override
            public void onResponse(Call<Models> call, final Response<Models> response) {
                System.out.println("raw" + response.raw());

                try {
                    if (response.body().isStatus() && response.body().getCode() == QuickstartPreferences.OK_STATUS_CODE) {
                        if (response.body().getData().size() > 0) {
                            showContent();

                            item.setValues(response.body().getData());
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
                            AdapterPostAttribute adapterPostAttribute = new AdapterPostAttribute(AddPostAttributeActivity.this, item);
                            rvPostItemAttributes.setLayoutManager(linearLayoutManager);
                            rvPostItemAttributes.setAdapter(adapterPostAttribute);
                            adapterPostAttribute.setOnAttrItemSelectListener(AddPostAttributeActivity.this);

                        } else {
                            FinishSteps();
                        }

                    } else if (response.body().getCode() == UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(getApplicationContext(), R.string.Unauthorized, Toast.LENGTH_LONG).show();
                        Utils.LunchLogin(getApplicationContext());
                    } else {
                        ShowErrorLayout(R.string.err_msg_server);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    ShowErrorLayout(R.string.err_msg_server);
                }
            }


            @Override
            public void onFailure(Call<Models> call, Throwable t) {
                if (!call.isCanceled() && !"Canceled".equals(t.getMessage())) {

                    //swipeLayout.setRefreshing(false);
                    t.printStackTrace();
                    ShowErrorLayout(R.string.err_msg_server);
                }
            }
        });
    }

    private void FinishSteps() {
        Intent i = new Intent();
        i.putExtra("map", map);
        i.putExtra("textMap", textMap);
        setResult(RESULT_OK, i);
        finish();
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        toolbar.setNavigationOnClickListener(view -> onBackPressed());
    }


    private void showContent() {
        try {
            rvPostItemAttributes.setVisibility(View.VISIBLE);
            rlNoInternetConnection.setVisibility(View.GONE);
            rlEmptyLayout.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private void ShowErrorLayout(int txt) {
        try {
            rvPostItemAttributes.setVisibility(View.GONE);
            rlNoInternetConnection.setVisibility(View.VISIBLE);
            rlEmptyLayout.setVisibility(View.GONE);
            tvError.setText(txt);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        if ((CurrentPosition - 1) >= 0) {
            CurrentPosition--;
            setupList(CurrentPosition);
        } else {
            super.onBackPressed();
            finish();
        }
    }

    @OnClick({R.id.btn_refresh})
    public void onViewClicked(View view) {

        switch (view.getId()) {
            case R.id.btn_refresh:
                CurrentPosition = 0;
                getPostSteps();
                break;
        }

    }


    @Override
    public void onAttrItemSelect(String key, String value) {

        if (key != null && value != null) {
            map.put(key, value);
        }
             if ((CurrentPosition + 1) <= postStepsList.size() - 1) {
                CurrentPosition++;
                setupList(CurrentPosition);
            } else {
                FinishSteps();
            }

    }

    @Override
    public void onAttrMultiItemSelect(String key1, String value1, String key2, String value2) {
        if (key1 != null && value1 != null) {
            map.put(key1, value1);
        }

        if (key2 != null && value2 != null) {
            map.put(key2, value2);
        }

        if ((CurrentPosition + 1) <= postStepsList.size() - 1) {
            CurrentPosition++;
            setupList(CurrentPosition);
        } else {
            FinishSteps();
        }


    }


    @Override
    public void onAttrItemSelectText(String valueTitle) {
        if (valueTitle != null) {
            System.out.println("" + postStepsList.get(CurrentPosition).getTitle() + "," + valueTitle);
            textMap.put(postStepsList.get(CurrentPosition).getTitle(), valueTitle);
        }
    }


}
