package com.wasset.ui.search

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.Toast
import com.facebook.FacebookSdk.getApplicationContext
import com.google.gson.Gson
import com.wasset.R
import com.wasset.adapter.AdapterCountries
import com.wasset.interfaces.ApiClient
import com.wasset.interfaces.ApiInterface
import com.wasset.models.Config
import com.wasset.models.Login
import com.wasset.models.categoryList
import com.wasset.ui.search.SearchSpinnerAdapter
import com.wasset.utility.PreferenceEditor
import com.wasset.utility.QuickstartPreferences
import com.wasset.utility.QuickstartPreferences.UNAUTHORIZED_STATUS_CODE
import com.wasset.utility.Utils
import kotlinx.android.synthetic.main.fragment_search.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [SearchFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [SearchFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SearchFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null
    private var pf: PreferenceEditor? = null
    private var login: Login.DataBean? = null
    private var CountryId: String? = null
    private var CityId: String? = null
    private var CategoryId: String? = null
    private var GetCategoryListCall: Call<categoryList>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        pf = PreferenceEditor.getInstance()
        login = Gson().fromJson(pf!!.getStringPreference(QuickstartPreferences.USER_INFO), Login.DataBean::class.java)

        val cfg = Gson().fromJson<Config.DataBean>(pf!!.getStringPreference(QuickstartPreferences.CONFIG), Config.DataBean::class.java)

        val CountriesList = cfg.countries

        var selection = -1
        if (login != null) {
            for (i in CountriesList.indices) {
                if (login!!.getCity_id().equals(CountriesList[i].id, ignoreCase = true)) {
                    selection = i
                    CountryId = login!!.city_id
                }
            }
        }
        val adapter = AdapterCountries(activity, R.layout.row_countries_spinner_item, R.id.tvName, CountriesList)
        sp_countries.setAdapter(adapter)

        if (selection != -1) {
            sp_countries.setSelection(selection)
        }
        sp_countries.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, pos: Int, l: Long) {
                CountryId = CountriesList[pos].id
                CityId = null

                if (CountriesList[pos].cities.size > 0) {
                    sp_cities.setVisibility(View.VISIBLE)
                    setupCountryCitiesList(CountriesList[pos].cities)
                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {

            }
        }
    }

    private fun setupCountryCitiesList(cities: List<Config.DataBean.CountriesBean.CitiesBean>) {

        val spinnerList = ArrayList<String>()
        for (item in cities) {
            spinnerList.add(item.name)
        }

        val dataAdapter = SearchSpinnerAdapter(activity!!, R.layout.spin_item, R.id.tvName, spinnerList)
        //dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_cities.setAdapter(dataAdapter)
        sp_cities.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, pos: Int, l: Long) {
                println("City : " + cities[pos].name)
                CityId = cities[pos].id
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {

            }
        }
    }

    private fun requestGetCategoryList() {
        if (!Utils.isNetworkAvailable()) {
            return
        }

        val apiService = ApiClient.getClient().create(ApiInterface::class.java)

        GetCategoryListCall = apiService.getCategoryList(null) as Call<categoryList>?
        GetCategoryListCall!!.enqueue(object : Callback<categoryList> {
            override fun onResponse(call: Call<categoryList>, response: Response<categoryList>) {
                println("raw" + response.raw())
                try {
                    if (response.body()!!.isStatus && response.body()!!.code == QuickstartPreferences.OK_STATUS_CODE) {
                        if (response.body()!!.data.size > 0) {

                            val CategoriesList = response.body()!!.data
                            val spinnerList = ArrayList<String>()
                            for (i in CategoriesList.indices) {
                                val item = CategoriesList[i]
                                spinnerList.add(item.name)
                            }

                            val dataAdapter = SearchSpinnerAdapter(activity!!, R.layout.spin_item, R.id.tvName, spinnerList)
                            //dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sp_categories.setAdapter(dataAdapter)

                            sp_categories.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                                override fun onItemSelected(adapterView: AdapterView<*>, view: View, pos: Int, l: Long) {
                                    CategoryId = CategoriesList[pos].category_id
                                }

                                override fun onNothingSelected(adapterView: AdapterView<*>) {

                                }
                            }


                        }

                    } else if (response.body()!!.code == UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(getApplicationContext(), R.string.Unauthorized, Toast.LENGTH_LONG).show()
                        Utils.LunchLogin(getApplicationContext())
                    } else {
                        Toast.makeText(activity!!, R.string.err_msg_server, Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    Toast.makeText(activity!!, R.string.err_msg_server, Toast.LENGTH_SHORT).show()
                }

            }

            override fun onFailure(call: Call<categoryList>, t: Throwable) {
                if (!call.isCanceled && "Canceled" != t.message) {
                    t.printStackTrace()
                    Toast.makeText(activity, R.string.err_msg_server, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SearchFragment.
         */
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                SearchFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
