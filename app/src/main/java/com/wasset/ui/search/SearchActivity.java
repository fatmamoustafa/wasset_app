package com.wasset.ui.search;

import android.content.Intent;
import android.os.Bundle;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wasset.R;
import com.wasset.adapter.AdapterCountries;
import com.wasset.adapter.AdapterPostDetails;
import com.wasset.interfaces.ApiClient;
import com.wasset.interfaces.ApiInterface;
import com.wasset.interfaces.OnItemClickListener;
import com.wasset.models.Config;
import com.wasset.models.FormalResponse;
import com.wasset.models.Login;
import com.wasset.models.MyPosts;
import com.wasset.models.PostDataBean;
import com.wasset.models.categoryList;
import com.wasset.ui.BaseActivity;
import com.wasset.ui.PostDetailsActivity;
import com.wasset.ui.PublisherProfileFragment;
import com.wasset.utility.EndlessRecyclerViewScrollListener;
import com.wasset.utility.PreferenceEditor;
import com.wasset.utility.QuickstartPreferences;
import com.wasset.utility.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cn.refactor.lib.colordialog.PromptDialog.DIALOG_TYPE_SUCCESS;
import static com.wasset.utility.QuickstartPreferences.UNAUTHORIZED_STATUS_CODE;

public class SearchActivity extends BaseActivity {


    @BindView(R.id.tv_toolbar_title)
    TextView tvToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
//
//    private List<PostDataBean> searchList;
//    private AdapterPostDetails adapter_ٍSearch;
//    private Call<MyPosts> call;
//    private MyPosts searchBody;
//    private String query;
//    private int STARTING_PAGE_INDEX = 1;
//    private EndlessRecyclerViewScrollListener endlessScrollListener;
//    private boolean isThereMore = true;
//    private PreferenceEditor pf;
//    private String CountryId;
//    private String CityId;
//    private Call<categoryList> GetCategoryListCall;
//    private Call<FormalResponse> AddToFavCall;
//    private String CategoryId;
//    private Login.DataBean login;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        getSupportFragmentManager().beginTransaction().replace(R.id.pu_nav_host_fragment, SearchFragment.newInstance("", "")).
                setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out).commit();
//        pf = PreferenceEditor.getInstance();
//        login = new Gson().fromJson(pf.getStringPreference(QuickstartPreferences.USER_INFO), Login.DataBean.class);
//
//
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//        Config.DataBean cfg = new Gson().fromJson(pf.getStringPreference(QuickstartPreferences.CONFIG), Config.DataBean.class);
//
//        searchList = new ArrayList<>();
//        setToolbar();
//        swipeLayout.setOnRefreshListener(this);
//
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
//        endlessScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, STARTING_PAGE_INDEX) {
//            @Override
//            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
//                if (Utils.isNetworkAvailable()) {
//                    if (isThereMore) {
//                        request_search(page);
//                    }
//                }
//            }
//        };
//        rvSearchList.addOnScrollListener(endlessScrollListener);
//        rvSearchList.setLayoutManager(linearLayoutManager);
//        adapter_ٍSearch = new AdapterPostDetails(searchList);
//        rvSearchList.setAdapter(adapter_ٍSearch);
//        adapter_ٍSearch.setListener(new OnItemClickListener() {
//            @Override
//            public void onItemClick(View itemView, int position) {
//                if (itemView.getId() == R.id.btn_fav) {
//                    AddToFav(searchList.get(position));
//                } else {
//                    Intent i = new Intent(getApplicationContext(), PostDetailsActivity.class);
//                    i.putExtra("Id", searchList.get(position).getId());
//                    startActivity(i);
//                }
//            }
//        });
//
//        final List<Config.DataBean.CountriesBean> CountriesList = cfg.getCountries();
//
//        int selection = -1;
//        if (login != null) {
//            for (int i = 0; i < CountriesList.size(); i++) {
//                if (login.getCity_id().equalsIgnoreCase(CountriesList.get(i).getId())) {
//                    selection = i;
//                    CountryId = login.getCity_id();
//                }
//            }
//        }
//
//        AdapterCountries adapter = new AdapterCountries(SearchActivity.this, R.layout.row_countries_spinner_item, R.id.tvName, CountriesList);
//        spCountries.setAdapter(adapter);
//
//        if (selection != -1) {
//            spCountries.setSelection(selection);
//        }
//
//        spCountries.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
//                CountryId = CountriesList.get(pos).getId();
//                CityId = null;
//
//                if (CountriesList.get(pos).getCities().size() > 0) {
//                    spCities.setVisibility(View.VISIBLE);
//                    sortIc.setVisibility(View.VISIBLE);
//                    setupCountryCitiesList(CountriesList.get(pos).getCities());
//                } else {
//                    spCities.setVisibility(View.GONE);
//                    sortIc.setVisibility(View.GONE);
//                }
//
//                Refresh();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });
//        requestGetCategoryList();


    }


//    private void requestGetCategoryList() {
//        if (!Utils.isNetworkAvailable()) {
//            ShowErrorLayout(R.string.no_internet_connection);
//            return;
//        }
//
//        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
//
//        GetCategoryListCall = apiService.getCategoryList(null);
//        GetCategoryListCall.enqueue(new Callback<categoryList>() {
//            @Override
//            public void onResponse(Call<categoryList> call, final Response<categoryList> response) {
//                System.out.println("raw" + response.raw());
//                try {
//                    if (response.body().isStatus() && response.body().getCode() == QuickstartPreferences.OK_STATUS_CODE) {
//                        if (response.body().getData().size() > 0) {
//
//                            final List<categoryList.DataBean> CategoriesList = response.body().getData();
//                            List<String> spinnerList = new ArrayList<>();
//                            for (int i = 0; i < CategoriesList.size(); i++) {
//                                categoryList.DataBean item = CategoriesList.get(i);
//                                spinnerList.add(item.getName());
//                            }
//
//                            SearchSpinnerAdapter dataAdapter = new SearchSpinnerAdapter(SearchActivity.this, R.layout.spin_item, R.id.tvName, spinnerList);
//                            //dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                            spCategories.setAdapter(dataAdapter);
//
//                            spCategories.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                                @Override
//                                public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
//                                    CategoryId = CategoriesList.get(pos).getCategory_id();
//                                    Refresh();
//                                }
//
//                                @Override
//                                public void onNothingSelected(AdapterView<?> adapterView) {
//
//                                }
//                            });
//
//
//                        }
//
//                    } else if (response.body().getCode() == UNAUTHORIZED_STATUS_CODE) {
//                        Toast.makeText(getApplicationContext(), R.string.Unauthorized, Toast.LENGTH_LONG).show();
//                        Utils.LunchLogin(getApplicationContext());
//                    } else {
//                        Toast.makeText(SearchActivity.this, R.string.err_msg_server, Toast.LENGTH_SHORT).show();
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    Toast.makeText(SearchActivity.this, R.string.err_msg_server, Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<categoryList> call, Throwable t) {
//                if (!call.isCanceled() && !"Canceled".equals(t.getMessage())) {
//                    t.printStackTrace();
//                    Toast.makeText(SearchActivity.this, R.string.err_msg_server, Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
//    }
//
//
//    private void setupCountryCitiesList(final List<Config.DataBean.CountriesBean.CitiesBean> cities) {
//
//        List<String> spinnerList = new ArrayList<>();
//        for (Config.DataBean.CountriesBean.CitiesBean item : cities) {
//            spinnerList.add(item.getName());
//        }
//
//        SearchSpinnerAdapter dataAdapter = new SearchSpinnerAdapter(SearchActivity.this, R.layout.spin_item, R.id.tvName, spinnerList);
//        //dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spCities.setAdapter(dataAdapter);
//        spCities.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
//                System.out.println("City : " + cities.get(pos).getName());
//                CityId = cities.get(pos).getId();
//                Refresh();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });
//    }


    public void setToolbar() {
        tvToolbarTitle.setText(R.string.search);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }


    @Override
    public void onBackPressed() {

//        if (call != null) {
//            call.cancel();
//        }
        super.onBackPressed();
        finish();
    }

//    public boolean onCreateOptionsMenu(final Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.activity_search_menu, menu);
//        final MenuItem searchItem = menu.findItem(R.id.item_search);
//        SearchView searchView = (SearchView) searchItem.getActionView();
//
//
//        // Detect SearchView icon clicks
//        searchView.setOnSearchClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                rlEmptyLayout.setVisibility(View.GONE);
//                tvToolbarTitle.setVisibility(View.GONE);
//            }
//        });
//
//
//        // Detect SearchView close
//        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
//            @Override
//            public boolean onClose() {
//                tvToolbarTitle.setVisibility(View.VISIBLE);
//                return false;
//            }
//        });
//
//        searchView.setIconified(false);
//        searchView.requestFocus();
//
//        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String q) {
//                query = q;
//                request_search(STARTING_PAGE_INDEX);
//
//                return true;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String q) {
//                return false;
//            }
//        });
//
//        return true;
//
//
//    }


//    private void request_search(final int page) {
//
//        if (!Utils.isNetworkAvailable()) {
//            if (page == STARTING_PAGE_INDEX) {
//                ShowErrorLayout(R.string.no_internet_connection);
//            } else {
//                Toast.makeText(this, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
//            }
//            return;
//        }
//
//        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
//        if (swipeLayout != null && page == STARTING_PAGE_INDEX) {
//            swipeLayout.setRefreshing(true);
//        }
//
//        call = apiService.search(query, page, CategoryId, CountryId, CityId);
//        call.enqueue(new Callback<MyPosts>() {
//            @Override
//            public void onResponse(Call<MyPosts> call, final Response<MyPosts> response) {
//                System.out.println("raw" + response.raw());
//                if (swipeLayout != null) {
//                    swipeLayout.setRefreshing(false);
//                }
//
//                try {
//                    if (response.body().isStatus() && response.body().getCode() == QuickstartPreferences.OK_STATUS_CODE) {
//                        searchBody = response.body();
//                        if (searchBody.getData().getPosts().size() > 0) {
//                            showContent();
//                            rvSearchList.post(new Runnable() {
//                                @Override
//                                public void run() {
//                                    searchList.clear();
//                                    searchList.addAll(searchBody.getData().getPosts());
//                                    adapter_ٍSearch.notifyDataSetChanged();
//                                }
//                            });
//                        } else {
//                            isThereMore = false;
//                            if (page == STARTING_PAGE_INDEX) {
//                                showEmpty();
//                            }
//                        }
//
//                    } else {
//                        ShowErrorLayout(R.string.err_msg_server);
//                    }
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    ShowErrorLayout(R.string.err_msg_server);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<MyPosts> call, Throwable t) {
//                if (swipeLayout != null) {
//                    swipeLayout.setRefreshing(false);
//                }
//                System.out.println("onFailure");
//                t.printStackTrace();
//                ShowErrorLayout(R.string.err_msg_server);
//
//            }
//        });
//
//    }


//    private void showContent() {
//        rvSearchList.setVisibility(View.VISIBLE);
//        rlEmptyLayout.setVisibility(View.GONE);
//        rlNoInternetConnection.setVisibility(View.GONE);
//    }
//
//    private void showEmpty() {
//        tvEmptyText.setText(R.string.empty_search_result);
//        ivEmptyIcon.setImageResource(R.mipmap.no_search_result);
//        rvSearchList.setVisibility(View.GONE);
//        rlEmptyLayout.setVisibility(View.VISIBLE);
//        rlNoInternetConnection.setVisibility(View.GONE);
//    }
//
//    private void ShowErrorLayout(int txt) {
//        if (swipeLayout != null) {
//            swipeLayout.setRefreshing(false);
//        }
//        rvSearchList.setVisibility(View.GONE);
//        rlNoInternetConnection.setVisibility(View.VISIBLE);
//        tvError.setText(txt);
//    }
//
//    @OnClick(R.id.btn_refresh)
//    public void onViewClicked() {
//        Refresh();
//    }

//    private void Refresh() {
//        isThereMore = true;
//        searchList.clear();
//        adapter_ٍSearch.notifyDataSetChanged();
//        endlessScrollListener.resetState();
//        request_search(STARTING_PAGE_INDEX);
//
//    }
//
//    @Override
//    public void onRefresh() {
//        Refresh();
//    }
//
//    private void AddToFav(PostDataBean postDataBean) {
//
//        if (!Utils.isNetworkAvailable()) {
//            Utils.ShowNoInternetConnectionDialog(SearchActivity.this);
//            return;
//        }
//
//        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
//        AddToFavCall = apiService.addToFavorites(postDataBean.getId());
//        AddToFavCall.enqueue(new Callback<FormalResponse>() {
//            @Override
//            public void onResponse(Call<FormalResponse> call, final Response<FormalResponse> response) {
//                try {
//                    if (response.body() != null) {
//                        FormalResponse formalResponse = response.body();
//                        if (formalResponse.isStatus() && formalResponse.getCode() == QuickstartPreferences.OK_STATUS_CODE) {
//                            //Toast.makeText(getApplicationContext(), formalResponse.getMessage(), Toast.LENGTH_SHORT).show();
//                            Utils.ShowDialog(SearchActivity.this, getString(R.string.success), formalResponse.getMessage(), DIALOG_TYPE_SUCCESS);
//                            postDataBean.setIs_favorite(formalResponse.isFavorite_status());
//
//                        } else if (response.body().getCode() == QuickstartPreferences.UNAUTHORIZED_STATUS_CODE) {
//                            Toast.makeText(getApplicationContext(), R.string.Unauthorized, Toast.LENGTH_LONG).show();
//                            Utils.LunchLogin(getApplicationContext());
//
//                        } else {
//                            Utils.ShowServerDialog(SearchActivity.this);
//                        }
//                    } else {
//                        Utils.ShowServerDialog(SearchActivity.this);
//                    }
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//
//                    Utils.ShowServerDialog(SearchActivity.this);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<FormalResponse> call, Throwable t) {
//
//                t.printStackTrace();
//                Utils.ShowServerDialog(SearchActivity.this);
//            }
//        });
//    }
//

}
