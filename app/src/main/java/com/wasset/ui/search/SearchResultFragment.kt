package com.wasset.ui.search

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import butterknife.BindView
import butterknife.OnClick
import cn.refactor.lib.colordialog.PromptDialog.DIALOG_TYPE_SUCCESS
import com.facebook.FacebookSdk.getApplicationContext
import com.google.gson.Gson

import com.wasset.R
import com.wasset.adapter.AdapterCountries
import com.wasset.adapter.AdapterPostDetails
import com.wasset.interfaces.ApiClient
import com.wasset.interfaces.ApiInterface
import com.wasset.interfaces.OnItemClickListener
import com.wasset.models.*
import com.wasset.ui.PostDetailsActivity
import com.wasset.utility.EndlessRecyclerViewScrollListener
import com.wasset.utility.PreferenceEditor
import com.wasset.utility.QuickstartPreferences
import com.wasset.utility.QuickstartPreferences.UNAUTHORIZED_STATUS_CODE
import com.wasset.utility.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [SearchResultFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [SearchResultFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SearchResultFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener {
    private var param1: String? = null
    private var param2: String? = null
    @BindView(R.id.sp_categories)
    internal var spCategories: Spinner? = null
    @BindView(R.id.sp_countries)
    internal var spCountries: Spinner? = null
    @BindView(R.id.sp_cities)
    internal var spCities: Spinner? = null
    @BindView(R.id.content)
    internal var content: LinearLayout? = null
    @BindView(R.id.rv_search_list)
    internal var rvSearchList: RecyclerView? = null
    @BindView(R.id.swipe_layout)
    internal var swipeLayout: SwipeRefreshLayout? = null
    @BindView(R.id.tv_error)
    internal var tvError: TextView? = null
    @BindView(R.id.btn_refresh)
    internal var btnRefresh: Button? = null
    @BindView(R.id.rl_no_internet_connection)
    internal var rlNoInternetConnection: RelativeLayout? = null
    @BindView(R.id.iv_empty_icon)
    internal var ivEmptyIcon: ImageView? = null
    @BindView(R.id.ic_sort)
    internal var sortIc: ImageView? = null
    @BindView(R.id.tv_empty_text)
    internal var tvEmptyText: TextView? = null
    @BindView(R.id.rl_empty_layout)
    internal var rlEmptyLayout: LinearLayout? = null

    private var searchList: ArrayList<PostDataBean>? = null
    lateinit var searchAdapter: AdapterPostDetails
    private var call: Call<MyPosts>? = null
    private var searchBody: MyPosts? = null
    private var query: String? = null
    private val STARTING_PAGE_INDEX = 1
    private var endlessScrollListener: EndlessRecyclerViewScrollListener? = null
    private var isThereMore = true
    private var pf: PreferenceEditor? = null
    private var CountryId: String? = null
    private var CityId: String? = null
    private var GetCategoryListCall: Call<categoryList>? = null
    private var AddToFavCall: Call<FormalResponse>? = null
    private var CategoryId: String? = null
    private var login: Login.DataBean? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search_result, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pf = PreferenceEditor.getInstance()
        login = Gson().fromJson(pf!!.getStringPreference(QuickstartPreferences.USER_INFO), Login.DataBean::class.java)


        activity!!.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        val cfg = Gson().fromJson(pf!!.getStringPreference(QuickstartPreferences.CONFIG), Config.DataBean::class.java)

        searchList = ArrayList<PostDataBean>()
        swipeLayout!!.setOnRefreshListener(this)

        val linearLayoutManager = LinearLayoutManager(getApplicationContext())
        endlessScrollListener = object : EndlessRecyclerViewScrollListener(linearLayoutManager, STARTING_PAGE_INDEX) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
                if (Utils.isNetworkAvailable()) {
                    if (isThereMore) {
                        request_search(page)
                    }
                }
            }
        }
        rvSearchList!!.addOnScrollListener(endlessScrollListener as EndlessRecyclerViewScrollListener)
        rvSearchList!!.setLayoutManager(linearLayoutManager)
        searchAdapter = AdapterPostDetails(searchList)
        rvSearchList!!.setAdapter(searchAdapter)
        searchAdapter.setListener(object : OnItemClickListener {
            override fun onItemClick(itemView: View, position: Int) {
                if (itemView.id == R.id.btn_fav) {
                    AddToFav(searchList!!.get(position))
                } else {
                    val i = Intent(getApplicationContext(), PostDetailsActivity::class.java)
                    i.putExtra("Id", searchList!!.get(position).id)
                    startActivity(i)
                }
            }
        })

        val CountriesList = cfg.countries

        var selection = -1
        if (login != null) {
            for (i in CountriesList.indices) {
                if (login!!.getCity_id().equals(CountriesList[i].id, ignoreCase = true)) {
                    selection = i
                    CountryId = login!!.getCity_id()
                }
            }
        }

        val adapter = AdapterCountries(activity, R.layout.row_countries_spinner_item, R.id.tvName, CountriesList)
        spCountries!!.setAdapter(adapter)

        if (selection != -1) {
            spCountries!!.setSelection(selection)
        }

        spCountries!!.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, pos: Int, l: Long) {
                CountryId = CountriesList[pos].id
                CityId = null

                if (CountriesList[pos].cities.size > 0) {
                    spCities!!.setVisibility(View.VISIBLE)
                    sortIc!!.setVisibility(View.VISIBLE)
                    setupCountryCitiesList(CountriesList[pos].cities)
                } else {
                    spCities!!.setVisibility(View.GONE)
                    sortIc!!.setVisibility(View.GONE)
                }

                Refresh()
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {

            }
        })
        requestGetCategoryList()
    }
    private fun requestGetCategoryList() {
        if (!Utils.isNetworkAvailable()) {
            ShowErrorLayout(R.string.no_internet_connection)
            return
        }

        val apiService = ApiClient.getClient().create(ApiInterface::class.java)

        GetCategoryListCall = apiService.getCategoryList(null)
        GetCategoryListCall!!.enqueue(object : Callback<categoryList> {
            override fun onResponse(call: Call<categoryList>, response: Response<categoryList>) {
                println("raw" + response.raw())
                try {
                    if (response.body()!!.isStatus && response.body()!!.code == QuickstartPreferences.OK_STATUS_CODE) {
                        if (response.body()!!.data.size > 0) {

                            val CategoriesList = response.body()!!.data
                            val spinnerList = ArrayList<String>()
                            for (i in CategoriesList.indices) {
                                val item = CategoriesList[i]
                                spinnerList.add(item.name)
                            }

                            val dataAdapter = SearchSpinnerAdapter(activity!!, R.layout.spin_item, R.id.tvName, spinnerList)
                            //dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spCategories!!.setAdapter(dataAdapter)

                            spCategories!!.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
                                override fun onItemSelected(adapterView: AdapterView<*>, view: View, pos: Int, l: Long) {
                                    CategoryId = CategoriesList[pos].category_id
                                    Refresh()
                                }

                                override fun onNothingSelected(adapterView: AdapterView<*>) {

                                }
                            })


                        }

                    } else if (response.body()!!.code == UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(getApplicationContext(), R.string.Unauthorized, Toast.LENGTH_LONG).show()
                        Utils.LunchLogin(getApplicationContext())
                    } else {
                        Toast.makeText(activity, R.string.err_msg_server, Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    Toast.makeText(activity, R.string.err_msg_server, Toast.LENGTH_SHORT).show()
                }

            }

            override fun onFailure(call: Call<categoryList>, t: Throwable) {
                if (!call.isCanceled && "Canceled" != t.message) {
                    t.printStackTrace()
                    Toast.makeText(activity, R.string.err_msg_server, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SearchResultFragment.
         */
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                SearchResultFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }

    private fun Refresh() {
        isThereMore = true
        searchList!!.clear()
        searchAdapter.notifyDataSetChanged()
        endlessScrollListener!!.resetState()
        request_search(STARTING_PAGE_INDEX)
    }

    private fun showContent() {
        rvSearchList!!.setVisibility(View.VISIBLE)
        rlEmptyLayout!!.setVisibility(View.GONE)
        rlNoInternetConnection!!.setVisibility(View.GONE)
    }

    private fun showEmpty() {
        tvEmptyText!!.setText(R.string.empty_search_result)
        ivEmptyIcon!!.setImageResource(R.mipmap.no_search_result)
        rvSearchList!!.setVisibility(View.GONE)
        rlEmptyLayout!!.setVisibility(View.VISIBLE)
        rlNoInternetConnection!!.setVisibility(View.GONE)
    }

    private fun ShowErrorLayout(txt: Int) {
        if (swipeLayout != null) {
            swipeLayout!!.setRefreshing(false)
        }
        rvSearchList!!.setVisibility(View.GONE)
        rlNoInternetConnection!!.setVisibility(View.VISIBLE)
        tvError!!.setText(txt)
    }

    @OnClick(R.id.btn_refresh)
    fun onViewClicked() {
        Refresh()
    }

    override fun onRefresh() {
        Refresh()
    }

    private fun AddToFav(postDataBean: PostDataBean) {

        if (!Utils.isNetworkAvailable()) {
            Utils.ShowNoInternetConnectionDialog(activity)
            return
        }

        val apiService = ApiClient.getClient().create(ApiInterface::class.java)
        AddToFavCall = apiService.addToFavorites(postDataBean.id)
        AddToFavCall!!.enqueue(object : Callback<FormalResponse> {
            override fun onResponse(call: Call<FormalResponse>, response: Response<FormalResponse>) {
                try {
                    if (response.body() != null) {
                        val formalResponse = response.body()
                        if (formalResponse!!.isStatus && formalResponse.code == QuickstartPreferences.OK_STATUS_CODE) {
                            //Toast.makeText(getApplicationContext(), formalResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            Utils.ShowDialog(activity, getString(R.string.success), formalResponse.message,
                                    DIALOG_TYPE_SUCCESS)
                            postDataBean.is_favorite = formalResponse.isFavorite_status

                        } else if (response.body()!!.code == QuickstartPreferences.UNAUTHORIZED_STATUS_CODE) {
                            Toast.makeText(getApplicationContext(), R.string.Unauthorized, Toast.LENGTH_LONG).show()
                            Utils.LunchLogin(getApplicationContext())

                        } else {
                            Utils.ShowServerDialog(activity)
                        }
                    } else {
                        Utils.ShowServerDialog(activity)
                    }

                } catch (e: Exception) {
                    e.printStackTrace()

                    Utils.ShowServerDialog(activity)
                }

            }

            override fun onFailure(call: Call<FormalResponse>, t: Throwable) {

                t.printStackTrace()
                Utils.ShowServerDialog(activity)
            }
        })
    }
    private fun setupCountryCitiesList(cities: List<Config.DataBean.CountriesBean.CitiesBean>) {

        val spinnerList = ArrayList<String>()
        for (item in cities) {
            spinnerList.add(item.name)
        }

        val dataAdapter = SearchSpinnerAdapter(activity!!, R.layout.spin_item, R.id.tvName, spinnerList)
        //dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCities!!.setAdapter(dataAdapter)
        spCities!!.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, pos: Int, l: Long) {
                println("City : " + cities[pos].name)
                CityId = cities[pos].id
                Refresh()
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {

            }
        })
    }

    private fun request_search(page: Int) {

        if (!Utils.isNetworkAvailable()) {
            if (page == STARTING_PAGE_INDEX) {
                ShowErrorLayout(R.string.no_internet_connection)
            } else {
                Toast.makeText(activity!!, R.string.no_internet_connection, Toast.LENGTH_SHORT).show()
            }
            return
        }

        val apiService = ApiClient.getClient().create(ApiInterface::class.java)
        if (swipeLayout != null && page == STARTING_PAGE_INDEX) {
            swipeLayout!!.setRefreshing(true)
        }

        call = apiService.search(query, page, CategoryId, CountryId, CityId)
        call!!.enqueue(object : Callback<MyPosts> {
            override fun onResponse(call: Call<MyPosts>, response: Response<MyPosts>) {
                println("raw" + response.raw())
                if (swipeLayout != null) {
                    swipeLayout!!.setRefreshing(false)
                }

                try {
                    if (response.body()!!.isStatus && response.body()!!.code == QuickstartPreferences.OK_STATUS_CODE) {
                        searchBody = response.body()
                        if (searchBody!!.getData()!!.posts!!.size> 0) {
                            showContent()
                            rvSearchList!!.post(Runnable {
                                searchList!!.clear()
                                searchList!!.addAll(searchBody!!.getData().posts)
                                searchAdapter.notifyDataSetChanged()
                            })
                        } else {
                            isThereMore = false
                            if (page == STARTING_PAGE_INDEX) {
                                showEmpty()
                            }
                        }

                    } else {
                        ShowErrorLayout(R.string.err_msg_server)
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                    ShowErrorLayout(R.string.err_msg_server)
                }

            }

            override fun onFailure(call: Call<MyPosts>, t: Throwable) {
                if (swipeLayout != null) {
                    swipeLayout!!.setRefreshing(false)
                }
                println("onFailure")
                t.printStackTrace()
                ShowErrorLayout(R.string.err_msg_server)

            }
        })

    }
}
