package com.wasset.ui

import android.Manifest
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager

import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.github.chrisbanes.photoview.PhotoView
import com.google.gson.Gson
import com.wasset.R
import com.wasset.adapter.AdapterComments
import com.wasset.adapter.AdapterPostDetailsAttribute
import com.wasset.adapter.AdapterSimilarAds
import com.wasset.adapter.AdapterVpPostImages
import com.wasset.interfaces.ApiClient
import com.wasset.interfaces.ApiInterface
import com.wasset.interfaces.OnItemClickListener
import com.wasset.models.Comments
import com.wasset.models.FormalResponse
import com.wasset.models.Login
import com.wasset.models.PostDataBean
import com.wasset.models.postDetails
import com.wasset.utility.GridSpacingItemDecoration
import com.wasset.utility.PreferenceEditor
import com.wasset.utility.QuickstartPreferences
import com.wasset.utility.Utils
import com.wasset.views.AVLoadingIndicatorDialog

import java.util.ArrayList

import butterknife.ButterKnife
import butterknife.OnClick
import cn.refactor.lib.colordialog.ColorDialog
import me.relex.circleindicator.CircleIndicator
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import cn.refactor.lib.colordialog.PromptDialog.DIALOG_TYPE_SUCCESS
import cn.refactor.lib.colordialog.PromptDialog.DIALOG_TYPE_WRONG
import com.wasset.databinding.FragmentPostDetailsBinding
import com.wasset.utility.QuickstartPreferences.UNAUTHORIZED_STATUS_CODE
import com.wasset.utility.Utils.ShowLoginRequiredDialog
import com.wasset.utility.Utils.isLogin
import kotlinx.android.synthetic.main.fragment_post_details.*
import kotlinx.android.synthetic.main.no_internet_connection_layout.*
import kotlinx.android.synthetic.main.toolbar.*

class PostDetailsActivity : BaseActivity(), SwipeRefreshLayout.OnRefreshListener {

    private var PostId: String? = null
    private var call: Call<postDetails>? = null
    private var commentsCall: Call<Comments>? = null
    private var binding: FragmentPostDetailsBinding? = null
    private var NumberMobile: String? = null
    private var handlers: MyPostDetailsHandlers? = null
    private var indicatorView: AVLoadingIndicatorDialog? = null
    private var AddToFavCall: Call<FormalResponse>? = null
    private var reportAd: Call<FormalResponse>? = null
    private var SendCommentCall: Call<Comments>? = null
    private var postDetailsData: PostDataBean? = null
    internal var comments: MutableList<Comments.CommentsBean>? = ArrayList()
    internal var adapterComments: AdapterComments? = null
    internal var user: Login.DataBean? = null
    private var updateCall: Call<FormalResponse>? = null
    private var deleteCall: Call<FormalResponse>? = null
    internal var isOnCreateCalled = false
    internal var shoudlScrollToComment = false

    private val commentsCallback = object : Callback<Comments> {
        override fun onResponse(call: Call<Comments>, response: Response<Comments>) {
            if (comments == null)
                comments = ArrayList<Comments.CommentsBean>()
            else
                comments!!.clear()
            if (response.body()!!.comments.size > 5) {
                val startIndex = response.body()!!.comments.size - 5
                comments!!.addAll(response.body()!!.comments.subList(startIndex, startIndex + 5))
            } else
                comments!!.addAll(response.body()!!.comments)

            if (adapterComments == null && user != null) {
                adapterComments = AdapterComments(comments, user!!.id)
                adapterComments!!.notifyDataSetChanged()
                val linearLayoutManager = LinearLayoutManager(this@PostDetailsActivity)
                comments_rv!!.layoutManager = linearLayoutManager
                comments_rv!!.adapter = adapterComments
            }
        }

        override fun onFailure(call: Call<Comments>, t: Throwable) {
            if (!call.isCanceled && "Canceled" != t.message) {
                swipe!!.isRefreshing = false
                t.printStackTrace()
                ShowErrorLayout(R.string.err_msg_server)
            }
        }
    }

    internal var gridLayoutManager: GridLayoutManager? = null
    internal var adapterSimilarAds: AdapterSimilarAds? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.fragment_post_details)
        ButterKnife.bind(this)

        handlers = MyPostDetailsHandlers(this@PostDetailsActivity)
        user = Gson().fromJson(PreferenceEditor.getInstance().getStringPreference(QuickstartPreferences.USER_INFO), Login.DataBean::class.java!!)
        shoudlScrollToComment = intent.getBooleanExtra("SHOULD_SCROLL", false)

        setToolbar()

    }

    override fun onStart() {
        super.onStart()
        if (comments != null && isOnCreateCalled)
            getComments()

        btn_more_comments!!.setOnClickListener {
            if (user != null) {
                val i = Intent(applicationContext, CommentsActivity::class.java)
                i.putExtra("Id", postDetailsData!!.id)
                i.putExtra("USER_ID", user!!.id)
                startActivity(i)
            } else {
                Toast.makeText(this@PostDetailsActivity, "برجاء تسجيل الدخول لتتمكن من التعامل مع المحتوى", Toast.LENGTH_SHORT).show()
            }
        }
        iv_send!!.setOnClickListener {
            if (et_comment!!.getText() != null && et_comment!!.getText().toString().length > 0)
                if (user != null)
                    sendComment(et_comment!!.getText().toString())
                else
                    displayUserDialog(this@PostDetailsActivity)
        }
        btn_report!!.setOnClickListener(View.OnClickListener {
            val reportDialogFragment = ReportDialogFragment()
            reportDialogFragment.setOnSendClickListener { reason -> reportAd(reason) }
            reportDialogFragment.show(supportFragmentManager, "")
        })
        swipe!!.setOnRefreshListener(this)
        try {
            PostId = intent.extras!!.get("Id") as String
            getPostDetails()
        } catch (e: Exception) {
            e.printStackTrace()

        }
    }

    private fun displayUserDialog(context: Context) {
        val builder1 = AlertDialog.Builder(context)
        builder1.setMessage("يجب عليك تسجيل الدخول اولا")
        builder1.setCancelable(true)

        builder1.setPositiveButton(
                "نعم"
        ) { dialog, id ->
            Utils.LunchLogin(applicationContext)
            dialog.cancel()
        }

        builder1.setNegativeButton(
                "لا",
                object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface, id: Int) {
                        dialog.cancel()
                    }
                })

        val alert11 = builder1.create()
        alert11.show()
    }

    fun setToolbar() {
        tv_toolbar_title!!.setText(R.string.post_details)
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)

        toolbar!!.setNavigationOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                onBackPressed()
            }
        })

    }

    private fun reportAd(body: String) {
        if (!Utils.isNetworkAvailable()) {
            ShowErrorLayout(R.string.no_internet_connection)
            return
        }
        val apiInterface = ApiClient.getClient().create(ApiInterface::class.java!!)
        swipe!!.setRefreshing(true)
        reportAd = apiInterface.createAdReport(PostId, body)
        reportAd!!.enqueue(object : Callback<FormalResponse> {
            override fun onResponse(call: Call<FormalResponse>, response: Response<FormalResponse>) {
                swipe!!.setRefreshing(false)
                try {
                    if (response.body()!!.isStatus && response.body()!!.code == QuickstartPreferences.UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(this@PostDetailsActivity, "برجاء تسجيل الدخول لتتمكن من الإبلاغ عن المحتوى", Toast.LENGTH_SHORT).show()
                    } else if (response.body()!!.isStatus && response.body()!!.code == QuickstartPreferences.OK_STATUS_CODE) {
                        Toast.makeText(this@PostDetailsActivity, "تم الإبلاغ عن المحتوى", Toast.LENGTH_SHORT).show()

                    }
                } catch (e: Exception) {

                }

            }

            override fun onFailure(call: Call<FormalResponse>, t: Throwable) {
                swipe!!.setRefreshing(false)
                Utils.ShowServerDialog(this@PostDetailsActivity)

            }
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    private fun getPostDetails() {
        if (!Utils.isNetworkAvailable()) {
            ShowErrorLayout(R.string.no_internet_connection)
            return
        }

        val apiService = ApiClient.getClient().create(ApiInterface::class.java!!)
        swipe!!.setRefreshing(true)

        call = apiService.getPostDetails(PostId)
        call!!.enqueue(object : Callback<postDetails> {
            override fun onResponse(call: Call<postDetails>, response: Response<postDetails>) {
                println("raw" + response.raw())
                isOnCreateCalled = true
                content!!.post(object : Runnable {
                    override fun run() {
                        content!!.smoothScrollTo(0, 0)
                    }
                })
                swipe!!.setRefreshing(false)

                try {
                    if (response.body()!!.isStatus && response.body()!!.code == QuickstartPreferences.OK_STATUS_CODE) {
                        if (response.body()!!.data != null) {
                            showContent()
                            if (shoudlScrollToComment) {
                                content!!.post(object : Runnable {
                                    override fun run() {
                                        content!!.smoothScrollTo(0, comments_rv!!.getTop())
                                        shoudlScrollToComment = false
                                    }
                                })
                            }
                            postDetailsData = response.body()!!.data
                            binding!!.postDetails = response.body()!!.data
                            binding!!.handlersListener = handlers
                            setUpViewPager(response.body()!!.data.photos)
                            setupSimilarPostsList(response.body()!!.data.related)
                            SetupPostAttribute(response.body()!!.data.attributes)
                            setUpEnabledViews(response.body()!!.data.isComment_enable, response.body()!!.data.isCall_enable)
                            if (response.body()!!.data.isUser_special)
                                handleSpecialUser()
                            else
                                handleNonSpecialUser()
                            getComments()

                        } else {
                            ShowErrorLayout(R.string.err_msg_server)
                        }

                    } else if (response.body()!!.code == UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(applicationContext, R.string.Unauthorized, Toast.LENGTH_LONG).show()
                        Utils.LunchLogin(applicationContext)
                    } else {
                        ShowErrorLayout(R.string.err_msg_server)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    ShowErrorLayout(R.string.err_msg_server)
                }

            }


            override fun onFailure(call: Call<postDetails>, t: Throwable) {
                if (!call.isCanceled && "Canceled" != t.message) {
                    swipe!!.setRefreshing(false)
                    t.printStackTrace()
                    ShowErrorLayout(R.string.err_msg_server)
                }
            }
        })


    }

    private fun getComments() {
        val apiService = ApiClient.getClient().create(ApiInterface::class.java!!)
        commentsCall = apiService.getPostComments(PostId)
        commentsCall!!.enqueue(commentsCallback)
    }

    private fun setUpEnabledViews(comment_enable: Boolean, call_enable: Boolean) {
        if (!comment_enable) {
            comment_lin!!.visibility = View.GONE
            et_comment!!.visibility = View.GONE
            iv_send!!.visibility = View.GONE
            btn_more_comments!!.visibility = View.GONE
            comments_rv!!.visibility = View.GONE
            comment_tv!!.visibility = View.VISIBLE
        } else {
            et_comment!!.visibility = View.VISIBLE
            iv_send!!.visibility = View.VISIBLE
            btn_more_comments!!.visibility = View.VISIBLE
            comments_rv!!.visibility = View.VISIBLE
            comment_tv!!.visibility = View.INVISIBLE
        }
        if (!call_enable)
            btn_call!!.visibility = View.GONE
        else
            btn_call!!.visibility = View.VISIBLE

    }

    private fun handleSpecialUser() {
        ic_user!!.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.verified_account))
    }

    private fun handleNonSpecialUser() {
        ic_user!!.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_person))
    }

    private fun showContent() {
        try {
            content!!.visibility = View.VISIBLE
            rl_no_internet_connection!!.visibility = View.GONE
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun ShowErrorLayout(txt: Int) {
        try {
            content!!.visibility = View.GONE
            rl_no_internet_connection!!.visibility = View.VISIBLE
            tv_error!!.setText(txt)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun setupSimilarPostsList(related: List<PostDataBean.RelatedBean>) {
        if (gridLayoutManager == null)
            gridLayoutManager = GridLayoutManager(applicationContext, 2)
        if (adapterSimilarAds == null) {
            adapterSimilarAds = AdapterSimilarAds(applicationContext, related)
            rv_similar_ads!!.setLayoutManager(gridLayoutManager)
            rv_similar_ads!!.setAdapter(adapterSimilarAds)

            rv_similar_ads!!.isNestedScrollingEnabled = false
            val spanCount = 2 // 3 columns
            val spacing = 20 // 50px
            val includeEdge = true
            rv_similar_ads!!.addItemDecoration(GridSpacingItemDecoration(spanCount, spacing, includeEdge))
            //rvSimilarAds.addItemDecoration(new SpacesItemDecoration(10));

            adapterSimilarAds!!.setListener(object : OnItemClickListener {
                override fun onItemClick(itemView: View, position: Int) {
                    PostId = related[position].id

                    getPostDetails()
                }
            })

        } else {
            adapterSimilarAds!!.setAds(related)
            adapterSimilarAds!!.notifyDataSetChanged()
        }
    }

    private fun setUpViewPager(photos: List<PostDataBean.PhotosBean>) {

        val viewPagerAdapter = MyViewPagerAdapter(applicationContext, photos)
        vp_post_images!!.adapter = viewPagerAdapter
        viewPagerAdapter.setListener(object : OnItemClickListener {
            override fun onItemClick(itemView: View, position: Int) {

                try {
                    //AppCompatImageView imgView = (AppCompatImageView) itemView;
                    //Bitmap bitmap = ((BitmapDrawable) imgView.getDrawable()).getBitmap();
                    //showFullImageDialog(bitmap);

                    //viewPagerAdapter.getImagesBitmap();
                    showFullImagesDialog(photos)
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        })
        layoutDots!!.setViewPager(vp_post_images)

    }

    private fun showFullImageDialog(bitmap: Bitmap) {
        val dialog = Dialog(this@PostDetailsActivity)

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.full_image_dialog)
        dialog.setCancelable(true)
        dialog.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        val full_image = dialog.findViewById<View>(R.id.img_full_image) as PhotoView
        full_image.setImageBitmap(bitmap)
        dialog.show()

    }

    private fun sendComment(comment: String) {

        if (!Utils.isNetworkAvailable()) {
            Utils.ShowNoInternetConnectionDialog(this@PostDetailsActivity)
            return
        }

        Utils.HideKeyBord(this@PostDetailsActivity)
        showProgress()

        val apiService = ApiClient.getClient().create(ApiInterface::class.java!!)
        SendCommentCall = apiService.SendComment(PostId, comment)
        SendCommentCall!!.enqueue(object : Callback<Comments> {
            override fun onResponse(call: Call<Comments>, response: Response<Comments>) {
                println("raw" + response.raw())
                hideProgress()
                try {
                    if (response.body()!!.isStatus && response.body()!!.code == QuickstartPreferences.OK_STATUS_CODE) {

                        Utils.ShowDialog(this@PostDetailsActivity, getString(R.string.success), response.body()!!.message, DIALOG_TYPE_SUCCESS)
                        comments!!.clear()
                        if (response.body()!!.comments.size > 5) {
                            val startIndex = response.body()!!.comments.size - 5
                            comments!!.addAll(response.body()!!.comments.subList(startIndex, startIndex + 5))
                        } else
                            comments!!.addAll(response.body()!!.comments)

                        adapterComments!!.notifyDataSetChanged()
                        et_comment!!.getText().clear()
                    } else if (response.body()!!.code == UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(this@PostDetailsActivity, R.string.Unauthorized, Toast.LENGTH_LONG).show()
                        Utils.LunchLogin(this@PostDetailsActivity)

                    } else {
                        ShowErrorLayout(R.string.err_msg_server)
                    }


                } catch (e: Exception) {

                    e.printStackTrace()
                    ShowErrorLayout(R.string.err_msg_server)
                }

            }


            override fun onFailure(call: Call<Comments>, t: Throwable) {
                t.printStackTrace()
                hideProgress()
                Utils.ShowServerDialog(this@PostDetailsActivity)
            }
        })
    }


    private fun showFullImagesDialog(photos: List<PostDataBean.PhotosBean>) {
        val dialog = Dialog(this@PostDetailsActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.post_images_view_pager_views)
        dialog.setCancelable(true)
        dialog.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        val viewPager = dialog.findViewById<View>(R.id.vp_post_images) as ViewPager
        val layoutDots = dialog.findViewById<View>(R.id.layoutDots) as CircleIndicator
        val viewPagerAdapter = AdapterVpPostImages(applicationContext, photos)
        viewPager.adapter = viewPagerAdapter
        layoutDots.setViewPager(viewPager)

        dialog.show()

    }


    private fun SetupPostAttribute(Attributes: List<PostDataBean.AttributesBean>) {
        val gridLayoutManager = LinearLayoutManager(applicationContext)
        val adapterPostAttr = AdapterPostDetailsAttribute(applicationContext, Attributes)
        rv_post_attributes!!.layoutManager = gridLayoutManager
        rv_post_attributes!!.adapter = adapterPostAttr
        rv_post_attributes!!.isNestedScrollingEnabled = false

    }

    override fun onRefresh() {
        getPostDetails()
    }

    @OnClick(R.id.btn_refresh)
    fun onViewClicked() {
        getPostDetails()
    }


    inner class MyViewPagerAdapter internal constructor(private var context: Context, private var images: List<PostDataBean.PhotosBean>) : PagerAdapter() {
        override fun getCount(): Int {
            return images.size
        }

        private var layoutInflater: LayoutInflater? = null
        private var listener: OnItemClickListener? = null
        val imagesBitmap: List<Bitmap>

        init {
            imagesBitmap = ArrayList()

        }

        fun setListener(listener: OnItemClickListener) {
            this.listener = listener
        }

        @NonNull
        override fun instantiateItem(container: ViewGroup, position: Int): Any {

            layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val view = layoutInflater!!.inflate(R.layout.row_post_image_item, container, false)
            val post_img = view.findViewById<View>(R.id.iv_category_image) as ImageView
            Glide.with(view.context)
                    .load(this.images[position].url)
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(@Nullable e: GlideException?, model: Any, target: Target<Drawable>, isFirstResource: Boolean): Boolean {
                            post_img.setImageResource(R.mipmap.ic_launcher)
                            return false
                        }

                        override fun onResourceReady(resource: Drawable, model: Any, target: Target<Drawable>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                            return false
                        }
                    })
                    .into(post_img)

            post_img.setOnClickListener(object : View.OnClickListener {
                override fun onClick(view: View) {
                    if (listener != null) {
                        listener!!.onItemClick(view, position)
                    }
                }
            })
            container.addView(view)
            return view
        }

        override fun isViewFromObject(view: View, obj: Any): Boolean {
            return view === obj
        }


        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            val view = `object` as View
            container.removeView(view)
        }
    }

    private fun CallPhone() {
        if (Build.VERSION.SDK_INT >= 23 && ContextCompat.checkSelfPermission(this@PostDetailsActivity, Manifest.permission.CALL_PHONE) !== PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this@PostDetailsActivity, arrayOf(Manifest.permission.CALL_PHONE), REQUEST_CODE_CALL_PHONE)
            return
        }

        if (NumberMobile != null && !NumberMobile!!.isEmpty()) {
            val intent = Intent(Intent.ACTION_CALL)
            intent.data = Uri.parse("tel:" + NumberMobile!!)
            startActivity(intent)
        } else {

            Utils.ShowDialog(this@PostDetailsActivity, getString(R.string.err), getString(R.string.err_mobile_number_empty), DIALOG_TYPE_WRONG)
        }
    }


    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_CODE_CALL_PHONE -> {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    CallPhone()
                }
            }
        }// other 'case' lines to check for other
        // permissions this app might request
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        var position = -1
        Log.d("CONTEXTMENU", "ASD")
        try {
            position = adapterComments!!.position

        } catch (e: Exception) {
            return super.onContextItemSelected(item)
        }

        when (item.itemId) {
            12349 -> deleteComment(position)
            12348 -> updateComment(position)
        }
        return super.onContextItemSelected(item)

    }

    private fun updateComment(position: Int) {
        val updateCommentDialogFragment = UpdateCommentDialogFragment.newInstance(comments!![position].comment_text)
        updateCommentDialogFragment.show(supportFragmentManager, "")
        updateCommentDialogFragment.setOnUpdateClickListener(object : UpdateCommentDialogFragment.OnUpdateClickListener {
            override fun onCommentUpdated(comment: String) {
                updateComment(comment, position)
            }
        })
    }


    private fun deleteComment(position: Int) {
        if (!Utils.isNetworkAvailable()) {
            ShowErrorLayout(R.string.no_internet_connection)
            return
        }
        val apiInterface = ApiClient.getClient().create(ApiInterface::class.java!!)
        swipe!!.setRefreshing(true)
        deleteCall = apiInterface.deleteComment(comments!![position].comment_id)
        comments!!.removeAt(position)

        deleteCall!!.enqueue(object : Callback<FormalResponse> {
            override fun onResponse(call: Call<FormalResponse>, response: Response<FormalResponse>) {
                swipe!!.setRefreshing(false)
                try {
                    if (response.body()!!.isStatus && response.body()!!.code == QuickstartPreferences.UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(this@PostDetailsActivity, "برجاء تسجيل الدخول لتتمكن من الإبلاغ عن المحتوى", Toast.LENGTH_SHORT).show()
                    } else if (response.body()!!.isStatus && response.body()!!.code == QuickstartPreferences.OK_STATUS_CODE) {
                        Toast.makeText(this@PostDetailsActivity, "تم مسح التلعيق", Toast.LENGTH_SHORT).show()
                        adapterComments!!.notifyItemRemoved(position)
                    }
                } catch (e: Exception) {
                    Utils.ShowServerDialog(this@PostDetailsActivity)
                }

            }

            override fun onFailure(call: Call<FormalResponse>, t: Throwable) {
                swipe!!.setRefreshing(false)
                Utils.ShowServerDialog(this@PostDetailsActivity)

            }
        })

    }


    private fun showConfirmDeleteItem() {

        val dialog = ColorDialog(this@PostDetailsActivity)
        dialog.setAnimationEnable(true)
        dialog.setTitle(getString(R.string.info))
        dialog.contentText = getString(R.string.msg_delete_item)
        //dialog.setContentImage(getResources().getDrawable(R.drawable.ic_warning));
        dialog.setPositiveListener(getString(R.string.yes), object : ColorDialog.OnPositiveListener {
            override fun onClick(dialog: ColorDialog) {
                dialog.dismiss()
                AddToFav()

            }
        })
                .setNegativeListener(getString(R.string.Cancel), object : ColorDialog.OnNegativeListener {
                    override fun onClick(dialog: ColorDialog) {
                        dialog.dismiss()
                    }
                }).show()
    }

    private fun hideProgress() {
        if (indicatorView != null && indicatorView!!.isShowing) {
            indicatorView!!.dismiss()
        }
    }

    private fun showProgress() {

        if (indicatorView == null) {
            indicatorView = AVLoadingIndicatorDialog(this@PostDetailsActivity)
            //indicatorView.setMessage(msg);
            indicatorView!!.setCancelable(false)
        }
        indicatorView!!.show()
    }


    private fun AddToFav() {

        if (!Utils.isNetworkAvailable()) {
            Utils.ShowNoInternetConnectionDialog(this@PostDetailsActivity)
            return
        }

        showProgress()
        val apiService = ApiClient.getClient().create(ApiInterface::class.java!!)
        AddToFavCall = apiService.addToFavorites(postDetailsData!!.id)
        AddToFavCall!!.enqueue(object : Callback<FormalResponse> {
            override fun onResponse(call: Call<FormalResponse>, response: Response<FormalResponse>) {
                hideProgress()
                try {
                    if (response.body() != null) {
                        val formalResponse = response.body()
                        if (formalResponse!!.isStatus && formalResponse!!.code == QuickstartPreferences.OK_STATUS_CODE) {
                            //Toast.makeText(getApplicationContext(), formalResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            Utils.ShowDialog(this@PostDetailsActivity, getString(R.string.success), formalResponse!!.message, DIALOG_TYPE_SUCCESS)
                            postDetailsData!!.is_favorite = formalResponse!!.isFavorite_status

                        } else if (response.body()!!.code == QuickstartPreferences.UNAUTHORIZED_STATUS_CODE) {
                            Toast.makeText(applicationContext, R.string.Unauthorized, Toast.LENGTH_LONG).show()
                            Utils.LunchLogin(applicationContext)

                        } else {
                            Utils.ShowServerDialog(this@PostDetailsActivity)
                        }
                    } else {
                        Utils.ShowServerDialog(this@PostDetailsActivity)
                    }

                } catch (e: Exception) {
                    e.printStackTrace()

                    Utils.ShowServerDialog(this@PostDetailsActivity)
                }

            }

            override fun onFailure(call: Call<FormalResponse>, t: Throwable) {
                hideProgress()
                t.printStackTrace()
                Utils.ShowServerDialog(this@PostDetailsActivity)
            }
        })
    }


    inner class MyPostDetailsHandlers(internal var context: Context) {

        fun onCallClicked(view: View, postDetails: PostDataBean) {
            NumberMobile = postDetails.mobile_number
            CallPhone()
        }

        fun onShareClicked(view: View, postDetails: PostDataBean) {

            val link = postDetails.post_url
            println("link" + link!!)
            /*

                       final Uri deepLink = Utils.buildDeepLink(context ,Uri.parse(link));
                       Intent shareIntent = new Intent(Intent.ACTION_SEND);
                       shareIntent.setType("text/plain");
                       shareIntent.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.app_name));
                       shareIntent.putExtra(Intent.EXTRA_TEXT, deepLink.toString());
                       shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                       Intent new_intent = Intent.createChooser(shareIntent, context.getString(R.string.share));
                       new_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                       context.startActivity(new_intent);
           */

            try {
                if (link != null && !link!!.isEmpty()) {
                    val i = Intent(Intent.ACTION_SEND)
                    i.type = "text/plain"
                    i.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.app_name))
                    i.putExtra(Intent.EXTRA_TEXT, link)
                    startActivity(Intent.createChooser(i, context.getString(R.string.share)))
                } else {
                    Toast.makeText(context, R.string.invalid_post_link, Toast.LENGTH_LONG).show()
                }
            } catch (e: Exception) {
                //e.toString();
                e.printStackTrace()
            }

        }


        fun onWhatsAppClick(view: View, postDetails: PostDataBean) {

            try {
                var number = postDetails.mobile_number
                number = number.replace("+", "").replace(" ", "").replace("00", "")
                val url = "https://api.whatsapp.com/send?phone=$number"
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                startActivity(browserIntent)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        fun onPostComments(postDetails: PostDataBean) {
            val i = Intent(applicationContext, CommentsActivity::class.java)
            i.putExtra("Id", postDetails.id)
            startActivity(i)
        }

        fun onFavClicked(view: View, postDetails: PostDataBean) {

            if (!isLogin()) {
                ShowLoginRequiredDialog(this@PostDetailsActivity)
                return
            }


            val isFav = postDetails.is_favorite
            if (isFav) {
                showConfirmDeleteItem()
            } else {
                AddToFav()
            }
        }

        fun onPublisherNameClicked(view: View, postDetails: PostDataBean) {
            if (postDetails.user_id != null) {
                val i = Intent(applicationContext, PublisherProfileActivity::class.java)
                i.putExtra("Id", postDetails.user_id)
                startActivity(i)
            }

        }

    }

    private fun updateComment(body: String, position: Int) {
        if (!Utils.isNetworkAvailable()) {
            ShowErrorLayout(R.string.no_internet_connection)
            return
        }
        val apiInterface = ApiClient.getClient().create(ApiInterface::class.java!!)
        swipe!!.setRefreshing(true)
        updateCall = apiInterface.updateComment(comments!![position].comment_id, body)
        var updatedComment = Comments.CommentsBean()
        updatedComment = comments!![position]
        updatedComment.comment_text = body
        updateCall!!.enqueue(object : Callback<FormalResponse> {
            override fun onResponse(call: Call<FormalResponse>, response: Response<FormalResponse>) {
                swipe!!.setRefreshing(false)
                try {
                    if (response.body()!!.isStatus && response.body()!!.code == QuickstartPreferences.UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(this@PostDetailsActivity, "برجاء تسجيل الدخول لتتمكن من الإبلاغ عن المحتوى", Toast.LENGTH_SHORT).show()
                    } else if (response.body()!!.isStatus && response.body()!!.code == QuickstartPreferences.OK_STATUS_CODE) {
                        Toast.makeText(this@PostDetailsActivity, "تم تحديث التلعيق", Toast.LENGTH_SHORT).show()
                        adapterComments!!.notifyItemChanged(position)
                    }
                } catch (e: Exception) {
                    Utils.ShowServerDialog(this@PostDetailsActivity)
                }

            }

            override fun onFailure(call: Call<FormalResponse>, t: Throwable) {
                swipe!!.setRefreshing(false)
                Utils.ShowServerDialog(this@PostDetailsActivity)

            }
        })
    }

    companion object {

        private val REQUEST_CODE_CALL_PHONE = 100
    }
}
