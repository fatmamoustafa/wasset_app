package com.wasset.ui;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wasset.R;
import com.wasset.adapter.AdapterPostDetails;
import com.wasset.interfaces.ApiClient;
import com.wasset.interfaces.ApiInterface;
import com.wasset.models.CategoryProducts;
import com.wasset.models.FormalResponse;
import com.wasset.models.PostDataBean;
import com.wasset.models.publisherProfile;
import com.wasset.ui.fragment.CategoriesFragment;
import com.wasset.utility.EndlessRecyclerViewScrollListener;
import com.wasset.utility.PreferenceEditor;
import com.wasset.utility.QuickstartPreferences;
import com.wasset.utility.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cn.refactor.lib.colordialog.PromptDialog.DIALOG_TYPE_SUCCESS;
import static com.facebook.FacebookSdk.getApplicationContext;
import static com.wasset.utility.QuickstartPreferences.UNAUTHORIZED_STATUS_CODE;
import static com.wasset.utility.Utils.ShowLoginRequiredDialog;
import static com.wasset.utility.Utils.isLogin;

public class CategoryPostsActivity extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    //    @BindView(R.id.tv_toolbar_title)
//    TextView tvToolbarTitle;
    @BindView(R.id.rv_products)
    RecyclerView rvProducts;
    @BindView(R.id.tv_error)
    TextView tvError;
    @BindView(R.id.btn_refresh)
    Button btnRefresh;
    @BindView(R.id.rl_no_internet_connection)
    RelativeLayout rlNoInternetConnection;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeLayout;
    @BindView(R.id.iv_empty_icon)
    ImageView ivEmptyIcon;
    @BindView(R.id.tv_empty_text)
    TextView tvEmptyText;
    @BindView(R.id.rl_empty_layout)
    LinearLayout rlEmptyLayout;
    private String userId = "";
    private String CategoryId, CategoryName;
    private Call<CategoryProducts> call;
    private Call<publisherProfile> userPoductsCall;
    private Call<FormalResponse> AddToFavCall;
    private List<PostDataBean> postDataList;
    private AdapterPostDetails adapterPostDetails;
    private int STARTING_PAGE_INDEX = 1;
    private EndlessRecyclerViewScrollListener endlessScrollListener;
    private boolean isThereMore = true;
    private AppCompatTextView textNotificationItemCount;
    private int mNotificationItemCount;
    private PreferenceEditor pf;
    boolean isFromUserProfile = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            CategoryId = getArguments().getString("Id");
            CategoryName = getArguments().getString("name");
            userId = getArguments().getString("userId");
            isFromUserProfile = getArguments().getBoolean("fromProfile");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_category_products, container, false);
        ButterKnife.bind(this, view);
        setToolbar();
        postDataList = new ArrayList<>();
        swipeLayout.setOnRefreshListener(this);
        try {
            if (userId == null || userId.isEmpty())
                getCategoryProducts(STARTING_PAGE_INDEX);
            else
                getUserCategoryPosts(userId, CategoryId, STARTING_PAGE_INDEX);
        } catch (Exception e) {
            e.printStackTrace();
        }
        setupList();
        return view;
    }


    private void getUserCategoryPosts(String userId, String categoryId, int page) {


        if (!Utils.isNetworkAvailable()) {
            ShowErrorLayout(R.string.no_internet_connection);
            return;
        }

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        swipeLayout.setRefreshing(true);

        userPoductsCall = apiService.getPublisherProfileWithSharedCategory(userId, CategoryId, page);
        userPoductsCall.enqueue(new Callback<publisherProfile>() {
            @Override
            public void onResponse(Call<publisherProfile> call, final Response<publisherProfile> response) {
                System.out.println("raw" + response.raw());

                swipeLayout.setRefreshing(false);
                try {
                    if (response.body().isStatus() && response.body().getCode() == QuickstartPreferences.OK_STATUS_CODE) {
                        if (response.body().getData() != null) {

                            if (response.body().getData().getPosts().size() > 0) {
                                showContent();
                                postDataList.addAll(response.body().getData().getPosts());
                                adapterPostDetails.notifyDataSetChanged();

                            } else {
                                isThereMore = false;
                                if (page == STARTING_PAGE_INDEX) {
                                    showEmpty();
                                }
                            }

                        } else {
                            ShowErrorLayout(R.string.err_msg_server);
                        }

                    } else if (response.body().getCode() == UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(getApplicationContext(), R.string.Unauthorized, Toast.LENGTH_LONG).show();
                        Utils.LunchLogin(getApplicationContext());
                    } else {
                        ShowErrorLayout(R.string.err_msg_server);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    ShowErrorLayout(R.string.err_msg_server);
                }
            }


            @Override
            public void onFailure(Call<publisherProfile> call, Throwable t) {
                if (!call.isCanceled() && !"Canceled".equals(t.getMessage())) {
                    swipeLayout.setRefreshing(false);
                    t.printStackTrace();
                    ShowErrorLayout(R.string.err_msg_server);
                }
            }
        });


    }

    public void setToolbar() {
        try {
            if (getActivity() != null && getArguments().getString("from").equals("publish")) {
                ((PublisherProfileActivity) getActivity()).setToolbar(CategoryName);
            } else
                ((HomeActivity) getActivity()).setToolbar(CategoryName);
        } catch (Exception e) {
            ((PublisherProfileActivity) getActivity()).setToolbar(CategoryName);
        }
    }

    private void setupList() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        adapterPostDetails = new AdapterPostDetails(postDataList, isFromUserProfile);
        rvProducts.setLayoutManager(linearLayoutManager);
        endlessScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, STARTING_PAGE_INDEX) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (Utils.isNetworkAvailable()) {
                    if (isThereMore && userId == null ||
                            isThereMore && userId.isEmpty()) {
                        getCategoryProducts(page);
                    } else
                        getUserCategoryPosts(userId, CategoryId, page);
                }
            }
        };

        rvProducts.addOnScrollListener(endlessScrollListener);
        rvProducts.setAdapter(adapterPostDetails);
        adapterPostDetails.setListener((itemView, position) -> {
            if (itemView.getId() == R.id.btn_fav) {
                AddToFav(postDataList.get(position));
            } else {
                Intent intent = new Intent(getActivity(), PostDetailsActivity.class);
                intent.putExtra("Id", postDataList.get(position).getId());
                intent.putExtra("SHOULD_SCROLL", false);
                startActivity(intent);
            }

        });
    }


    private void AddToFav(PostDataBean postDataBean) {

        if (!Utils.isNetworkAvailable()) {
            ShowErrorLayout(R.string.no_internet_connection);
            return;
        }

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        AddToFavCall = apiService.addToFavorites(postDataBean.getId());
        AddToFavCall.enqueue(new Callback<FormalResponse>() {
            @Override
            public void onResponse(Call<FormalResponse> call, final Response<FormalResponse> response) {
                try {
                    if (response.body() != null) {
                        FormalResponse formalResponse = response.body();
                        if (formalResponse.isStatus() && formalResponse.getCode() == QuickstartPreferences.OK_STATUS_CODE) {
                            //Toast.makeText(getApplicationContext(), formalResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            Utils.ShowDialog(getActivity(), getString(R.string.success), formalResponse.getMessage(), DIALOG_TYPE_SUCCESS);
                            postDataBean.setIs_favorite(formalResponse.isFavorite_status());

                        } else if (response.body().getCode() == QuickstartPreferences.UNAUTHORIZED_STATUS_CODE) {
                            Toast.makeText(getApplicationContext(), R.string.Unauthorized, Toast.LENGTH_LONG).show();
                            Utils.LunchLogin(getApplicationContext());

                        } else {
                            Utils.ShowServerDialog(getActivity());
                        }
                    } else {
                        Utils.ShowServerDialog(getActivity());
                    }

                } catch (Exception e) {
                    e.printStackTrace();

                    Utils.ShowServerDialog(getActivity());
                }
            }

            @Override
            public void onFailure(Call<FormalResponse> call, Throwable t) {

                t.printStackTrace();
                Utils.ShowServerDialog(getActivity());
            }
        });
    }

    private void getCategoryProducts(final int page) {


        if (!Utils.isNetworkAvailable()) {
            ShowErrorLayout(R.string.no_internet_connection);
            return;
        }

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        swipeLayout.setRefreshing(true);

        call = apiService.getCategoryPosts(CategoryId, page);
        call.enqueue(new Callback<CategoryProducts>() {
            @Override
            public void onResponse(Call<CategoryProducts> call, final Response<CategoryProducts> response) {
                System.out.println("raw" + response.raw());

                swipeLayout.setRefreshing(false);
                try {
                    if (response.body().isStatus() && response.body().getCode() == QuickstartPreferences.OK_STATUS_CODE) {
                        if (response.body().getData() != null) {

                            if (response.body().getData().size() > 0) {
                                showContent();
                                postDataList.addAll(response.body().getData());
                                adapterPostDetails.notifyDataSetChanged();

                            } else {
                                isThereMore = false;
                                if (page == STARTING_PAGE_INDEX) {
                                    showEmpty();
                                }
                            }

                        } else {
                            ShowErrorLayout(R.string.err_msg_server);
                        }

                    } else if (response.body().getCode() == UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(getApplicationContext(), R.string.Unauthorized, Toast.LENGTH_LONG).show();
                        Utils.LunchLogin(getApplicationContext());
                    } else {
                        ShowErrorLayout(R.string.err_msg_server);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    ShowErrorLayout(R.string.err_msg_server);
                }
            }


            @Override
            public void onFailure(Call<CategoryProducts> call, Throwable t) {
                if (!call.isCanceled() && !"Canceled".equals(t.getMessage())) {
                    swipeLayout.setRefreshing(false);
                    t.printStackTrace();
                    ShowErrorLayout(R.string.err_msg_server);
                }
            }
        });


    }

    private void showContent() {
        try {
            rvProducts.setVisibility(View.VISIBLE);
            rlNoInternetConnection.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showEmpty() {
        try {
            rvProducts.setVisibility(View.GONE);
            rlEmptyLayout.setVisibility(View.VISIBLE);
            rlNoInternetConnection.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void ShowErrorLayout(int txt) {
        try {
            rvProducts.setVisibility(View.GONE);
            rlNoInternetConnection.setVisibility(View.VISIBLE);
            tvError.setText(txt);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btn_refresh)
    public void onViewClicked() {
        Refresh();
    }

    private void Refresh() {
        isThereMore = true;
        postDataList.clear();
        adapterPostDetails.notifyDataSetChanged();
        endlessScrollListener.resetState();
        if (userId == null || userId.isEmpty())
            getCategoryProducts(STARTING_PAGE_INDEX);
        else
            getUserCategoryPosts(userId, CategoryId, STARTING_PAGE_INDEX);

    }

    public static CategoryPostsActivity newInstance(String id, String name,String from) {
        CategoryPostsActivity fragment = new CategoryPostsActivity();
        Bundle bundle = new Bundle();
        bundle.putString("Id", id);
        bundle.putString("name", name);
        bundle.putString("from", from);
        return fragment;
    }

    public static CategoryPostsActivity newInstance(String id, String name, String userId, Boolean fromProfile) {
        CategoryPostsActivity fragment = new CategoryPostsActivity();
        Bundle bundle = new Bundle();
        bundle.putString("Id", id);
        bundle.putString("name", name);
        bundle.putString("userId", userId);
        bundle.putBoolean("fromProfile", fromProfile);
        fragment.setArguments(bundle);
        return fragment;
    }
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.home_menu, menu);
//        final MenuItem menuItem = menu.findItem(R.id.item_notifications);
//        View actionView = MenuItemCompat.getActionView(menuItem);
//        textNotificationItemCount = (AppCompatTextView) actionView.findViewById(R.id.cart_badge);
//        setupBadge();
//        actionView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onOptionsItemSelected(menuItem);
//            }
//        });
//        return true;
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.item_search:
//                Intent i = new Intent(getApplicationContext(), SearchActivity.class);
//                startActivity(i);
//                break;
//            case R.id.item_notifications:
//
//                if (!isLogin()) {
//                    ShowLoginRequiredDialog(CategoryPostsActivity.this);
//                    break;
//                }
//
//                pf.setIntegerPreference(QuickstartPreferences.BADGE_COUNT, 0);
//                mNotificationItemCount = 0;
//                setupBadge();
//                Intent ii = new Intent(getApplicationContext(), NotificationsActivity.class);
//                startActivity(ii);
//                break;
//            case R.id.item_add:
//
//                if (!isLogin()) {
//                    ShowLoginRequiredDialog(CategoryPostsActivity.this);
//                    break;
//                }
//
//                Intent iii = new Intent(getApplicationContext(), AddPostActivity.class);
//                startActivity(iii);
//                break;
//        }
//        return super.onOptionsItemSelected(item);
//    }

    private void setupBadge() {

        if (textNotificationItemCount != null) {
            if (mNotificationItemCount == 0) {
                if (textNotificationItemCount.getVisibility() != View.GONE) {
                    textNotificationItemCount.setVisibility(View.GONE);
                }
            } else {

                //    Utils.setBadge(getApplicationContext(),Math.min(mNotificationItemCount, 99));

                textNotificationItemCount.setText(String.valueOf(Math.min(mNotificationItemCount, 99)));
                if (textNotificationItemCount.getVisibility() != View.VISIBLE) {
                    textNotificationItemCount.setVisibility(View.VISIBLE);
                }
            }
        }
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        pf = PreferenceEditor.getInstance();
//        mNotificationItemCount = pf.getIntegerPreference(QuickstartPreferences.BADGE_COUNT);
//        //Utils.setBadge(getApplicationContext(),mNotificationItemCount);
//    }


    @Override
    public void onRefresh() {
        Refresh();
    }
}
