package com.wasset.ui;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.wasset.R;
import com.wasset.interfaces.ApiClient;
import com.wasset.interfaces.ApiInterface;
import com.wasset.models.Login;
import com.wasset.utility.PreferenceEditor;
import com.wasset.utility.QuickstartPreferences;
import com.wasset.utility.Utils;
import com.wasset.views.AVLoadingIndicatorDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cn.refactor.lib.colordialog.PromptDialog.DIALOG_TYPE_WRONG;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.et_username)
    EditText etUserName;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.tv_forget_password)
    TextView tvForgetPassword;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.btn_guest)
    Button btnGuest;
    @BindView(R.id.btn_register)
    LinearLayout btnRegister;
    @BindView(R.id.btn_twitter_login)
    ImageView btnTwitterLogin;
    //    TwitterLoginButton btnTwitterLogin;
    @BindView(R.id.btn_fb_login)
    ImageView btnFbLogin;
    //    LoginButton btnFbLogin;
    private AVLoadingIndicatorDialog indicatorView;
    private Call<Login> call;
    private PreferenceEditor pf;
    private TwitterAuthClient twitterAuthClient;
    private CallbackManager callbackManager;
    //private AccessToken accessToken;
    private Call<Login> CheckUserCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        pf = PreferenceEditor.getInstance();



        /*try {
            PackageInfo info = getPackageManager().getPackageInfo("com.wasset",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                System.out.println("KeyHash:"+ Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }*/

        twitterAuthClient = new TwitterAuthClient();
        callbackManager = CallbackManager.Factory.create();
        //accessToken = AccessToken.getCurrentAccessToken();
        //boolean isLoggedIn = accessToken != null && !accessToken.isExpired();


        btnFbLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        // App code
                        System.out.println("registerCallback onSuccess");

                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        Log.v("LoginActivity", response.toString());

                                        // Application code
                                        try {
                                            String email = object.getString("email");
                                            String UserName = object.getString("name");

                                            System.out.println("email " + email);
                                            System.out.println("name " + UserName);
                                            requestCheckUser(email, UserName);
                                            // requestLoginByFB(email,name, loginResult.getAccessToken());


                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });

                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "name,email");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        System.out.println("registerCallback onCancel");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        System.out.println("registerCallback Exception :" + exception.getMessage());
                        exception.printStackTrace();
                        // App code
                    }
                });

                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("email", "public_profile"));
            }
        });

        btnTwitterLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                twitterAuthClient.authorize(LoginActivity.this, new com.twitter.sdk.android.core.Callback<TwitterSession>() {
                    @Override
                    public void success(Result<TwitterSession> result) {
                        // Do something with result, which provides a TwitterSession for making API calls

                        final String UserName = result.data.getUserName();
                        System.out.println("Twitter UserName" + UserName);
                        final TwitterSession twitterSession = result.data;

                        twitterAuthClient.requestEmail(twitterSession, new com.twitter.sdk.android.core.Callback<String>() {
                            @Override
                            public void success(Result<String> emailResult) {
                                String email = emailResult.data;

                                if (email != null && email.length() > 0) {
                                    requestCheckUser(email, UserName);
                                    //requestLoginByTwitter(email, session.getAuthToken());
                                } else {
                                    Toast.makeText(LoginActivity.this, R.string.twitter_request_email_failed, Toast.LENGTH_SHORT).show();

                                }

                            }

                            @Override
                            public void failure(TwitterException e) {
                                System.out.println("TwitterException" + e.getMessage());
                                e.printStackTrace();
                                Toast.makeText(LoginActivity.this, R.string.twitter_request_email_failed, Toast.LENGTH_SHORT).show();
                                // callback.onTwitterSignInFailed(e);
                            }
                        });


                    }

                    @Override
                    public void failure(TwitterException exception) {
                        // Do something on failure
                        Toast.makeText(LoginActivity.this, R.string.twitter_session_token_experid, Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });
    }

    @OnClick({R.id.tv_forget_password, R.id.btn_login, R.id.btn_guest, R.id.btn_register})
    public void onViewClicked(View view) {

        switch (view.getId()) {
            case R.id.tv_forget_password:
                startActivity(new Intent(getApplicationContext(), ForgetPasswordActivity.class));
                break;
            case R.id.btn_login:
                checkForm();
                break;
            case R.id.btn_guest:
                goToHomeActivity();
                finish();
                break;
            case R.id.btn_register:
                Intent i = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(i);
                break;
        }
    }

    /*
        private void requestLoginByFB(final String fb_email,final String fb_name, AccessToken authToken) {


            if (!Utils.isNetworkAvailable()) {
                Utils.ShowNoInternetConnectionDialog(LoginActivity.this);
                return;
            }

            showProgress();
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            Map<String, RequestBody> map = new HashMap<>();
            final RequestBody email = RequestBody.create(MediaType.parse("text/plain"), fb_email);
            RequestBody token = RequestBody.create(MediaType.parse("text/plain"), authToken.getToken());
            //RequestBody twitter_secret = RequestBody.create(MediaType.parse("text/plain"), authToken.get);
            RequestBody fcm = RequestBody.create(MediaType.parse("text/plain"), FirebaseInstanceId.getInstance().getToken());
            map.put("email", email);
            map.put("token", token);
           // map.put("secret_token", twitter_secret);
            map.put("FCM_token", fcm);

            TwitterLoginCall = apiService.loginByTwitter(map);
            TwitterLoginCall.enqueue(new Callback<Login>() {
                @Override
                public void onResponse(Call<Login> call, Response<Login> response) {

                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());
                    hideProgress();

                    if (response.body().isStatus() && response.code() == 200) {

                        pf.setStringPreference(QuickstartPreferences.USER_INFO, new Gson().toJson(response.body().getData()));
                        goToHomeActivity();
                        finish();

                    } else if (!response.body().isStatus() && response.code() == 200) {

                        Intent i = new Intent(getApplicationContext(), RegisterActivity.class);
                        i.putExtra("fb_name", fb_name);
                        i.putExtra("email", fb_email);
                        startActivity(i);

                    } else {
                        Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();

                    }

                }


                @Override
                public void onFailure(Call<Login> call, Throwable t) {
                    if (!call.isCanceled() && !"Canceled".equals(t.getMessage())) {
                        t.printStackTrace();
                        hideProgress();
                        System.out.println("onFailure");
                        Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();

                    }
                }
            });

        }

        private void requestLoginByTwitter(final String Twitter_email, TwitterAuthToken authToken) {


            if (!Utils.isNetworkAvailable()) {
                Utils.ShowNoInternetConnectionDialog(LoginActivity.this);
                return;
            }

            showProgress();
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            Map<String, RequestBody> map = new HashMap<>();
            final RequestBody email = RequestBody.create(MediaType.parse("text/plain"), Twitter_email);
            RequestBody twitter_token = RequestBody.create(MediaType.parse("text/plain"), authToken.token);
            RequestBody twitter_secret = RequestBody.create(MediaType.parse("text/plain"), authToken.secret);
            RequestBody fcm = RequestBody.create(MediaType.parse("text/plain"), FirebaseInstanceId.getInstance().getToken());
            map.put("email", email);
            map.put("token", twitter_token);
            map.put("secret_token", twitter_secret);
            map.put("FCM_token", fcm);

            TwitterLoginCall = apiService.loginByTwitter(map);
            TwitterLoginCall.enqueue(new Callback<Login>() {
                @Override
                public void onResponse(Call<Login> call, Response<Login> response) {

                    System.out.println("Code" + response.code());
                    System.out.println("raw" + response.raw());
                    System.out.println("Code" + response.message());
                    hideProgress();

                    if (response.body().isStatus() && response.code() == 200) {

                        pf.setStringPreference(QuickstartPreferences.USER_INFO, new Gson().toJson(response.body().getData()));
                        goToHomeActivity();
                        finish();

                    } else if (!response.body().isStatus() && response.code() == 200) {

                        Intent i = new Intent(getApplicationContext(), RegisterActivity.class);
                        i.putExtra("username", session.getUserName());
                        i.putExtra("email", Twitter_email);
                        startActivity(i);

                    } else {
                        Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();

                    }

                }


                @Override
                public void onFailure(Call<Login> call, Throwable t) {
                    if (!call.isCanceled() && !"Canceled".equals(t.getMessage())) {
                        t.printStackTrace();
                        hideProgress();
                        System.out.println("onFailure");
                        Toast.makeText(getApplicationContext(), R.string.err_msg_server, Toast.LENGTH_SHORT).show();

                    }
                }
            });

        }

    */
    private void requestCheckUser(final String email, final String UserName) {


        if (!Utils.isNetworkAvailable()) {
            Utils.ShowNoInternetConnectionDialog(LoginActivity.this);
            return;
        }

        String token = FirebaseInstanceId.getInstance().getToken();
        String ANDROID_ID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        String device_type = "android";

        showProgress();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        CheckUserCall = apiService.CheckUser(email, ANDROID_ID, device_type, token);
        CheckUserCall.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(@NonNull Call<Login> call, @NonNull Response<Login> response) {
                hideProgress();
                try {
                    if (response.body().isStatus() && response.body().getCode() == 200) {
                        pf.setStringPreference(QuickstartPreferences.USER_INFO, new Gson().toJson(response.body().getData()));
                        goToHomeActivity();
                        finish();

                    } else if (response.body().isStatus() && response.body().getCode() == 422) {

                        Intent i = new Intent(getApplicationContext(), RegisterActivity.class);
                        i.putExtra("username", UserName);
                        i.putExtra("email", email);
                        startActivity(i);

                    } else {
                        Utils.ShowServerDialog(LoginActivity.this);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.ShowServerDialog(LoginActivity.this);
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                if (!call.isCanceled() && !"Canceled".equals(t.getMessage())) {
                    t.printStackTrace();
                    hideProgress();
                    System.out.println("onFailure");
                    Utils.ShowServerDialog(LoginActivity.this);
                }
            }
        });

    }


    private void checkForm() {
        String username = etUserName.getText().toString().trim();
        String password = etPassword.getText().toString().trim();

        if (username.isEmpty()) {
            Utils.ShowDialog(LoginActivity.this, getString(R.string.err), getString(R.string.err_msg_username), DIALOG_TYPE_WRONG);
            return;
        }

        if (password.isEmpty()) {
            Utils.ShowDialog(LoginActivity.this, getString(R.string.err), getString(R.string.err_msg_password), DIALOG_TYPE_WRONG);
            return;
        }

        requestLogin();

    }

    private void requestLogin() {
        String username = etUserName.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        String token = FirebaseInstanceId.getInstance().getToken();
        String ANDROID_ID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        String device_type = "android";


        if (!Utils.isNetworkAvailable()) {
            Utils.ShowNoInternetConnectionDialog(LoginActivity.this);
            return;
        }

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Utils.HideKeyBord(LoginActivity.this);
        showProgress();
        call = apiService.login(username, password, ANDROID_ID, device_type, token);
        call.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, final Response<Login> response) {

                hideProgress();
                try {
                    Login login = response.body();
                    if (login.isStatus() && login.getCode() == QuickstartPreferences.OK_STATUS_CODE) {
                        Toast.makeText(LoginActivity.this, login.getMessage(), Toast.LENGTH_SHORT).show();
                        pf.setStringPreference(QuickstartPreferences.USER_INFO, new Gson().toJson(login.getData()));
                        goToHomeActivity();
                        finish();

                    } else {
                        Utils.ShowDialog(LoginActivity.this, getString(R.string.err), login.getMessage(), DIALOG_TYPE_WRONG);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.ShowServerDialog(LoginActivity.this);
                }
            }


            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                if (!call.isCanceled() && !"Canceled".equals(t.getMessage())) {
                    hideProgress();
                    t.printStackTrace();
                    Utils.ShowServerDialog(LoginActivity.this);
                }
            }
        });
    }

    private void goToHomeActivity() {
        Intent i = new Intent(getApplicationContext(), HomeActivity.class);
        finish();
        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(i);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }


    private void hideProgress() {
        if (indicatorView != null && indicatorView.isShowing()) {
            indicatorView.dismiss();
        }
    }

    private void showProgress() {

        if (indicatorView == null) {
            indicatorView = new AVLoadingIndicatorDialog(LoginActivity.this);
            //indicatorView.setMessage(msg);
            indicatorView.setCancelable(false);
        }
        indicatorView.show();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (call != null) {
            call.cancel();
        }

        if (CheckUserCall != null) {
            CheckUserCall.cancel();
        }

        hideProgress();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println("onActivityResult : " + "requestCode:" + requestCode + "resultCode:" + resultCode);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        twitterAuthClient.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
