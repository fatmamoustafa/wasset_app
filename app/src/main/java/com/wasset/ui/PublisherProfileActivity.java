package com.wasset.ui;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import android.net.Uri;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.joooonho.SelectableRoundedImageView;
import com.wasset.R;
import com.wasset.adapter.AdapterCategoryTitle;
import com.wasset.adapter.AdapterPostDetails;
import com.wasset.databinding.ActivityPublisherProfileBinding;
import com.wasset.interfaces.ApiClient;
import com.wasset.interfaces.ApiInterface;
import com.wasset.interfaces.OnItemClickListener;
import com.wasset.models.ExpandableHolder;
import com.wasset.models.Home;
import com.wasset.models.PostDataBean;
import com.wasset.models.UserPosts;
import com.wasset.models.UserPostsCategories;
import com.wasset.models.publisherProfile;
import com.wasset.utility.EndlessRecyclerViewScrollListener;
import com.wasset.utility.QuickstartPreferences;
import com.wasset.utility.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.wasset.utility.QuickstartPreferences.UNAUTHORIZED_STATUS_CODE;

public class PublisherProfileActivity extends BaseActivity {

    @BindView(R.id.tv_toolbar_title)
    TextView tvToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

//    private String UserId;
//    private Call<publisherProfile> call;
//    private MyHandlers handlers;
//    private ActivityPublisherProfileBinding binding;
//    private publisherProfile.DataBean userDetailsData;
//    private EndlessRecyclerViewScrollListener endlessScrollListener;
//    String userUrl;
//    private boolean isThereMore = true;
//    boolean isFromDeepLink = false;
//

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        binding = DataBindingUtil.setContentView(this, R.layout.activity_publisher_profile);
//        handlers = new MyHandlers(this);

        setContentView(R.layout.activity_publisher_profile);

        ButterKnife.bind(this);
        setToolbar(getString(R.string.publisher_details));
        onNewIntent(getIntent());
//        swipeLayout.setOnRefreshListener(this);
//
//        try {
//
//            UserId = (String) getIntent().getExtras().get("Id");
//            if (UserId != null && !UserId.isEmpty()) {
//                getUserDetails();
//            } else {
//                handleIntent(getIntent());
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        shareProfileImageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String link = "http://hwaseet.com.sa/user/profile/" + UserId;
//                share(link);
//            }
//        });

    }


//    private void getUserDetails() {
//
//        if (!Utils.isNetworkAvailable()) {
//            ShowErrorLayout(R.string.no_internet_connection);
//            return;
//        }
//        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
//        swipeLayout.setRefreshing(true);
//        call = apiService.getPublisherProfile(UserId);
//        call.enqueue(new Callback<publisherProfile>() {
//            @Override
//            public void onResponse(Call<publisherProfile> call, final Response<publisherProfile> response) {
//                System.out.println("raw" + response.raw());
//
//                swipeLayout.setRefreshing(false);
//
//
//                try {
//                    if (response.body().isStatus() && response.body().getCode() == QuickstartPreferences.OK_STATUS_CODE) {
//                        if (response.body().getData() != null) {
//                            showContent();
//
//                            userDetailsData = response.body().getData();
//                            binding.setUserProfile(userDetailsData);
//                            binding.setHandlersListener(handlers);
//                            if (userDetailsData.getUser_info().getUser_special()) {
//                                verifiedBadge.setVisibility(View.VISIBLE);
//                                shareProfileImageView.setVisibility(View.VISIBLE);
//                                ivPublisherName.setText(userDetailsData.getUser_info().getUsername());
//                                iv_publisher_desc.setVisibility(View.VISIBLE);
//                            } else {
//                                verifiedBadge.setVisibility(View.GONE);
//                                shareProfileImageView.setVisibility(View.GONE);
//                                ivPublisherName.setText(userDetailsData.getUser_info().getUsername());
//                                iv_publisher_desc.setVisibility(View.GONE);
//                            }
//                            //setUpViewPager(response.body().getData().getPhotos());
//                            if (response.body().getData().getUser_categories() != null) {
//                                userUrl = response.body().getData().getUser_url();
//                                setupPublisherPostsList(response.body().getData().getUser_categories());
//                            } else if (response.body().getData().getPosts() != null) {
//                                setupNonVerifiedPublisherPostsList(response.body().getData().getPosts(), true);
//                            } else {
//                                isThereMore = false;
//
//                            }
//                            //SetupPostAttribute(response.body().getData().getAttributes());
//
//                        } else {
//                            ShowErrorLayout(R.string.err_msg_server);
//                        }
//
//                    } else if (response.body().getCode() == UNAUTHORIZED_STATUS_CODE) {
//                        Toast.makeText(getApplicationContext(), R.string.Unauthorized, Toast.LENGTH_LONG).show();
//                        Utils.LunchLogin(getApplicationContext());
//                    } else {
//                        ShowErrorLayout(R.string.err_msg_server);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    ShowErrorLayout(R.string.err_msg_server);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<publisherProfile> call, Throwable t) {
//                if (!call.isCanceled() && !"Canceled".equals(t.getMessage())) {
//
//                    swipeLayout.setRefreshing(false);
//                    t.printStackTrace();
//                    ShowErrorLayout(R.string.err_msg_server);
//                }
//            }
//        });
//
//    }

    LinearLayoutManager linearLayoutManager;
    AdapterPostDetails adapterPostDetails;

//    private void setupNonVerifiedPublisherPostsList(List<PostDataBean> posts, boolean shouldClearPreviousPosts) {
//        if (linearLayoutManager == null)
//            linearLayoutManager = new LinearLayoutManager(PublisherProfileActivity.this);
//        if (endlessScrollListener == null)
//            endlessScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, STARTING_PAGE_INDEX) {
//                @Override
//                public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
//                    if (Utils.isNetworkAvailable() && isThereMore) {
//                        getUserDetailsWithSharedCategory(UserId, categoryId, page);
//                    }
//                }
//            };
//
//        if (adapterPostDetails == null) {
//            adapterPostDetails = new AdapterPostDetails(posts, true);
//            rvPublisherPosts.setLayoutManager(linearLayoutManager);
//            rvPublisherPosts.addOnScrollListener(endlessScrollListener);
//            rvPublisherPosts.setAdapter(adapterPostDetails);
//            adapterPostDetails.setListener(new OnItemClickListener() {
//                @Override
//                public void onItemClick(View itemView, int position) {
//                    String PostId = posts.get(position).getId();
//                    Intent i = new Intent(PublisherProfileActivity.this, CategoryPostsActivity.class);
////                    Intent i = new Intent(getApplicationContext(), PostDetailsActivity.class);
//                    i.putExtra("Id", PostId);
//                    startActivity(i);
//                }
//            });
//        } else {
//
//            adapterPostDetails.addNewPosts(posts, shouldClearPreviousPosts);
//            adapterPostDetails.notifyDataSetChanged();
//        }
//
//
//    }


//    private void getUserDetailsWithSharedCategory(String userId, String categoryId, int page) {
//
//        if (!Utils.isNetworkAvailable()) {
//            ShowErrorLayout(R.string.no_internet_connection);
//            return;
//        }
//
//        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
//        swipeLayout.setRefreshing(true);
//
//        call = apiService.getPublisherProfileWithSharedCategory(userId, categoryId, page);
//
//        call.enqueue(new Callback<publisherProfile>() {
//            @Override
//            public void onResponse(Call<publisherProfile> call, final Response<publisherProfile> response) {
//                swipeLayout.setRefreshing(false);
//                try {
//                    if (response.body().isStatus() && response.body().getCode() == QuickstartPreferences.OK_STATUS_CODE) {
//                        if (response.body().getData() != null) {
//                            if (response.body().getData().getPosts().size() > 0) {
//                                showContent();
//
//                                userDetailsData = response.body().getData();
//                                binding.setUserProfile(userDetailsData);
//                                binding.setHandlersListener(handlers);
//                                if (userDetailsData.getUser_info().getUser_special()) {
//                                    verifiedBadge.setImageDrawable(ContextCompat.getDrawable(PublisherProfileActivity.this, R.drawable.verified_badge));
//                                    verifiedBadge.setVisibility(View.VISIBLE);
//                                    shareProfileImageView.setVisibility(View.VISIBLE);
//                                    ivPublisherName.setText(userDetailsData.getUser_info().getUsername() + " أحد الداعمين بمجهوداته لتطبيق وسيط");
//                                    if (response.body().getData().getPosts() != null)
//                                        setupNonVerifiedPublisherPostsList(response.body().getData().getPosts(), false);
//                                } else {
//                                    verifiedBadge.setVisibility(View.GONE);
//                                    shareProfileImageView.setVisibility(View.GONE);
//                                    ivPublisherName.setText(userDetailsData.getUser_info().getUsername());
//                                    if (response.body().getData().getPosts() != null)
//                                        setupNonVerifiedPublisherPostsList(response.body().getData().getPosts(), false);
//                                }
//                            } else
//                                isThereMore = false;
//
//                        } else {
//                            ShowErrorLayout(R.string.err_msg_server);
//                        }
//
//                    } else if (response.body().getCode() == UNAUTHORIZED_STATUS_CODE) {
//                        Toast.makeText(PublisherProfileActivity.this, R.string.Unauthorized, Toast.LENGTH_LONG).show();
//                        Utils.LunchLogin(PublisherProfileActivity.this);
//                    } else {
//                        ShowErrorLayout(R.string.err_msg_server);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    swipeLayout.setRefreshing(false);
//                    ShowErrorLayout(R.string.err_msg_server);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<publisherProfile> call, Throwable t) {
//                if (!call.isCanceled() && !"Canceled".equals(t.getMessage())) {
//
//                    swipeLayout.setRefreshing(false);
//                    t.printStackTrace();
//                    ShowErrorLayout(R.string.err_msg_server);
//                }
//            }
//        });
//
//    }


//    private void showContent() {
//        try {
//            content.setVisibility(View.VISIBLE);
//            rlNoInternetConnection.setVisibility(View.GONE);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

//    private void ShowErrorLayout(int txt) {
//        try {
//            content.setVisibility(View.GONE);
//            rlNoInternetConnection.setVisibility(View.VISIBLE);
//            tvError.setText(txt);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }


//    private void setupPublisherPostsList(final List<Home.DataBean> postDataBeanList) {
//        ArrayList<UserPostsCategories> userPostsCategories = new ArrayList<>();
//        for (int i = 0; i < postDataBeanList.size(); i++) {
//            ArrayList<UserPosts> userPosts = new ArrayList<>();
//            for (int p = 0; p < postDataBeanList.get(i).getPosts().size(); p++) {
//                PostDataBean postDataBean = postDataBeanList.get(i).getPosts().get(p);
//                UserPosts userPost = new UserPosts();
//                userPost.setAdded_date(postDataBean.getAdded_date());
//                userPost.setAttributes(postDataBean.getAttributes());
//                userPost.setCategory_id(postDataBean.getCategory_id());
//                userPost.setCity(postDataBean.getLocation());
//                userPost.setComments_count(postDataBean.getComments_count());
//                userPost.setDetails(postDataBean.getDetails());
//                userPost.setId(postDataBean.getId());
//                userPost.setIs_favorite(postDataBean.getIs_favorite());
//                userPost.setMobile_number(postDataBean.getMobile_number());
//                userPost.setPhoto(postDataBean.getPhoto());
//                userPost.setPhotos(postDataBean.getPhotos());
//                userPost.setPost_url(postDataBean.getPost_url());
//                userPost.setPrice(postDataBean.getPrice());
//                userPost.setRelated(postDataBean.getRelated());
//                userPost.setTitle(postDataBean.getTitle());
//                userPost.setUser(postDataBean.getUser());
//                userPost.setUser_id(postDataBean.getUser_id());
//                userPost.setViews_count(postDataBean.getViews_count());
//                userPosts.add(userPost);
//            }
//            ArrayList<ExpandableHolder> expandableHolders = new ArrayList<>();
//            ExpandableHolder expandableHolder = new ExpandableHolder();
//            expandableHolder.setUserPosts(userPosts);
//            expandableHolders.add(expandableHolder);
//            UserPostsCategories userPostsCategory = new UserPostsCategories(postDataBeanList.get(i).getName(), expandableHolders);
//            userPostsCategory.setCategory_id(postDataBeanList.get(i).getCategory_id());
//            userPostsCategory.setUserUrl(userUrl);
//            userPostsCategories.add(userPostsCategory);
//        }
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PublisherProfileActivity.this);
//        AdapterCategoryTitle adapterPostDetails = new AdapterCategoryTitle(userPostsCategories, userDetailsData.getUser_info().getId());
//
//        rvPublisherPosts.setLayoutManager(linearLayoutManager);
//        rvPublisherPosts.setAdapter(adapterPostDetails);
//        // rvPublisherPosts.setNestedScrollingEnabled(false);
//
//        adapterPostDetails.setOnItemClickListener((itemView, position) -> {
//            Home.DataBean item = postDataBeanList.get(position);
//            if (itemView.getId() == R.id.tv_category_name) {
//                try {
//                    getSupportFragmentManager().beginTransaction().replace(R.id.pu_nav_host_fragment, CategoryPostsActivity.newInstance(item.getCategory_id(), item.getName(), userDetailsData.getUser_info().getId(), true)).
//                            setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out).commit();
//                } catch (Exception e) {
//
//                }
////                Intent i = new Intent(PublisherProfileActivity.this, CategoryPostsActivity.class);
////                i.putExtra("Id", item.getCategory_id());
////                i.putExtra("name", item.getName());
////                i.putExtra("fromProfile", true);
////                i.putExtra("userId", userDetailsData.getUser_info().getId());
////                startActivity(i);
//            } else if (itemView.getId() == R.id.iv_share) {
//                if (userPostsCategories.get(position) != null) {
//                    String link = userPostsCategories.get(position).getUserUrl() + "/" + userPostsCategories.get(position).getCategory_id();
//                    share(link);
//                }
//            }
//        });
//    }

    void share(String link) {
        if (link != null && !link.isEmpty()) {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
            i.putExtra(Intent.EXTRA_TEXT, link);
            startActivity(Intent.createChooser(i, getString(R.string.share)));
        } else {
            Toast.makeText(PublisherProfileActivity.this, R.string.invalid_post_link, Toast.LENGTH_LONG).show();
        }
    }

    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        getSupportFragmentManager().beginTransaction().replace(R.id.pu_nav_host_fragment, PublisherProfileFragment.newInstance(intent,"publish")).
                setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out).commit();
//        handleIntent(intent);
    }

    String categoryId = "";

//    private void handleIntent(Intent intent) {
//        if (intent != null) {
//            isFromDeepLink = true;
//            String appLinkAction = intent.getAction();
//            Uri appLinkData = intent.getData();
//            if (Intent.ACTION_VIEW.equals(appLinkAction) && appLinkData != null) {
//                List<String> segments = appLinkData.getPathSegments();
//                if (segments.size() == 4) {
//
//                    UserId = segments.get(segments.size() - 2);
//                    categoryId = segments.get(segments.size() - 1);
//                    getUserDetailsWithSharedCategory(segments.get(segments.size() - 2), segments.get(segments.size() - 1), STARTING_PAGE_INDEX);
//                } else {
//                    UserId = segments.get(segments.size() - 1);
//                    getUserDetails();
//
//                }
//            }
//        }
//    }


    public void setToolbar(String string) {
        try {
            tvToolbarTitle.setText(string);
            setSupportActionBar(toolbar);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        } catch (Exception e) {

        }

    }

//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        finish();
//    }

//    @Override
//    public void onRefresh() {
//        isThereMore = true;
//
//        if (endlessScrollListener != null)
//            endlessScrollListener.resetState();
//        if (!isFromDeepLink) {
//            getUserDetails();
//        } else {
//            handleIntent(getIntent());
//        }
//    }


    public class MyHandlers {

        Context context;

        MyHandlers(Context context) {
            this.context = context;
        }

        public void onFollowClicked(View view, publisherProfile.DataBean profile) {

        }


        public void onChatClicked(View view, publisherProfile.DataBean profile) {

        }

    }


}
