package com.wasset.ui;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.wasset.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ahmed Adel on 30/05/2019.
 * email : a3adel@hotmail.com
 */
public class ReportDialogFragment extends DialogFragment {
    @BindView(R.id.reason_et)
    EditText reasonEditText;
    @BindView(R.id.send_button)
    Button sendButton;
    OnSendClickListener onSendClickListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void setOnSendClickListener(OnSendClickListener onSendClickListener) {
        this.onSendClickListener = onSendClickListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_report, container, false);
        ButterKnife.bind(this, view);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (reasonEditText.getText().toString().length() == 0)
                    Toast.makeText(getContext(), "برجاء ادخال سبب الإبلاغ", Toast.LENGTH_SHORT).show();
                else {
                    onSendClickListener.onSendClicked(reasonEditText.getText().toString());
                    dismiss();
                }
            }
        });
        return view;
    }

    interface OnSendClickListener {
        void onSendClicked(String reason);
    }
}
