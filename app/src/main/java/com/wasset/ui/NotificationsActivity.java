package com.wasset.ui;

import android.content.Intent;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wasset.R;
import com.wasset.adapter.AdapterNotifications;
import com.wasset.interfaces.ApiClient;
import com.wasset.interfaces.ApiInterface;
import com.wasset.interfaces.OnItemClickListener;
import com.wasset.models.Notifications;
import com.wasset.utility.EndlessRecyclerViewScrollListener;
import com.wasset.utility.QuickstartPreferences;
import com.wasset.utility.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.wasset.utility.QuickstartPreferences.UNAUTHORIZED_STATUS_CODE;

public class NotificationsActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.tv_toolbar_title)
    AppCompatTextView tvToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rv_notifications)
    RecyclerView rvNotifications;
    @BindView(R.id.swipe_layout)
    SwipeRefreshLayout swipeLayout;
    @BindView(R.id.tv_error)
    TextView tvError;
    @BindView(R.id.btn_refresh)
    Button btnRefresh;
    @BindView(R.id.rl_no_internet_connection)
    RelativeLayout rlNoInternetConnection;
    @BindView(R.id.iv_empty_icon)
    ImageView ivEmptyIcon;
    @BindView(R.id.tv_empty_text)
    TextView tvEmptyText;
    @BindView(R.id.rl_empty_layout)
    LinearLayout rlEmptyLayout;
    private List<Notifications.DataBean> notificationsList;
    private AdapterNotifications adapterNotifications;
    private Call<Notifications> NotificationCall;
    private int STARTING_PAGE_INDEX = 1;
    private EndlessRecyclerViewScrollListener endlessScrollListener;
    private boolean isThereMore = true;
    private boolean isLoad = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        ButterKnife.bind(this);
        setToolbar();
        notificationsList = new ArrayList<>();
        swipeLayout.setOnRefreshListener(this);
        setNotificationsList();
        getNotifications(STARTING_PAGE_INDEX);

    }

    private void setNotificationsList() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        adapterNotifications = new AdapterNotifications(getApplicationContext(), notificationsList);
        rvNotifications.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvNotifications.getContext(), linearLayoutManager.getOrientation());
        rvNotifications.addItemDecoration(dividerItemDecoration);
        rvNotifications.setAdapter(adapterNotifications);
        adapterNotifications.setListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {

                Notifications.DataBean item = notificationsList.get(position);
                String type = item.getType();
                String postId = item.getPost_id();

                Intent intent = new Intent(NotificationsActivity.this, PostDetailsActivity.class);
                intent.putExtra("Id", postId);
                if (type.equalsIgnoreCase("comment"))
                    intent.putExtra("SHOULD_SCROLL", true);
                startActivity(intent);

            }
        });

        endlessScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, STARTING_PAGE_INDEX) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {

                System.out.println("page" + page);
                if (Utils.isNetworkAvailable()) {
                    if (isThereMore) {
                        System.out.println("Page" + page);
                        getNotifications(page);
                    }
                }
            }
        };

        rvNotifications.addOnScrollListener(endlessScrollListener);

    }

    private void getNotifications(final int page) {


        if (!Utils.isNetworkAvailable()) {
            ShowErrorLayout(R.string.no_internet_connection);
            return;
        }

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        if (swipeLayout != null) {
            swipeLayout.setRefreshing(true);
        }

        NotificationCall = apiService.getNotifications(page);
        NotificationCall.enqueue(new Callback<Notifications>() {
            @Override
            public void onResponse(Call<Notifications> call, final Response<Notifications> response) {
                System.out.println("raw" + response.raw());
                if (swipeLayout != null) {
                    swipeLayout.setRefreshing(false);
                }
                try {
                    if (response.body().isStatus() && response.body().getCode() == QuickstartPreferences.OK_STATUS_CODE) {
                        if (response.body() != null) {
                            if (response.body().getData().size() > 0) {
                                notificationsList.addAll(response.body().getData());
                                adapterNotifications.notifyDataSetChanged();
                                showContent();
                            } else {
                                isThereMore = false;
                                if (page == STARTING_PAGE_INDEX) {
                                    showEmpty();
                                }
                            }
                        } else {
                            ShowErrorLayout(R.string.err_msg_server);
                        }

                    } else if (response.body().getCode() == UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(getApplicationContext(), R.string.Unauthorized, Toast.LENGTH_LONG).show();
                        Utils.LunchLogin(getApplicationContext());
                    } else {
                        ShowErrorLayout(R.string.err_msg_server);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    ShowErrorLayout(R.string.err_msg_server);
                }
            }

            @Override
            public void onFailure(Call<Notifications> call, Throwable t) {
                if (!call.isCanceled() && !"Canceled".equals(t.getMessage())) {

                    if (swipeLayout != null) {
                        swipeLayout.setRefreshing(false);
                    }
                    t.printStackTrace();
                    ShowErrorLayout(R.string.err_msg_server);
                }
            }
        });


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (NotificationCall != null) {
            NotificationCall.cancel();
        }
    }

    public void setToolbar() {
        tvToolbarTitle.setText(R.string.notifications);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void showContent() {
        try {
            rvNotifications.setVisibility(View.VISIBLE);
            rlNoInternetConnection.setVisibility(View.GONE);
            rlEmptyLayout.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showEmpty() {
        try {
            rvNotifications.setVisibility(View.GONE);
            rlEmptyLayout.setVisibility(View.VISIBLE);
            rlNoInternetConnection.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ShowErrorLayout(int txt) {
        try {
            rvNotifications.setVisibility(View.GONE);
            rlNoInternetConnection.setVisibility(View.VISIBLE);
            rlEmptyLayout.setVisibility(View.GONE);
            tvError.setText(txt);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick({R.id.btn_refresh})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_refresh:
                Refresh();
                break;
        }
    }


    private void Refresh() {
        isThereMore = true;
        notificationsList.clear();
        adapterNotifications.notifyDataSetChanged();
        endlessScrollListener.resetState();
        getNotifications(STARTING_PAGE_INDEX);

    }


    @Override
    public void onRefresh() {
        Refresh();
    }
}
