package com.wasset.ui;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wasset.R;
import com.wasset.adapter.AdapterSubCategories;
import com.wasset.interfaces.OnItemClickListener;
import com.wasset.models.categoryList;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SubCategoryActivity extends Fragment {


    @BindView(R.id.rv_sub_category)
    RecyclerView rvSubCategory;
    @BindView(R.id.iv_empty_icon)
    ImageView ivEmptyIcon;
    @BindView(R.id.tv_empty_text)
    TextView tvEmptyText;
    @BindView(R.id.rl_empty_layout)
    LinearLayout rlEmptyLayout;
    private List<categoryList.DataBean.SubCategoriesBean> subCategoryList;
    private String SubCategoryName;
    private String action = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_sub_category, container, false);
        ButterKnife.bind(this, view);
        subCategoryList = new ArrayList<>();
//            if (getIntent().getAction() != null) {
//                action = getIntent().getAction();
//            }

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        if (getArguments() != null) {
            subCategoryList = (List<categoryList.DataBean.SubCategoriesBean>) getArguments().getSerializable("list");
            SubCategoryName = getArguments().getString("name");
            setupList(subCategoryList);
            if (getActivity() != null)
                ((HomeActivity) getActivity()).setToolbar(SubCategoryName);
        }
    }

    public SubCategoryActivity newInstance(String subCategoryName, ArrayList<categoryList.DataBean.SubCategoriesBean> subCategoryList) {
        SubCategoryActivity fragment = new SubCategoryActivity();
        Bundle bundle = new Bundle();
        bundle.putString("name", subCategoryName);
        bundle.putSerializable("list", subCategoryList);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private void setupList(List<categoryList.DataBean.SubCategoriesBean> subCategoryList) {

        System.out.println("size" + subCategoryList.size());

        if (subCategoryList.size() > 0) {
            showContent();
        } else {
            showEmpty();
        }

        AdapterSubCategories adapterSubCategories = new AdapterSubCategories(getContext(), subCategoryList);
        rvSubCategory.setAdapter(adapterSubCategories);
        adapterSubCategories.setListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                categoryList.DataBean.SubCategoriesBean subCategory = subCategoryList.get(position);
//                if (action != null) {
//                    Intent i = new Intent();
//                    i.putExtra("Id", Integer.valueOf(subCategory.getCategory_id()));
//                    i.putExtra("Name", subCategory.getName());
//                    getActivity().setResult(RESULT_OK, i);
//                    getActivity().finish();
//
//                } else {
                try {
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, CategoryPostsActivity.newInstance(subCategory.getCategory_id(), subCategory.getName(),"home")).
                            setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out).commit();
                } catch (Exception e) {

                }
//                    Intent i = new Intent(SubCategoryActivity.this, CategoryPostsActivity.class);
//                    i.putExtra("Id", subCategory.getCategory_id());
//                    i.putExtra("name", subCategory.getName());
//                    startActivity(i);
//                }
            }
        });
    }

    private void showContent() {
        try {
            rvSubCategory.setVisibility(View.VISIBLE);
            rlEmptyLayout.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showEmpty() {
        try {
            rvSubCategory.setVisibility(View.GONE);
            rlEmptyLayout.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}