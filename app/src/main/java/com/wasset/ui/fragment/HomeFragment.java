package com.wasset.ui.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wasset.R;
import com.wasset.adapter.AdapterHome;
import com.wasset.interfaces.ApiClient;
import com.wasset.interfaces.ApiInterface;
import com.wasset.interfaces.OnItemClickListener;
import com.wasset.models.Home;
import com.wasset.ui.CategoryPostsActivity;
import com.wasset.ui.HomeActivity;
import com.wasset.utility.Utils;
import com.wasset.utility.QuickstartPreferences;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.wasset.utility.QuickstartPreferences.UNAUTHORIZED_STATUS_CODE;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.rv_homelist)
    RecyclerView rvHomelist;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeLayout;
    Unbinder unbinder;
    @BindView(R.id.tv_error)
    TextView tvError;
    @BindView(R.id.rl_no_internet_connection)
    RelativeLayout rlNoInternetConnection;
    @BindView(R.id.btn_refresh)
    Button btnRefresh;
    @BindView(R.id.iv_empty_icon)
    ImageView ivEmptyIcon;
    @BindView(R.id.tv_empty_text)
    TextView tvEmptyText;
    @BindView(R.id.rl_empty_layout)
    LinearLayout rlEmptyLayout;

    private AdapterHome adapter_home;
    private List<Home.DataBean> homeList;
    private Call<Home> call;

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }


    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);
        if (getActivity() != null)
            ((HomeActivity) getActivity()).setToolbar(null);
        swipeLayout.setOnRefreshListener(this);
        homeList = new ArrayList<>();

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupList();
        requestGetCategoryList();
    }

    private void setupList() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvHomelist.setLayoutManager(linearLayoutManager);
        adapter_home = new AdapterHome(homeList);
        rvHomelist.setAdapter(adapter_home);
        adapter_home.setListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {

                Home.DataBean item = homeList.get(position);
                if (itemView.getId() == R.id.btn_more) {
                    try {
                        getFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, CategoryPostsActivity.newInstance(item.getCategory_id(), item.getName(),"home")).
                                setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out).commit();
                    } catch (Exception e) {

                    }
                }
            }
        });
    }

    private void requestGetCategoryList() {
        if (!Utils.isNetworkAvailable()) {
            ShowErrorLayout(R.string.no_internet_connection);
            return;
        }

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        swipeLayout.setRefreshing(true);

        call = apiService.getMainList();
        call.enqueue(new Callback<Home>() {
            @Override
            public void onResponse(Call<Home> call, final Response<Home> response) {
                System.out.println("raw" + response.raw());

                swipeLayout.setRefreshing(false);
                try {
                    if (response.body().isStatus() && response.body().getCode() == QuickstartPreferences.OK_STATUS_CODE) {
                        if (response.body().getData().size() > 0) {
                            showContent();
                            homeList.clear();
                            homeList.addAll(response.body().getData());
                            adapter_home.notifyDataSetChanged();
                        } else {
                            showEmpty();
                        }

                    } else if (response.body().getCode() == UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(getActivity(), R.string.Unauthorized, Toast.LENGTH_LONG).show();
                        Utils.LunchLogin(getActivity());
                    } else {
                        ShowErrorLayout(R.string.err_msg_server);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    ShowErrorLayout(R.string.err_msg_server);
                }
            }

            @Override
            public void onFailure(Call<Home> call, Throwable t) {
                if (!call.isCanceled() && !"Canceled".equals(t.getMessage())) {

                    swipeLayout.setRefreshing(false);
                    t.printStackTrace();
                    ShowErrorLayout(R.string.err_msg_server);
                }
            }
        });
    }

    private void showContent() {
        try {
            rvHomelist.setVisibility(View.VISIBLE);
            rlNoInternetConnection.setVisibility(View.GONE);
            rlEmptyLayout.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showEmpty() {
        try {
            rvHomelist.setVisibility(View.GONE);
            rlEmptyLayout.setVisibility(View.VISIBLE);
            rlNoInternetConnection.setVisibility(View.GONE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ShowErrorLayout(int txt) {
        try {
            rvHomelist.setVisibility(View.GONE);
            rlNoInternetConnection.setVisibility(View.VISIBLE);
            rlEmptyLayout.setVisibility(View.GONE);
            tvError.setText(txt);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {

        if (call != null) {
            call.cancel();
        }
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btn_refresh)
    public void onViewClicked() {
        requestGetCategoryList();
    }

    @Override
    public void onRefresh() {
        requestGetCategoryList();
    }
}
