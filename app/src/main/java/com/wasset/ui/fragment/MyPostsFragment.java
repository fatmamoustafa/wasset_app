package com.wasset.ui.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wasset.R;
import com.wasset.adapter.AdapterMyPost;
import com.wasset.interfaces.ApiClient;
import com.wasset.interfaces.ApiInterface;
import com.wasset.interfaces.OnItemClickListener;
import com.wasset.models.FormalResponse;
import com.wasset.models.MyPosts;
import com.wasset.models.PostDataBean;
import com.wasset.ui.AddPostActivity;
import com.wasset.ui.PostDetailsActivity;
import com.wasset.utility.EndlessRecyclerViewScrollListener;
import com.wasset.utility.QuickstartPreferences;
import com.wasset.utility.Utils;
import com.wasset.views.AVLoadingIndicatorDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.refactor.lib.colordialog.ColorDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cn.refactor.lib.colordialog.PromptDialog.DIALOG_TYPE_SUCCESS;
import static com.wasset.utility.QuickstartPreferences.UNAUTHORIZED_STATUS_CODE;

public class MyPostsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.rv_my_posts)
    RecyclerView rvMyPosts;
    @BindView(R.id.iv_empty_icon)
    ImageView ivEmptyIcon;
    @BindView(R.id.tv_empty_text)
    TextView tvEmptyText;
    @BindView(R.id.rl_empty_layout)
    LinearLayoutCompat rlEmptyLayout;
    @BindView(R.id.tv_error)
    TextView tvError;
    @BindView(R.id.btn_refresh)
    Button btnRefresh;
    @BindView(R.id.rl_no_internet_connection)
    RelativeLayout rlNoInternetConnection;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeLayout;
    Unbinder unbinder;
    private List<PostDataBean> myPostsList;
    private AdapterMyPost adapterPostDetails;
    private Call<MyPosts> call;
    private Call<FormalResponse> DeleteCall;
    private AVLoadingIndicatorDialog indicatorView;
    private Call<FormalResponse> HideCall;
    private Call<FormalResponse> ShowCall;
    private int STARTING_PAGE_INDEX = 1;
    private EndlessRecyclerViewScrollListener endlessScrollListener;
    private boolean isThereMore = true;

    public static MyPostsFragment newInstance() {
        MyPostsFragment fragment = new MyPostsFragment();
        return fragment;
    }


    public MyPostsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_posts, container, false);
        unbinder = ButterKnife.bind(this, view);
        swipeLayout.setOnRefreshListener(this);
        myPostsList = new ArrayList<>();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupList();
        requestGetMyPostList(STARTING_PAGE_INDEX);
    }

    private void setupList() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        adapterPostDetails = new AdapterMyPost(getActivity(), myPostsList);
        rvMyPosts.setLayoutManager(linearLayoutManager);
        rvMyPosts.setAdapter(adapterPostDetails);
        endlessScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, STARTING_PAGE_INDEX) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                System.out.println("page" + page);
                if (Utils.isNetworkAvailable()) {
                    if (isThereMore) {
                        System.out.println("Page" + page);
                        requestGetMyPostList(page);
                    }
                }
            }
        };

        rvMyPosts.addOnScrollListener(endlessScrollListener);
        adapterPostDetails.setListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                try {
                    PostDataBean item = myPostsList.get(position);
                    switch (itemView.getId()) {

                        case R.id.btn_delete:
                            DeletePost(position);
                            break;

                        case R.id.btn_hide:
                            if (item.isIs_hidden()) {
                                ShowPost(position);
                            } else {
                                HidePost(position);
                            }
                            break;

                        case R.id.btn_edit:
                            Intent i = new Intent(getActivity(), AddPostActivity.class);
                            i.putExtra("Id", item.getId());
                            getActivity().startActivity(i);
                            break;

                        default:
                            Intent intent = new Intent(getContext(), PostDetailsActivity.class);
                            intent.putExtra("Id", item.getId());
                            intent.putExtra("SHOULD_SCROLL", false);
                            startActivity(intent);
                            break;
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    Refresh();
                }
            }
        });

    }


    private void requestGetMyPostList(final int page) {
        if (!Utils.isNetworkAvailable()) {
            ShowErrorLayout(R.string.no_internet_connection);
            return;
        }

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        swipeLayout.setRefreshing(true);
        call = apiService.getMyPostsList(page);
        call.enqueue(new Callback<MyPosts>() {
            @Override
            public void onResponse(Call<MyPosts> call, final Response<MyPosts> response) {
                System.out.println("raw" + response.raw());
                swipeLayout.setRefreshing(false);

                try {
                    if (response.body().isStatus() && response.body().getCode() == QuickstartPreferences.OK_STATUS_CODE) {
                        if (response.body().getData().getPosts().size() > 0) {
                            showContent();
                            try {
                                myPostsList.addAll(response.body().getData().getPosts());
                                adapterPostDetails.notifyDataSetChanged();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            isThereMore = false;
                            if (page == STARTING_PAGE_INDEX) {
                                showEmpty();
                            }
                        }

                    } else if (response.body().getCode() == UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(getActivity(), R.string.Unauthorized, Toast.LENGTH_LONG).show();
                        Utils.LunchLogin(getActivity());
                    } else {
                        ShowErrorLayout(R.string.err_msg_server);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    ShowErrorLayout(R.string.err_msg_server);
                }
            }

            @Override
            public void onFailure(Call<MyPosts> call, Throwable t) {
                if (!call.isCanceled() && !"Canceled".equals(t.getMessage())) {
                    swipeLayout.setRefreshing(false);
                    t.printStackTrace();
                    ShowErrorLayout(R.string.err_msg_server);
                }
            }
        });
    }

    private void showContent() {
        try {
            rvMyPosts.setVisibility(View.VISIBLE);
            rlNoInternetConnection.setVisibility(View.GONE);
            rlEmptyLayout.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showEmpty() {
        try {
            rvMyPosts.setVisibility(View.GONE);
            rlEmptyLayout.setVisibility(View.VISIBLE);
            rlNoInternetConnection.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ShowErrorLayout(int txt) {
        try {
            rvMyPosts.setVisibility(View.GONE);
            rlNoInternetConnection.setVisibility(View.VISIBLE);
            rlEmptyLayout.setVisibility(View.GONE);
            tvError.setText(txt);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {

        if (call != null) {
            call.cancel();
        }

        if (DeleteCall != null) {
            DeleteCall.cancel();
        }

        hideProgress();

        super.onDestroyView();
        unbinder.unbind();
    }


    private void Refresh() {
        isThereMore = true;
        myPostsList.clear();
        adapterPostDetails.notifyDataSetChanged();
        endlessScrollListener.resetState();
        requestGetMyPostList(STARTING_PAGE_INDEX);

    }


    @OnClick(R.id.btn_refresh)
    public void onViewClicked() {
        Refresh();
    }

    @Override
    public void onRefresh() {
        Refresh();
    }


    private void DeletePost(final int pos) {
        ColorDialog dialog = new ColorDialog(getActivity());
        dialog.setAnimationEnable(true);
        dialog.setTitle(getString(R.string.info));
        dialog.setContentText(getString(R.string.msg_delete_item));
        //dialog.setContentImage(getResources().getDrawable(R.drawable.ic_warning));
        dialog.setPositiveListener(getString(R.string.Ok), new ColorDialog.OnPositiveListener() {
            @Override
            public void onClick(ColorDialog dialog) {
                dialog.dismiss();
                requestDeletePost(pos);

            }
        })
                .setNegativeListener(getString(R.string.Cancel), new ColorDialog.OnNegativeListener() {
                    @Override
                    public void onClick(ColorDialog dialog) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private void requestDeletePost(final int pos) {

        if (!Utils.isNetworkAvailable()) {
            Utils.ShowNoInternetConnectionDialog(getActivity());
            return;
        }

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Utils.HideKeyBord(getActivity());
        showProgress();
        PostDataBean post = myPostsList.get(pos);

        DeleteCall = apiService.DeletePost(post.getId());
        DeleteCall.enqueue(new Callback<FormalResponse>() {
            @Override
            public void onResponse(Call<FormalResponse> call, final Response<FormalResponse> response) {

                hideProgress();
                try {
                    FormalResponse formalResponse = response.body();
                    if (formalResponse.isStatus() && formalResponse.getCode() == QuickstartPreferences.OK_STATUS_CODE) {
                        Utils.ShowDialog(getActivity(), getString(R.string.success), formalResponse.getMessage(), DIALOG_TYPE_SUCCESS);
                        try {


                            myPostsList.remove(pos);
                            adapterPostDetails.notifyItemRemoved(pos);
                        } catch (Exception e) {
                            e.printStackTrace();
                            Refresh();
                        }

                    } else if (response.body().getCode() == UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(getActivity(), R.string.Unauthorized, Toast.LENGTH_LONG).show();
                        Utils.LunchLogin(getActivity());
                    } else {
                        ShowErrorLayout(R.string.err_msg_server);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.ShowServerDialog(getActivity());
                }
            }


            @Override
            public void onFailure(Call<FormalResponse> call, Throwable t) {
                if (!call.isCanceled() && !"Canceled".equals(t.getMessage())) {
                    hideProgress();
                    t.printStackTrace();
                    Utils.ShowServerDialog(getActivity());
                }
            }
        });
    }


    private void hideProgress() {
        if (indicatorView != null && indicatorView.isShowing()) {
            indicatorView.dismiss();
        }
    }

    private void showProgress() {

        if (indicatorView == null) {
            indicatorView = new AVLoadingIndicatorDialog(getActivity());
            //indicatorView.setMessage(msg);
            indicatorView.setCancelable(false);
        }
        indicatorView.show();
    }


    private void HidePost(final int pos) {

        if (!Utils.isNetworkAvailable()) {
            Utils.ShowNoInternetConnectionDialog(getActivity());
            return;
        }

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Utils.HideKeyBord(getActivity());
        showProgress();
        final PostDataBean post = myPostsList.get(pos);
        HideCall = apiService.HidePost(post.getId());
        HideCall.enqueue(new Callback<FormalResponse>() {
            @Override
            public void onResponse(Call<FormalResponse> call, final Response<FormalResponse> response) {

                hideProgress();
                try {
                    FormalResponse formalResponse = response.body();
                    if (formalResponse.isStatus() && formalResponse.getCode() == QuickstartPreferences.OK_STATUS_CODE) {
                        Utils.ShowDialog(getActivity(), getString(R.string.success), formalResponse.getMessage(), DIALOG_TYPE_SUCCESS);
                        post.setIs_hidden(true);
                        adapterPostDetails.notifyItemChanged(pos);

                    } else if (response.body().getCode() == UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(getActivity(), R.string.Unauthorized, Toast.LENGTH_LONG).show();
                        Utils.LunchLogin(getActivity());
                    } else {
                        ShowErrorLayout(R.string.err_msg_server);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.ShowServerDialog(getActivity());
                }
            }


            @Override
            public void onFailure(Call<FormalResponse> call, Throwable t) {
                if (!call.isCanceled() && !"Canceled".equals(t.getMessage())) {
                    hideProgress();
                    t.printStackTrace();
                    Utils.ShowServerDialog(getActivity());
                }
            }
        });
    }

    private void ShowPost(final int pos) {

        if (!Utils.isNetworkAvailable()) {
            Utils.ShowNoInternetConnectionDialog(getActivity());
            return;
        }


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Utils.HideKeyBord(getActivity());
        showProgress();

        final PostDataBean post = myPostsList.get(pos);

        ShowCall = apiService.ShowPost(post.getId());
        ShowCall.enqueue(new Callback<FormalResponse>() {
            @Override
            public void onResponse(Call<FormalResponse> call, final Response<FormalResponse> response) {

                hideProgress();
                try {
                    FormalResponse formalResponse = response.body();
                    if (formalResponse.isStatus() && formalResponse.getCode() == QuickstartPreferences.OK_STATUS_CODE) {
                        Utils.ShowDialog(getActivity(), getString(R.string.success), formalResponse.getMessage(), DIALOG_TYPE_SUCCESS);
                        post.setIs_hidden(false);
                        adapterPostDetails.notifyItemChanged(pos);

                    } else if (response.body().getCode() == UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(getActivity(), R.string.Unauthorized, Toast.LENGTH_LONG).show();
                        Utils.LunchLogin(getActivity());
                    } else {
                        ShowErrorLayout(R.string.err_msg_server);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.ShowServerDialog(getActivity());
                }
            }


            @Override
            public void onFailure(Call<FormalResponse> call, Throwable t) {
                if (!call.isCanceled() && !"Canceled".equals(t.getMessage())) {
                    hideProgress();
                    t.printStackTrace();
                    Utils.ShowServerDialog(getActivity());
                }
            }
        });
    }
}
