package com.wasset.ui.fragment;


import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wasset.R;
import com.wasset.adapter.AdapterPostDetails;
import com.wasset.interfaces.ApiClient;
import com.wasset.interfaces.ApiInterface;
import com.wasset.interfaces.OnItemClickListener;
import com.wasset.models.FormalResponse;
import com.wasset.models.PostDataBean;
import com.wasset.models.favorite;
import com.wasset.utility.EndlessRecyclerViewScrollListener;
import com.wasset.utility.Utils;
import com.wasset.utility.QuickstartPreferences;
import com.wasset.views.AVLoadingIndicatorDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.refactor.lib.colordialog.ColorDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cn.refactor.lib.colordialog.PromptDialog.DIALOG_TYPE_SUCCESS;
import static com.wasset.utility.QuickstartPreferences.UNAUTHORIZED_STATUS_CODE;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.rv_fav)
    RecyclerView rvFav;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeLayout;
    Unbinder unbinder;
    @BindView(R.id.tv_error)
    TextView tvError;
    @BindView(R.id.btn_refresh)
    Button btnRefresh;
    @BindView(R.id.rl_no_internet_connection)
    RelativeLayout rlNoInternetConnection;
    @BindView(R.id.iv_empty_icon)
    ImageView ivEmptyIcon;
    @BindView(R.id.tv_empty_text)
    TextView tvEmptyText;
    @BindView(R.id.rl_empty_layout)
    LinearLayout rlEmptyLayout;
    private AdapterPostDetails favadapterPostDetails;
    private List<PostDataBean> favList;
    private Call<favorite> call;
    private AVLoadingIndicatorDialog indicatorView;
    private Call<FormalResponse> RemoveFromFavCall;
    private int STARTING_PAGE_INDEX = 1;
    private EndlessRecyclerViewScrollListener endlessScrollListener;
    private boolean isThereMore = true ;

    public static FavFragment newInstance() {
        FavFragment fragment = new FavFragment();
        return fragment;
    }

    public FavFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fav, container, false);
        unbinder = ButterKnife.bind(this, view);
        swipeLayout.setOnRefreshListener(this);
        favList = new ArrayList<>();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupFavList();
        requestGetFavoriteList(STARTING_PAGE_INDEX);
    }

    private void setupFavList() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        favadapterPostDetails = new AdapterPostDetails( favList);
        rvFav.setLayoutManager(linearLayoutManager);
        endlessScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager,STARTING_PAGE_INDEX) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {

                if (Utils.isNetworkAvailable()) {
                    if (isThereMore) {
                        requestGetFavoriteList(page);
                    }
                }
            }
        };

        rvFav.addOnScrollListener(endlessScrollListener);
        rvFav.setAdapter(favadapterPostDetails);
        favadapterPostDetails.setListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                showConfirmDeleteItem(position);

            }
        });
    }

    private void requestGetFavoriteList(final int page) {
        if (!Utils.isNetworkAvailable()) {
            ShowErrorLayout(R.string.no_internet_connection);
            return;
        }

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        swipeLayout.setRefreshing(true);

        call = apiService.getFavoriteList(page);
        call.enqueue(new Callback<favorite>() {
            @Override
            public void onResponse(Call<favorite> call, final Response<favorite> response) {
                System.out.println("raw" + response.raw());

                swipeLayout.setRefreshing(false);
                try {
                    if (response.body().isStatus() && response.body().getCode() == QuickstartPreferences.OK_STATUS_CODE) {
                        if (response.body().getPosts().size() > 0) {
                            showContent();
                            favList.addAll(response.body().getPosts());
                            favadapterPostDetails.notifyDataSetChanged();
                        } else {
                            isThereMore =  false ;
                            if (page == STARTING_PAGE_INDEX) {
                                showEmpty();
                            }
                        }

                    } else if (response.body().getCode() == UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(getActivity(), R.string.Unauthorized, Toast.LENGTH_LONG).show();
                        Utils.LunchLogin(getActivity());
                    } else {
                        ShowErrorLayout(R.string.err_msg_server);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    ShowErrorLayout(R.string.err_msg_server);
                }
            }

            @Override
            public void onFailure(Call<favorite> call, Throwable t) {
                if (!call.isCanceled() && !"Canceled".equals(t.getMessage())) {

                    swipeLayout.setRefreshing(false);
                    t.printStackTrace();
                    ShowErrorLayout(R.string.err_msg_server);
                }
            }
        });
    }

    private void showContent() {
        try {
            rvFav.setVisibility(View.VISIBLE);
            rlNoInternetConnection.setVisibility(View.GONE);
            rlEmptyLayout.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showEmpty() {
        try {
            rvFav.setVisibility(View.GONE);
            rlEmptyLayout.setVisibility(View.VISIBLE);
            rlNoInternetConnection.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ShowErrorLayout(int txt) {
        try {
            rvFav.setVisibility(View.GONE);
            rlNoInternetConnection.setVisibility(View.VISIBLE);
            rlEmptyLayout.setVisibility(View.GONE);
            tvError.setText(txt);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {

        if (call != null) {
            call.cancel();
        }
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btn_refresh)
    public void onViewClicked() {
        Refresh();
    }

    private void RemoveFromFav(final int pos) {

        if (!Utils.isNetworkAvailable()) {
            Utils.ShowNoInternetConnectionDialog(getActivity());
            return;
        }

        showProgress();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        PostDataBean item = favList.get(pos);
        RemoveFromFavCall = apiService.addToFavorites(item.getId());
        RemoveFromFavCall.enqueue(new Callback<FormalResponse>() {
            @Override
            public void onResponse(Call<FormalResponse> call, final Response<FormalResponse> response) {
                hideProgress();
                try {
                    if (response.body() != null) {
                        FormalResponse formalResponse = response.body();
                        if (formalResponse.isStatus() && formalResponse.getCode() == QuickstartPreferences.OK_STATUS_CODE) {
                            //Toast.makeText(getApplicationContext(), formalResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            Utils.ShowDialog(getActivity(), getString(R.string.success), formalResponse.getMessage(), DIALOG_TYPE_SUCCESS);
                            favList.remove(pos);
                            favadapterPostDetails.notifyItemRemoved(pos);
                            if (favList.size() == 0) {
                                showEmpty();
                            }

                        } else if (response.body().getCode() == QuickstartPreferences.UNAUTHORIZED_STATUS_CODE) {
                            Toast.makeText(getActivity(), R.string.Unauthorized, Toast.LENGTH_LONG).show();
                            Utils.LunchLogin(getActivity());

                        } else {
                            Utils.ShowServerDialog(getActivity());
                        }
                    } else {
                        Utils.ShowServerDialog(getActivity());
                    }

                } catch (Exception e) {
                    e.printStackTrace();

                    Utils.ShowServerDialog(getActivity());
                }
            }

            @Override
            public void onFailure(Call<FormalResponse> call, Throwable t) {
                hideProgress();
                t.printStackTrace();
                Utils.ShowServerDialog(getActivity());
            }
        });
    }

    private void showConfirmDeleteItem(final int pos) {

        ColorDialog dialog = new ColorDialog(getActivity());
        dialog.setAnimationEnable(true);
        dialog.setTitle(getString(R.string.info));
        dialog.setContentText(getString(R.string.msg_delete_item));
        //dialog.setContentImage(getResources().getDrawable(R.drawable.ic_warning));
        dialog.setPositiveListener(getString(R.string.yes), new ColorDialog.OnPositiveListener() {
            @Override
            public void onClick(ColorDialog dialog) {

                dialog.dismiss();
                RemoveFromFav(pos);

            }
        })
                .setNegativeListener(getString(R.string.Cancel), new ColorDialog.OnNegativeListener() {
                    @Override
                    public void onClick(ColorDialog dialog) {
                        dialog.dismiss();

                    }
                }).show();
    }

    private void hideProgress() {
        if (indicatorView != null && indicatorView.isShowing()) {
            indicatorView.dismiss();
        }
    }

    private void showProgress() {

        if (indicatorView == null) {
            indicatorView = new AVLoadingIndicatorDialog(getActivity());
            //indicatorView.setMessage(msg);
            indicatorView.setCancelable(false);
        }
        indicatorView.show();
    }


    private void Refresh(){
        isThereMore = true ;
        favList.clear();
        favadapterPostDetails.notifyDataSetChanged();
        endlessScrollListener.resetState();
        requestGetFavoriteList(STARTING_PAGE_INDEX);

    }


    @Override
    public void onRefresh() {
        Refresh();
    }
}
