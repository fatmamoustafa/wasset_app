package com.wasset.ui.fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.wasset.R;
import com.wasset.interfaces.ApiClient;
import com.wasset.interfaces.ApiInterface;
import com.wasset.models.FormalResponse;
import com.wasset.ui.HomeActivity;
import com.wasset.utility.Utils;
import com.wasset.utility.QuickstartPreferences;
import com.wasset.views.AVLoadingIndicatorDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cn.refactor.lib.colordialog.PromptDialog.DIALOG_TYPE_SUCCESS;
import static cn.refactor.lib.colordialog.PromptDialog.DIALOG_TYPE_WRONG;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactUsFragment extends Fragment {


    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_suggestion)
    EditText etSuggestion;
    @BindView(R.id.et_message)
    EditText etMessage;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    Unbinder unbinder;
    private Call<FormalResponse> call;
    private AVLoadingIndicatorDialog indicatorView;

    public static ContactUsFragment newInstance() {
        ContactUsFragment fragment = new ContactUsFragment();
        return fragment;
    }


    public ContactUsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contact_us, container, false);
        unbinder = ButterKnife.bind(this, view);
        if (getActivity() != null)
            ((HomeActivity) getActivity()).setToolbar(getString(R.string.nav_contact_us));
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btn_submit)
    public void onViewClicked() {
        checkForm();
    }


    private void checkForm() {
        String name = etName.getText().toString().trim();
        String email = etEmail.getText().toString().trim();
        String phone = etPhone.getText().toString().trim();
        String suggestion = etSuggestion.getText().toString().trim();
        String message = etMessage.getText().toString().trim();

        if (name.isEmpty()) {
            Utils.ShowDialog(getActivity(), getString(R.string.err), getString(R.string.err_msg_name), DIALOG_TYPE_WRONG);
            return;
        }

        if (email.isEmpty()) {
            Utils.ShowDialog(getActivity(), getString(R.string.err), getString(R.string.err_msg_email), DIALOG_TYPE_WRONG);
            return;
        }

        if (!Utils.isValidMail(email)) {
            Utils.ShowDialog(getActivity(), getString(R.string.err), getString(R.string.err_msg_valid_email), DIALOG_TYPE_WRONG);

            return;
        }

        if (phone.isEmpty()) {
            Utils.ShowDialog(getActivity(), getString(R.string.err), getString(R.string.err_msg_phone_number), DIALOG_TYPE_WRONG);
            return;
        }


        if (suggestion.isEmpty()) {
            Utils.ShowDialog(getActivity(), getString(R.string.err), getString(R.string.err_msg_suggestion), DIALOG_TYPE_WRONG);
            return;
        }

        if (message.isEmpty()) {
            Utils.ShowDialog(getActivity(), getString(R.string.err), getString(R.string.err_msg_suggestion_message), DIALOG_TYPE_WRONG);
            return;
        }

        requestContactUs();

    }

    private void requestContactUs() {
        if (!Utils.isNetworkAvailable()) {
            Utils.ShowNoInternetConnectionDialog(getActivity());
            return;
        }

        String name = etName.getText().toString().trim();
        String email = etEmail.getText().toString().trim();
        String phone = etPhone.getText().toString().trim();
        String suggestion = etSuggestion.getText().toString().trim();
        String message = etMessage.getText().toString().trim();


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Utils.HideKeyBord(getActivity());
        showProgress();
        call = apiService.contactUs(name, email, phone, suggestion, message);
        call.enqueue(new Callback<FormalResponse>() {
            @Override
            public void onResponse(Call<FormalResponse> call, final Response<FormalResponse> response) {

                hideProgress();
                try {
                    FormalResponse formalResponse = response.body();
                    if (formalResponse.isStatus() && formalResponse.getCode() == QuickstartPreferences.OK_STATUS_CODE) {
                        Utils.ShowDialog(getActivity(), getString(R.string.success), formalResponse.getMessage(), DIALOG_TYPE_SUCCESS);
                        ClearEditText();
                    } else {
                        Utils.ShowDialog(getActivity(), getString(R.string.err), formalResponse.getMessage(), DIALOG_TYPE_WRONG);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.ShowServerDialog(getActivity());
                }
            }


            @Override
            public void onFailure(Call<FormalResponse> call, Throwable t) {
                if (!call.isCanceled() && !"Canceled".equals(t.getMessage())) {
                    hideProgress();
                    t.printStackTrace();
                    Utils.ShowServerDialog(getActivity());
                }
            }
        });
    }


    private void hideProgress() {
        if (indicatorView != null && indicatorView.isShowing()) {
            indicatorView.dismiss();
        }
    }

    private void showProgress() {

        if (indicatorView == null) {
            indicatorView = new AVLoadingIndicatorDialog(getActivity());
            //indicatorView.setMessage(msg);
            indicatorView.setCancelable(false);
        }
        indicatorView.show();
    }

    private void ClearEditText() {

        etName.getText().clear();
        etEmail.getText().clear();
        etPhone.getText().clear();
        etSuggestion.getText().clear();
        etMessage.getText().clear();

    }

}
