package com.wasset.ui.fragment;


import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatTextView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.joooonho.SelectableRoundedImageView;
import com.wasset.R;
import com.wasset.adapter.AdapterCountries;
import com.wasset.databinding.FragmentMyAccountBinding;
import com.wasset.interfaces.ApiClient;
import com.wasset.interfaces.ApiInterface;
import com.wasset.models.Config;
import com.wasset.models.Login;
import com.wasset.ui.HomeActivity;
import com.wasset.utility.PreferenceEditor;
import com.wasset.utility.QuickstartPreferences;
import com.wasset.utility.Utils;
import com.wasset.views.AVLoadingIndicatorDialog;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static cn.refactor.lib.colordialog.PromptDialog.DIALOG_TYPE_SUCCESS;
import static cn.refactor.lib.colordialog.PromptDialog.DIALOG_TYPE_WRONG;
import static com.wasset.utility.QuickstartPreferences.UNAUTHORIZED_STATUS_CODE;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyAccountFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_current_password)
    EditText etCurrentPassword;
    @BindView(R.id.et_new_password)
    EditText etNewPassword;
    @BindView(R.id.btn_update)
    Button btnUpdate;
    Unbinder unbinder;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeLayout;
    @BindView(R.id.content)
    NestedScrollView content;
    @BindView(R.id.tv_error)
    TextView tvError;
    @BindView(R.id.btn_refresh)
    Button btnRefresh;
    @BindView(R.id.rl_no_internet_connection)
    RelativeLayout rlNoInternetConnection;
    @BindView(R.id.iv_user_image)
    ImageView ivUserImage;
    @BindView(R.id.et_confirm_new_password)
    EditText etConfirmNewPassword;
    @BindView(R.id.sp_countries)
    Spinner spCountries;
    private Call<Login> call;
    private AVLoadingIndicatorDialog indicatorView;
    private Call<Login> updateCall;
    private PreferenceEditor pf;
    private FragmentMyAccountBinding binding;
    private File ImagesFile;
    private File outPutFile;
    private Uri mImageCaptureUri;
    private static final int CAMERA_CODE = 101, GALLERY_CODE = 201, CROPPING_CODE = 301;
    private String CityId;


    public static MyAccountFragment newInstance() {
        MyAccountFragment fragment = new MyAccountFragment();
        return fragment;
    }

    public MyAccountFragment() {
        // Required empty public constructor
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null)
            ((HomeActivity) getActivity()).setToolbar(getString(R.string.nav_my_account));

        ivUserImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createImageDialog();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_account, container, false);
        View view = binding.getRoot();

        //here data must be an instance of the class MarsDataProvider


        //View view = inflater.inflate(R.layout.fragment_my_account, container, false);
        unbinder = ButterKnife.bind(this, view);
        swipeLayout.setOnRefreshListener(this);
        pf = PreferenceEditor.getInstance();


        return view;
    }


    private void setUpCountriesSpinner(Login.DataBean userInfo) {
        final List<Config.DataBean.CountriesBean> countriesBeanList = new ArrayList<>();

        Config.DataBean cfg = new Gson().fromJson(pf.getStringPreference(QuickstartPreferences.CONFIG), Config.DataBean.class);

        countriesBeanList.addAll(cfg.getCountries());
        countriesBeanList.remove(0);

        int selection = -1;
        //List<String> spinnerList = new ArrayList<>();
        for (int i = 0; i < countriesBeanList.size(); i++) {
            Config.DataBean.CountriesBean item = countriesBeanList.get(i);

            if (userInfo.getCity_id().equalsIgnoreCase(item.getId())) {
                selection = i;
                CityId = userInfo.getCity_id();
            }
            // spinnerList.add(item.getName());
        }



       /* List<String> spinnerList = new ArrayList<>();
        for (int i = 0; i < countriesBeanList.size(); i++) {
            Config.DataBean.CountriesBean item = countriesBeanList.get(i);
            spinnerList.add(item.getName());
        }*/

        //ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, spinnerList);

        AdapterCountries adapter = new AdapterCountries(getActivity(), R.layout.row_countries_spinner_item, R.id.tvName, countriesBeanList);
        //adapter.setDropDownViewResource(R.layout.row_countries_spinner_item);
        spCountries.setAdapter(adapter);


        //ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, spinnerList);
        //dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCountries.setAdapter(adapter);
        if (selection != -1) {
            spCountries.setSelection(selection);
        }

        spCountries.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {

                System.out.println(countriesBeanList.get(pos).getName());
                CityId = countriesBeanList.get(pos).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        get_UserInfo();

    }

    private void createImageDialog() {


        //outPutFile = new File(ImagesFile, System.currentTimeMillis() + ".jpg");

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.select_image_dialog);


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);


        ImageButton select_image_camera = (ImageButton) dialog.findViewById(R.id.select_image_camera);
        ImageButton select_image_gallery = (ImageButton) dialog.findViewById(R.id.select_image_gallery);


        select_image_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA, Manifest.permission.CAMERA}, 33);
                    return;
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 33);
                    return;
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 33);
                    return;
                }


                ImagesFile = new File(Environment.getExternalStorageDirectory() + "/" + Utils.FolderName);
                if (!ImagesFile.exists()) {
                    ImagesFile.mkdir();
                }

                outPutFile = new File(ImagesFile, System.currentTimeMillis() + ".jpg");
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                //File f = new File(ImagesFile, System.currentTimeMillis() + ".jpg");
                mImageCaptureUri = Uri.fromFile(outPutFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
                if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivityForResult(intent, CAMERA_CODE);
                }
                dialog.dismiss();

            }
        });

        select_image_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 33);
                    return;
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 33);
                    return;
                }

                ImagesFile = new File(Environment.getExternalStorageDirectory() + "/" + Utils.FolderName);
                if (!ImagesFile.exists()) {
                    ImagesFile.mkdir();
                }

                outPutFile = new File(ImagesFile, System.currentTimeMillis() + ".jpg");
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, GALLERY_CODE);
                dialog.dismiss();
            }
        });


        dialog.show();
    }


    private void get_UserInfo() {

        if (!Utils.isNetworkAvailable()) {
            ShowErrorLayout(R.string.no_internet_connection);
            return;
        }

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        call = apiService.getUserInfo();
        if (swipeLayout != null) {
            swipeLayout.setRefreshing(true);
        }
        call.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, final Response<Login> response) {


                if (swipeLayout != null) {
                    swipeLayout.setRefreshing(false);
                }

                try {
                    if (response.body().isStatus() && response.body().getCode() == QuickstartPreferences.OK_STATUS_CODE) {

                        if (response.body().getData() != null) {

                            Login.DataBean UserInfo = response.body().getData();
                            PreferenceEditor.getInstance().setStringPreference(QuickstartPreferences.USER_INFO, new Gson().toJson(UserInfo));
                            binding.setLogin(UserInfo);
                            setUpCountriesSpinner(UserInfo);
                            ShowContentLayout();
                        }
                    } else if (response.body().getCode() == UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(getActivity(), R.string.Unauthorized, Toast.LENGTH_LONG).show();
                        Utils.LunchLogin(getActivity());

                    } else {
                        ShowErrorLayout(R.string.err_msg_server);
                    }


                } catch (Exception e) {

                    e.printStackTrace();
                    ShowErrorLayout(R.string.err_msg_server);
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                if (!call.isCanceled() && !"Canceled".equals(t.getMessage())) {

                    if (swipeLayout != null) {
                        swipeLayout.setRefreshing(false);
                    }

                    t.printStackTrace();
                    ShowErrorLayout(R.string.err_msg_server);
                }
            }
        });


    }


    private void checkForm() {

        String userName = etName.getText().toString().trim();
        String email = etEmail.getText().toString().trim();
        String phone = etPhone.getText().toString().trim();
        String current_password = etCurrentPassword.getText().toString().trim();
        String new_password = etNewPassword.getText().toString().trim();
        String confirm_new_password = etConfirmNewPassword.getText().toString().trim();


        if (userName.isEmpty()) {
            Utils.ShowDialog(getActivity(), getString(R.string.err), getString(R.string.err_msg_first_name), DIALOG_TYPE_WRONG);
            return;
        }


        if (email.isEmpty()) {
            Utils.ShowDialog(getActivity(), getString(R.string.err), getString(R.string.err_msg_email), DIALOG_TYPE_WRONG);
            return;
        }


        if (!Utils.isValidMail(email)) {
            Utils.ShowDialog(getActivity(), getString(R.string.err), getString(R.string.err_msg_valid_email), DIALOG_TYPE_WRONG);
            return;
        }

        if (phone.isEmpty()) {
            Utils.ShowDialog(getActivity(), getString(R.string.err), getString(R.string.err_msg_phone_number), DIALOG_TYPE_WRONG);
            return;
        }

        if (CityId == null) {
            Utils.ShowDialog(getActivity(), getString(R.string.err), getString(R.string.err_msg_city), DIALOG_TYPE_WRONG);
            return;
        }


        if (!current_password.isEmpty() && !checkPassword()) {
            return;

        }

        if (!new_password.isEmpty() && !checkPassword()) {
            return;

        }

        if (!confirm_new_password.isEmpty() && !checkPassword()) {
            return;
        }


        requestUpdateProfile();
    }

    private boolean checkPassword() {

        String current_password = etCurrentPassword.getText().toString().trim();
        String new_password = etNewPassword.getText().toString().trim();
        String confirm_new_password = etConfirmNewPassword.getText().toString().trim();


        if (current_password.isEmpty()) {
            Utils.ShowDialog(getActivity(), getString(R.string.err), getString(R.string.err_current_password), DIALOG_TYPE_WRONG);
            return false;
        }

        if (new_password.isEmpty()) {
            Utils.ShowDialog(getActivity(), getString(R.string.err), getString(R.string.err_new_password), DIALOG_TYPE_WRONG);
            return false;
        }

        if (confirm_new_password.isEmpty()) {
            Utils.ShowDialog(getActivity(), getString(R.string.err), getString(R.string.err_msg_confirm_password), DIALOG_TYPE_WRONG);
            return false;
        }

        if (!new_password.equalsIgnoreCase(confirm_new_password)) {
            Utils.ShowDialog(getActivity(), getString(R.string.err), getString(R.string.err_msg_mis_match_password), DIALOG_TYPE_WRONG);
            return false;
        }


        return true;


    }


    private void requestUpdateProfile() {

        if (!Utils.isNetworkAvailable()) {
            Utils.ShowNoInternetConnectionDialog(getActivity());
            return;
        }

        RequestBody firstName = RequestBody.create(MediaType.parse("text/plain"), etName.getText().toString().trim());
        RequestBody email = RequestBody.create(MediaType.parse("text/plain"), etEmail.getText().toString().trim());
        RequestBody phone = RequestBody.create(MediaType.parse("text/plain"), etPhone.getText().toString().trim());
        RequestBody current_password = RequestBody.create(MediaType.parse("text/plain"), etCurrentPassword.getText().toString().trim());
        RequestBody new_password = RequestBody.create(MediaType.parse("text/plain"), etNewPassword.getText().toString().trim());
        RequestBody ANDROID_ID = RequestBody.create(MediaType.parse("text/plain"), Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID));
        RequestBody fcm = RequestBody.create(MediaType.parse("text/plain"), FirebaseInstanceId.getInstance().getToken());
        RequestBody platform = RequestBody.create(MediaType.parse("text/plain"), "android");
        RequestBody city = RequestBody.create(MediaType.parse("text/plain"), CityId);

        Map<String, RequestBody> map = new HashMap<>();

        map.put("username", firstName);
        map.put("email", email);
        map.put("mobile", phone);
        map.put("device_id", ANDROID_ID);
        map.put("device_type", platform);
        map.put("city", city);
        map.put("fcm", fcm);

        if (!etCurrentPassword.getText().toString().trim().isEmpty()) {
            map.put("old_password", current_password);
        }

        if (!etNewPassword.getText().toString().trim().isEmpty()) {
            map.put("password", new_password);
        }

        MultipartBody.Part body = null;
        if (outPutFile != null && outPutFile.exists()) {
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), outPutFile);
            body = MultipartBody.Part.createFormData("photo", outPutFile.getName(), reqFile);
        }

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Utils.HideKeyBord(getActivity());
        showProgress();

        updateCall = apiService.updateProfile(body, map);


        updateCall.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, final Response<Login> response) {
                hideProgress();
                try {
                    if (response.body() != null) {
                        Login login = response.body();
                        if (login.isStatus() && login.getCode() == QuickstartPreferences.OK_STATUS_CODE) {
                            Utils.ShowDialog(getActivity(), getString(R.string.success), login.getMessage(), DIALOG_TYPE_SUCCESS);
                            pf.setStringPreference(QuickstartPreferences.USER_INFO, new Gson().toJson(response.body().getData()));
                        } else {
                            Utils.ShowDialog(getActivity(), getString(R.string.err), login.getMessage(), DIALOG_TYPE_WRONG);
                        }

                    } else {
                        Utils.ShowServerDialog(getActivity());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.ShowServerDialog(getActivity());
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                hideProgress();
                t.printStackTrace();
                Utils.ShowServerDialog(getActivity());
            }
        });
    }

    private void hideProgress() {
        if (indicatorView != null && indicatorView.isShowing()) {
            indicatorView.dismiss();
        }
    }

    private void showProgress() {

        if (indicatorView == null) {
            indicatorView = new AVLoadingIndicatorDialog(getActivity());
            //indicatorView.setMessage(msg);
            indicatorView.setCancelable(false);
        }
        indicatorView.show();
    }


    private void ShowContentLayout() {
        content.setVisibility(View.VISIBLE);
        rlNoInternetConnection.setVisibility(View.GONE);
    }

    private void ShowErrorLayout(int txt) {
        if (swipeLayout != null) {
            swipeLayout.setRefreshing(false);
        }

        content.setVisibility(View.GONE);
        rlNoInternetConnection.setVisibility(View.VISIBLE);
        tvError.setText(txt);
    }

    @Override
    public void onRefresh() {
        get_UserInfo();
    }

    @OnClick({R.id.btn_update, R.id.btn_refresh})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_update:
                checkForm();
                break;
            case R.id.btn_refresh:
                get_UserInfo();
                break;

        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_CODE && resultCode == RESULT_OK) {
            try {
                mImageCaptureUri = data.getData();

                InputStream input = getActivity().getContentResolver().openInputStream(data.getData());
                FileOutputStream output = new FileOutputStream(outPutFile);
                byte[] buffer = new byte[4096];
                int n = 0;
                while (-1 != (n = input.read(buffer))) {
                    output.write(buffer, 0, n);
                }
                mImageCaptureUri = Uri.fromFile(outPutFile);
                outPutFile = new File(ImagesFile, System.currentTimeMillis() + ".jpg");

                CropIMG();

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (requestCode == CAMERA_CODE && resultCode == RESULT_OK) {
            try {
                CropIMG();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == CROPPING_CODE && resultCode == RESULT_OK) {
            try {
                if (outPutFile.exists()) {
                    ivUserImage.setImageBitmap(Utils.decodeFile(outPutFile));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    private void CropIMG() {

        Intent cropIntent = new Intent("com.android.camera.action.CROP");
        cropIntent.setDataAndType(mImageCaptureUri, "image/*");
        cropIntent.putExtra("crop", "true");
        cropIntent.putExtra("return-data", true);
        cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(outPutFile));
        startActivityForResult(cropIntent, CROPPING_CODE);


    }


    @Override
    public void onDestroyView() {

        if (call != null) {
            call.cancel();
        }

        if (updateCall != null) {
            updateCall.cancel();
        }


        super.onDestroyView();
        unbinder.unbind();
    }

}
