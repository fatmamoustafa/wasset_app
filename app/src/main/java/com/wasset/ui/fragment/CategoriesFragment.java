package com.wasset.ui.fragment;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wasset.R;
import com.wasset.adapter.AdapterCategories;
import com.wasset.interfaces.ApiClient;
import com.wasset.interfaces.ApiInterface;
import com.wasset.interfaces.OnItemClickListener;
import com.wasset.models.categoryList;
import com.wasset.ui.CategoryPostsActivity;
import com.wasset.ui.HomeActivity;
import com.wasset.ui.SubCategoryActivity;
import com.wasset.utility.Utils;
import com.wasset.utility.QuickstartPreferences;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.wasset.utility.QuickstartPreferences.UNAUTHORIZED_STATUS_CODE;


public class CategoriesFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.rv_categories)
    RecyclerView rvCategories;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeLayout;
    Unbinder unbinder;
    @BindView(R.id.tv_error)
    TextView tvError;
    @BindView(R.id.btn_refresh)
    Button btnRefresh;
    @BindView(R.id.rl_no_internet_connection)
    RelativeLayout rlNoInternetConnection;
    @BindView(R.id.iv_empty_icon)
    ImageView ivEmptyIcon;
    @BindView(R.id.tv_empty_text)
    TextView tvEmptyText;
    @BindView(R.id.rl_empty_layout)
    LinearLayout rlEmptyLayout;
    private AdapterCategories adapterCategories;
    private List<categoryList.DataBean> categoriesList;
    private Call<categoryList> call;
    private String postId;


    public static CategoriesFragment newInstance(String postId) {
        CategoriesFragment fragment = new CategoriesFragment();
        Bundle bundle = new Bundle();
        bundle.putString("post_id", postId);
        fragment.setArguments(bundle);
        return fragment;
    }

    public CategoriesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_categories, container, false);
        unbinder = ButterKnife.bind(this, view);
        if (getActivity() != null)
            ((HomeActivity) getActivity()).setToolbar(null);

        swipeLayout.setOnRefreshListener(this);
        categoriesList = new ArrayList<>();

        try {
            if (getArguments() != null) {
                postId = getArguments().getString("post_id");
                requestGetCategoryList(postId);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupList();
    }

    private void setupList() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        adapterCategories = new AdapterCategories(getActivity(), categoriesList);
        rvCategories.setLayoutManager(gridLayoutManager);
        rvCategories.setAdapter(adapterCategories);
        adapterCategories.setListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                categoryList.DataBean item = categoriesList.get(position);

                if (item.getSub_categories().size() > 0) {
                    try {
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, new SubCategoryActivity().newInstance(item.getName(), (ArrayList<categoryList.DataBean.SubCategoriesBean>) item.getSub_categories())).
                                setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out).commit();
                    } catch (Exception e) {

                    }
                } else {
                    try {
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, CategoryPostsActivity.newInstance(item.getCategory_id(), item.getName(),"home")).
                                setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out).commit();
                    } catch (Exception e) {

                    }
                }

            }
        });
    }

    private void requestGetCategoryList(String postId) {
        if (!Utils.isNetworkAvailable()) {
            ShowErrorLayout(R.string.no_internet_connection);
            return;
        }

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        swipeLayout.setRefreshing(true);

        call = apiService.getCategoryList(postId);
        call.enqueue(new Callback<categoryList>() {
            @Override
            public void onResponse(Call<categoryList> call, final Response<categoryList> response) {
                System.out.println("raw" + response.raw());

                swipeLayout.setRefreshing(false);
                try {
                    if (response.body().isStatus() && response.body().getCode() == QuickstartPreferences.OK_STATUS_CODE) {
                        if (response.body().getData().size() > 0) {
                            showContent();
                            categoriesList.clear();
                            categoriesList.addAll(response.body().getData());
                            adapterCategories.notifyDataSetChanged();
                        } else {
                            showEmpty();
                        }

                    } else if (response.body().getCode() == UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(getActivity(), R.string.Unauthorized, Toast.LENGTH_LONG).show();
                        Utils.LunchLogin(getActivity());
                    } else {
                        ShowErrorLayout(R.string.err_msg_server);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    ShowErrorLayout(R.string.err_msg_server);
                }
            }

            @Override
            public void onFailure(Call<categoryList> call, Throwable t) {
                if (!call.isCanceled() && !"Canceled".equals(t.getMessage())) {

                    swipeLayout.setRefreshing(false);
                    t.printStackTrace();
                    ShowErrorLayout(R.string.err_msg_server);
                }
            }
        });
    }

    private void showContent() {
        try {
            rvCategories.setVisibility(View.VISIBLE);
            rlNoInternetConnection.setVisibility(View.GONE);
            rlEmptyLayout.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showEmpty() {
        try {
            rvCategories.setVisibility(View.GONE);
            rlEmptyLayout.setVisibility(View.VISIBLE);
            rlNoInternetConnection.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ShowErrorLayout(int txt) {
        try {
            rvCategories.setVisibility(View.GONE);
            rlNoInternetConnection.setVisibility(View.VISIBLE);
            rlEmptyLayout.setVisibility(View.GONE);
            tvError.setText(txt);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {

        if (call != null) {
            call.cancel();
        }
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btn_refresh)
    public void onViewClicked() {
        requestGetCategoryList(postId);
    }


    @Override
    public void onRefresh() {
        requestGetCategoryList(postId);
    }

//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        setResult(RESULT_CANCELED);
//        finish();
//    }
}


