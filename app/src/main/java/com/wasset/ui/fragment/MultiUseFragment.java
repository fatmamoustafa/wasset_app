package com.wasset.ui.fragment;


import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.wasset.R;
import com.wasset.ui.HomeActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class MultiUseFragment extends Fragment {



    Unbinder unbinder;
    @BindView(R.id.wv_data)
    WebView wvData;

    public static MultiUseFragment newInstance() {
        MultiUseFragment fragment = new MultiUseFragment();
        return fragment;
    }

    public MultiUseFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        String data = getArguments().getString("data");
        View view = inflater.inflate(R.layout.fragment_multi_use, container, false);
        unbinder = ButterKnife.bind(this, view);

        if (data != null) {
             WebSettings settings = wvData.getSettings();
            // Enable Javascript
            settings.setJavaScriptEnabled(true);
            // Use WideViewport and Zoom out if there is no viewport defined
            settings.setUseWideViewPort(true);
            settings.setLoadWithOverviewMode(true);
            // Enable pinch to zoom without the zoom buttons
            settings.setBuiltInZoomControls(true);
            // Hide the zoom controls for HONEYCOMB+
            settings.setDisplayZoomControls(false);

            // Enable remote debugging via chrome://inspect
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                WebView.setWebContentsDebuggingEnabled(true);
            }
            if (getActivity() != null)
                ((HomeActivity) getActivity()).setToolbar(getString(R.string.nav_why_wasset));

            wvData.loadData(data, "text/html; charset=UTF-8", null);

        }
        return view;
    }




    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
