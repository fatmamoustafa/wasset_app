package com.wasset.ui.mainactivity

import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.view.LayoutInflater
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import com.wasset.R


class MenuItemAdapter(var items: List<String>) : BaseAdapter() {
    lateinit var onMenuItemClickListener: OnMenuItemClickListener

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var vieww = convertView

        if (vieww == null) {
            vieww =
                    LayoutInflater.from(parent!!.context).inflate(R.layout.rules, parent, false)
        }

        var s = getItem(position) as String
        var tvTitle: TextView = vieww!!.findViewById(R.id.menutext)
        var view: View = vieww!!.findViewById(R.id.view)
        tvTitle.text = s
        vieww.setOnClickListener {
            onMenuItemClickListener.onClick(s)
        }
        var typeface = ResourcesCompat.getFont(parent!!.context, R.font.cairo_regular)

        tvTitle.typeface = typeface

        return vieww
    }

    override fun getItem(p0: Int): Any {
        return items[p0]
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getCount(): Int {
        return items.size
    }

}