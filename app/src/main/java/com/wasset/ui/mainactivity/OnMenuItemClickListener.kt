package com.wasset.ui.mainactivity

interface OnMenuItemClickListener {
    fun onClick(name: String)
}