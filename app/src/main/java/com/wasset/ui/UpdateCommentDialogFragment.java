package com.wasset.ui;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.wasset.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ahmed Adel on 30/05/2019.
 * email : a3adel@hotmail.com
 */
public class UpdateCommentDialogFragment extends DialogFragment {
    @BindView(R.id.comment_et)
    EditText commentEditText;
    @BindView(R.id.send_button)
    Button sendButton;
    OnUpdateClickListener onUpdateClickListener;
    String oldComment = "";

    public static UpdateCommentDialogFragment newInstance(String oldComment) {
        Bundle bundle = new Bundle();
        bundle.putString("OLD_COMMENT", oldComment);
        UpdateCommentDialogFragment updateCommentDialogFragment = new UpdateCommentDialogFragment();
        updateCommentDialogFragment.setArguments(bundle);
        return updateCommentDialogFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            oldComment = getArguments().getString("OLD_COMMENT");
        }
    }

    public void setOnUpdateClickListener(OnUpdateClickListener onSendClickListener) {
        this.onUpdateClickListener = onSendClickListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_update_comment, container, false);
        ButterKnife.bind(this, view);
        commentEditText.setText(oldComment);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (commentEditText.getText().toString().length() == 0)
                    dismiss();
                else {
                    onUpdateClickListener.onCommentUpdated(commentEditText.getText().toString());
                    dismiss();
                }
            }
        });
        return view;
    }

    interface OnUpdateClickListener {
        void onCommentUpdated(String reason);
    }
}
