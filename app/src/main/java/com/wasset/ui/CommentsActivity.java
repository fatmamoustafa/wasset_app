package com.wasset.ui;

import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.wasset.R;
import com.wasset.adapter.AdapterComments;
import com.wasset.interfaces.ApiClient;
import com.wasset.interfaces.ApiInterface;
import com.wasset.models.Comments;
import com.wasset.models.FormalResponse;
import com.wasset.utility.QuickstartPreferences;
import com.wasset.utility.Utils;
import com.wasset.views.AVLoadingIndicatorDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cn.refactor.lib.colordialog.PromptDialog.DIALOG_TYPE_SUCCESS;
import static cn.refactor.lib.colordialog.PromptDialog.DIALOG_TYPE_WRONG;
import static com.wasset.utility.QuickstartPreferences.UNAUTHORIZED_STATUS_CODE;
import static com.wasset.utility.Utils.ShowLoginRequiredDialog;
import static com.wasset.utility.Utils.isLogin;

public class CommentsActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.tv_toolbar_title)
    AppCompatTextView tvToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.btnSend)
    AppCompatImageButton btnSend;
    @BindView(R.id.rv_chat)
    RecyclerView rvComments;
    @BindView(R.id.swipe_layout)
    SwipeRefreshLayout swipeLayout;
    @BindView(R.id.editWriteMessage)
    AppCompatEditText editWriteMessage;
    @BindView(R.id.content)
    RelativeLayout content;
    @BindView(R.id.tv_error)
    AppCompatTextView tvError;
    @BindView(R.id.btn_refresh)
    AppCompatButton btnRefresh;
    @BindView(R.id.rl_no_internet_connection)
    RelativeLayout rlNoInternetConnection;
    @BindView(R.id.iv_empty_icon)
    AppCompatImageView ivEmptyIcon;
    @BindView(R.id.tv_empty_text)
    AppCompatTextView tvEmptyText;
    @BindView(R.id.rl_empty_layout)
    LinearLayoutCompat rlEmptyLayout;
    private String PostId;
    private Call<Comments> CommentsCall;
    private List<Comments.CommentsBean> commentsBeanList;
    private AdapterComments adapterComments;
    private Call<Comments> SendCommentCall;
    private Call<FormalResponse> updateCall;
    private Call<FormalResponse> deleteCall;
    private AVLoadingIndicatorDialog indicatorView;
    String userId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);
        ButterKnife.bind(this);
        setToolbar();
        commentsBeanList = new ArrayList<>();
        swipeLayout.setOnRefreshListener(this);
        try {
            PostId = (String) getIntent().getExtras().get("Id");
            userId = getIntent().getStringExtra("USER_ID");
            //PostId = "1";
            getPostComments();
        } catch (Exception e) {
            e.printStackTrace();

        }
        setCommentsList();

    }


    private void setCommentsList() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        adapterComments = new AdapterComments(commentsBeanList, userId);
        rvComments.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvComments.getContext(), linearLayoutManager.getOrientation());
        rvComments.addItemDecoration(dividerItemDecoration);
        rvComments.setAdapter(adapterComments);

    }

    private void getPostComments() {


        if (!Utils.isNetworkAvailable()) {
            ShowErrorLayout(R.string.no_internet_connection);
            return;
        }

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        if (swipeLayout != null) {

            swipeLayout.setRefreshing(true);
        }

        CommentsCall = apiService.getPostComments(PostId);
        CommentsCall.enqueue(new Callback<Comments>() {
            @Override
            public void onResponse(Call<Comments> call, final Response<Comments> response) {
                System.out.println("raw" + response.raw());

                if (swipeLayout != null) {
                    swipeLayout.setRefreshing(false);
                }

                try {
                    if (response.body().isStatus() && response.body().getCode() == QuickstartPreferences.OK_STATUS_CODE) {
                        if (response.body() != null) {
                            if (response.body().getComments().size() > 0) {

                                commentsBeanList.clear();
                                commentsBeanList.addAll(response.body().getComments());
                                adapterComments.notifyDataSetChanged();

                                showContent();

                            } else {
                                showEmpty();
                            }


                        } else {
                            ShowErrorLayout(R.string.err_msg_server);
                        }

                    } else if (response.body().getCode() == UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(getApplicationContext(), R.string.Unauthorized, Toast.LENGTH_LONG).show();
                        Utils.LunchLogin(getApplicationContext());
                    } else {
                        ShowErrorLayout(R.string.err_msg_server);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    ShowErrorLayout(R.string.err_msg_server);
                }
            }

            @Override
            public void onFailure(Call<Comments> call, Throwable t) {
                if (!call.isCanceled() && !"Canceled".equals(t.getMessage())) {

                    if (swipeLayout != null) {
                        swipeLayout.setRefreshing(false);
                    }
                    t.printStackTrace();
                    ShowErrorLayout(R.string.err_msg_server);
                }
            }
        });


    }


    public void setToolbar() {
        tvToolbarTitle.setText(R.string.comments);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void showContent() {
        try {
            rvComments.setVisibility(View.VISIBLE);
            rlNoInternetConnection.setVisibility(View.GONE);
            rlEmptyLayout.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showEmpty() {
        try {
            rvComments.setVisibility(View.GONE);
            rlEmptyLayout.setVisibility(View.VISIBLE);
            rlNoInternetConnection.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ShowErrorLayout(int txt) {
        try {
            rvComments.setVisibility(View.GONE);
            rlNoInternetConnection.setVisibility(View.VISIBLE);
            rlEmptyLayout.setVisibility(View.GONE);
            tvError.setText(txt);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick({R.id.btnSend, R.id.btn_refresh})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnSend:

                if (!isLogin()) {
                    ShowLoginRequiredDialog(CommentsActivity.this);
                    return;
                }

                String comment = editWriteMessage.getText().toString().trim();
                if (comment.length() == 0) {
                    Utils.ShowDialog(CommentsActivity.this, getString(R.string.err), getString(R.string.err_comment_empty), DIALOG_TYPE_WRONG);
                    return;
                }

                sendComment(comment);

                break;
            case R.id.btn_refresh:
                getPostComments();
                break;
        }
    }

    private void sendComment(String comment) {

        if (!Utils.isNetworkAvailable()) {
            Utils.ShowNoInternetConnectionDialog(CommentsActivity.this);
            return;
        }

        Utils.HideKeyBord(CommentsActivity.this);
        showProgress();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        SendCommentCall = apiService.SendComment(PostId, comment);
        SendCommentCall.enqueue(new Callback<Comments>() {
            @Override
            public void onResponse(Call<Comments> call, final Response<Comments> response) {
                System.out.println("raw" + response.raw());
                hideProgress();
                try {
                    if (response.body().isStatus() && response.body().getCode() == QuickstartPreferences.OK_STATUS_CODE) {

                        Utils.ShowDialog(CommentsActivity.this, getString(R.string.success), response.body().getMessage(), DIALOG_TYPE_SUCCESS);
                        editWriteMessage.getText().clear();
                        commentsBeanList.clear();
                        commentsBeanList.addAll(response.body().getComments());
                        adapterComments.notifyDataSetChanged();


                    } else if (response.body().getCode() == UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(CommentsActivity.this, R.string.Unauthorized, Toast.LENGTH_LONG).show();
                        Utils.LunchLogin(CommentsActivity.this);

                    } else {
                        ShowErrorLayout(R.string.err_msg_server);
                    }


                } catch (Exception e) {

                    e.printStackTrace();
                    ShowErrorLayout(R.string.err_msg_server);
                }
            }


            @Override
            public void onFailure(Call<Comments> call, Throwable t) {
                t.printStackTrace();
                hideProgress();
                Utils.ShowServerDialog(CommentsActivity.this);
            }
        });
    }

    @Override
    public void onRefresh() {
        getPostComments();
    }


    private void hideProgress() {
        if (indicatorView != null && indicatorView.isShowing()) {
            indicatorView.dismiss();
        }
    }

    private void showProgress() {

        if (indicatorView == null) {
            indicatorView = new AVLoadingIndicatorDialog(CommentsActivity.this);
            //indicatorView.setMessage(msg);
            indicatorView.setCancelable(false);
        }
        indicatorView.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (CommentsCall != null) {
            CommentsCall.cancel();
        }
        if (SendCommentCall != null) {
            SendCommentCall.cancel();
        }


    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int position = -1;
        Log.d("CONTEXTMENU", "ASD");
        try {
            position = adapterComments.getPosition();

        } catch (Exception e) {
            return super.onContextItemSelected(item);
        }
        switch (item.getItemId()) {
            case 12349:
                deleteComment(position);
                break;
            case 12348:
                updateComment(position);
                break;
        }
        return super.onContextItemSelected(item);

    }

    private void updateComment(int position) {
        UpdateCommentDialogFragment updateCommentDialogFragment = UpdateCommentDialogFragment.newInstance(commentsBeanList.get(position).getComment_text());
        updateCommentDialogFragment.show(getSupportFragmentManager(), "");
        updateCommentDialogFragment.setOnUpdateClickListener(new UpdateCommentDialogFragment.OnUpdateClickListener() {
            @Override
            public void onCommentUpdated(String comment) {
                updateComment(comment, position);
            }
        });
    }

    private void deleteComment(int position) {
        if (!Utils.isNetworkAvailable()) {
            ShowErrorLayout(R.string.no_internet_connection);
            return;
        }
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        swipeLayout.setRefreshing(true);
        deleteCall = apiInterface.deleteComment(commentsBeanList.get(position).getComment_id());
        commentsBeanList.remove(position);

        deleteCall.enqueue(new Callback<FormalResponse>() {
            @Override
            public void onResponse(Call<FormalResponse> call, Response<FormalResponse> response) {
                swipeLayout.setRefreshing(false);
                try {
                    if (response.body().isStatus() && response.body().getCode() == QuickstartPreferences.UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(CommentsActivity.this, "برجاء تسجيل الدخول لتتمكن من الإبلاغ عن المحتوى", Toast.LENGTH_SHORT).show();
                    } else if (response.body().isStatus() && response.body().getCode() == QuickstartPreferences.OK_STATUS_CODE) {
                        Toast.makeText(CommentsActivity.this, "تم مسح التلعيق", Toast.LENGTH_SHORT).show();
                        adapterComments.notifyItemRemoved(position);
                    }
                } catch (Exception e) {
                    Utils.ShowServerDialog(CommentsActivity.this);
                }
            }

            @Override
            public void onFailure(Call<FormalResponse> call, Throwable t) {
                swipeLayout.setRefreshing(false);
                Utils.ShowServerDialog(CommentsActivity.this);

            }
        });

    }


    private void updateComment(String body, int position) {
        if (!Utils.isNetworkAvailable()) {
            ShowErrorLayout(R.string.no_internet_connection);
            return;
        }
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        swipeLayout.setRefreshing(true);
        updateCall = apiInterface.updateComment(commentsBeanList.get(position).getComment_id(), body);
        Comments.CommentsBean updatedComment = new Comments.CommentsBean();
        updatedComment = commentsBeanList.get(position);
        updatedComment.setComment_text(body);
        updateCall.enqueue(new Callback<FormalResponse>() {
            @Override
            public void onResponse(Call<FormalResponse> call, Response<FormalResponse> response) {
                swipeLayout.setRefreshing(false);
                try {
                    if (response.body().isStatus() && response.body().getCode() == QuickstartPreferences.UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(CommentsActivity.this, "برجاء تسجيل الدخول لتتمكن من الإبلاغ عن المحتوى", Toast.LENGTH_SHORT).show();
                    } else if (response.body().isStatus() && response.body().getCode() == QuickstartPreferences.OK_STATUS_CODE) {
                        Toast.makeText(CommentsActivity.this, "تم تحديث التلعيق", Toast.LENGTH_SHORT).show();
                        adapterComments.notifyItemChanged(position);
                    }
                } catch (Exception e) {
                    Utils.ShowServerDialog(CommentsActivity.this);
                }
            }

            @Override
            public void onFailure(Call<FormalResponse> call, Throwable t) {
                swipeLayout.setRefreshing(false);
                Utils.ShowServerDialog(CommentsActivity.this);

            }
        });
    }

}
