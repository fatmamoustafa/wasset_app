package com.wasset.ui;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;


import com.wasset.R;
import com.wasset.interfaces.ApiClient;
import com.wasset.interfaces.ApiInterface;
import com.wasset.models.FormalResponse;
import com.wasset.utility.Utils;
import com.wasset.utility.QuickstartPreferences;
import com.wasset.views.AVLoadingIndicatorDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cn.refactor.lib.colordialog.PromptDialog.DIALOG_TYPE_SUCCESS;
import static cn.refactor.lib.colordialog.PromptDialog.DIALOG_TYPE_WRONG;

public class ForgetPasswordActivity extends BaseActivity {


    @BindView(R.id.btn_recover_password)
    Button btnRecoverPassword;
    @BindView(R.id.et_email)
    EditText etEmail;
    private Call<FormalResponse> call;
    private AVLoadingIndicatorDialog indicatorView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_recover_password)
    public void onViewClicked() {
        checkForm();
    }

    private void checkForm() {

        String email = etEmail.getText().toString().trim();
 
        if (email.isEmpty()) {
            Utils.ShowDialog(ForgetPasswordActivity.this, getString(R.string.err), getString(R.string.err_msg_email), DIALOG_TYPE_WRONG);
            return;
        }

        if (!Utils.isValidMail(email)) {
            Utils.ShowDialog(ForgetPasswordActivity.this, getString(R.string.err), getString(R.string.err_msg_valid_email), DIALOG_TYPE_WRONG);

            return;
        }
        

        requestRecoverPassword();

    }

    private void requestRecoverPassword() {
        String email = etEmail.getText().toString();
        

        if (!Utils.isNetworkAvailable()) {
            Utils.ShowNoInternetConnectionDialog(ForgetPasswordActivity.this);
            return;
        }

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Utils.HideKeyBord(ForgetPasswordActivity.this);
        showProgress();
        call = apiService.forgotPassword(email);
        call.enqueue(new Callback<FormalResponse>() {
            @Override
            public void onResponse(Call<FormalResponse> call, final Response<FormalResponse> response) {

                hideProgress();
                     try {
                        FormalResponse formalResponse = response.body();
                        if (formalResponse.isStatus() && formalResponse.getCode() == QuickstartPreferences.OK_STATUS_CODE) {
                            Utils.ShowDialog(ForgetPasswordActivity.this, getString(R.string.success), formalResponse.getMessage(), DIALOG_TYPE_SUCCESS);

                        } else {
                            Utils.ShowDialog(ForgetPasswordActivity.this, getString(R.string.err), formalResponse.getMessage(), DIALOG_TYPE_WRONG);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.ShowServerDialog(ForgetPasswordActivity.this);
                    }
                }


            @Override
            public void onFailure(Call<FormalResponse> call, Throwable t) {
                if (!call.isCanceled() && !"Canceled".equals(t.getMessage())) {
                    hideProgress();
                    t.printStackTrace();
                    Utils.ShowServerDialog(ForgetPasswordActivity.this);
                }
            }
        });
    }


    private void hideProgress() {
        if (indicatorView != null && indicatorView.isShowing()) {
            indicatorView.dismiss();
        }
    }

    private void showProgress() {

        if (indicatorView == null) {
            indicatorView = new AVLoadingIndicatorDialog(ForgetPasswordActivity.this);
            //indicatorView.setMessage(msg);
            indicatorView.setCancelable(false);
        }
        indicatorView.show();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (call != null) {
            call.cancel();
        }

        hideProgress();
    }
}
