package com.wasset.ui.addpost

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.wasset.R

class LocationAdapter(context: Context, i: Int, tvName: Int, var list: ArrayList<String>) : ArrayAdapter<String>(context, i, tvName, list) {


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return rowView(convertView, position)
    }


    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return rowView(convertView, position)
    }

    private fun rowView(convertView: View?, position: Int): View {
        var convertView = convertView
        if (convertView == null) {
            //flater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = LayoutInflater.from(context).inflate(R.layout.spin_item, null, false)
        }

        val rowItem = getItem(position)
        val countryName = convertView!!.findViewById<TextView>(R.id.tvName)
        countryName.text = rowItem
        return convertView

    }


    override fun getCount(): Int {
        return list.size
    }

    override fun getItem(i: Int): String? {
        return list.get(i)
    }

}
