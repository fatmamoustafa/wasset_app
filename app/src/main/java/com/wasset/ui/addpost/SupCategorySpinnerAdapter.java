package com.wasset.ui.addpost;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.wasset.R;
import com.wasset.models.categoryList;

import java.util.List;

public class SupCategorySpinnerAdapter extends ArrayAdapter<categoryList.DataBean.SubCategoriesBean> {
    private LayoutInflater flater;
    private List<categoryList.DataBean.SubCategoriesBean> list;

    public SupCategorySpinnerAdapter(@NonNull Context context, int resource, int textViewResourceId, @NonNull List<categoryList.DataBean.SubCategoriesBean> objects) {
        super(context, resource, textViewResourceId, objects);
        this.list = objects;
        flater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return rowView(convertView, position);
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return rowView(convertView, position);
    }

    private View rowView(View convertView, int position) {
        if (convertView == null) {
            //flater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = flater.inflate(R.layout.spin_item, null, false);
        }
        if (getItem(position) != null) {
            String rowItem = getItem(position).getName();
            TextView countryName = convertView.findViewById(R.id.tvName);
            countryName.setText(rowItem);
        }
        return convertView;

    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public categoryList.DataBean.SubCategoriesBean getItem(int i) {
        return list.get(i);
    }

}
