package com.wasset.ui.addpost

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.util.Log
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.wasset.models.*
import com.wasset.ui.BaseActivity
import com.wasset.ui.CategoriesActivity
import kotlinx.android.synthetic.main.activity_add_post.*
import com.wasset.adapter.AdapterPostAttribute

import androidx.core.content.FileProvider
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.recyclerview.widget.DividerItemDecoration
import com.wasset.ui.search.SearchSpinnerAdapter
import kotlinx.android.synthetic.main.no_internet_connection_layout.*
import kotlinx.android.synthetic.main.toolbar_add_post.*

import android.view.View
import android.view.Window
import android.view.WindowManager
import android.webkit.WebView
import android.widget.Button
import android.widget.CheckBox
import android.widget.ImageButton
import android.widget.Toast

import com.crashlytics.android.Crashlytics
import com.google.gson.Gson
import com.wasset.R
import com.wasset.adapter.AdapterPostImages
import com.wasset.adapter.AdapterSelectedPostAttribute
import com.wasset.interfaces.ApiClient
import com.wasset.interfaces.ApiInterface
import com.wasset.interfaces.OnItemClickListener
import com.wasset.models.Config
import com.wasset.models.FormalResponse
import com.wasset.models.Login
import com.wasset.models.PostImage
import com.wasset.models.SelectedAttr
import com.wasset.models.postDetails
import com.wasset.utility.PreferenceEditor
import com.wasset.utility.QuickstartPreferences
import com.wasset.utility.Utils
import com.wasset.views.AVLoadingIndicatorDialog
import com.zhihu.matisse.Matisse
import com.zhihu.matisse.MimeType
import com.zhihu.matisse.engine.impl.GlideEngine
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.ArrayList
import java.util.Date
import java.util.HashMap

import cn.refactor.lib.colordialog.ColorDialog
import id.zelory.compressor.Compressor
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import cn.refactor.lib.colordialog.PromptDialog.DIALOG_TYPE_INFO
import cn.refactor.lib.colordialog.PromptDialog.DIALOG_TYPE_SUCCESS
import cn.refactor.lib.colordialog.PromptDialog.DIALOG_TYPE_WRONG
import com.wasset.utility.QuickstartPreferences.UNAUTHORIZED_STATUS_CODE

class AddPostActivity : BaseActivity(), SwipeRefreshLayout.OnRefreshListener {
    private var postImagesList: MutableList<PostImage?>? = null
    private var ImagesFile: File? = null
    private var outPutFile: File? = null
    private val mImageCaptureUri: Uri? = null
    private val CAMERA_CODE = 101
    val GALLERY_CODE = 201
    val CROPPING_CODE = 301
    val SELECT_CATEGORY_CODE = 401
    val POST_SETPS_CODE = 501
    private var adapterPostImages: AdapterPostImages? = null
    private val MaxPostImages = 10
    var textMap = HashMap<String, String?>()
    private var selectedAttrList: MutableList<SelectedAttr>? = null
    private var adapterSelectedPostAttr: AdapterSelectedPostAttribute? = null
    private var addPostCall: Call<FormalResponse>? = null
    private var indicatorView: AVLoadingIndicatorDialog? = null
    private var categoryId = -1
    private var categoryName: String? = null
    private var PostId: String? = null
    private var isEdit = false
    private var PostDetailsCall: Call<postDetails>? = null
    private var DeletePostImageCall: Call<FormalResponse>? = null
    private var login: Login.DataBean? = null
    private var pf: PreferenceEditor? = null
    private var mCurrentPhotoPath: String? = null
    private var GetCategoryListCall: Call<categoryList>? = null
    private var PostStepsCall: Call<PostSteps>? = null
    private var postStepsList = ArrayList<PostSteps.DataBean>()
    private var map = HashMap<String, String>()
    private var hashMap: HashMap<String, String>? = null
    var supCatList = ArrayList<categoryList.DataBean.SubCategoriesBean?>()
    lateinit var adapterSubCategories: SupCategorySpinnerAdapter
    var yearMap: HashMap<String, PostSteps.DataBean.FieldBean>? = null
    var statusMap: HashMap<String, PostSteps.DataBean.FieldBean>? = null
    var cityMap: HashMap<String, PostSteps.DataBean.FieldBean>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_post)
        pf = PreferenceEditor.getInstance()
        login = Gson().fromJson(pf!!.getStringPreference(QuickstartPreferences.USER_INFO), Login.DataBean::class.java)

        // Required empty public constructor
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        postImagesList = ArrayList()
        selectedAttrList = ArrayList()
        SetupPostImagesList()
        SetupPostAttrList()


        if (intent != null && intent.extras != null && intent.extras!!.get("Id") != null) {
            PostId = intent.extras!!.get("Id") as String
            println("PostId$PostId")
            getPostDetails()
            isEdit = true
        } else {
            isEdit = false
        }

        if (isEdit) {
            swipe.setEnabled(true)
            swipe.setOnRefreshListener(this)
        } else {
            swipe.setEnabled(false)
        }

        btn_delete.setVisibility(if (isEdit) View.GONE else View.VISIBLE)
        btn_add_post.setVisibility(if (isEdit) View.GONE else View.VISIBLE)
        btn_edit_post.setVisibility(if (!isEdit) View.GONE else View.VISIBLE)


        try {
            et_publisher_mobile.setText("" + login!!.getMobile_code() + login!!.getMobile_number())
            tv_currency.setText("" + login!!.getCurrency())
        } catch (e: Exception) {
            e.printStackTrace()
        }
        requestGetCategoryList()

        btn_edit_post.setOnClickListener {
            addPost()
        }
        btn_submit.setOnClickListener {
            addPost()
        }
        btn_add_post.setOnClickListener {
            addPost()
        }
        btn_close.setOnClickListener { closeScreen() }

        btn_delete.setOnClickListener { EmptyFieldsDialog() }

        if (btn_refresh != null)
            btn_refresh.setOnClickListener {
                if (isEdit) {
                    getPostDetails()
                }
            }
    }

    private fun requestDeletePostImage(pos: Int) {

        if (!Utils.isNetworkAvailable()) {
            Utils.ShowNoInternetConnectionDialog(this@AddPostActivity)
            return
        }

        showProgress()
        val apiService = ApiClient.getClient().create(ApiInterface::class.java)
        DeletePostImageCall = apiService.removePhoto(postImagesList!!.get(pos)!!.id)
        DeletePostImageCall!!.enqueue(object : Callback<FormalResponse> {
            override fun onResponse(call: Call<FormalResponse>, response: Response<FormalResponse>) {
                hideProgress()

                try {
                    if (response.body() != null) {
                        val formalResponse = response.body()
                        if (formalResponse!!.isStatus && formalResponse.code == QuickstartPreferences.OK_STATUS_CODE) {
                            //Toast.makeText(getApplicationContext(), formalResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            Utils.ShowDialog(this@AddPostActivity, getString(R.string.success), formalResponse.message, DIALOG_TYPE_SUCCESS)
                            postImagesList?.removeAt(pos)
                            adapterPostImages?.notifyItemRemoved(pos)


                        } else if (response.body()!!.code == QuickstartPreferences.UNAUTHORIZED_STATUS_CODE) {
                            Toast.makeText(applicationContext, R.string.Unauthorized, Toast.LENGTH_LONG).show()
                            Utils.LunchLogin(applicationContext)

                        } else {
                            Utils.ShowServerDialog(this@AddPostActivity)
                        }
                    } else {
                        Utils.ShowServerDialog(this@AddPostActivity)
                    }

                } catch (e: Exception) {
                    e.printStackTrace()

                    Utils.ShowServerDialog(this@AddPostActivity)
                }

            }


            override fun onFailure(call: Call<FormalResponse>, t: Throwable) {
                if (!call.isCanceled && "Canceled" != t.message) {
                    hideProgress()
                    t.printStackTrace()
                    Utils.ShowServerDialog(this@AddPostActivity)
                }
            }
        })


    }

    private fun showConfirmDeleteItem(pos: Int) {

        println("position$pos")
        val dialog = ColorDialog(this@AddPostActivity)
        dialog.setAnimationEnable(true)
        dialog.setTitle(getString(R.string.info))
        dialog.contentText = getString(R.string.msg_delete_post_image)
        //dialog.setContentImage(getResources().getDrawable(R.drawable.ic_warning));
        dialog.setPositiveListener(getString(R.string.yes)) { dialog ->
            if (postImagesList!!.get(pos)!!.type == AdapterPostImages.FILE_TYPE) {
                dialog.dismiss()
                postImagesList!!.removeAt(pos)
                adapterPostImages!!.notifyItemRemoved(pos)

            } else if (postImagesList!!.get(pos)?.type == AdapterPostImages.URL_TYPE) {
                dialog.dismiss()
                requestDeletePostImage(pos)

            }
        }
                .setNegativeListener(getString(R.string.Cancel)) { dialog1 -> dialog1.dismiss() }.show()
    }

    private fun SetupPostImagesList() {
        postImagesList?.add(null)
        val linearLayoutManager = LinearLayoutManager(applicationContext)
        linearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
        adapterPostImages = AdapterPostImages(this, postImagesList)
        rv_post_images.setLayoutManager(linearLayoutManager)
        rv_post_images.setAdapter(adapterPostImages)

        adapterPostImages!!.setListener(OnItemClickListener { itemView, position ->
            if (postImagesList!!.get(position) == null) {
                if (postImagesList!!.size < MaxPostImages) {
                    createImageDialog()
                } else {
                    Utils.ShowDialog(this@AddPostActivity, getString(R.string.info), getString(R.string.err_reach_to_max_post_images), DIALOG_TYPE_INFO)
                }
            } else {
                showConfirmDeleteItem(position)
            }
        })

    }

    private fun createImageDialog() {
        //outPutFile = new File(ImagesFile, System.currentTimeMillis() + ".jpg");

        val dialog = Dialog(this@AddPostActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.select_image_dialog)


        val lp = WindowManager.LayoutParams()
        val window = dialog.window
        lp.copyFrom(window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        window.attributes = lp


        val select_image_camera = dialog.findViewById<View>(R.id.select_image_camera) as ImageButton
        val select_image_gallery = dialog.findViewById<View>(R.id.select_image_gallery) as ImageButton


        select_image_camera.setOnClickListener(View.OnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this@AddPostActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this@AddPostActivity, arrayOf(Manifest.permission.CAMERA, Manifest.permission.CAMERA), 33)
                return@OnClickListener
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this@AddPostActivity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this@AddPostActivity, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE), 33)
                return@OnClickListener
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this@AddPostActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this@AddPostActivity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE), 33)
                return@OnClickListener
            }

            ImagesFile = File(Environment.getExternalStorageDirectory().toString() + "/" + Utils.FolderName)
            if (!ImagesFile!!.exists()) {
                ImagesFile!!.mkdir()
            }

            try {
                outPutFile = createImageFile()
                val photoUri = FileProvider.getUriForFile(this@AddPostActivity, "com.wasset.fileprovider", outPutFile!!)
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
                startActivityForResult(intent, CAMERA_CODE)

            } catch (e: IOException) {
                e.printStackTrace()
            }


            dialog.dismiss()
        })

        select_image_gallery.setOnClickListener(View.OnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this@AddPostActivity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this@AddPostActivity, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE), 33)
                return@OnClickListener
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this@AddPostActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this@AddPostActivity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE), 33)
                return@OnClickListener
            }

            ImagesFile = File(Environment.getExternalStorageDirectory().toString() + "/" + Utils.FolderName)
            if (!ImagesFile!!.exists()) {
                ImagesFile!!.mkdir()
            }
            outPutFile = File(ImagesFile, System.currentTimeMillis().toString() + ".jpg")

            Matisse.from(this@AddPostActivity)
                    .choose(MimeType.ofImage())
                    .countable(true)
                    .maxSelectable(MaxPostImages)
                    .gridExpectedSize(resources.getDimensionPixelSize(R.dimen.grid_expected_size))
                    .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                    .thumbnailScale(0.85f)
                    .imageEngine(GlideEngine())
                    .forResult(666)

            dialog.dismiss()
        })

        dialog.show()
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
                imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir      /* directory */
        )
        mCurrentPhotoPath = image.absolutePath

        return image
    }

    private fun SetupPostAttrList() {
        val linearLayoutManager = LinearLayoutManager(applicationContext)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        adapterSelectedPostAttr = AdapterSelectedPostAttribute(applicationContext, selectedAttrList)
        rv_post_attributes.layoutManager = linearLayoutManager
        val dividerItemDecoration = DividerItemDecoration(rv_post_attributes.getContext(), linearLayoutManager.orientation)
        rv_post_attributes.addItemDecoration(dividerItemDecoration)
        rv_post_attributes.setAdapter(adapterSelectedPostAttr)
        adapterSelectedPostAttr!!.setListener(OnItemClickListener { itemView, position ->
            val i = Intent(applicationContext, CategoriesActivity::class.java)
            i.putExtra("post_id", PostId)
            startActivityForResult(i, SELECT_CATEGORY_CODE)
        })

    }

    override fun onBackPressed() {
        closeScreen()
    }

    private fun closeScreen() {
        val dialog = ColorDialog(this@AddPostActivity)
        dialog.setAnimationEnable(true)
        dialog.setTitle(getString(R.string.info))
        dialog.contentText = getString(R.string.leave_create_post)
        //dialog.setContentImage(getResources().getDrawable(R.drawable.ic_warning));
        dialog.setPositiveListener(getString(R.string.yes)) { finish() }
                .setNegativeListener(getString(R.string.Cancel)) { dialog1 -> dialog1.dismiss() }.show()
    }

    private fun showAgreementDialog() {

        val dialog = Dialog(this@AddPostActivity)
        dialog.setContentView(R.layout.agreement_layout)
        val wvAgreement = dialog.findViewById<View>(R.id.wv_agreement) as WebView
        val cbAgreement = dialog.findViewById<View>(R.id.cb_accept) as CheckBox
        val btnAccept = dialog.findViewById<View>(R.id.btn_accept) as Button


        val settings = wvAgreement.settings
        // Enable Javascript
        settings.javaScriptEnabled = true
        // Use WideViewport and Zoom out if there is no viewport defined
        settings.useWideViewPort = true
        settings.loadWithOverviewMode = true
        // Enable pinch to zoom without the zoom buttons
        settings.builtInZoomControls = true
        // Hide the zoom controls for HONEYCOMB+
        settings.displayZoomControls = false

        // Enable remote debugging via chrome://inspect
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true)
        }

        val cnfg = Gson().fromJson(pf!!.getStringPreference(QuickstartPreferences.CONFIG), Config.DataBean::class.java)
        wvAgreement.loadData(cnfg.agreement, "text/html; charset=UTF-8", null)

        cbAgreement.setOnCheckedChangeListener { compoundButton, b ->
            btnAccept.isEnabled = b
            btnAccept.setBackgroundColor(if (b) resources.getColor(R.color.colorPrimary) else resources.getColor(R.color.disable_color))
        }


        btnAccept.setOnClickListener { view ->
            dialog.dismiss()
            requestAddPost()
        }


        dialog.setCancelable(true)

        val lp = WindowManager.LayoutParams()
        val window = dialog.window
        lp.copyFrom(window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT
        window.attributes = lp
        dialog.show()
    }

    private fun requestAddPost() {

        val postTitle = et_post_subject.getText().toString()
        val postDesc = et_post_desc.getText().toString()
        val salary = et_salary.getText().toString()
        val phone = et_publisher_mobile.getText().toString()

        if (!Utils.isNetworkAvailable()) {
            Utils.ShowNoInternetConnectionDialog(this@AddPostActivity)
            return
        }


        val mapp = HashMap<String, RequestBody>()
        val titleBody = RequestBody.create(MediaType.parse("text/plain"), postTitle)
        val descBody = RequestBody.create(MediaType.parse("text/plain"), postDesc)
        val salaryBody = RequestBody.create(MediaType.parse("text/plain"), salary)
        val phoneBody = RequestBody.create(MediaType.parse("text/plain"), phone)
        val cityBody = RequestBody.create(MediaType.parse("text/plain"), "1")
        val commentsEnabledBody = RequestBody.create(MediaType.parse("text/plain"), (!switch_comments.isChecked()).toString())
        val callEnabledBody = RequestBody.create(MediaType.parse("text/plain"), (!switch_phone_calls.isChecked()).toString())

        mapp["title"] = titleBody
        mapp["details"] = descBody
        mapp["price"] = salaryBody
        mapp["mobile_number"] = phoneBody
        mapp["city_id"] = cityBody
        mapp["comment_enable"] = commentsEnabledBody
        mapp["call_enable"] = callEnabledBody


        val keys = hashMap!!.keys
        for (i in keys) {
            println("Key : " + i + " Value : " + hashMap!![i])
            val AttrValue = RequestBody.create(MediaType.parse("text/plain"), hashMap!![i])
            mapp[i] = AttrValue
        }


        val ImagesParts = arrayOfNulls<MultipartBody.Part>(postImagesList!!.size)
        for (index in 1 until postImagesList!!.size) {
            println("Path : " + postImagesList!!.get(index)?.imageFile?.absolutePath)
            val file = postImagesList!!.get(index)?.imageFile

            if (file != null && file.exists()) {
                val surveyBody = RequestBody.create(MediaType.parse("image/*"), file)
                ImagesParts[index - 1] = MultipartBody.Part.createFormData("photo_$index", file.name, surveyBody)
            }
        }


        val apiService = ApiClient.getClient().create(ApiInterface::class.java)
        Utils.HideKeyBord(this@AddPostActivity)
        showProgress()
        addPostCall = apiService.addPost(mapp, ImagesParts)
        addPostCall!!.enqueue(object : Callback<FormalResponse> {
            override fun onResponse(call: Call<FormalResponse>, response: Response<FormalResponse>) {
                hideProgress()
                try {
                    val formalResponse = response.body()
                    if (formalResponse!!.isStatus && formalResponse.code == QuickstartPreferences.OK_STATUS_CODE) {
                        Toast.makeText(this@AddPostActivity, "" + formalResponse.message, Toast.LENGTH_SHORT).show()
                        finish()
                    } else if (!formalResponse.isStatus && formalResponse.code == QuickstartPreferences.VALIDATION_ERROR_CODE) {

                        val error = formalResponse.message.replace("-", "\n")
                        Utils.ShowDialog(this@AddPostActivity, getString(R.string.err), error, DIALOG_TYPE_WRONG)

                    } else {
                        Utils.ShowDialog(this@AddPostActivity, getString(R.string.err), formalResponse.message, DIALOG_TYPE_WRONG)
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                    Utils.ShowServerDialog(this@AddPostActivity)
                }

            }

            override fun onFailure(call: Call<FormalResponse>, t: Throwable) {
                if (!call.isCanceled && "Canceled" != t.message) {
                    hideProgress()
                    t.printStackTrace()
                    Utils.ShowServerDialog(this@AddPostActivity)
                }
            }
        })

    }

    private fun hideProgress() {
        if (indicatorView != null && indicatorView!!.isShowing()) {
            indicatorView!!.dismiss()
        }
    }

    private fun showProgress() {

        if (indicatorView == null) {
            indicatorView = AVLoadingIndicatorDialog(this@AddPostActivity)
            //indicatorView.setMessage(msg);
            indicatorView!!.setCancelable(false)
        }
        indicatorView!!.show()
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        println("requestCode$requestCode")
        println("resultCode$resultCode")
        println("data" + data)

        if (resultCode != Activity.RESULT_CANCELED) {
            if (resultCode == Activity.RESULT_OK) {

                if (requestCode == 666) {

                    val mSelected = Matisse.obtainResult(data)

                    for (uri in mSelected) {

                        Log.d("TAG" + ".PICK_GALLERY_IMAGE", "Selected image uri path :$uri")
                        val sourceFile = File(Utils.getPathFromGooglePhotosUri(this@AddPostActivity, uri))
                        val destFile = File(ImagesFile, "img_" + System.currentTimeMillis() + ".png")
                        println("sourceFile$sourceFile")
                        println("destFile$destFile")
                        val bmp = Utils.decodeFilee(sourceFile, destFile)

                        val postImage = PostImage()
                        postImage.type = AdapterPostImages.FILE_TYPE
                        postImage.imageFile = destFile
                        postImage.imageBitmap = bmp
                        postImagesList!!.add(postImage)
                        adapterPostImages!!.notifyItemInserted(postImagesList!!.size - 1)

                    }


                } else if (requestCode == GALLERY_CODE) {

                    /*
            try {
                mImageCaptureUri = data.getData();

                InputStream input = getContentResolver().openInputStream(data.getData());
                FileOutputStream output = new FileOutputStream(outPutFile);
                byte[] buffer = new byte[4096];
                int n = 0;
                while (-1 != (n = input.read(buffer))) {
                    output.write(buffer, 0, n);
                }
                mImageCaptureUri = Uri.fromFile(outPutFile);
                outPutFile = new File(ImagesFile, System.currentTimeMillis() + ".jpg");

                CropIMG();

            } catch (Exception e) {
                e.printStackTrace();
            }
            */


                    val clipData = data?.clipData

                    if (clipData == null) {

                        try {
                            val uri = data?.data


                            /* InputStream input = getContentResolver().openInputStream(uri);
                        File outPutFile = new File(ImagesFile, System.currentTimeMillis() + ".jpg");

                        FileOutputStream output = new FileOutputStream(outPutFile);
                        byte[] buffer = new byte[4096];
                        int n = 0;
                        while (-1 != (n = input.read(buffer))) {
                            output.write(buffer, 0, n);
                        }*/


                            Log.d("TAG" + ".PICK_GALLERY_IMAGE", "Selected image uri path :" + uri!!.toString())
                            val sourceFile = File(Utils.getPathFromGooglePhotosUri(this@AddPostActivity, uri))
                            val destFile = File(ImagesFile, "img_" + System.currentTimeMillis() + ".png")
                            println("sourceFile$sourceFile")
                            println("destFile$destFile")

                            val bmp = Utils.decodeFilee(sourceFile, destFile)
                            val postImage = PostImage()
                            postImage.type = AdapterPostImages.FILE_TYPE
                            postImage.imageFile = destFile
                            postImage.imageBitmap = bmp
                            postImagesList!!.add(postImage)
                            adapterPostImages!!.notifyItemInserted(postImagesList!!.size - 1)

                        } catch (e: Exception) {
                            e.printStackTrace()
                            Crashlytics.logException(e)
                        }


                    } else
                        for (i in 0 until data.clipData!!.itemCount) {

                            try {

                                val uri = data.clipData!!.getItemAt(i).uri
                                Log.d("TAG" + ".PICK_GALLERY_IMAGE", "Selected image uri path :$uri")
                                val sourceFile = File(Utils.getPathFromGooglePhotosUri(this@AddPostActivity, uri))
                                val destFile = File(ImagesFile, "img_" + System.currentTimeMillis() + ".png")
                                println("sourceFile$sourceFile")
                                println("destFile$destFile")
                                val bmp = Utils.decodeFilee(sourceFile, destFile)

                                val postImage = PostImage()
                                postImage.type = AdapterPostImages.FILE_TYPE
                                postImage.imageFile = destFile
                                postImage.imageBitmap = bmp
                                postImagesList!!.add(postImage)
                                adapterPostImages!!.notifyItemInserted(postImagesList!!.size - 1)


                            } catch (e: Exception) {
                                e.printStackTrace()
                                Crashlytics.logException(e)
                            }

                        }


                } else if (requestCode == CAMERA_CODE) {
                    try {
                        //CropIMG();
                        val path = File(this.filesDir, mCurrentPhotoPath)
                        if (!path.exists())
                            path.mkdirs()
                        if (outPutFile!!.exists()) {
                            val postImage = PostImage()
                            postImage.type = AdapterPostImages.FILE_TYPE
                            val file = File(mCurrentPhotoPath)
                            postImage.imageFile = file
                            val bitmap = Compressor(this@AddPostActivity).compressToBitmap(file)
                            postImage.imageBitmap = bitmap
                            postImagesList!!.add(postImage)
                            adapterPostImages!!.notifyItemInserted(postImagesList!!.size - 1)

                        } else {

                            Toast.makeText(this, R.string.image_not_found, Toast.LENGTH_SHORT).show()
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                } else if (requestCode == CROPPING_CODE) {

                    Log.d("TAG", "CROPPING_CODE")
                    /* try {
                if (outPutFile.exists()) {

                    System.out.println("outPutFile" + outPutFile.getAbsolutePath());
                    PostImage postImage = new PostImage();
                    postImage.setType(AdapterPostImages.FILE_TYPE);
                    postImage.setImageFile(outPutFile);
                    postImagesList.add(postImage);
                    adapterPostImages.notifyItemInserted(postImagesList.size() - 1);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }*/


                } else if (requestCode == SELECT_CATEGORY_CODE) {
                    categoryId = data?.extras!!.get("Id") as Int
                    categoryName = data?.extras!!.get("Name") as String
                    //categoryId = 21;
//                    val i = Intent(applicationContext, AddPostAttributeActivity::class.java)
//                    i.putExtra("categoryId", categoryId)
//                    i.putExtra("post_id", PostId)
//                    startActivityForResult(i, POST_SETPS_CODE)
                    getPostSteps(categoryId, PostId)
                } else if (requestCode == POST_SETPS_CODE) {

                    hashMap = data?.getSerializableExtra("map") as HashMap<String, String>
                    textMap = data?.getSerializableExtra("textMap") as HashMap<String, String?>
                    hashMap!!["category_id"] = categoryId.toString()
                    textMap!![getString(R.string.category)] = categoryName
                    selectedAttrList!!.clear()
                    val keys2 = textMap!!.keys  //get all keys
                    for (i in keys2) {
                        selectedAttrList!!.add(0, SelectedAttr(i, textMap!![i]))
                        println("Key_Alt : " + i + " Value_Alt : " + textMap!![i])
                    }
                }

            }
        }
    }

    private fun getPostDetails() {
        if (!Utils.isNetworkAvailable()) {
            ShowErrorLayout(R.string.no_internet_connection)
            return
        }

        val apiService = ApiClient.getClient().create(ApiInterface::class.java)
        swipe.setRefreshing(true)

        PostDetailsCall = apiService.getPostDetails(PostId)
        PostDetailsCall!!.enqueue(object : Callback<postDetails> {
            override fun onResponse(call: Call<postDetails>, response: Response<postDetails>) {
                println("raw" + response.raw())
                swipe.isRefreshing = false

                try {
                    if (response.body()!!.isStatus && response.body()!!.code == QuickstartPreferences.OK_STATUS_CODE) {
                        if (response.body()!!.data != null) {
                            val post = response.body()!!.data
                            showContent()

                            et_post_subject.setText(post.title)
                            et_post_desc.setText(post.details)
                            et_publisher_mobile.setText(post.mobile_number)
                            et_salary.setText(post.price)
                            selectedAttrList!!.clear()

                            switch_comments.setChecked(!post.isComment_enable)
                            switch_phone_calls.setChecked(!post.isCall_enable)
                            for (i in post.attributes) {
                                selectedAttrList!!.add(SelectedAttr(i.key, i.value))

                                if (i.key == "city_id") {
                                    location_tv.visibility = View.VISIBLE
                                    et_location.visibility = View.VISIBLE
                                    var c = i.value
//                                    et_location.setSelection()
                                }
                                if (i.key == "year_id") {
                                    pro_year_tx.visibility = View.VISIBLE
                                    et_pro_year.visibility = View.VISIBLE
                                    var y = i.value
                                }
                                if (i.key == "used_type") {
                                    status_tx.visibility = View.VISIBLE
                                    status_gr.visibility = View.VISIBLE
                                    var t = i.value
                                    if (t.contentEquals(getString(R.string.used))) {
                                        new_state.isChecked = true
                                    } else {
                                        used_state.isChecked = true
                                    }
                                }
                            }

                            adapterSelectedPostAttr!!.notifyDataSetChanged()
                            postImagesList!!.clear()
                            postImagesList?.add(null)
                            for (url in post.photos) {
                                val postImage = PostImage()
                                postImage.type = AdapterPostImages.URL_TYPE
                                postImage.url = url.url
                                postImagesList!!.add(postImage)
                            }
//                            adapterPostImages=AdapterPostImages(this@AddPostActivity,postImagesList)
                            adapterPostImages!!.notifyDataSetChanged()

                        } else {
                            ShowErrorLayout(R.string.err_msg_server)
                        }

                    } else if (response.body()!!.code == UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(applicationContext, R.string.Unauthorized, Toast.LENGTH_LONG).show()
                        Utils.LunchLogin(applicationContext)
                    } else {
                        ShowErrorLayout(R.string.err_msg_server)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    ShowErrorLayout(R.string.err_msg_server)
                }

            }

            override fun onFailure(call: Call<postDetails>, t: Throwable) {
                if (!call.isCanceled && "Canceled" != t.message) {

                    swipe.setRefreshing(false)
                    t.printStackTrace()
                    ShowErrorLayout(R.string.err_msg_server)
                }
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()

        if (addPostCall != null) {
            addPostCall!!.cancel()
        }

        if (PostDetailsCall != null) {
            PostDetailsCall!!.cancel()
        }

        if (DeletePostImageCall != null) {
            DeletePostImageCall!!.cancel()
        }
    }

    override fun onRefresh() {
        if (isEdit) {
            getPostDetails()
        } else {
            swipe.setRefreshing(false)
        }
    }

    private fun showContent() {
        try {
            //            nsvContent.setVisibility(View.VISIBLE);
            rl_no_internet_connection.setVisibility(View.GONE)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun ShowErrorLayout(txt: Int) {
        try {
            //            nsv_content.setVisibility(View.GONE);
            rl_no_internet_connection.setVisibility(View.VISIBLE)
            tv_error.setText(txt)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun addPost() {
        val postTitle = et_post_subject.getText().toString()
        val postDesc = et_post_desc.getText().toString()
        val salary = et_salary.getText().toString()
        val phone = et_publisher_mobile.getText().toString()

        if (postImagesList!!.size <= 1) {
            Utils.ShowDialog(this@AddPostActivity, getString(R.string.err), getString(R.string.add_post_image), DIALOG_TYPE_WRONG)
            return
        }

        if (!isEdit && (map == null || map!!.size == 0)) {
            Utils.ShowDialog(this@AddPostActivity, getString(R.string.err), getString(R.string.add_post_attr), DIALOG_TYPE_WRONG)
            return
        }

        if (postTitle.isEmpty()) {
            Utils.ShowDialog(this@AddPostActivity, getString(R.string.err), getString(R.string.add_post_title), DIALOG_TYPE_WRONG)
            return
        }


        if (postDesc.isEmpty()) {
            Utils.ShowDialog(this@AddPostActivity, getString(R.string.err), getString(R.string.add_post_desc), DIALOG_TYPE_WRONG)
            return
        }

        /* if (salary.isEmpty()) {
              Utils.ShowDialog(AddPostActivity.this, getString(R.string.err), getString(R.string.err_msg_salary), DIALOG_TYPE_WRONG);
              return;
        }

        if (phone.isEmpty()) {
            Utils.ShowDialog(AddPostActivity.this, getString(R.string.err), getString(R.string.err_msg_phone_number), DIALOG_TYPE_WRONG);
            return;
        }
        */

        if (isEdit) {
            requestEditPost()
        } else {
            showAgreementDialog()
        }
    }

    private fun requestEditPost() {

        val postTitle = et_post_subject.getText().toString()
        val postDesc = et_post_desc.getText().toString()
        val salary = et_salary.getText().toString()
        val phone = et_publisher_mobile.getText().toString()

        if (!Utils.isNetworkAvailable()) {
            Utils.ShowNoInternetConnectionDialog(this@AddPostActivity)
            return
        }


        val mapp = HashMap<String, RequestBody>()
        val PostIdBody = RequestBody.create(MediaType.parse("text/plain"), PostId)
        val titleBody = RequestBody.create(MediaType.parse("text/plain"), postTitle)
        val descBody = RequestBody.create(MediaType.parse("text/plain"), postDesc)
        val salaryBody = RequestBody.create(MediaType.parse("text/plain"), salary)
        val phoneBody = RequestBody.create(MediaType.parse("text/plain"), phone)
        val cityBody = RequestBody.create(MediaType.parse("text/plain"), "1")
        val enableCommentsBody = RequestBody.create(MediaType.parse("text/plain"), (!switch_comments.isChecked()).toString())
        val enablePhoneBody = RequestBody.create(MediaType.parse("text/plain"), (!switch_phone_calls.isChecked()).toString())

        mapp["post_id"] = PostIdBody
        mapp["title"] = titleBody
        mapp["details"] = descBody
        mapp["price"] = salaryBody
        mapp["mobile_number"] = phoneBody
        mapp["city_id"] = cityBody
        mapp["comment_enable"] = enableCommentsBody
        mapp["call_enable"] = enablePhoneBody


        if (hashMap != null) {
            val keys = hashMap!!.keys  //get all keys
            for (i in keys) {
                println("Key : " + i + " Value : " + hashMap!![i])
                val AttrValue = RequestBody.create(MediaType.parse("text/plain"), hashMap!![i])
                mapp[i] = AttrValue
            }
        }

        val ImagesParts = arrayOfNulls<MultipartBody.Part>(postImagesList!!.size)

        var index = 0
        for (postImage in postImagesList!!) {
            if (postImage != null && postImage!!.getType() == AdapterPostImages.FILE_TYPE && postImage!!.getImageFile() != null && postImage!!.getImageFile().exists()) {
                val surveyBody = RequestBody.create(MediaType.parse("image/*"), postImage!!.getImageFile())
                ImagesParts[index] = MultipartBody.Part.createFormData("photo_" + (index + 1), postImage!!.getImageFile().getName(), surveyBody)
                index++
            }
        }

        val apiService = ApiClient.getClient().create(ApiInterface::class.java)
        Utils.HideKeyBord(this@AddPostActivity)
        showProgress()
        addPostCall = apiService.editPost(mapp, ImagesParts)
        addPostCall!!.enqueue(object : Callback<FormalResponse> {
            override fun onResponse(call: Call<FormalResponse>, response: Response<FormalResponse>) {

                hideProgress()

                try {
                    val formalResponse = response.body()
                    if (formalResponse!!.isStatus && formalResponse.code == QuickstartPreferences.OK_STATUS_CODE) {
                        Toast.makeText(this@AddPostActivity, "" + formalResponse.message, Toast.LENGTH_SHORT).show()
                        finish()
                    } else {
                        Utils.ShowDialog(this@AddPostActivity, getString(R.string.err), formalResponse.message, DIALOG_TYPE_WRONG)
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                    Utils.ShowServerDialog(this@AddPostActivity)
                }

            }


            override fun onFailure(call: Call<FormalResponse>, t: Throwable) {
                if (!call.isCanceled && "Canceled" != t.message) {
                    hideProgress()
                    t.printStackTrace()
                    Utils.ShowServerDialog(this@AddPostActivity)
                }
            }
        })

    }

    private fun EmptyFieldsDialog() {
        val dialog = ColorDialog(this@AddPostActivity)
        dialog.setAnimationEnable(true)
        dialog.setTitle(getString(R.string.err_empty_fields_title))
        dialog.contentText = getString(R.string.err_empty_fields_body)
        //dialog.setContentImage(getResources().getDrawable(R.drawable.ic_warning));
        dialog.setPositiveListener(getString(R.string.Ok)) { dialog ->
            dialog.dismiss()

            EmptyFields()
        }
                .setNegativeListener(getString(R.string.Cancel)) { dialog -> dialog.dismiss() }.show()
    }

    private fun EmptyFields() {
        et_post_subject.getText().clear()
        et_post_desc.getText().clear()
        et_salary.getText().clear()
        et_publisher_mobile.getText().clear()
        postImagesList!!.clear()
        postImagesList?.add(null)
        adapterPostImages!!.notifyDataSetChanged()
        if (map != null) {
            map!!.clear()
        }
        if (hashMap != null) {
            hashMap!!.clear()
        }

        if (textMap != null) {
            textMap!!.clear()
        }
        selectedAttrList!!.clear()
        adapterSelectedPostAttr!!.notifyDataSetChanged()


    }

    private fun requestGetCategoryList() {
        if (!Utils.isNetworkAvailable()) {
            ShowErrorLayout(R.string.no_internet_connection)
            return
        }

        val apiService = ApiClient.getClient().create(ApiInterface::class.java)
        GetCategoryListCall = apiService.getCategoryList(null)
        GetCategoryListCall!!.enqueue(object : Callback<categoryList> {
            override fun onResponse(call: Call<categoryList>, response: Response<categoryList>) {
                println("raw" + response.raw())
                try {
                    if (response.body()!!.isStatus && response.body()!!.code == QuickstartPreferences.OK_STATUS_CODE) {
                        if (response.body()!!.data.size > 0) {
                            val spinnerList = ArrayList<String>()
                            spinnerList.add("اختر النوع")

                            val CategoriesList = response.body()!!.data
                            for (i in CategoriesList.indices) {
                                val item = CategoriesList[i]
                                spinnerList.add(item.name)
                            }

                            val dataAdapter = SearchSpinnerAdapter(this@AddPostActivity, R.layout.spin_item, R.id.tvName, spinnerList)
                            //dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            tv_what_you_want_to_advertise.adapter = dataAdapter
                            tv_what_you_want_to_advertise.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                                override fun onItemSelected(adapterView: AdapterView<*>, view: View, pos: Int, l: Long) {
                                    if (pos != 0) {
                                        supCatList.clear()
                                        supCatList.add(null)
                                        if (CategoriesList[pos - 1].sub_categories.size > 0) {
                                            if (CategoriesList[pos - 1].category_id != null) {
                                                categoryId = Integer.parseInt(CategoriesList[pos - 1].category_id)
                                                map!!.put("category_id", CategoriesList[pos - 1].category_id)
                                                for (i in CategoriesList[pos - 1].sub_categories) {
                                                    supCatList.add(i)
                                                    adapterSubCategories = SupCategorySpinnerAdapter(this@AddPostActivity, R.layout.spin_item, R.id.tvName, supCatList)
                                                    tv_what_you_want_to_advertise_type.adapter = (adapterSubCategories)
                                                }
                                            }
                                        }
                                    }
                                }

                                override fun onNothingSelected(adapterView: AdapterView<*>) {

                                }
                            }

                            tv_what_you_want_to_advertise_type.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                                override fun onItemSelected(adapterView: AdapterView<*>, view: View, pos: Int, l: Long) {
                                    if (supCatList[pos] != null) {
                                        textMap!![getString(R.string.category)] = supCatList[pos]!!.name
                                        getPostSteps(Integer.parseInt(supCatList[pos]!!.category_id), PostId)
                                    }
                                }

                                override fun onNothingSelected(adapterView: AdapterView<*>) {

                                }

                            }
                        }

                    } else if (response.body()!!.code == UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(applicationContext, R.string.Unauthorized, Toast.LENGTH_LONG).show()
                        Utils.LunchLogin(applicationContext)
                    } else {
                        Toast.makeText(this@AddPostActivity, R.string.err_msg_server, Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    Toast.makeText(this@AddPostActivity, R.string.err_msg_server, Toast.LENGTH_SHORT).show()
                }

            }

            override fun onFailure(call: Call<categoryList>, t: Throwable) {
                if (!call.isCanceled && "Canceled" != t.message) {
                    t.printStackTrace()
                    Toast.makeText(this@AddPostActivity, R.string.err_msg_server, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun getPostSteps(categoryId: Int, PostId: String?) {

        if (!Utils.isNetworkAvailable()) {
            ShowErrorLayout(R.string.no_internet_connection)
            return
        }

        val apiService = ApiClient.getClient().create(ApiInterface::class.java)
        PostStepsCall = apiService.getPostSteps(categoryId, PostId)
        PostStepsCall!!.enqueue(object : Callback<PostSteps> {
            override fun onResponse(call: Call<PostSteps>, response: Response<PostSteps>) {
                println("raw" + response.raw())

                try {
                    if (response.body()!!.isStatus && response.body()!!.code == QuickstartPreferences.OK_STATUS_CODE) {
                        if (response.body()!!.data != null) {
                            if (response.body()!!.data.size > 0) {
                                showContent()
                                postStepsList!!.clear()
                                yearMap = null
                                statusMap = null
                                cityMap = null
                                hashMap = null
                                for (i in response.body()!!.data) {
                                    if (i.title.equals("سنة الصنع")) {
                                        yearMap = HashMap<String, PostSteps.DataBean.FieldBean>()
                                        yearMap!!.put(i.title, i.field)
                                    }
                                    if (i.title.equals("الحالة")) {
                                        statusMap = HashMap<String, PostSteps.DataBean.FieldBean>()
                                        statusMap!!.put(i.title, i.field)
                                    }
                                    if (i.title.equals("المدينة")) {
                                        cityMap = HashMap<String, PostSteps.DataBean.FieldBean>()
                                        cityMap!!.put(i.title, i.field)
                                    }
                                }
                                postStepsList!!.addAll(response.body()!!.data)
                                setupList()
                            } else {
//                                hashMap = data.getSerializableExtra("map")
//                                textMap = data.getSerializableExtra("textMap")
                                map!!["category_id"] = categoryId.toString()
                                textMap!![getString(R.string.category)] = categoryName
                                selectedAttrList!!.clear()
                                val keys2 = textMap!!.keys  //get all keys
                                for (i in keys2) {
                                    selectedAttrList!!.add(0, SelectedAttr(i, textMap!![i]))
                                    println("Key_Alt : " + i + " Value_Alt : " + textMap!![i])
                                }

                            }
                        } else {
                            ShowErrorLayout(R.string.err_msg_server)
                        }

                    } else if (response.body()!!.code == UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(applicationContext, R.string.Unauthorized, Toast.LENGTH_LONG).show()
                        Utils.LunchLogin(applicationContext)
                    } else {
                        ShowErrorLayout(R.string.err_msg_server)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    println(e.message)
                    ShowErrorLayout(R.string.err_msg_server)
                }

            }

            override fun onFailure(call: Call<PostSteps>, t: Throwable) {
                if (!call.isCanceled && "Canceled" != t.message) {
                    t.printStackTrace()
                    ShowErrorLayout(R.string.err_msg_server)
                }
            }
        })
    }


    private fun setupList() {
        hashMap = HashMap<String, String>()
        hashMap!!["category_id"] = categoryId.toString()
        if (cityMap != null) {
            location_tv.visibility = View.VISIBLE
            et_location.visibility = View.VISIBLE
            val list = cityMap!!.get("المدينة")
            val spinnerList = ArrayList<String>()
            spinnerList.add("اختر المدينة")
            for (i in list!!.values) {
                spinnerList.add(i.name)
            }
            et_location.adapter = LocationAdapter(this@AddPostActivity, R.layout.spin_item, R.id.tvName, spinnerList)
            et_location.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(p0: AdapterView<*>?) {

                }

                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    if (p2 != 0)
                        hashMap!!.put("city_id", list.values[p2 - 1].id.toString())
                }
            }
        } else {
            location_tv.visibility = View.GONE
            et_location.visibility = View.GONE
            hashMap!!.remove("city_id")
        }
        if (yearMap != null) {
            pro_year_tx.visibility = View.VISIBLE
            et_pro_year.visibility = View.VISIBLE
            val list = yearMap!!.get("سنة الصنع")
            val spinnerList = ArrayList<String>()
            spinnerList.add("اختر سنة الصنع")
            for (i in list!!.values) {
                spinnerList.add(i.name)
            }
            et_pro_year.adapter = LocationAdapter(this@AddPostActivity, R.layout.spin_item, R.id.tvName, spinnerList)
            et_pro_year.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(p0: AdapterView<*>?) {

                }

                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    if (p2 != 0)
                        hashMap!!.put("year_id", list.values[p2-1].id.toString())
                }
            }
        } else {
            pro_year_tx.visibility = View.GONE
            et_pro_year.visibility = View.GONE
            hashMap!!.remove("year_id")
        }

        if (statusMap != null) {
            status_tx.visibility = View.VISIBLE
            status_gr.visibility = View.VISIBLE
            val list = statusMap!!.get("الحالة")
            if (list!!.values[0].isSelected) {
                used_state.isChecked = true
                hashMap!!.put("used_type", "1")
            } else if (list!!.values[1].isSelected) {
                new_state.isChecked = true
                hashMap!!.put("used_type", "2")
            } else {
                status_gr.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
                    override fun onCheckedChanged(p0: RadioGroup?, p1: Int) {
                        when (p1) {
                            R.id.used_state -> hashMap!!.put("used_type", "1")
                            R.id.new_state -> hashMap!!.put("used_type", "2")
                        }
                    }
                })
            }
        } else {
            status_tx.visibility = View.GONE
            status_gr.visibility = View.GONE
            hashMap!!.remove("used_type")
        }


        for (item in postStepsList!!) {
            if (item.getField().getAttribute().equals("model_id", ignoreCase = true)) {
                requestModelsList(map!!.get("brand_id")!!, item.getField())
                return
            }
        }
    }


    private fun requestModelsList(brandId: String, item: PostSteps.DataBean.FieldBean) {
        if (!Utils.isNetworkAvailable()) {
            ShowErrorLayout(R.string.no_internet_connection)
            return
        }

        val apiService = ApiClient.getClient().create(ApiInterface::class.java)
        val call = apiService.getModels(brandId, PostId)
        call.enqueue(object : Callback<Models> {
            override fun onResponse(call: Call<Models>, response: Response<Models>) {
                println("raw" + response.raw())

                try {
                    if (response.body()!!.isStatus && response.body()!!.code == QuickstartPreferences.OK_STATUS_CODE) {
                        if (response.body()!!.data.size > 0) {
                            showContent()

                            item.values = response.body()!!.data
                            val linearLayoutManager = LinearLayoutManager(applicationContext)
                            val adapterPostAttribute = AdapterPostAttribute(this@AddPostActivity, item)
//                            rvPostItemAttributes.setLayoutManager(linearLayoutManager)
//                            rvPostItemAttributes.setAdapter(adapterPostAttribute)
//                            adapterPostAttribute.setOnAttrItemSelectListener(this@AddPostActivity)

                        } else {
//                            FinishSteps()
                        }

                    } else if (response.body()!!.code == UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(applicationContext, R.string.Unauthorized, Toast.LENGTH_LONG).show()
                        Utils.LunchLogin(applicationContext)
                    } else {
                        ShowErrorLayout(R.string.err_msg_server)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    ShowErrorLayout(R.string.err_msg_server)
                }

            }


            override fun onFailure(call: Call<Models>, t: Throwable) {
                if (!call.isCanceled && "Canceled" != t.message) {

                    //swipeLayout.setRefreshing(false);
                    t.printStackTrace()
                    ShowErrorLayout(R.string.err_msg_server)
                }
            }
        })
    }

}
