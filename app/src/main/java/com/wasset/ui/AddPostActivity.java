package com.wasset.ui;

import android.Manifest;
import android.app.Dialog;
import android.content.ClipData;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.widget.NestedScrollView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.wasset.R;
import com.wasset.adapter.AdapterPostImages;
import com.wasset.adapter.AdapterSelectedPostAttribute;
import com.wasset.interfaces.ApiClient;
import com.wasset.interfaces.ApiInterface;
import com.wasset.interfaces.OnItemClickListener;
import com.wasset.models.Config;
import com.wasset.models.FormalResponse;
import com.wasset.models.Login;
import com.wasset.models.PostDataBean;
import com.wasset.models.PostImage;
import com.wasset.models.SelectedAttr;
import com.wasset.models.postDetails;
import com.wasset.utility.PreferenceEditor;
import com.wasset.utility.QuickstartPreferences;
import com.wasset.utility.Utils;
import com.wasset.views.AVLoadingIndicatorDialog;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.refactor.lib.colordialog.ColorDialog;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cn.refactor.lib.colordialog.PromptDialog.DIALOG_TYPE_INFO;
import static cn.refactor.lib.colordialog.PromptDialog.DIALOG_TYPE_SUCCESS;
import static cn.refactor.lib.colordialog.PromptDialog.DIALOG_TYPE_WRONG;
import static com.wasset.utility.QuickstartPreferences.UNAUTHORIZED_STATUS_CODE;

public class AddPostActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.btn_close)
    ImageButton btnClose;
    @BindView(R.id.tv_toolbar_title)
    TextView tvToolbarTitle;
    @BindView(R.id.btn_submit)
    ImageButton btnSubmit;
    @BindView(R.id.btn_delete)
    ImageButton btnDelete;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rv_post_images)
    RecyclerView rvPostImages;
    @BindView(R.id.tv_max_post_images)
    TextView tvMaxPostImages;
    @BindView(R.id.rv_post_attributes)
    RecyclerView rvPostAttributes;
    @BindView(R.id.tv_what_you_want_to_advertise)
    Spinner tvWhatYouWantToAdvertise;
    @BindView(R.id.et_post_subject)
    EditText etPostSubject;
    @BindView(R.id.et_post_desc)
    EditText etPostDesc;
    @BindView(R.id.et_publisher_mobile)
    EditText etPublisherMobile;
    @BindView(R.id.btn_add_post)
    Button btnAddPost;
    @BindView(R.id.et_salary)
    EditText etSalary;
    @BindView(R.id.btn_edit_post)
    Button btnEditPost;
    @BindView(R.id.nsv_content)
    NestedScrollView nsvContent;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeLayout;
    @BindView(R.id.tv_error)
    TextView tvError;
    @BindView(R.id.btn_refresh)
    Button btnRefresh;
    @BindView(R.id.rl_no_internet_connection)
    RelativeLayout rlNoInternetConnection;
    @BindView(R.id.tv_currency)
    TextView tvCurrency;
    @BindView(R.id.switch_comments)
    Switch commentsSwitch;
    @BindView(R.id.switch_phone_calls)
    Switch phoneCallsSwitch;

    private List<PostImage> postImagesList;
    private File ImagesFile;
    private File outPutFile;
    private Uri mImageCaptureUri;
    private static final int CAMERA_CODE = 101, GALLERY_CODE = 201, CROPPING_CODE = 301, SELECT_CATEGORY_CODE = 401, POST_SETPS_CODE = 501;
    private AdapterPostImages adapterPostImages;
    private int MaxPostImages = 10;
    private HashMap<String, String> hashMap, textMap;
    private List<SelectedAttr> selectedAttrList;
    private AdapterSelectedPostAttribute adapterSelectedPostAttr;
    private Call<FormalResponse> addPostCall;
    private AVLoadingIndicatorDialog indicatorView;
    private int categoryId = -1;
    private String categoryName = null;
    private String PostId;
    private boolean isEdit = false;
    private Call<postDetails> PostDetailsCall;
    private Call<FormalResponse> DeletePostImageCall;
    private Login.DataBean login;
    private PreferenceEditor pf;
    private String mCurrentPhotoPath;

    @Override
    public void onBackPressed() {
        closeScreen();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_post);
        ButterKnife.bind(this);
        pf = PreferenceEditor.getInstance();
        login = new Gson().fromJson(pf.getStringPreference(QuickstartPreferences.USER_INFO), Login.DataBean.class);

        // Required empty public constructor
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        postImagesList = new ArrayList<>();
        selectedAttrList = new ArrayList<>();
        SetupPostImagesList();
        SetupPostAttrList();


        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().get("Id") != null) {
            PostId = (String) getIntent().getExtras().get("Id");
            System.out.println("PostId" + PostId);
            getPostDetails();
            isEdit = true;

        } else {
            isEdit = false;
        }

        if (isEdit) {
            swipeLayout.setEnabled(true);
            swipeLayout.setOnRefreshListener(this);
        } else {
            swipeLayout.setEnabled(false);
        }

        nsvContent.setVisibility(isEdit ? View.GONE : View.VISIBLE);
        btnDelete.setVisibility(isEdit ? View.GONE : View.VISIBLE);
        btnAddPost.setVisibility(isEdit ? View.GONE : View.VISIBLE);
        btnEditPost.setVisibility(!isEdit ? View.GONE : View.VISIBLE);


        try {
            etPublisherMobile.setText("" + login.getMobile_code() + login.getMobile_number());
            tvCurrency.setText("" + login.getCurrency());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void SetupPostImagesList() {
        postImagesList.add(null);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        adapterPostImages = new AdapterPostImages(getApplicationContext(), postImagesList);
        rvPostImages.setLayoutManager(linearLayoutManager);
        rvPostImages.setAdapter(adapterPostImages);

        adapterPostImages.setListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {

                if (postImagesList.get(position) == null) {
                    if ((postImagesList.size()) < MaxPostImages) {
                        createImageDialog();
                    } else {
                        Utils.ShowDialog(AddPostActivity.this, getString(R.string.info), getString(R.string.err_reach_to_max_post_images), DIALOG_TYPE_INFO);
                    }
                } else {
                    showConfirmDeleteItem(position);
                }
            }
        });

    }

    private void SetupPostAttrList() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        adapterSelectedPostAttr = new AdapterSelectedPostAttribute(getApplicationContext(), selectedAttrList);
        rvPostAttributes.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvPostAttributes.getContext(), linearLayoutManager.getOrientation());
        rvPostAttributes.addItemDecoration(dividerItemDecoration);
        rvPostAttributes.setAdapter(adapterSelectedPostAttr);
        adapterSelectedPostAttr.setListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                Intent i = new Intent(getApplicationContext(), CategoriesActivity.class);
                i.putExtra("post_id", PostId);
                startActivityForResult(i, SELECT_CATEGORY_CODE);
            }
        });

    }

    private void showConfirmDeleteItem(final int pos) {

        System.out.println("position" + pos);
        ColorDialog dialog = new ColorDialog(AddPostActivity.this);
        dialog.setAnimationEnable(true);
        dialog.setTitle(getString(R.string.info));
        dialog.setContentText(getString(R.string.msg_delete_post_image));
        //dialog.setContentImage(getResources().getDrawable(R.drawable.ic_warning));
        dialog.setPositiveListener(getString(R.string.yes), new ColorDialog.OnPositiveListener() {
            @Override
            public void onClick(ColorDialog dialog) {

                if (postImagesList.get(pos).getType() == AdapterPostImages.FILE_TYPE) {
                    dialog.dismiss();
                    postImagesList.remove(pos);
                    adapterPostImages.notifyItemRemoved(pos);

                } else if (postImagesList.get(pos).getType() == AdapterPostImages.URL_TYPE) {
                    dialog.dismiss();
                    requestDeletePostImage(pos);
                }
            }
        })
                .setNegativeListener(getString(R.string.Cancel), dialog1 -> dialog1.dismiss()).show();
    }

    private void requestDeletePostImage(final int pos) {

        if (!Utils.isNetworkAvailable()) {
            Utils.ShowNoInternetConnectionDialog(AddPostActivity.this);
            return;
        }

        showProgress();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        DeletePostImageCall = apiService.removePhoto(postImagesList.get(pos).getId());
        DeletePostImageCall.enqueue(new Callback<FormalResponse>() {
            @Override
            public void onResponse(Call<FormalResponse> call, final Response<FormalResponse> response) {
                hideProgress();

                try {
                    if (response.body() != null) {
                        FormalResponse formalResponse = response.body();
                        if (formalResponse.isStatus() && formalResponse.getCode() == QuickstartPreferences.OK_STATUS_CODE) {
                            //Toast.makeText(getApplicationContext(), formalResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            Utils.ShowDialog(AddPostActivity.this, getString(R.string.success), formalResponse.getMessage(), DIALOG_TYPE_SUCCESS);
                            postImagesList.remove(pos);
                            adapterPostImages.notifyItemRemoved(pos);


                        } else if (response.body().getCode() == QuickstartPreferences.UNAUTHORIZED_STATUS_CODE) {
                            Toast.makeText(getApplicationContext(), R.string.Unauthorized, Toast.LENGTH_LONG).show();
                            Utils.LunchLogin(getApplicationContext());

                        } else {
                            Utils.ShowServerDialog(AddPostActivity.this);
                        }
                    } else {
                        Utils.ShowServerDialog(AddPostActivity.this);
                    }

                } catch (Exception e) {
                    e.printStackTrace();

                    Utils.ShowServerDialog(AddPostActivity.this);
                }
            }


            @Override
            public void onFailure(Call<FormalResponse> call, Throwable t) {
                if (!call.isCanceled() && !"Canceled".equals(t.getMessage())) {
                    hideProgress();
                    t.printStackTrace();
                    Utils.ShowServerDialog(AddPostActivity.this);
                }
            }
        });


    }


    private void createImageDialog() {


        //outPutFile = new File(ImagesFile, System.currentTimeMillis() + ".jpg");

        final Dialog dialog = new Dialog(AddPostActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.select_image_dialog);


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);


        ImageButton select_image_camera = (ImageButton) dialog.findViewById(R.id.select_image_camera);
        ImageButton select_image_gallery = (ImageButton) dialog.findViewById(R.id.select_image_gallery);


        select_image_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(AddPostActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(AddPostActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.CAMERA}, 33);
                    return;
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(AddPostActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(AddPostActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 33);
                    return;
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(AddPostActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(AddPostActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 33);
                    return;
                }

                ImagesFile = new File(Environment.getExternalStorageDirectory() + "/" + Utils.FolderName);
                if (!ImagesFile.exists()) {
                    ImagesFile.mkdir();
                }

                try {
                    outPutFile = createImageFile();
                    Uri photoUri = FileProvider.getUriForFile(AddPostActivity.this, "com.wasset.fileprovider", outPutFile);
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                    startActivityForResult(intent, CAMERA_CODE);

                } catch (IOException e) {
                    e.printStackTrace();
                }


                dialog.dismiss();

            }
        });

        select_image_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(AddPostActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(AddPostActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 33);
                    return;
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(AddPostActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(AddPostActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 33);
                    return;
                }

                ImagesFile = new File(Environment.getExternalStorageDirectory() + "/" + Utils.FolderName);
                if (!ImagesFile.exists()) {
                    ImagesFile.mkdir();
                }
                outPutFile = new File(ImagesFile, System.currentTimeMillis() + ".jpg");

                Matisse.from(AddPostActivity.this)
                        .choose(MimeType.ofImage())
                        .countable(true)
                        .maxSelectable(MaxPostImages)
                        .gridExpectedSize(getResources().getDimensionPixelSize(R.dimen.grid_expected_size))
                        .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                        .thumbnailScale(0.85f)
                        .imageEngine(new GlideEngine())
                        .forResult(666);

                dialog.dismiss();
            }
        });

        dialog.show();
    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir      /* directory */
        );
        mCurrentPhotoPath = image.getAbsolutePath();

        return image;
    }


    @OnClick({R.id.btn_edit_post, R.id.btn_refresh, R.id.tv_what_you_want_to_advertise, R.id.btn_close, R.id.btn_submit, R.id.btn_delete, R.id.btn_add_post})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_what_you_want_to_advertise:
                Intent i = new Intent(getApplicationContext(), CategoriesActivity.class);
                i.putExtra("post_id", PostId);
                startActivityForResult(i, SELECT_CATEGORY_CODE);
                break;
            case R.id.btn_close:
                closeScreen();
                break;
            case R.id.btn_submit:
            case R.id.btn_add_post:
            case R.id.btn_edit_post:

                String postTitle = etPostSubject.getText().toString();
                String postDesc = etPostDesc.getText().toString();
                String salary = etSalary.getText().toString();
                String phone = etPublisherMobile.getText().toString();

                if (postImagesList.size() <= 1) {
                    Utils.ShowDialog(AddPostActivity.this, getString(R.string.err), getString(R.string.add_post_image), DIALOG_TYPE_WRONG);
                    return;
                }

                if (!isEdit && (hashMap == null || hashMap.size() == 0)) {
                    Utils.ShowDialog(AddPostActivity.this, getString(R.string.err), getString(R.string.add_post_attr), DIALOG_TYPE_WRONG);
                    return;
                }

                if (postTitle.isEmpty()) {
                    Utils.ShowDialog(AddPostActivity.this, getString(R.string.err), getString(R.string.add_post_title), DIALOG_TYPE_WRONG);
                    return;
                }


                if (postDesc.isEmpty()) {
                    Utils.ShowDialog(AddPostActivity.this, getString(R.string.err), getString(R.string.add_post_desc), DIALOG_TYPE_WRONG);
                    return;
                }

               /* if (salary.isEmpty()) {
                      Utils.ShowDialog(AddPostActivity.this, getString(R.string.err), getString(R.string.err_msg_salary), DIALOG_TYPE_WRONG);
                      return;
                }

                if (phone.isEmpty()) {
                    Utils.ShowDialog(AddPostActivity.this, getString(R.string.err), getString(R.string.err_msg_phone_number), DIALOG_TYPE_WRONG);
                    return;
                }
                */

                if (isEdit) {
                    requestEditPost();
                } else {
                    showAgreementDialog();
                }

                break;
            case R.id.btn_delete:
                EmptyFieldsDialog();
                break;

            case R.id.btn_refresh:
                if (isEdit) {
                    getPostDetails();
                }
                break;

        }
    }

    private void closeScreen() {
        ColorDialog dialog = new ColorDialog(AddPostActivity.this);
        dialog.setAnimationEnable(true);
        dialog.setTitle(getString(R.string.info));
        dialog.setContentText(getString(R.string.leave_create_post));
        //dialog.setContentImage(getResources().getDrawable(R.drawable.ic_warning));
        dialog.setPositiveListener(getString(R.string.yes), new ColorDialog.OnPositiveListener() {
            @Override
            public void onClick(ColorDialog dialog) {
                finish();
            }
        })
                .setNegativeListener(getString(R.string.Cancel), dialog1 -> dialog1.dismiss()).show();
    }

    private void showAgreementDialog() {

        final Dialog dialog = new Dialog(AddPostActivity.this);
        dialog.setContentView(R.layout.agreement_layout);
        WebView wvAgreement = (WebView) dialog.findViewById(R.id.wv_agreement);
        CheckBox cbAgreement = (CheckBox) dialog.findViewById(R.id.cb_accept);
        final Button btnAccept = (Button) dialog.findViewById(R.id.btn_accept);


        WebSettings settings = wvAgreement.getSettings();
        // Enable Javascript
        settings.setJavaScriptEnabled(true);
        // Use WideViewport and Zoom out if there is no viewport defined
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        // Enable pinch to zoom without the zoom buttons
        settings.setBuiltInZoomControls(true);
        // Hide the zoom controls for HONEYCOMB+
        settings.setDisplayZoomControls(false);

        // Enable remote debugging via chrome://inspect
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }

        Config.DataBean cnfg = new Gson().fromJson(pf.getStringPreference(QuickstartPreferences.CONFIG), Config.DataBean.class);
        wvAgreement.loadData(cnfg.getAgreement(), "text/html; charset=UTF-8", null);

        cbAgreement.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                btnAccept.setEnabled(b);
                btnAccept.setBackgroundColor(b ? getResources().getColor(R.color.colorPrimary) : getResources().getColor(R.color.disable_color));
            }
        });


        btnAccept.setOnClickListener(view -> {
            dialog.dismiss();
            requestAddPost();
        });


        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);
        dialog.show();
    }

    private void requestAddPost() {

        String postTitle = etPostSubject.getText().toString();
        String postDesc = etPostDesc.getText().toString();
        String salary = etSalary.getText().toString();
        String phone = etPublisherMobile.getText().toString();

        if (!Utils.isNetworkAvailable()) {
            Utils.ShowNoInternetConnectionDialog(AddPostActivity.this);
            return;
        }


        Map<String, RequestBody> map = new HashMap<>();
        RequestBody titleBody = RequestBody.create(MediaType.parse("text/plain"), postTitle);
        RequestBody descBody = RequestBody.create(MediaType.parse("text/plain"), postDesc);
        RequestBody salaryBody = RequestBody.create(MediaType.parse("text/plain"), salary);
        RequestBody phoneBody = RequestBody.create(MediaType.parse("text/plain"), phone);
        RequestBody cityBody = RequestBody.create(MediaType.parse("text/plain"), "1");
        RequestBody commentsEnabledBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(!commentsSwitch.isChecked()));
        RequestBody callEnabledBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(!phoneCallsSwitch.isChecked()));

        map.put("title", titleBody);
        map.put("details", descBody);
        map.put("price", salaryBody);
        map.put("mobile_number", phoneBody);
        map.put("city_id", cityBody);
        map.put("comment_enable", commentsEnabledBody);
        map.put("call_enable", callEnabledBody);


        Set<String> keys = hashMap.keySet();
        for (String i : keys) {
            System.out.println("Key : " + i + " Value : " + hashMap.get(i));
            RequestBody AttrValue = RequestBody.create(MediaType.parse("text/plain"), hashMap.get(i));
            map.put(i, AttrValue);
        }


        MultipartBody.Part[] ImagesParts = new MultipartBody.Part[postImagesList.size()];
        for (int index = 1; index < postImagesList.size(); index++) {
            System.out.println("Path : " + postImagesList.get(index).getImageFile().getAbsolutePath());
            File file = postImagesList.get(index).getImageFile();

            if (file != null && file.exists()) {
                RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), file);
                ImagesParts[index - 1] = MultipartBody.Part.createFormData("photo_" + (index), file.getName(), surveyBody);
            }
        }


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Utils.HideKeyBord(AddPostActivity.this);
        showProgress();
        addPostCall = apiService.addPost(map, ImagesParts);
        addPostCall.enqueue(new Callback<FormalResponse>() {
            @Override
            public void onResponse(Call<FormalResponse> call, final Response<FormalResponse> response) {
                hideProgress();
                try {
                    FormalResponse formalResponse = response.body();
                    if (formalResponse.isStatus() && formalResponse.getCode() == QuickstartPreferences.OK_STATUS_CODE) {
                        Toast.makeText(AddPostActivity.this, "" + formalResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        finish();
                    } else if (!formalResponse.isStatus() && formalResponse.getCode() == QuickstartPreferences.VALIDATION_ERROR_CODE) {

                        String error = formalResponse.getMessage().replace("-", "\n");
                        Utils.ShowDialog(AddPostActivity.this, getString(R.string.err), error, DIALOG_TYPE_WRONG);

                    } else {
                        Utils.ShowDialog(AddPostActivity.this, getString(R.string.err), formalResponse.getMessage(), DIALOG_TYPE_WRONG);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.ShowServerDialog(AddPostActivity.this);
                }

            }

            @Override
            public void onFailure(Call<FormalResponse> call, Throwable t) {
                if (!call.isCanceled() && !"Canceled".equals(t.getMessage())) {
                    hideProgress();
                    t.printStackTrace();
                    Utils.ShowServerDialog(AddPostActivity.this);
                }
            }
        });

    }

    private void EmptyFieldsDialog() {
        ColorDialog dialog = new ColorDialog(AddPostActivity.this);
        dialog.setAnimationEnable(true);
        dialog.setTitle(getString(R.string.err_empty_fields_title));
        dialog.setContentText(getString(R.string.err_empty_fields_body));
        //dialog.setContentImage(getResources().getDrawable(R.drawable.ic_warning));
        dialog.setPositiveListener(getString(R.string.Ok), new ColorDialog.OnPositiveListener() {
            @Override
            public void onClick(ColorDialog dialog) {
                dialog.dismiss();

                EmptyFields();

            }
        })
                .setNegativeListener(getString(R.string.Cancel), new ColorDialog.OnNegativeListener() {
                    @Override
                    public void onClick(ColorDialog dialog) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private void EmptyFields() {

        etPostSubject.getText().clear();
        etPostDesc.getText().clear();
        etSalary.getText().clear();
        etPublisherMobile.getText().clear();
        postImagesList.clear();
        postImagesList.add(null);
        adapterPostImages.notifyDataSetChanged();
        if (hashMap != null) {
            hashMap.clear();
        }

        if (textMap != null) {
            textMap.clear();
        }
        selectedAttrList.clear();
        adapterSelectedPostAttr.notifyDataSetChanged();


    }

    private void hideProgress() {
        if (indicatorView != null && indicatorView.isShowing()) {
            indicatorView.dismiss();
        }
    }

    private void showProgress() {

        if (indicatorView == null) {
            indicatorView = new AVLoadingIndicatorDialog(AddPostActivity.this);
            //indicatorView.setMessage(msg);
            indicatorView.setCancelable(false);
        }
        indicatorView.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        System.out.println("requestCode" + requestCode);
        System.out.println("resultCode" + resultCode);
        System.out.println("data" + data);

        if (resultCode != RESULT_CANCELED) {
            if (resultCode == RESULT_OK) {

                if (requestCode == 666) {

                    List<Uri> mSelected = Matisse.obtainResult(data);

                    for (Uri uri : mSelected) {

                        Log.d("TAG" + ".PICK_GALLERY_IMAGE", "Selected image uri path :" + uri.toString());
                        File sourceFile = new File(Utils.getPathFromGooglePhotosUri(AddPostActivity.this, uri));
                        File destFile = new File(ImagesFile, "img_" + System.currentTimeMillis() + ".png");
                        System.out.println("sourceFile" + sourceFile);
                        System.out.println("destFile" + destFile);
                        Bitmap bmp = Utils.decodeFilee(sourceFile, destFile);

                        PostImage postImage = new PostImage();
                        postImage.setType(AdapterPostImages.FILE_TYPE);
                        postImage.setImageFile(destFile);
                        postImage.setImageBitmap(bmp);
                        postImagesList.add(postImage);
                        adapterPostImages.notifyItemInserted(postImagesList.size() - 1);

                    }


                } else if (requestCode == GALLERY_CODE) {

            /*
            try {
                mImageCaptureUri = data.getData();

                InputStream input = getContentResolver().openInputStream(data.getData());
                FileOutputStream output = new FileOutputStream(outPutFile);
                byte[] buffer = new byte[4096];
                int n = 0;
                while (-1 != (n = input.read(buffer))) {
                    output.write(buffer, 0, n);
                }
                mImageCaptureUri = Uri.fromFile(outPutFile);
                outPutFile = new File(ImagesFile, System.currentTimeMillis() + ".jpg");

                CropIMG();

            } catch (Exception e) {
                e.printStackTrace();
            }
            */


                    ClipData clipData = data.getClipData();

                    if (clipData == null) {

                        try {
                            Uri uri = data.getData();



                       /* InputStream input = getContentResolver().openInputStream(uri);
                        File outPutFile = new File(ImagesFile, System.currentTimeMillis() + ".jpg");

                        FileOutputStream output = new FileOutputStream(outPutFile);
                        byte[] buffer = new byte[4096];
                        int n = 0;
                        while (-1 != (n = input.read(buffer))) {
                            output.write(buffer, 0, n);
                        }*/


                            Log.d("TAG" + ".PICK_GALLERY_IMAGE", "Selected image uri path :" + uri.toString());
                            File sourceFile = new File(Utils.getPathFromGooglePhotosUri(AddPostActivity.this, uri));
                            File destFile = new File(ImagesFile, "img_" + System.currentTimeMillis() + ".png");
                            System.out.println("sourceFile" + sourceFile);
                            System.out.println("destFile" + destFile);

                            Bitmap bmp = Utils.decodeFilee(sourceFile, destFile);
                            PostImage postImage = new PostImage();
                            postImage.setType(AdapterPostImages.FILE_TYPE);
                            postImage.setImageFile(destFile);
                            postImage.setImageBitmap(bmp);
                            postImagesList.add(postImage);
                            adapterPostImages.notifyItemInserted(postImagesList.size() - 1);

                        } catch (Exception e) {
                            e.printStackTrace();
                            Crashlytics.logException(e);
                        }


                    } else for (int i = 0; i < data.getClipData().getItemCount(); i++) {

                        try {

                            Uri uri = data.getClipData().getItemAt(i).getUri();
                            Log.d("TAG" + ".PICK_GALLERY_IMAGE", "Selected image uri path :" + uri.toString());
                            File sourceFile = new File(Utils.getPathFromGooglePhotosUri(AddPostActivity.this, uri));
                            File destFile = new File(ImagesFile, "img_" + System.currentTimeMillis() + ".png");
                            System.out.println("sourceFile" + sourceFile);
                            System.out.println("destFile" + destFile);
                            Bitmap bmp = Utils.decodeFilee(sourceFile, destFile);

                            PostImage postImage = new PostImage();
                            postImage.setType(AdapterPostImages.FILE_TYPE);
                            postImage.setImageFile(destFile);
                            postImage.setImageBitmap(bmp);
                            postImagesList.add(postImage);
                            adapterPostImages.notifyItemInserted(postImagesList.size() - 1);


                        } catch (Exception e) {
                            e.printStackTrace();
                            Crashlytics.logException(e);
                        }
                    }


                } else if (requestCode == CAMERA_CODE) {
                    try {
                        //CropIMG();
                        File path = new File(this.getFilesDir(), mCurrentPhotoPath);
                        if (!path.exists())
                            path.mkdirs();
                        if (outPutFile.exists()) {
                            PostImage postImage = new PostImage();
                            postImage.setType(AdapterPostImages.FILE_TYPE);
                            File file = new File(mCurrentPhotoPath);
                            postImage.setImageFile(file);
                            Bitmap bitmap = new Compressor(AddPostActivity.this).compressToBitmap(file);
                            postImage.setImageBitmap(bitmap);
                            postImagesList.add(postImage);
                            adapterPostImages.notifyItemInserted(postImagesList.size() - 1);

                        } else {

                            Toast.makeText(this, R.string.image_not_found, Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (requestCode == CROPPING_CODE) {

                    Log.d("TAG", "CROPPING_CODE");
           /* try {
                if (outPutFile.exists()) {

                    System.out.println("outPutFile" + outPutFile.getAbsolutePath());
                    PostImage postImage = new PostImage();
                    postImage.setType(AdapterPostImages.FILE_TYPE);
                    postImage.setImageFile(outPutFile);
                    postImagesList.add(postImage);
                    adapterPostImages.notifyItemInserted(postImagesList.size() - 1);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }*/


                } else if (requestCode == SELECT_CATEGORY_CODE) {
                    categoryId = (int) data.getExtras().get("Id");
                    categoryName = (String) data.getExtras().get("Name");
                    //categoryId = 21;
                    Intent i = new Intent(getApplicationContext(), AddPostAttributeActivity.class);
                    i.putExtra("categoryId", categoryId);
                    i.putExtra("post_id", PostId);
                    startActivityForResult(i, POST_SETPS_CODE);
                } else if (requestCode == POST_SETPS_CODE) {

                    hashMap = (HashMap<String, String>) data.getSerializableExtra("map");
                    textMap = (HashMap<String, String>) data.getSerializableExtra("textMap");
                    hashMap.put("category_id", String.valueOf(categoryId));
                    textMap.put(getString(R.string.category), categoryName);
                    selectedAttrList.clear();
                    Set<String> keys2 = textMap.keySet();  //get all keys
                    for (String i : keys2) {
                        selectedAttrList.add(0, new SelectedAttr(i, textMap.get(i)));
                        System.out.println("Key_Alt : " + i + " Value_Alt : " + textMap.get(i));
                    }

                    adapterSelectedPostAttr.notifyDataSetChanged();
                    tvWhatYouWantToAdvertise.setVisibility(View.GONE);
                }

            }
        }
    }

    private void CropIMG() {

        Intent cropIntent = new Intent("com.android.camera.action.CROP");
        cropIntent.setDataAndType(mImageCaptureUri, "image/*");
        cropIntent.putExtra("crop", "true");
        cropIntent.putExtra("aspectX", 1);
        cropIntent.putExtra("aspectY", 1);
        cropIntent.putExtra("return-data", true);
        cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(outPutFile));
        startActivityForResult(cropIntent, CROPPING_CODE);

    }

    private void getPostDetails() {


        if (!Utils.isNetworkAvailable()) {
            ShowErrorLayout(R.string.no_internet_connection);
            return;
        }

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        swipeLayout.setRefreshing(true);

        PostDetailsCall = apiService.getPostDetails(PostId);
        PostDetailsCall.enqueue(new Callback<postDetails>() {
            @Override
            public void onResponse(Call<postDetails> call, final Response<postDetails> response) {
                System.out.println("raw" + response.raw());

                swipeLayout.setRefreshing(false);

                try {
                    if (response.body().isStatus() && response.body().getCode() == QuickstartPreferences.OK_STATUS_CODE) {
                        if (response.body().getData() != null) {
                            PostDataBean post = response.body().getData();
                            showContent();


                            etPostSubject.setText(post.getTitle());
                            etPostDesc.setText(post.getDetails());
                            etPublisherMobile.setText(post.getMobile_number());
                            etSalary.setText(post.getPrice());
                            selectedAttrList.clear();
                            commentsSwitch.setChecked(!post.isComment_enable());
                            phoneCallsSwitch.setChecked(!post.isCall_enable());
                            for (PostDataBean.AttributesBean i : post.getAttributes()) {
                                selectedAttrList.add(new SelectedAttr(i.getKey(), i.getValue()));
                            }

                            adapterSelectedPostAttr.notifyDataSetChanged();
                            postImagesList.clear();
                            postImagesList.add(null);
                            for (PostDataBean.PhotosBean url : post.getPhotos()) {
                                PostImage postImage = new PostImage();
                                postImage.setType(AdapterPostImages.URL_TYPE);
                                postImage.setUrl(url.getUrl());
                                postImagesList.add(postImage);
                            }
                            adapterPostImages.notifyDataSetChanged();

                        } else {
                            ShowErrorLayout(R.string.err_msg_server);
                        }

                    } else if (response.body().getCode() == UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(getApplicationContext(), R.string.Unauthorized, Toast.LENGTH_LONG).show();
                        Utils.LunchLogin(getApplicationContext());
                    } else {
                        ShowErrorLayout(R.string.err_msg_server);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    ShowErrorLayout(R.string.err_msg_server);
                }
            }

            @Override
            public void onFailure(Call<postDetails> call, Throwable t) {
                if (!call.isCanceled() && !"Canceled".equals(t.getMessage())) {

                    swipeLayout.setRefreshing(false);
                    t.printStackTrace();
                    ShowErrorLayout(R.string.err_msg_server);
                }
            }
        });
    }

    private void requestEditPost() {

        String postTitle = etPostSubject.getText().toString();
        String postDesc = etPostDesc.getText().toString();
        String salary = etSalary.getText().toString();
        String phone = etPublisherMobile.getText().toString();

        if (!Utils.isNetworkAvailable()) {
            Utils.ShowNoInternetConnectionDialog(AddPostActivity.this);
            return;
        }


        Map<String, RequestBody> map = new HashMap<>();
        RequestBody PostIdBody = RequestBody.create(MediaType.parse("text/plain"), PostId);
        RequestBody titleBody = RequestBody.create(MediaType.parse("text/plain"), postTitle);
        RequestBody descBody = RequestBody.create(MediaType.parse("text/plain"), postDesc);
        RequestBody salaryBody = RequestBody.create(MediaType.parse("text/plain"), salary);
        RequestBody phoneBody = RequestBody.create(MediaType.parse("text/plain"), phone);
        RequestBody cityBody = RequestBody.create(MediaType.parse("text/plain"), "1");
        RequestBody enableCommentsBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(!commentsSwitch.isChecked()));
        RequestBody enablePhoneBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(!phoneCallsSwitch.isChecked()));

        map.put("post_id", PostIdBody);
        map.put("title", titleBody);
        map.put("details", descBody);
        map.put("price", salaryBody);
        map.put("mobile_number", phoneBody);
        map.put("city_id", cityBody);
        map.put("comment_enable", enableCommentsBody);
        map.put("call_enable", enablePhoneBody);


        if (hashMap != null) {
            Set<String> keys = hashMap.keySet();  //get all keys
            for (String i : keys) {
                System.out.println("Key : " + i + " Value : " + hashMap.get(i));
                RequestBody AttrValue = RequestBody.create(MediaType.parse("text/plain"), hashMap.get(i));
                map.put(i, AttrValue);
            }
        }

        MultipartBody.Part[] ImagesParts = new MultipartBody.Part[postImagesList.size()];

        int index = 0;
        for (PostImage postImage : postImagesList) {
            if (postImage != null && postImage.getType() == AdapterPostImages.FILE_TYPE && postImage.getImageFile() != null && postImage.getImageFile().exists()) {
                RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), postImage.getImageFile());
                ImagesParts[index] = MultipartBody.Part.createFormData("photo_" + (index + 1), postImage.getImageFile().getName(), surveyBody);
                index++;
            }
        }

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Utils.HideKeyBord(AddPostActivity.this);
        showProgress();
        addPostCall = apiService.editPost(map, ImagesParts);
        addPostCall.enqueue(new Callback<FormalResponse>() {
            @Override
            public void onResponse(Call<FormalResponse> call, final Response<FormalResponse> response) {

                hideProgress();

                try {
                    FormalResponse formalResponse = response.body();
                    if (formalResponse.isStatus() && formalResponse.getCode() == QuickstartPreferences.OK_STATUS_CODE) {
                        Toast.makeText(AddPostActivity.this, "" + formalResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Utils.ShowDialog(AddPostActivity.this, getString(R.string.err), formalResponse.getMessage(), DIALOG_TYPE_WRONG);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.ShowServerDialog(AddPostActivity.this);
                }
            }


            @Override
            public void onFailure(Call<FormalResponse> call, Throwable t) {
                if (!call.isCanceled() && !"Canceled".equals(t.getMessage())) {
                    hideProgress();
                    t.printStackTrace();
                    Utils.ShowServerDialog(AddPostActivity.this);
                }
            }
        });

    }

    private void showContent() {
        try {
//            nsvContent.setVisibility(View.VISIBLE);
            rlNoInternetConnection.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ShowErrorLayout(int txt) {
        try {
//            nsv_content.setVisibility(View.GONE);
            rlNoInternetConnection.setVisibility(View.VISIBLE);
            tvError.setText(txt);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (addPostCall != null) {
            addPostCall.cancel();
        }

        if (PostDetailsCall != null) {
            PostDetailsCall.cancel();
        }

        if (DeletePostImageCall != null) {
            DeletePostImageCall.cancel();
        }
    }

    @Override
    public void onRefresh() {
        if (isEdit) {
            getPostDetails();
        } else {
            swipeLayout.setRefreshing(false);
        }
    }
}
