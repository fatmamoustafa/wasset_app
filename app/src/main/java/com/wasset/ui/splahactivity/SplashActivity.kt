package com.wasset.ui.splahactivity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.view.WindowManager
import com.google.gson.Gson
import com.wasset.R
import com.wasset.interfaces.ApiClient
import com.wasset.interfaces.ApiInterface
import com.wasset.models.Config
import com.wasset.models.Login
import com.wasset.ui.*
import com.wasset.utility.Utils.isNetworkAvailable
import com.wasset.utility.PreferenceEditor
import com.wasset.utility.QuickstartPreferences
import kotlinx.android.synthetic.main.activity_splash.*
import kotlinx.android.synthetic.main.no_internet_connection_layout.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SplashActivity : AppCompatActivity() {

    private var call: Call<Config>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        getConfig()
    }


    private fun getConfig() {

        if (!isNetworkAvailable()) {
            ShowErrorLayout(getString(R.string.no_internet_connection))
            return
        }

        ShowContentLayout()
        val apiService = ApiClient.getClient().create(ApiInterface::class.java)
        call = apiService.getConfig()
        call!!.enqueue(object : Callback<Config> {
            override fun onResponse(call: Call<Config>, response: Response<Config>) {

                if (response.isSuccessful) {
                    try {
                        val config = response.body()
                        if (config != null && config.code == QuickstartPreferences.OK_STATUS_CODE) {
                            PreferenceEditor.getInstance().setStringPreference(QuickstartPreferences.CONFIG, Gson().toJson(config.data, Config.DataBean::class.java))
                            Handler().postDelayed({
                                val login = Gson().fromJson(PreferenceEditor.getInstance().getStringPreference(QuickstartPreferences.USER_INFO), Login.DataBean::class.java)
                                if (login != null) {
                                    goToHomeActivity()
                                } else {
                                    goToLoginActivity()
                                }
                            }, 1500)
                        } else {
                            ShowErrorLayout(getString(R.string.err_msg_server))

                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                        ShowErrorLayout(getString(R.string.err_msg_server))
                    }
                }
            }

            override fun onFailure(call: Call<Config>, t: Throwable) {
                if (!call.isCanceled && !"Canceled".equals(t.message)) {
                    t.printStackTrace()
                    ShowErrorLayout(getString(R.string.err_msg_server))
                }
            }
        })

    }


    private fun ShowContentLayout() {
        content.visibility = View.VISIBLE
        rl_no_internet_connection.visibility = View.GONE
    }

    private fun ShowErrorLayout(txt: String) {
        println("no internet connection")
        content.visibility = View.GONE
        rl_no_internet_connection.visibility = View.VISIBLE
        tv_error.text = txt

    }

    private fun goToHomeActivity() {
        val i = Intent(applicationContext, HomeActivity::class.java)
        finish()
        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
        startActivity(i)
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }


    private fun goToLoginActivity() {
        val i = Intent(applicationContext, LoginActivity::class.java)
        finish()
        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
        startActivity(i)
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }

    override fun onDestroy() {
        super.onDestroy()

        if (call != null) {
            call?.cancel()
        }
    }


}
