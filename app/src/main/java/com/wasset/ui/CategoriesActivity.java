package com.wasset.ui;

import android.content.Intent;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wasset.R;
import com.wasset.adapter.AdapterCategories;
import com.wasset.interfaces.ApiClient;
import com.wasset.interfaces.ApiInterface;
import com.wasset.interfaces.OnItemClickListener;
import com.wasset.models.categoryList;
import com.wasset.utility.Utils;
import com.wasset.utility.QuickstartPreferences;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.wasset.utility.QuickstartPreferences.UNAUTHORIZED_STATUS_CODE;

public class CategoriesActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.rv_categories)
    RecyclerView rvCategories;
    @BindView(R.id.iv_empty_icon)
    ImageView ivEmptyIcon;
    @BindView(R.id.tv_empty_text)
    TextView tvEmptyText;
    @BindView(R.id.rl_empty_layout)
    LinearLayout rlEmptyLayout;
    @BindView(R.id.tv_error)
    TextView tvError;
    @BindView(R.id.btn_refresh)
    Button btnRefresh;
    @BindView(R.id.rl_no_internet_connection)
    RelativeLayout rlNoInternetConnection;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeLayout;
    @BindView(R.id.tv_toolbar_title)
    TextView tvToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private AdapterCategories adapterCategories;
    private List<categoryList.DataBean> categoriesList;
    private Call<categoryList> call;
    private String postId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        ButterKnife.bind(this);
        swipeLayout.setOnRefreshListener(this);
        categoriesList = new ArrayList<>();

        try {
            if (getIntent() != null && getIntent().getExtras() != null) {
                postId = (String) getIntent().getExtras().get("post_id");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        setToolbar();
        setupList();
        requestGetCategoryList();
    }

    public void setToolbar() {
        tvToolbarTitle.setText(R.string.categories);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void setupList() {

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
        adapterCategories = new AdapterCategories(getApplicationContext(), categoriesList);
        rvCategories.setLayoutManager(gridLayoutManager);
        rvCategories.setAdapter(adapterCategories);
        adapterCategories.setListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                categoryList.DataBean item = categoriesList.get(position);

                if (item.getSub_categories().size() > 0) {
                    Intent intent = new Intent(CategoriesActivity.this, SubCategoryActivity.class);
                    intent.setAction("add");
                    intent.putExtra("name", item.getName());
                    intent.putExtra("list", (ArrayList<categoryList.DataBean.SubCategoriesBean>) item.getSub_categories());
                    startActivityForResult(intent, 100);

                } else {
                    Intent i = new Intent();
                    i.putExtra("Id", Integer.valueOf(categoriesList.get(position).getCategory_id()));
                    i.putExtra("Name", categoriesList.get(position).getName());
                    setResult(RESULT_OK, i);
                    finish();

                }

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(RESULT_CANCELED);
        finish();
    }

    private void requestGetCategoryList() {
        if (!Utils.isNetworkAvailable()) {
            ShowErrorLayout(R.string.no_internet_connection);
            return;
        }

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        swipeLayout.setRefreshing(true);

        call = apiService.getCategoryList(postId);
        call.enqueue(new Callback<categoryList>() {
            @Override
            public void onResponse(Call<categoryList> call, final Response<categoryList> response) {
                System.out.println("raw" + response.raw());

                swipeLayout.setRefreshing(false);
                try {
                    if (response.body().isStatus() && response.body().getCode() == QuickstartPreferences.OK_STATUS_CODE) {
                        if (response.body().getData().size() > 0) {
                            showContent();
                            categoriesList.clear();
                            categoriesList.addAll(response.body().getData());
                            adapterCategories.notifyDataSetChanged();
                        } else {
                            showEmpty();
                        }

                    } else if (response.body().getCode() == UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(getApplicationContext(), R.string.Unauthorized, Toast.LENGTH_LONG).show();
                        Utils.LunchLogin(getApplicationContext());
                    } else {
                        ShowErrorLayout(R.string.err_msg_server);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    ShowErrorLayout(R.string.err_msg_server);
                }
            }


            @Override
            public void onFailure(Call<categoryList> call, Throwable t) {
                if (!call.isCanceled() && !"Canceled".equals(t.getMessage())) {

                    swipeLayout.setRefreshing(false);
                    t.printStackTrace();
                    ShowErrorLayout(R.string.err_msg_server);
                }
            }
        });
    }

    private void showContent() {
        try {
            rvCategories.setVisibility(View.VISIBLE);
            rlNoInternetConnection.setVisibility(View.GONE);
            rlEmptyLayout.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showEmpty() {
        try {
            rvCategories.setVisibility(View.GONE);
            rlEmptyLayout.setVisibility(View.VISIBLE);
            rlNoInternetConnection.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ShowErrorLayout(int txt) {
        try {
            rvCategories.setVisibility(View.GONE);
            rlNoInternetConnection.setVisibility(View.VISIBLE);
            rlEmptyLayout.setVisibility(View.GONE);
            tvError.setText(txt);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (call != null) {
            call.cancel();
        }

    }

    @OnClick(R.id.btn_refresh)
    public void onViewClicked() {
        requestGetCategoryList();
    }


    @Override
    public void onRefresh() {
        requestGetCategoryList();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100 && resultCode == RESULT_OK) {
            int categoryId = (int) data.getExtras().get("Id");
            String categoryName = (String) data.getExtras().get("Name");

            Intent i = new Intent();
            i.putExtra("Id", categoryId);
            i.putExtra("Name", categoryName);
            setResult(RESULT_OK, i);
            finish();

        }
    }
}
