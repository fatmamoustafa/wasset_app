package com.wasset.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;


import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wasset.ui.addpost.AddPostActivity;
import com.wasset.ui.mainactivity.MenuItemAdapter;
import com.wasset.ui.mainactivity.OnMenuItemClickListener;
import com.wasset.R;
import com.wasset.interfaces.ApiClient;
import com.wasset.interfaces.ApiInterface;
import com.wasset.models.Config;
import com.wasset.models.FormalResponse;
import com.wasset.models.Login;
import com.wasset.ui.fragment.CategoriesFragment;
import com.wasset.ui.fragment.ContactUsFragment;
import com.wasset.ui.fragment.FavFragment;
import com.wasset.ui.fragment.HomeFragment;
import com.wasset.ui.fragment.MultiUseFragment;
import com.wasset.ui.fragment.MyPostsFragment;
import com.wasset.ui.myprofile.MyProfileFragment;
import com.wasset.ui.search.SearchActivity;
import com.wasset.utility.PreferenceEditor;
import com.wasset.utility.QuickstartPreferences;
import com.wasset.utility.Utils;
import com.wasset.views.AVLoadingIndicatorDialog;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.refactor.lib.colordialog.ColorDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends BaseActivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_search)
    EditText search_et;
    @BindView(R.id.search_ic)
    ImageView search_ic;
    @BindView(R.id.toolbar_title)
    TextView toolbartitle;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.ic_home)
    LinearLayout homeIc;
    @BindView(R.id.ic_department)
    LinearLayout departmentIc;
    @BindView(R.id.ic_notification)
    LinearLayout notificationIc;
    @BindView(R.id.ic_profile)
    LinearLayout profileIc;

    PopupWindow listPopupWindow;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private Fragment fragment;
    private FragmentManager fragmentManager;
    private Config.DataBean cnfg;
    private Call<FormalResponse> LogoutCall;
    private AVLoadingIndicatorDialog indicatorView;
    private AppCompatTextView textNotificationItemCount;
    private int mNotificationItemCount;
    private PreferenceEditor pf;
    private int flag = 0;


    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mNotificationItemCount = pf.getIntegerPreference(QuickstartPreferences.BADGE_COUNT);
            //Utils.setBadge(getApplicationContext(),mNotificationItemCount);
            setupBadge();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_home);
            ButterKnife.bind(this);
            registerReceiver(broadcastReceiver, new IntentFilter("NewNotification"));
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent iii = new Intent(HomeActivity.this, AddPostActivity.class);
                    startActivity(iii);
                }
            });
            NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
            // Passing each menu ID as a set of Ids because each
            // menu should be considered as top level destinations.
//        AppBarConfiguration appBarConfiguration = new AppBarConfiguration(setOf(
//                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications));
//        setupActionBarWithNavController(navController, appBarConfiguration)

            fragmentManager = getSupportFragmentManager();
            if (fragmentManager != null)
                displayView(R.id.nav_home);

            ArrayList list = new ArrayList<String>();
            list.add(getString(R.string.about_wasset));
            list.add(getString(R.string.nav_rules));
            list.add(getString(R.string.nav_site_fee));
            list.add(getString(R.string.nav_discount_system));
            list.add(getString(R.string.nav_contact_us));
            ImageView view = findViewById(R.id.more);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listPopupWindow != null) {
                        listPopupWindow.dismiss();
                        listPopupWindow = null;
                    } else {
                        listPopupWindow = createListPopupWindow(list);
                        listPopupWindow.showAsDropDown(view, 10, 10);
                    }
                }
            });
            homeIc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    displayView(R.id.nav_home);
                }
            });
            profileIc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    displayView(R.id.nav_my_account);
                }
            });
            departmentIc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    displayView(R.id.nav_categories);
                }
            });
            notificationIc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(HomeActivity.this, NotificationsActivity.class));
                }
            });
        } catch (Exception e) {

        }
    }

    @OnClick({R.id.search_ic, R.id.toolbar_search})
    void openSearch() {
        Intent i = new Intent(getApplicationContext(), SearchActivity.class);
        startActivity(i);
    }

    private void displayView(int id) {
        switch (id) {
            case R.id.nav_home:
                fragment = HomeFragment.newInstance();
                break;
            case R.id.nav_categories:
                fragment = CategoriesFragment.newInstance(null);
                break;
            case R.id.nav_contact_us:
                fragment = ContactUsFragment.newInstance();
                break;

            case R.id.nav_my_account:
                fragment = MyProfileFragment.newInstance();
                break;

            case R.id.nav_my_posts:
                fragment = MyPostsFragment.newInstance();
//                tvToolbarTitle.setText(R.string.nav_my_posts);
                break;


            case R.id.nav_fav_posts:
                fragment = FavFragment.newInstance();
//                tvToolbarTitle.setText(R.string.nav_fav_ads);
                break;

            case R.id.nav_discount_system:
                Bundle bundle = new Bundle();
                bundle.putString("data", cnfg.getDiscount_system());
                fragment = MultiUseFragment.newInstance();
                fragment.setArguments(bundle);
//                tvToolbarTitle.setText(R.string.nav_discount_system);
                break;
            case R.id.nav_site_fee:
                Bundle bundle2 = new Bundle();
                bundle2.putString("data", cnfg.getCommission_system());
                fragment = MultiUseFragment.newInstance();
                fragment.setArguments(bundle2);
//                tvToolbarTitle.setText(R.string.nav_site_fee);

                break;
            case R.id.nav_why_wasset:
                Bundle bundle3 = new Bundle();
                bundle3.putString("data", cnfg.getAbout_us());
                fragment = MultiUseFragment.newInstance();
                fragment.setArguments(bundle3);
//                tvToolbarTitle.setText(R.string.nav_why_wasset);
                break;
            case R.id.nav_rules:
                Bundle bundle4 = new Bundle();
                bundle4.putString("data", cnfg.getRules());
                fragment = MultiUseFragment.newInstance();
                fragment.setArguments(bundle4);
//                tvToolbarTitle.setText(R.string.nav_rules);
                break;

            case R.id.nav_logout:
                Logout();
                break;

            case R.id.nav_login:
                goToLoginActivity();
                break;
        }

        if (fragment != null) {
            fragmentManager.beginTransaction().replace(R.id.nav_host_fragment, fragment).
                    setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out).commit();
        }

    }

    private void goToLoginActivity() {
        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
        finish();
        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(i);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }


    private void Logout() {
        ColorDialog dialog = new ColorDialog(HomeActivity.this);
        dialog.setAnimationEnable(true);
        dialog.setTitle(getString(R.string.nav_logout));
        dialog.setContentText(getString(R.string.logout_message));
        //dialog.setContentImage(getResources().getDrawable(R.drawable.ic_warning));
        dialog.setPositiveListener(getString(R.string.Ok), dialog1 -> {
            dialog1.dismiss();
            requestLogout();
        })
                .setNegativeListener(getString(R.string.Cancel), ColorDialog::dismiss).show();
    }

    private void requestLogout() {
        if (!Utils.isNetworkAvailable()) {
            Utils.ShowNoInternetConnectionDialog(HomeActivity.this);
            return;
        }

        showProgress();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        LogoutCall = apiService.logout();
        LogoutCall.enqueue(new Callback<FormalResponse>() {
            @Override
            public void onResponse(Call<FormalResponse> call, Response<FormalResponse> response) {
                hideProgress();
                try {
                    if (response.body().isStatus() && response.body().getCode() == 200) {
                        Toast.makeText(HomeActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        Utils.LunchLogin(getApplicationContext());
                    } else {
                        Utils.ShowServerDialog(HomeActivity.this);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.ShowServerDialog(HomeActivity.this);
                }
            }

            @Override
            public void onFailure(Call<FormalResponse> call, Throwable t) {
                if (!call.isCanceled() && !"Canceled".equals(t.getMessage())) {
                    t.printStackTrace();
                    hideProgress();
                    System.out.println("onFailure");
                    Utils.ShowServerDialog(HomeActivity.this);
                }
            }
        });

    }


    public void setToolbar(String title) {
        try {
            if (title != null) {
                toolbartitle.setVisibility(View.VISIBLE);
                toolbartitle.setText(title);
                search_et.setVisibility(View.GONE);
            } else {
                toolbartitle.setVisibility(View.GONE);
                search_et.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {

        }
    }

    private void setupBadge() {

        if (textNotificationItemCount != null) {
            if (mNotificationItemCount == 0) {
                if (textNotificationItemCount.getVisibility() != View.GONE) {
                    textNotificationItemCount.setVisibility(View.GONE);
                }
            } else {

                //    Utils.setBadge(getApplicationContext(),Math.min(mNotificationItemCount, 99));

                textNotificationItemCount.setText(String.valueOf(Math.min(mNotificationItemCount, 99)));
                if (textNotificationItemCount.getVisibility() != View.VISIBLE) {
                    textNotificationItemCount.setVisibility(View.VISIBLE);
                }
            }
        }
    }


    private void hideProgress() {
        if (indicatorView != null && indicatorView.isShowing()) {
            indicatorView.dismiss();
        }
    }

    private void showProgress() {

        if (indicatorView == null) {
            indicatorView = new AVLoadingIndicatorDialog(HomeActivity.this);
            //indicatorView.setMessage(msg);
            indicatorView.setCancelable(false);
        }
        indicatorView.show();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
    }

    @Override
    public void onBackPressed() {

        if (fragment instanceof HomeFragment) {
            switch (flag) {
                case 0:
                    flag = 1;
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.back_again_message), Toast.LENGTH_SHORT).show();
                    break;
                case 1:
                    super.onBackPressed();
                    finish();
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    break;
            }
        } else {
            displayView(R.id.nav_home);
            System.gc();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        pf = PreferenceEditor.getInstance();
        cnfg = new Gson().fromJson(pf.getStringPreference(QuickstartPreferences.CONFIG), Config.DataBean.class);
        mNotificationItemCount = pf.getIntegerPreference(QuickstartPreferences.BADGE_COUNT);
        //Utils.setBadge(getApplicationContext(),mNotificationItemCount);

        Login.DataBean login = new Gson().fromJson(pf.getStringPreference(QuickstartPreferences.USER_INFO), Login.DataBean.class);
//        Menu menuNav = navView.getMenu();
//
//        menuNav.findItem(R.id.nav_login).setVisible(login == null);
//        menuNav.findItem(R.id.nav_logout).setVisible(login != null);
//        menuNav.findItem(R.id.nav_my_posts).setVisible(login != null);
//        menuNav.findItem(R.id.nav_fav_posts).setVisible(login != null);
//        menuNav.findItem(R.id.nav_my_account).setVisible(login != null);

    }

    private PopupWindow createListPopupWindow(List<String> items) {
        LayoutInflater layoutInflater = (LayoutInflater) getApplication().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup container = (ViewGroup) layoutInflater.inflate(R.layout.connect_with, null);
        ListView listView = container.findViewById(R.id.listview);
        PopupWindow popup = new PopupWindow(container);
        MenuItemAdapter adapter = new MenuItemAdapter(items);
        adapter.onMenuItemClickListener = new OnMenuItemClickListener() {
            @Override
            public void onClick(@NotNull String name) {
                if (name == getString(R.string.about_wasset)) {
                    displayView(R.id.nav_why_wasset);
                    listPopupWindow.dismiss();
                } else if (name == getString(R.string.nav_site_fee)) {
                    listPopupWindow.dismiss();
                    displayView(R.id.nav_site_fee);
                } else if (name == getString(R.string.nav_rules)) {
                    displayView(R.id.nav_rules);
                    listPopupWindow.dismiss();
                } else if (name == getString(R.string.nav_contact_us)) {
                    displayView(R.id.nav_contact_us);
                    listPopupWindow.dismiss();
                } else if (name == getString(R.string.nav_discount_system)) {
                    displayView(R.id.nav_discount_system);
                    listPopupWindow.dismiss();
                } else {
                    listPopupWindow.dismiss();
                }
            }
        };
        listView.setAdapter(adapter);
        popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setContentView(container);
        return popup;
    }
}
