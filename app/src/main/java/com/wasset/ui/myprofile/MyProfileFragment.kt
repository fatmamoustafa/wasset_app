package com.wasset.ui.myprofile

import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import butterknife.ButterKnife
import butterknife.OnClick
import cn.refactor.lib.colordialog.ColorDialog
import cn.refactor.lib.colordialog.ColorDialog.DIALOG_TYPE_SUCCESS
import com.google.gson.Gson
import com.wasset.R
import com.wasset.adapter.AdapterMyPost
import com.wasset.adapter.AdapterPostDetails
import com.wasset.databinding.FragementProfileBinding
import com.wasset.interfaces.ApiClient
import com.wasset.interfaces.ApiInterface
import com.wasset.interfaces.OnItemClickListener
import com.wasset.models.*
import com.wasset.ui.addpost.AddPostActivity
import com.wasset.ui.HomeActivity
import com.wasset.ui.PostDetailsActivity
import com.wasset.ui.fragment.MyAccountFragment
import com.wasset.utility.EndlessRecyclerViewScrollListener
import com.wasset.utility.PreferenceEditor
import com.wasset.utility.QuickstartPreferences
import com.wasset.utility.QuickstartPreferences.UNAUTHORIZED_STATUS_CODE
import com.wasset.utility.Utils
import com.wasset.views.AVLoadingIndicatorDialog
import kotlinx.android.synthetic.main.fragement_profile.*
import kotlinx.android.synthetic.main.no_internet_connection_layout.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class MyProfileFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener {
    private var call: Call<Login>? = null
    private var pf: PreferenceEditor? = null
    private var binding: FragementProfileBinding? = null
    private var myPostsList: MutableList<PostDataBean>? = null
    private var adapterPostDetails: AdapterMyPost? = null
    private var myPcall: Call<MyPosts>? = null
    private var DeleteCall: Call<FormalResponse>? = null
    private var indicatorView: AVLoadingIndicatorDialog? = null
    private var HideCall: Call<FormalResponse>? = null
    private var ShowCall: Call<FormalResponse>? = null
    private val STARTING_PAGE_INDEX = 1
    private var endlessScrollListener: EndlessRecyclerViewScrollListener? = null
    private var isThereMore = true
    private var favadapterPostDetails: AdapterPostDetails? = null
    private var favList: MutableList<PostDataBean>? = null
    private var favcall: Call<favorite>? = null
    private var RemoveFromFavCall: Call<FormalResponse>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (activity != null)
            (activity as HomeActivity).setToolbar(getString(R.string.nav_my_account))
        swipe.setOnRefreshListener(this)
        edit_file_tx.setOnClickListener {
            activity!!.supportFragmentManager.beginTransaction().replace(R.id.nav_host_fragment, MyAccountFragment.newInstance()).setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out).commit()
        }
        getMyPosts()

        my_adv.setOnClickListener {
            getMyPosts()
        }
        fav_adv.setOnClickListener {
            getMyFav()
        }
        getUserInfo()

    }

    fun getMyPosts() {
        my_adv.setTextColor(ContextCompat.getColor(context!!, R.color.colorAccent))
        fav_adv.setTextColor(ContextCompat.getColor(context!!, android.R.color.black))
        bookmark_ic.setColorFilter(ContextCompat.getColor(context!!, android.R.color.black))

        setupList()
        rv_publisher_posts.visibility = View.VISIBLE
        rv_fav_posts.visibility = View.GONE
        requestGetMyPostList(STARTING_PAGE_INDEX)
    }

    fun getMyFav() {
        fav_adv.setTextColor(ContextCompat.getColor(context!!, R.color.colorAccent))
        my_adv.setTextColor(ContextCompat.getColor(context!!, android.R.color.black))
        bookmark_ic.setColorFilter(ContextCompat.getColor(context!!, R.color.colorAccent))
        rv_publisher_posts.visibility = View.GONE
        rv_fav_posts.visibility = View.VISIBLE
        setupFavList()
        requestGetFavoriteList(STARTING_PAGE_INDEX)
    }

    private fun setupFavList() {

        val linearLayoutManager = LinearLayoutManager(activity)
        favadapterPostDetails = AdapterPostDetails(favList)
        rv_fav_posts.setLayoutManager(linearLayoutManager)
        endlessScrollListener = object : EndlessRecyclerViewScrollListener(linearLayoutManager, STARTING_PAGE_INDEX) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {

                if (Utils.isNetworkAvailable()) {
                    if (isThereMore) {
                        requestGetFavoriteList(page)
                    }
                }
            }
        }

        rv_fav_posts.addOnScrollListener(endlessScrollListener as EndlessRecyclerViewScrollListener)
        rv_fav_posts.setAdapter(favadapterPostDetails)
        favadapterPostDetails!!.setListener(OnItemClickListener { itemView, position -> showConfirmDeleteItem(position) })
    }

    private fun requestGetFavoriteList(page: Int) {
        if (!Utils.isNetworkAvailable()) {
            ShowErrorLayout(R.string.no_internet_connection)
            return
        }

        val apiService = ApiClient.getClient().create(ApiInterface::class.java)

        favcall = apiService.getFavoriteList(page)
        favcall!!.enqueue(object : Callback<favorite> {
            override fun onResponse(call: Call<favorite>, response: Response<favorite>) {
                println("raw" + response.raw())

                try {
                    if (response.body()!!.isStatus && response.body()!!.code == QuickstartPreferences.OK_STATUS_CODE) {
                        if (response.body()!!.posts.size > 0) {
                            showFavContent()
                            favList!!.addAll(response.body()!!.posts)
                            favadapterPostDetails!!.notifyDataSetChanged()
                        } else {
                            isThereMore = false
                            if (page == STARTING_PAGE_INDEX) {
                                showFavEmpty()
                            }
                        }

                    } else if (response.body()!!.code == UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(activity, R.string.Unauthorized, Toast.LENGTH_LONG).show()
                        Utils.LunchLogin(activity)
                    } else {
                        ShowErrorLayout(R.string.err_msg_server)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    ShowErrorLayout(R.string.err_msg_server)
                }

            }

            override fun onFailure(call: Call<favorite>, t: Throwable) {
                if (!call.isCanceled && "Canceled" != t.message) {
                    t.printStackTrace()
                    ShowErrorLayout(R.string.err_msg_server)
                }
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragement_profile, container, false)
        myPostsList = ArrayList()
        favList = ArrayList()
        val view = binding!!.root

        //here data must be an instance of the class MarsDataProvider


        //View view = inflater.inflate(R.layout.fragment_my_account, container, false);
        ButterKnife.bind(this, view)
        //here data must be an instance of the class MarsDataProvider
        pf = PreferenceEditor.getInstance()
        return view
    }

    private fun showFavContent() {
        try {
            rv_fav_posts.setVisibility(View.VISIBLE)
            rl_empty_layout.setVisibility(View.GONE)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun showFavEmpty() {
        try {
            rv_fav_posts.setVisibility(View.GONE)
            rl_empty_layout.setVisibility(View.VISIBLE)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun setupList() {
        myPostsList!!.clear()
        val linearLayoutManager = LinearLayoutManager(activity)
        adapterPostDetails = AdapterMyPost(activity, myPostsList)
        rv_publisher_posts.layoutManager = linearLayoutManager
        rv_publisher_posts.adapter = adapterPostDetails
        endlessScrollListener = object : EndlessRecyclerViewScrollListener(linearLayoutManager, STARTING_PAGE_INDEX) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
                println("page$page")
                if (Utils.isNetworkAvailable()) {
                    if (isThereMore) {
                        println("Page$page")
                        requestGetMyPostList(page)
                    }
                }
            }
        }

        rv_publisher_posts.addOnScrollListener(endlessScrollListener as EndlessRecyclerViewScrollListener)
        adapterPostDetails!!.setListener(OnItemClickListener { itemView, position ->
            try {
                val item = myPostsList!!.get(position)
                when (itemView.id) {

                    R.id.btn_delete -> DeletePost(position)

                    R.id.btn_hide -> if (item.isIs_hidden) {
                        ShowPost(position)
                    } else {
                        HidePost(position)
                    }

                    R.id.btn_edit -> {
                        val i = Intent(activity, AddPostActivity::class.java)
                        i.putExtra("Id", item.id)
                        activity!!.startActivity(i)
                    }

                    else -> {
                        val intent = Intent(context, PostDetailsActivity::class.java)
                        intent.putExtra("Id", item.id)
                        intent.putExtra("SHOULD_SCROLL", false)
                        startActivity(intent)
                    }
                }


            } catch (e: Exception) {
                e.printStackTrace()
                Refresh()
            }
        })

    }

    private fun showConfirmDeleteItem(pos: Int) {

        val dialog = ColorDialog(activity)
        dialog.setAnimationEnable(true)
        dialog.setTitle(getString(R.string.info))
        dialog.contentText = getString(R.string.msg_delete_item)
        //dialog.setContentImage(getResources().getDrawable(R.drawable.ic_warning));
        dialog.setPositiveListener(getString(R.string.yes)) { dialog ->
            dialog.dismiss()
            RemoveFromFav(pos)
        }
                .setNegativeListener(getString(R.string.Cancel)) { dialog -> dialog.dismiss() }.show()
    }

    private fun RemoveFromFav(pos: Int) {

        if (!Utils.isNetworkAvailable()) {
            Utils.ShowNoInternetConnectionDialog(activity!!)
            return
        }

        showProgress()
        val apiService = ApiClient.getClient().create(ApiInterface::class.java)

        val item = favList!!.get(pos)
        RemoveFromFavCall = apiService.addToFavorites(item.id)
        RemoveFromFavCall!!.enqueue(object : Callback<FormalResponse> {
            override fun onResponse(call: Call<FormalResponse>, response: Response<FormalResponse>) {
                hideProgress()
                try {
                    if (response.body() != null) {
                        val formalResponse = response.body()
                        if (formalResponse!!.isStatus && formalResponse.code == QuickstartPreferences.OK_STATUS_CODE) {
                            //Toast.makeText(getApplicationContext(), formalResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            Utils.ShowDialog(activity!!, getString(R.string.success), formalResponse.message, DIALOG_TYPE_SUCCESS)
                            favList!!.removeAt(pos)
                            favadapterPostDetails!!.notifyItemRemoved(pos)
                            if (favList!!.size == 0) {
                                showFavEmpty()
                            }

                        } else if (response.body()!!.code == QuickstartPreferences.UNAUTHORIZED_STATUS_CODE) {
                            Toast.makeText(activity, R.string.Unauthorized, Toast.LENGTH_LONG).show()
                            Utils.LunchLogin(activity)

                        } else {
                            Utils.ShowServerDialog(activity!!)
                        }
                    } else {
                        Utils.ShowServerDialog(activity!!)
                    }

                } catch (e: Exception) {
                    e.printStackTrace()

                    Utils.ShowServerDialog(activity!!)
                }

            }

            override fun onFailure(call: Call<FormalResponse>, t: Throwable) {
                hideProgress()
                t.printStackTrace()
                Utils.ShowServerDialog(activity!!)
            }
        })
    }


    private fun Refresh() {
        isThereMore = true
        myPostsList!!.clear()
        adapterPostDetails!!.notifyDataSetChanged()
        favList!!.clear()
        favadapterPostDetails?.notifyDataSetChanged()
        endlessScrollListener!!.resetState()
        requestGetMyPostList(STARTING_PAGE_INDEX)

    }

    private fun hideProgress() {
        if (indicatorView != null && indicatorView!!.isShowing()) {
            indicatorView!!.dismiss()
        }
    }

    private fun showProgress() {

        if (indicatorView == null) {
            indicatorView = AVLoadingIndicatorDialog(activity)
            //indicatorView.setMessage(msg);
            indicatorView!!.setCancelable(false)
        }
        indicatorView!!.show()
    }

    private fun HidePost(pos: Int) {

        if (!Utils.isNetworkAvailable()) {
            Utils.ShowNoInternetConnectionDialog(activity!!)
            return
        }

        val apiService = ApiClient.getClient().create(ApiInterface::class.java)
        Utils.HideKeyBord(activity!!)
        showProgress()
        val post = myPostsList!!.get(pos)
        HideCall = apiService.HidePost(post.id)
        HideCall!!.enqueue(object : Callback<FormalResponse> {
            override fun onResponse(call: Call<FormalResponse>, response: Response<FormalResponse>) {

                hideProgress()
                try {
                    val formalResponse = response.body()
                    if (formalResponse!!.isStatus && formalResponse.code == QuickstartPreferences.OK_STATUS_CODE) {
                        Utils.ShowDialog(activity!!, getString(R.string.success), formalResponse.message, DIALOG_TYPE_SUCCESS)
                        post.isIs_hidden = true
                        adapterPostDetails!!.notifyItemChanged(pos)

                    } else if (response.body()!!.code == UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(activity, R.string.Unauthorized, Toast.LENGTH_LONG).show()
                        Utils.LunchLogin(activity)
                    } else {
                        ShowErrorLayout(R.string.err_msg_server)
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                    Utils.ShowServerDialog(activity!!)
                }

            }


            override fun onFailure(call: Call<FormalResponse>, t: Throwable) {
                if (!call.isCanceled && "Canceled" != t.message) {
                    hideProgress()
                    t.printStackTrace()
                    Utils.ShowServerDialog(activity!!)
                }
            }
        })
    }

    private fun ShowPost(pos: Int) {
        if (!Utils.isNetworkAvailable()) {
            Utils.ShowNoInternetConnectionDialog(activity!!)
            return
        }


        val apiService = ApiClient.getClient().create(ApiInterface::class.java)
        Utils.HideKeyBord(activity!!)
        showProgress()

        val post = myPostsList!!.get(pos)

        ShowCall = apiService.ShowPost(post.id)
        ShowCall!!.enqueue(object : Callback<FormalResponse> {
            override fun onResponse(call: Call<FormalResponse>, response: Response<FormalResponse>) {

                hideProgress()
                try {
                    val formalResponse = response.body()
                    if (formalResponse!!.isStatus && formalResponse.code == QuickstartPreferences.OK_STATUS_CODE) {
                        Utils.ShowDialog(activity!!, getString(R.string.success), formalResponse.message, DIALOG_TYPE_SUCCESS)
                        post.isIs_hidden = false
                        adapterPostDetails!!.notifyItemChanged(pos)

                    } else if (response.body()!!.code == UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(activity, R.string.Unauthorized, Toast.LENGTH_LONG).show()
                        Utils.LunchLogin(activity)
                    } else {
                        ShowErrorLayout(R.string.err_msg_server)
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                    Utils.ShowServerDialog(activity!!)
                }

            }


            override fun onFailure(call: Call<FormalResponse>, t: Throwable) {
                if (!call.isCanceled && "Canceled" != t.message) {
                    hideProgress()
                    t.printStackTrace()
                    Utils.ShowServerDialog(activity!!)
                }
            }
        })
    }

    private fun requestGetMyPostList(page: Int) {
        if (!Utils.isNetworkAvailable()) {
            ShowErrorLayout(R.string.no_internet_connection)
            return
        }

        val apiService = ApiClient.getClient().create(ApiInterface::class.java)
        myPcall = apiService.getMyPostsList(page)
        myPcall!!.enqueue(object : Callback<MyPosts> {
            override fun onResponse(call: Call<MyPosts>, response: Response<MyPosts>) {
                println("raw" + response.raw())

                try {
                    if (response.body()!!.isStatus && response.body()!!.code == QuickstartPreferences.OK_STATUS_CODE) {
                        if (response.body()!!.data.posts.size > 0) {
                            showContent()
                            try {
                                myPostsList!!.addAll(response.body()!!.data.posts)
                                adapterPostDetails!!.notifyDataSetChanged()
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                        } else {
                            isThereMore = false
                            if (page == STARTING_PAGE_INDEX) {
                                showEmpty()
                            }
                        }

                    } else if (response.body()!!.code == UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(activity, R.string.Unauthorized, Toast.LENGTH_LONG).show()
                        Utils.LunchLogin(activity)
                    } else {
                        ShowErrorLayout(R.string.err_msg_server)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    ShowErrorLayout(R.string.err_msg_server)
                }

            }

            override fun onFailure(call: Call<MyPosts>, t: Throwable) {
                if (!call.isCanceled && "Canceled" != t.message) {
                    t.printStackTrace()
                    ShowErrorLayout(R.string.err_msg_server)
                }
            }
        })
    }


    private fun getUserInfo() {

        if (!Utils.isNetworkAvailable()) {
            ShowErrorLayout(R.string.no_internet_connection)
            return
        }

        val apiService = ApiClient.getClient().create(ApiInterface::class.java)
        call = apiService.userInfo
        if (swipe != null) {
            swipe.isRefreshing = true
        }
        call!!.enqueue(object : Callback<Login> {
            override fun onResponse(call: Call<Login>, response: Response<Login>) {

                if (swipe != null) {
                    swipe.isRefreshing = false
                }

                try {
                    if (response.body()!!.isStatus && response.body()!!.code == QuickstartPreferences.OK_STATUS_CODE) {

                        if (response.body()!!.data != null) {

                            val UserInfo = response.body()!!.data
                            PreferenceEditor.getInstance().setStringPreference(QuickstartPreferences.USER_INFO, Gson().toJson(UserInfo))
                            binding!!.login = (UserInfo)
                            ShowContentLayout()
                        }
                    } else if (response.body()!!.code == UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(activity, R.string.Unauthorized, Toast.LENGTH_LONG).show()
                        Utils.LunchLogin(activity)

                    } else {
                        ShowErrorLayout(R.string.err_msg_server)
                    }


                } catch (e: Exception) {

                    e.printStackTrace()
                    ShowErrorLayout(R.string.err_msg_server)
                }

            }

            override fun onFailure(call: Call<Login>, t: Throwable) {
                if (!call.isCanceled && "Canceled" != t.message) {

                    if (swipe != null) {
                        swipe.isRefreshing = false
                    }

                    t.printStackTrace()
                    ShowErrorLayout(R.string.err_msg_server)
                }
            }
        })


    }


    private fun ShowContentLayout() {
        content.visibility = View.VISIBLE
        rl_no_internet_connection.visibility = View.GONE
    }

    private fun ShowErrorLayout(txt: Int) {
        if (swipe != null) {
            swipe.isRefreshing = false
        }
        if (content != null) {
            content.visibility = View.GONE
            rl_no_internet_connection.visibility = View.VISIBLE
            tv_error.setText(txt)
        }
    }

    private fun showContent() {
        try {
            rv_publisher_posts.visibility = View.VISIBLE
            rl_empty_layout.visibility = View.GONE
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun showEmpty() {
        try {
            rv_publisher_posts.visibility = View.GONE
            rl_empty_layout.visibility = View.VISIBLE
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onRefresh() {
        getUserInfo()
    }

    @OnClick(R.id.btn_update, R.id.btn_refresh)
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.btn_refresh -> getUserInfo()
        }

    }

    override fun onDestroyView() {
        if (call != null) {
            call!!.cancel()
        }
        super.onDestroyView()
    }

    companion object {
        @JvmStatic
        fun newInstance(): MyProfileFragment {
            return MyProfileFragment()
        }
    }

    private fun DeletePost(pos: Int) {
        val dialog = ColorDialog(activity)
        dialog.setAnimationEnable(true)
        dialog.setTitle(getString(R.string.info))
        dialog.contentText = getString(R.string.msg_delete_item)
        //dialog.setContentImage(getResources().getDrawable(R.drawable.ic_warning));
        dialog.setPositiveListener(getString(R.string.Ok)) { dialog ->
            dialog.dismiss()
            requestDeletePost(pos)
        }
                .setNegativeListener(getString(R.string.Cancel)) { dialog -> dialog.dismiss() }.show()
    }

    private fun requestDeletePost(pos: Int) {

        if (!Utils.isNetworkAvailable()) {
            Utils.ShowNoInternetConnectionDialog(activity!!)
            return
        }

        val apiService = ApiClient.getClient().create(ApiInterface::class.java)
        Utils.HideKeyBord(activity!!)
        showProgress()
        val post = myPostsList!!.get(pos)

        DeleteCall = apiService.DeletePost(post.id)
        DeleteCall!!.enqueue(object : Callback<FormalResponse> {
            override fun onResponse(call: Call<FormalResponse>, response: Response<FormalResponse>) {

                hideProgress()
                try {
                    val formalResponse = response.body()
                    if (formalResponse!!.isStatus && formalResponse.code == QuickstartPreferences.OK_STATUS_CODE) {
                        Utils.ShowDialog(activity!!, getString(R.string.success), formalResponse.message, DIALOG_TYPE_SUCCESS)
                        try {


                            myPostsList!!.removeAt(pos)
                            adapterPostDetails!!.notifyItemRemoved(pos)
                        } catch (e: Exception) {
                            e.printStackTrace()
                            Refresh()
                        }

                    } else if (response.body()!!.code == UNAUTHORIZED_STATUS_CODE) {
                        Toast.makeText(activity, R.string.Unauthorized, Toast.LENGTH_LONG).show()
                        Utils.LunchLogin(activity)
                    } else {
                        ShowErrorLayout(R.string.err_msg_server)
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                    Utils.ShowServerDialog(activity!!)
                }

            }


            override fun onFailure(call: Call<FormalResponse>, t: Throwable) {
                if (!call.isCanceled && "Canceled" != t.message) {
                    hideProgress()
                    t.printStackTrace()
                    Utils.ShowServerDialog(activity!!)
                }
            }
        })
    }

}