package com.wasset.adapter;

import android.content.Context;

import androidx.databinding.DataBindingUtil;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.wasset.R;
import com.wasset.databinding.RowMyPostLayoutBinding;
import com.wasset.interfaces.OnItemClickListener;
import com.wasset.models.PostDataBean;

import java.util.List;

/**
 * Created by macbook on 6/12/18.
 */

public class AdapterMyPost extends RecyclerView.Adapter<AdapterMyPost.MyViewHolder> {

    private List<PostDataBean> MyPosts;
    private LayoutInflater layoutInflater;
    private OnItemClickListener listener;
    private Context context;


    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final RowMyPostLayoutBinding binding;

        public MyViewHolder(final RowMyPostLayoutBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }


    public AdapterMyPost(Context context, List<PostDataBean> MyPosts) {
        this.MyPosts = MyPosts;
        this.context = context;
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        RowMyPostLayoutBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.row_my_post_layout, parent, false);

        return new MyViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        try {
            holder.binding.setPost(MyPosts.get(position));

            Button btnDelete = holder.binding.getRoot().findViewById(R.id.btn_delete);
            Button btnEdit = holder.binding.getRoot().findViewById(R.id.btn_edit);
            Button btnHide = holder.binding.getRoot().findViewById(R.id.btn_hide);
            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onItemClick(view, position);
                    }
                }
            });

            btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onItemClick(view, position);
                    }
                }
            });
            btnHide.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onItemClick(view, position);
                    }
                }
            });


            holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onItemClick(v, position);
                    }
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public int getItemCount() {
        return MyPosts.size();
    }

}