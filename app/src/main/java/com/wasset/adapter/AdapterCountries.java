package com.wasset.adapter;

import android.app.Activity;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wasset.R;
import com.wasset.models.Config;

import java.util.List;

/**
 * Created by macbook on 9/6/18.
 */

public class AdapterCountries extends ArrayAdapter<Config.DataBean.CountriesBean> {

    private LayoutInflater flater;
    private List<Config.DataBean.CountriesBean> list ;

    public AdapterCountries(Activity context, int resouceId, int textviewId, List<Config.DataBean.CountriesBean> list) {
        super(context, resouceId, textviewId, list);
        this.list =list;
        flater = context.getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return rowView(convertView,position);
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return rowView(convertView,position);
    }


    private View rowView(View convertView , int position) {
        if (convertView == null) {
            //flater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = flater.inflate(R.layout.row_countries_spinner_item, null, false);
        }

        Config.DataBean.CountriesBean rowItem = getItem(position);
        TextView countryName = (TextView) convertView.findViewById(R.id.tvName);
        countryName.setText(rowItem.getName());
        System.out.println("row"+rowItem.getName());

        ImageView countryFlag = (ImageView) convertView.findViewById(R.id.ivFlag);
        Glide.with(getContext())
                .load(rowItem.getFlag())
                .into(countryFlag);


        return convertView;

    }


    @Override
    public int getCount() {
        return list.size();
    }


    @Override
    public Config.DataBean.CountriesBean getItem(int i) {
        return list.get(i);
    }
}