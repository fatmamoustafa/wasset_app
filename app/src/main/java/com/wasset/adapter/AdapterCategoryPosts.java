package com.wasset.adapter;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wasset.R;
import com.wasset.databinding.RowCategoryPostItemBinding;
import com.wasset.databinding.RowMoreLayoutBinding;
import com.wasset.interfaces.OnItemClickListener;
import com.wasset.models.PostDataBean;

import java.util.List;


public class AdapterCategoryPosts extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<PostDataBean> postDataBeanList;
    private OnItemClickListener listener;
    private Context context;
    private final int ITEM_VIEW = 1 ;
    private final int MORE_VIEW = 2 ;
    private LayoutInflater layoutInflater ;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final RowCategoryPostItemBinding binding;

        public MyViewHolder(final RowCategoryPostItemBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }

    public class MoreItemsViewHolder extends RecyclerView.ViewHolder {

        private final RowMoreLayoutBinding binding;

        MoreItemsViewHolder(final RowMoreLayoutBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }

    AdapterCategoryPosts(Context context, List<PostDataBean> postDataBeanList) {
        this.postDataBeanList = postDataBeanList;
        this.context = context;
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;

        if(layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }

        if (viewType == ITEM_VIEW) {
            RowCategoryPostItemBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.row_category_post_item, parent, false);
            viewHolder = new MyViewHolder(binding);
        } else  if (viewType == MORE_VIEW) {
            RowMoreLayoutBinding MoreBinding = DataBindingUtil.inflate(layoutInflater, R.layout.row_more_layout, parent, false);
            viewHolder = new MoreItemsViewHolder(MoreBinding);
        }
        return viewHolder;

    }


    @Override
    public int getItemViewType(int position) {
        if (postDataBeanList.get(position) == null) {
            return MORE_VIEW;
        }
            return ITEM_VIEW;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {


        if (viewHolder instanceof MyViewHolder) {

            MyViewHolder holder = (MyViewHolder) viewHolder;
            holder.binding.setPost(postDataBeanList.get(position));
            holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onItemClick(v, position);
                    }
                }
            });

        } else if (viewHolder instanceof MoreItemsViewHolder) {
            MoreItemsViewHolder holder = (MoreItemsViewHolder) viewHolder;
            holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onItemClick(view, position);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return postDataBeanList.size();
    }

}