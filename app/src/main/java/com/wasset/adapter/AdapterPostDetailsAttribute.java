package com.wasset.adapter;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wasset.R;
import com.wasset.databinding.RowAttrLayoutBinding;
import com.wasset.interfaces.OnItemClickListener;
import com.wasset.models.PostDataBean;

import java.util.List;

/**
 * Created by macbook on 6/12/18.
 */

public class AdapterPostDetailsAttribute extends RecyclerView.Adapter<AdapterPostDetailsAttribute.MyViewHolder> {

    private List<PostDataBean.AttributesBean> attributesBeanList;
    private LayoutInflater layoutInflater;
    private OnItemClickListener listener;
    private Context context ;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final RowAttrLayoutBinding binding;

        public MyViewHolder(final RowAttrLayoutBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }


    public AdapterPostDetailsAttribute(Context context , List<PostDataBean.AttributesBean> attributesBeanList) {
        this.attributesBeanList = attributesBeanList;
        this.context = context ;
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        RowAttrLayoutBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.row_attr_layout, parent, false);
        return new MyViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.binding.setAttr(attributesBeanList.get(position));
        holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemClick(v,position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return attributesBeanList.size();
    }
}