package com.wasset.adapter;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wasset.R;
import com.wasset.databinding.RowPostStepAttributeListItemLayoutBinding;
import com.wasset.interfaces.OnItemClickListener;
import com.wasset.models.PostSteps;

import java.util.List;

/**
 * Created by macbook on 6/12/18.
 */

public class AdapterPostListItem extends RecyclerView.Adapter<AdapterPostListItem.MyViewHolder> {

    private List<PostSteps.DataBean.FieldBean.ValuesBean> valuesBeanList;
    private LayoutInflater layoutInflater;
    private OnItemClickListener listener;
    private Context context ;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final RowPostStepAttributeListItemLayoutBinding binding;

        public MyViewHolder(final RowPostStepAttributeListItemLayoutBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }


    public AdapterPostListItem(Context context , List<PostSteps.DataBean.FieldBean.ValuesBean> valuesBeanList) {
        this.valuesBeanList = valuesBeanList;
        this.context = context ;
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        RowPostStepAttributeListItemLayoutBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.row_post_step_attribute_list_item_layout, parent, false);
        return new MyViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.binding.setValue(valuesBeanList.get(position));
        holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemClick(v,position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return valuesBeanList.size();
    }

}