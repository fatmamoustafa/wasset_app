package com.wasset.adapter;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.core.content.ContextCompat;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.wasset.R;
import com.wasset.databinding.RowPostDetailsBinding;
import com.wasset.interfaces.OnItemClickListener;
import com.wasset.models.PostDataBean;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by macbook on 6/12/18.
 */

public class AdapterPostDetails extends RecyclerView.Adapter<AdapterPostDetails.MyViewHolder> {

    private List<PostDataBean> FavList;
    private LayoutInflater layoutInflater;
    private OnItemClickListener listener;
    private Context context;
    boolean fromProfileActivity = false;

    public void addNewPosts(List<PostDataBean> posts,boolean shouldClearPreviousPosts) {
        if(shouldClearPreviousPosts)
            posts.clear();
        this.FavList.addAll(posts);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final RowPostDetailsBinding binding;
        @BindView(R.id.tv_product_desc)
        TextView productDescTv;

        public MyViewHolder(final RowPostDetailsBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
            ButterKnife.bind(this, itemView);
        }
    }


    public AdapterPostDetails(List<PostDataBean> FavList) {
        this.FavList = FavList;
    }

    public AdapterPostDetails(List<PostDataBean> FavList, boolean fromProfileActivity) {
        this.FavList = FavList;
        this.fromProfileActivity = fromProfileActivity;
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        RowPostDetailsBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.row_post_details, parent, false);

        return new MyViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.binding.setPost(FavList.get(position));

        ImageButton ivIsFav = holder.binding.getRoot().findViewById(R.id.btn_fav);
        ivIsFav.setOnClickListener(view -> {
            if (listener != null) {
                listener.onItemClick(view, position);
            }
        });

//        if (FavList.get(position).isUser_special()) {
//            holder.binding.icUserName.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.verified_account));
//            holder.binding.tvPublisherName.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
//        } else {
//            holder.binding.icUserName.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_person_24dp));
//            holder.binding.tvPublisherName.setTextColor(ContextCompat.getColor(context, R.color.tv_gray));
//
//        }

        holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemClick(v, position);
                }
            }
        });
        if (fromProfileActivity)
            holder.productDescTv.setVisibility(View.GONE);

    }

    @Override
    public int getItemCount() {
        return FavList.size();
    }

}