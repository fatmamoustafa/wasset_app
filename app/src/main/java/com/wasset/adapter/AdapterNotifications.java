package com.wasset.adapter;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wasset.R;
import com.wasset.databinding.RowNotificationLayoutBinding;
import com.wasset.interfaces.OnItemClickListener;
import com.wasset.models.Notifications;

import java.util.List;

/**
 * Created by macbook on 6/12/18.
 */

public class AdapterNotifications extends RecyclerView.Adapter<AdapterNotifications.MyViewHolder> {

    private List<Notifications.DataBean> notificationsList;
    private LayoutInflater layoutInflater;
    private OnItemClickListener listener;
    private Context context ;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final RowNotificationLayoutBinding binding;

        public MyViewHolder(final RowNotificationLayoutBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }


    public AdapterNotifications(Context context , List<Notifications.DataBean> notificationsList) {
        this.notificationsList = notificationsList;
        this.context = context ;
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        RowNotificationLayoutBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.row_notification_layout, parent, false);
        return new MyViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.binding.setNotification(notificationsList.get(position));
        holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemClick(v,position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return notificationsList.size();
    }

}