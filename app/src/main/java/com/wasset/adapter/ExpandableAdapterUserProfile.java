package com.wasset.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;
import com.wasset.R;
import com.wasset.interfaces.OnItemClickListener;
import com.wasset.models.AdapterExpandedPosts;
import com.wasset.models.ExpandableHolder;
import com.wasset.models.UserPostsCategories;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ahmed Adel on 25/05/2019.
 * email : a3adel@hotmail.com
 */
public class ExpandableAdapterUserProfile extends ExpandableRecyclerViewAdapter<ExpandableAdapterUserProfile.CategoryViewHolder, ExpandableAdapterUserProfile.CategoryPostsViewHolder> {
    private List<UserPostsCategories> userPostsCategories;
    private OnItemClickListener listener;
    Context context;
    String userId;

    public ExpandableAdapterUserProfile(List<UserPostsCategories> groups, String userId) {
        super(groups);
        this.userPostsCategories = groups;
        this.userId = userId;
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public CategoryViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_expandable_category, parent, false);
        context = parent.getContext();
        return new CategoryViewHolder(view);
    }

    @Override
    public CategoryPostsViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_child_category, parent, false);
        return new CategoryPostsViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(CategoryPostsViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        ExpandableHolder userPosts = ((UserPostsCategories) group).getItems().get(childIndex);
        userPosts.getUserPosts().add(null);
        AdapterExpandedPosts adapterExpandedPosts = new AdapterExpandedPosts(((ExpandableHolder) userPosts).getUserPosts(), ((UserPostsCategories) group).getCategory_id(), ((UserPostsCategories) group).getTitle());
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 2);
        holder.postsRecyclerView.setLayoutManager(gridLayoutManager);
        holder.postsRecyclerView.setAdapter(adapterExpandedPosts);

    }

    @Override
    public void onBindGroupViewHolder(CategoryViewHolder holder, int flatPosition, ExpandableGroup group) {
        holder.categoryNameTextView.setText(group.getTitle());
        holder.moreButton.setText("مشاركة");

        holder.moreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String link = userPostsCategories.get(flatPosition).getUserUrl() + "/" + userPostsCategories.get(flatPosition).getCategory_id();
                    if (link != null && !link.isEmpty()) {
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("text/plain");
                        i.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.app_name));
                        i.putExtra(Intent.EXTRA_TEXT, link);
                        context.startActivity(Intent.createChooser(i, context.getString(R.string.share)));
                    } else {
                        Toast.makeText(context, R.string.invalid_post_link, Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    //e.toString();
                    e.printStackTrace();
                }
            }
        });
    }

    class CategoryViewHolder extends GroupViewHolder {
        @BindView(R.id.btn_more)
        Button moreButton;
        @BindView(R.id.tv_category_name)
        TextView categoryNameTextView;

        public CategoryViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class CategoryPostsViewHolder extends ChildViewHolder {
        @BindView(R.id.child_rv)
        RecyclerView postsRecyclerView;

        public CategoryPostsViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
