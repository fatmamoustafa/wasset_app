package com.wasset.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wasset.R;
import com.wasset.interfaces.OnItemClickListener;
import com.wasset.models.UserPostsCategories;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ahmed Adel on 14/06/2019.
 * email : a3adel@hotmail.com
 */
public class AdapterCategoryTitle extends RecyclerView.Adapter<AdapterCategoryTitle.CategoryTitleViewHolder> {
    Context context;
    List<UserPostsCategories> categories;

    public AdapterCategoryTitle(List<UserPostsCategories> categories, String userId) {
        this.categories = categories;

    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    OnItemClickListener onItemClickListener;

    @NonNull
    @Override
    public CategoryTitleViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        context = viewGroup.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_user_categroies, viewGroup, false);
        return new CategoryTitleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryTitleViewHolder holder, int position) {
        holder.categoryNameTextView.setText(categories.get(position).getTitle());
        holder.categoryNameTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(v, position);
            }
        });
        holder.shareImageView.setOnClickListener(v -> onItemClickListener.onItemClick(v, position));
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    class CategoryTitleViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_category_name)
        TextView categoryNameTextView;
        @BindView(R.id.iv_share)
        ImageView shareImageView;

        public CategoryTitleViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
