package com.wasset.adapter;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.wasset.R;
import com.wasset.databinding.RowCommentLayoutBinding;
import com.wasset.interfaces.OnItemClickListener;
import com.wasset.models.Comments;

import java.util.List;

/**
 * Created by macbook on 6/12/18.
 */

public class AdapterComments extends RecyclerView.Adapter<AdapterComments.MyViewHolder> {

    private List<Comments.CommentsBean> commentsBeanList;
    private LayoutInflater layoutInflater;
    private OnItemClickListener listener;
    String userId;

    public void setOnLongClickListener(OnLongClickListener onLongClickListener) {
        this.onLongClickListener = onLongClickListener;
    }

    private OnLongClickListener onLongClickListener;
    private Context context;
    private int itemPosition = -1;

    public int getPosition() {
        return itemPosition;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {

        private final RowCommentLayoutBinding binding;

        public MyViewHolder(final RowCommentLayoutBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
            itemView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            itemPosition = getAdapterPosition();
            MenuItem myActionItem = menu.add(Menu.NONE, 12349,
                    Menu.NONE, "حذف");
            MenuItem editActionItem = menu.add(Menu.NONE, 12348,
                    Menu.NONE, "تعديل");
        }
    }


    public AdapterComments(List<Comments.CommentsBean> commentsBeanList, String userId) {
        this.commentsBeanList = commentsBeanList;
        this.userId = userId;
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        RowCommentLayoutBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.row_comment_layout, parent, false);
        return new MyViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.binding.setComment(commentsBeanList.get(position));
        holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemClick(v, position);
                }
            }
        });
        if (this.userId.equals(commentsBeanList.get(position).getUser_id()))
            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return false;
                }
            });
        else
            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return commentsBeanList.size();
    }

    public interface OnLongClickListener {
        void onLongClick(int position);
    }

}