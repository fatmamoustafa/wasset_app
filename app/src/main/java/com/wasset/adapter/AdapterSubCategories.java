package com.wasset.adapter;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wasset.R;
import com.wasset.databinding.RowSubCategoriesItemBinding;
import com.wasset.interfaces.OnItemClickListener;
import com.wasset.models.categoryList;

import java.util.List;

/**
 * Created by macbook on 6/12/18.
 */

public class AdapterSubCategories extends RecyclerView.Adapter<AdapterSubCategories.MyViewHolder> {

    private List<categoryList.DataBean.SubCategoriesBean> SubCategoriesList;
    private LayoutInflater layoutInflater;
    private OnItemClickListener listener;
    private Context context ;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final RowSubCategoriesItemBinding binding;

        public MyViewHolder(final RowSubCategoriesItemBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }


    public AdapterSubCategories(Context context , List<categoryList.DataBean.SubCategoriesBean> SubCategoriesList) {
        this.SubCategoriesList = SubCategoriesList;
        this.context = context ;
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        RowSubCategoriesItemBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.row_sub_categories_item, parent, false);
        return new MyViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.binding.setSubCategory(SubCategoriesList.get(position));
        holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemClick(v,position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return SubCategoriesList.size();
    }

}