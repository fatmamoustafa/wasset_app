package com.wasset.adapter;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wasset.R;
import com.wasset.databinding.RowSimilarLayoutBinding;
import com.wasset.interfaces.OnItemClickListener;
import com.wasset.models.PostDataBean;

import java.util.List;

/**
 * Created by macbook on 6/12/18.
 */

public class AdapterSimilarAds extends RecyclerView.Adapter<AdapterSimilarAds.MyViewHolder> {

    private List<PostDataBean.RelatedBean> relatedBeanList;
    private LayoutInflater layoutInflater;
    private OnItemClickListener listener;
    private Context context ;

    public void setAds(List<PostDataBean.RelatedBean> related) {
        relatedBeanList.clear();
        relatedBeanList.addAll(related);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final RowSimilarLayoutBinding binding;

        public MyViewHolder(final RowSimilarLayoutBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }


    public AdapterSimilarAds(Context context , List<PostDataBean.RelatedBean> relatedBeanList) {
        this.relatedBeanList = relatedBeanList;
        this.context = context ;
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        RowSimilarLayoutBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.row_similar_layout, parent, false);
        return new MyViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.binding.setSimilar(relatedBeanList.get(position));
        holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemClick(v,position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return relatedBeanList.size();
    }

}