package com.wasset.adapter;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.wasset.R;
import com.wasset.interfaces.OnAttrItemSelectListener;
import com.wasset.models.PostSteps;
import com.wasset.utility.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AdapterPostAttribute extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private LayoutInflater layoutInflater;
    private OnAttrItemSelectListener listener;
    private AppCompatActivity context;
    private final int LIST_TYPE = 0;
    private final int RADIO_GROUP_TYPE = 1;
    private final int EDIT_TEXT_TYPE = 2;
    private PostSteps.DataBean.FieldBean fieldBean;

    class ListViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.rv_post_attributes_list_values)
        RecyclerView rv_post_attributes_list_values;
        @BindView(R.id.btn_next)
        Button btnNext;

        ListViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    class EditTextViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.et_post_item_attributes)
        EditText etPostItemAttributes;
        @BindView(R.id.sp_post_item_attributes)
        Spinner spPostItemAttributes;
        @BindView(R.id.btn_next)
        Button btnNext;

        EditTextViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    class RadioGroupViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.rg_post_item_attributes)
        RadioGroup rgPostItemAttributes;
        @BindView(R.id.rb_option1)
        RadioButton rbOption1;
        @BindView(R.id.rb_option2)
        RadioButton rbOption2;
        @BindView(R.id.btn_next)
        Button btnNext;

        RadioGroupViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


    public AdapterPostAttribute(AppCompatActivity context, PostSteps.DataBean.FieldBean fieldBean) {
        this.context = context;
        this.fieldBean = fieldBean;
    }

    public void setOnAttrItemSelectListener(OnAttrItemSelectListener listener) {
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }

        View v;
        RecyclerView.ViewHolder viewHolder = null;

        switch (viewType) {
            case LIST_TYPE:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_post_step_attribute_list_layout, parent, false);
                viewHolder = new ListViewHolder(v);
                break;
            case EDIT_TEXT_TYPE:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_post_step_attribute_edit_text_item_layout, parent, false);
                viewHolder = new EditTextViewHolder(v);
                break;
            case RADIO_GROUP_TYPE:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_post_step_attribute_radio_group_item_layout, parent, false);
                viewHolder = new RadioGroupViewHolder(v);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof ListViewHolder) {
            ListViewHolder viewHolder = (ListViewHolder) holder;
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
            final AdapterPostListItem adapterPostListItem = new AdapterPostListItem(context, fieldBean.getValues());
            viewHolder.rv_post_attributes_list_values.setLayoutManager(linearLayoutManager);
            viewHolder.rv_post_attributes_list_values.setAdapter(adapterPostListItem);

            adapterPostListItem.setListener((itemView, pos) -> {
                if (listener != null) {
                    listener.onAttrItemSelectText(fieldBean.getValues().get(pos).getName());
                    listener.onAttrItemSelect(fieldBean.getAttribute(), String.valueOf(fieldBean.getValues().get(pos).getId()));
                }
            });

            viewHolder.btnNext.setOnClickListener(view -> {
                if (listener != null) {
                    listener.onAttrItemSelectText(null);
                    listener.onAttrItemSelect(null, null);
                }
            });


        } else if (holder instanceof EditTextViewHolder) {
            final EditTextViewHolder viewHolder = (EditTextViewHolder) holder;
            int spSelection = -1;

            if (String.valueOf(fieldBean.getInput_type()).equalsIgnoreCase("number")) {
                viewHolder.etPostItemAttributes.setInputType(InputType.TYPE_CLASS_NUMBER);
            } else {
                viewHolder.etPostItemAttributes.setInputType(InputType.TYPE_CLASS_TEXT);
            }

            if (fieldBean.getValue() != null && !fieldBean.getValue().isEmpty()) {
                viewHolder.etPostItemAttributes.setText(fieldBean.getValue());
            }

            if (fieldBean.getAlt_attribute() != null && !fieldBean.getAlt_attribute().isEmpty()) {
                viewHolder.spPostItemAttributes.setVisibility(View.VISIBLE);
                List<String> spinnerList = new ArrayList<>();
                for (int i = 0; i < fieldBean.getValues().size(); i++) {
                    PostSteps.DataBean.FieldBean.ValuesBean item = fieldBean.getValues().get(i);
                    if (item.isSelected()) {
                        spSelection = i;
                    }
                    spinnerList.add(item.getName());
                }

                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, spinnerList);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                viewHolder.spPostItemAttributes.setAdapter(dataAdapter);
                if (spSelection != -1) {
                    viewHolder.spPostItemAttributes.setSelection(spSelection);
                }
            } else {
                viewHolder.spPostItemAttributes.setVisibility(View.GONE);
            }

            viewHolder.btnNext.setOnClickListener(view -> {
                String value = viewHolder.etPostItemAttributes.getText().toString().trim();
                if (listener != null) {
                    if (!value.isEmpty()) {
                        listener.onAttrItemSelectText(value + " | " + String.valueOf(fieldBean.getValues().get(viewHolder.spPostItemAttributes.getSelectedItemPosition()).getName()));
                        listener.onAttrMultiItemSelect(fieldBean.getAttribute(), value, fieldBean.getAlt_attribute(), "" + fieldBean.getValues().get(viewHolder.spPostItemAttributes.getSelectedItemPosition()).getId());
                    } else {
                        listener.onAttrItemSelectText(null);
                        listener.onAttrItemSelect(null, null);
                    }
                }
                Utils.HideKeyBord(context);
            });


        } else if (holder instanceof RadioGroupViewHolder) {

            final RadioGroupViewHolder viewHolder = (RadioGroupViewHolder) holder;
            viewHolder.rbOption1.setText(fieldBean.getValues().get(0).getName());
            viewHolder.rbOption2.setText(fieldBean.getValues().get(1).getName());

            if (fieldBean.getValues().get(0).isSelected()) {
                viewHolder.rbOption1.setSelected(true);
            } else if (fieldBean.getValues().get(1).isSelected()) {
                viewHolder.rbOption2.setSelected(true);
            }

            viewHolder.btnNext.setOnClickListener(view -> {
                if (viewHolder.rbOption1.isChecked()) {
                    if (listener != null) {
                        listener.onAttrItemSelectText(fieldBean.getValues().get(0).getName());
                        listener.onAttrItemSelect(fieldBean.getAttribute(), String.valueOf(fieldBean.getValues().get(0).getId()));
                    }
                } else if (viewHolder.rbOption2.isChecked()) {
                    if (listener != null) {
                        listener.onAttrItemSelectText(fieldBean.getValues().get(1).getName());
                        listener.onAttrItemSelect(fieldBean.getAttribute(), String.valueOf(fieldBean.getValues().get(1).getId()));
                    }
                } else {

                    if (listener != null) {
                        listener.onAttrItemSelectText(null);
                        listener.onAttrItemSelect(null, null);
                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {

        int type = -1;
        if (fieldBean.getType().equalsIgnoreCase("ListView")) {
            type = LIST_TYPE;

        } else if (fieldBean.getType().equalsIgnoreCase("EditText")) {
            type = EDIT_TEXT_TYPE;

        } else if (fieldBean.getType().equalsIgnoreCase("RadioButton")) {
            type = RADIO_GROUP_TYPE;

        }
        return type;
    }

    @Override
    public int getItemCount() {

        return 1;
    }

}