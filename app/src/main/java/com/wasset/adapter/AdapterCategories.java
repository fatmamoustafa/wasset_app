package com.wasset.adapter;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wasset.R;
import com.wasset.databinding.RowCategoriesItemBinding;
import com.wasset.interfaces.OnItemClickListener;
import com.wasset.models.categoryList;

import java.util.List;

/**
 * Created by macbook on 6/12/18.
 */

public class AdapterCategories extends RecyclerView.Adapter<AdapterCategories.MyViewHolder> {

    private List<categoryList.DataBean> CategoriesList;
    private LayoutInflater layoutInflater;
    private OnItemClickListener listener;
    private Context context ;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final RowCategoriesItemBinding binding;

        public MyViewHolder(final RowCategoriesItemBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }


    public AdapterCategories(Context context , List<categoryList.DataBean> CategoriesList) {
        this.CategoriesList = CategoriesList;
        this.context = context ;
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        RowCategoriesItemBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.row_categories_item, parent, false);
        return new MyViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.binding.setCategories(CategoriesList.get(position));
        holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemClick(v,position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return CategoriesList.size();
    }

}