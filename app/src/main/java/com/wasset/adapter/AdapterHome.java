package com.wasset.adapter;

import android.content.Context;
import android.content.Intent;

import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wasset.R;
import com.wasset.databinding.ItemRowHomeBinding;
import com.wasset.interfaces.OnItemClickListener;
import com.wasset.models.Home;
import com.wasset.models.PostDataBean;
import com.wasset.ui.CategoryPostsActivity;
import com.wasset.ui.HomeActivity;
import com.wasset.ui.PostDetailsActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by macbook on 6/12/18.
 */

public class AdapterHome extends RecyclerView.Adapter<AdapterHome.MyViewHolder> {

    private List<Home.DataBean> HomeList;
    private LayoutInflater layoutInflater;
    private OnItemClickListener listener;
    private Context context;
    boolean fromProfile = false;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final ItemRowHomeBinding binding;

        public MyViewHolder(final ItemRowHomeBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }


    public AdapterHome(List<Home.DataBean> HomeList) {
        this.HomeList = HomeList;
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public AdapterHome.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        ItemRowHomeBinding HomeBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_row_home, parent, false);
        return new MyViewHolder(HomeBinding);

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {


        Home.DataBean homeItem = HomeList.get(position);
        holder.binding.setHome(homeItem);
        RecyclerView rv_products = holder.binding.getRoot().findViewById(R.id.rv_products);
        TextView btn_more = holder.binding.getRoot().findViewById(R.id.btn_more);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        final List<PostDataBean> CategoryPostsList = new ArrayList<>();
        CategoryPostsList.addAll(homeItem.getPosts());
        CategoryPostsList.add(null);
        AdapterCategoryPosts adapterCategoryPosts = new AdapterCategoryPosts(context, CategoryPostsList);
        rv_products.setLayoutManager(linearLayoutManager);
        rv_products.setAdapter(adapterCategoryPosts);
        adapterCategoryPosts.setListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int pos) {
                PostDataBean item = CategoryPostsList.get(pos);
                if (item != null) {
                    Intent intent = new Intent(context, PostDetailsActivity.class);
                    intent.putExtra("Id", item.getId());
                    intent.putExtra("SHOULD_SCROLL", false);
                    context.startActivity(intent);
//                    try {
//                        ((HomeActivity) (context)).getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, PostDetailsActivity.newInstance(item.getId(), true)).
//                                setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out).commit();
//                    } catch (Exception e) {
//
//                    }
                } else {

                    System.out.println(" Id " + HomeList.get(pos).getCategory_id());

                    try {
                        ((HomeActivity) (context)).getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, CategoryPostsActivity.newInstance(HomeList.get(position).getCategory_id(), HomeList.get(position).getName(),"home")).
                                setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out).commit();
                    } catch (Exception e) {

                    }
                }
            }
        });

        btn_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onItemClick(view, position);
                }
            }
        });

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return HomeList.size();
    }

}