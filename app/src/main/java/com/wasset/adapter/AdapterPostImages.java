package com.wasset.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.joooonho.SelectableRoundedImageView;
import com.wasset.R;
import com.wasset.interfaces.OnItemClickListener;
import com.wasset.models.PostImage;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by macbook on 6/12/18.
 */

public class AdapterPostImages extends RecyclerView.Adapter<AdapterPostImages.MyViewHolder> {

    List<PostImage> imagesList;
    private LayoutInflater layoutInflater;
    private OnItemClickListener listener;
    private Context context;
    public static int URL_TYPE = 0;
    public static int FILE_TYPE = 1;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_post_image)
        SelectableRoundedImageView ivPostImage;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


    public AdapterPostImages(Context context, List<PostImage> imagesList) {
        this.imagesList = imagesList;
        this.context = context;
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_post_image_layout, parent, false);
        return new MyViewHolder(v);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        if (imagesList.get(position) != null && imagesList.get(position).getType() == FILE_TYPE) {
            holder.ivPostImage.setImageBitmap(imagesList.get(position).getImageBitmap());
        } else if (imagesList.get(position) != null && imagesList.get(position).getType() == URL_TYPE) {
            if(imagesList.get(position).getUrl()!= null && !imagesList.get(position).getUrl().isEmpty()) {
                Glide.with(context)
                        .load(imagesList.get(position).getUrl())
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                return false;
                            }
                        })
                        .into(holder.ivPostImage);
            }else {
                holder.ivPostImage.setImageResource(R.drawable.ic_placeholder);
            }
        } else {
            holder.ivPostImage.setImageResource(R.drawable.ic_photo);
        }

        holder.ivPostImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onItemClick(view, holder.getAdapterPosition());
                }
            }
        });
    }

   /* public void remove(int pos){
        imagesList.remove(pos);
        notifyItemRemoved(pos);
    }


    public void add(PostImage item){
        imagesList.add(item);
        notifyItemInserted(imagesList.size() - 1);
    }*/

    @Override
    public int getItemCount() {
        return imagesList.size();
    }


}