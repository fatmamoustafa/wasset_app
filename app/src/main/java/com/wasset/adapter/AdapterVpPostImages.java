package com.wasset.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.github.chrisbanes.photoview.PhotoView;
import com.wasset.R;
import com.wasset.interfaces.OnAttrItemSelectListener;
import com.wasset.models.PostDataBean;

import java.util.List;

/**
 * Created by macbook on 9/21/18.
 */

public class AdapterVpPostImages extends PagerAdapter {
    private LayoutInflater layoutInflater;
    private List<PostDataBean.PhotosBean> photos;
    private Context context;
    private OnAttrItemSelectListener listener;

    public AdapterVpPostImages(Context context,List<PostDataBean.PhotosBean> photos) {
        this.context = context;
        this.photos = photos;

    }

    public void setOnAttrItemSelectListener(OnAttrItemSelectListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = null;

        final PostDataBean.PhotosBean image = photos.get(position);
        view = layoutInflater.inflate(R.layout.full_image_dialog, container, false);
        PhotoView full_image = (PhotoView) view.findViewById(R.id.img_full_image);

        Glide.with(view.getContext())
                .load(image.getUrl())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        full_image.setImageResource(R.mipmap.ic_launcher);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(full_image);



        container.addView(view);
        return view;
    }


    @Override
    public int getCount() {
        return photos.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }


}
