package com.wasset.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;

import com.wasset.R;
import com.wasset.interfaces.OnAttrItemSelectListener;
import com.wasset.interfaces.OnItemClickListener;
import com.wasset.models.PostSteps;
import com.wasset.utility.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by macbook on 9/21/18.
 */

public class AdapterVpPostAttribute extends PagerAdapter {
    private LayoutInflater layoutInflater;
    private List<PostSteps.DataBean> postStepsList;
    private AppCompatActivity context;
    private OnAttrItemSelectListener listener;

    public AdapterVpPostAttribute(AppCompatActivity context, List<PostSteps.DataBean> postStepsList) {
        this.context = context;
        this.postStepsList = postStepsList;

    }

    public void setOnAttrItemSelectListener(OnAttrItemSelectListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = null;

        final PostSteps.DataBean StepItem = postStepsList.get(position);
        if (StepItem.getField().getType().equalsIgnoreCase("ListView")) {
            view = layoutInflater.inflate(R.layout.row_post_step_attribute_list_layout, container, false);

            RecyclerView rv_post_attributes_list_values = (RecyclerView) view.findViewById(R.id.rv_post_attributes_list_values);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
            final AdapterPostListItem adapterPostListItem = new AdapterPostListItem(context, StepItem.getField().getValues());
            rv_post_attributes_list_values.setLayoutManager(linearLayoutManager);
            rv_post_attributes_list_values.setAdapter(adapterPostListItem);
            adapterPostListItem.setListener(new OnItemClickListener() {
                @Override
                public void onItemClick(View itemView, int pos) {
                        if (listener != null) {
                            listener.onAttrItemSelectText(StepItem.getField().getValues().get(pos).getName());
                            listener.onAttrItemSelect(StepItem.getField().getAttribute(), String.valueOf(StepItem.getField().getValues().get(pos).getId()));
                        }
                }
            });

        } else if (StepItem.getField().getType().equalsIgnoreCase("EditText")) {
            view = layoutInflater.inflate(R.layout.row_post_step_attribute_edit_text_item_layout, container, false);

            final AppCompatEditText etPostItemAttributes = (AppCompatEditText) view.findViewById(R.id.et_post_item_attributes);
            final AppCompatSpinner spPostItemAttributes = (AppCompatSpinner) view.findViewById(R.id.sp_post_item_attributes);
            AppCompatButton btnNext = (AppCompatButton) view.findViewById(R.id.btn_next);


            int spSelection = -1;

            if (String.valueOf(StepItem.getField().getInput_type()).equalsIgnoreCase("number")) {
                etPostItemAttributes.setInputType(InputType.TYPE_CLASS_NUMBER);
            } else {
                etPostItemAttributes.setInputType(InputType.TYPE_CLASS_TEXT);
            }


            if (StepItem.getField().getValue() != null && !StepItem.getField().getValue().isEmpty()) {
                etPostItemAttributes.setText(StepItem.getField().getValue());
            }

            if (StepItem.getField().getAlt_attribute() != null && !StepItem.getField().getAlt_attribute().isEmpty()) {
                spPostItemAttributes.setVisibility(View.VISIBLE);
                List<String> spinnerList = new ArrayList<>();
                for (int i = 0; i < StepItem.getField().getValues().size(); i++) {
                    PostSteps.DataBean.FieldBean.ValuesBean item = StepItem.getField().getValues().get(i);
                    if (item.isSelected()) {
                        spSelection = i;
                    }
                    spinnerList.add(item.getName());
                }

                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, spinnerList);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spPostItemAttributes.setAdapter(dataAdapter);

                if (spSelection != -1) {
                    spPostItemAttributes.setSelection(spSelection);
                }
                /*
                spPostItemAttributes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
                */

            } else {
                spPostItemAttributes.setVisibility(View.GONE);
            }


            btnNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String value = etPostItemAttributes.getText().toString().trim();


                        if (listener != null) {
                            if (!value.isEmpty()) {
                                listener.onAttrItemSelectText(value + " | " + String.valueOf(StepItem.getField().getValues().get(spPostItemAttributes.getSelectedItemPosition()).getName()));
                                listener.onAttrItemSelect(StepItem.getField().getAttribute(), value);
                            } else {

                                listener.onAttrItemSelectText(null);
                                listener.onAttrItemSelect(null, null);

                            }
                        }



                    Utils.HideKeyBord(context);

                }
            });


        } else if (StepItem.getField().getType().equalsIgnoreCase("RadioButton")) {
            view = layoutInflater.inflate(R.layout.row_post_step_attribute_radio_group_item_layout, container, false);

            final RadioButton rbOption1 = (RadioButton) view.findViewById(R.id.rb_option1);
            final RadioButton rbOption2 = (RadioButton) view.findViewById(R.id.rb_option2);
            AppCompatButton btnNext = (AppCompatButton) view.findViewById(R.id.btn_next);

            rbOption1.setText(StepItem.getField().getValues().get(0).getName());
            rbOption2.setText(StepItem.getField().getValues().get(1).getName());


            if (StepItem.getField().getValues().get(0).isSelected()) {
                rbOption1.setSelected(true);
            } else if (StepItem.getField().getValues().get(1).isSelected()) {
                rbOption2.setSelected(true);
            }


            btnNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                        /*
                        if (rbOption1.isChecked()) {

                            if (listener != null) {
                                listener.onAttrItemSelectText(StepItem.getField().getValues().get(0).getName());
                                listener.onAttrItemSelect(StepItem.getField().getAttribute(), String.valueOf(StepItem.getField().getValues().get(0).getId()));
                            }
                        } else if (rbOption2.isChecked()) {
                            if (listener != null) {
                                listener.onAttrItemSelectText(StepItem.getField().getValues().get(1).getName());
                                listener.onAttrItemSelect(StepItem.getField().getAttribute(), String.valueOf(StepItem.getField().getValues().get(1).getId()));
                            }
                        } else {

                            if (listener != null) {
                                listener.onAttrItemSelectText(null);
                                listener.onAttrItemSelect(null, null);
                            }


                        }
                        */

                }
            });

        }


        container.addView(view);
        return view;
    }


    @Override
    public int getCount() {
        return postStepsList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }


}
