package com.wasset.adapter;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.wasset.R;
import com.wasset.interfaces.OnItemClickListener;
import com.wasset.models.PostSteps;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by macbook on 6/12/18.
 */

public class AdapterPostEditTextItem extends RecyclerView.Adapter<AdapterPostEditTextItem.MyViewHolder> {

    private List<PostSteps.DataBean.FieldBean.ValuesBean> valuesBeanList;
    private LayoutInflater layoutInflater;
    private OnItemClickListener listener;
    private Context context ;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.et_post_item_attributes)
        AppCompatEditText etPostItemAttributes;
        @BindView(R.id.sp_post_item_attributes)
        AppCompatSpinner spPostItemAttributes;


        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


    public AdapterPostEditTextItem(Context context , List<PostSteps.DataBean.FieldBean.ValuesBean> valuesBeanList) {
        this.valuesBeanList = valuesBeanList;
        this.context = context ;
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_post_step_attribute_edit_text_item_layout, parent, false);
        return new MyViewHolder(v);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {


        holder.spPostItemAttributes.setVisibility(View.VISIBLE);
        List<String> spinnerList = new ArrayList<>();

        for (PostSteps.DataBean.FieldBean.ValuesBean item : valuesBeanList) {
            spinnerList.add(item.getName());
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, spinnerList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        holder.spPostItemAttributes.setAdapter(dataAdapter);
        holder.spPostItemAttributes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



    }

    @Override
    public int getItemCount() {
        return valuesBeanList.size();
    }

}