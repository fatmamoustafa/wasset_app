package com.wasset.adapter;

import android.content.Context;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wasset.R;
import com.wasset.interfaces.OnItemClickListener;
import com.wasset.models.SelectedAttr;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by macbook on 6/12/18.
 */

public class AdapterSelectedPostAttribute extends RecyclerView.Adapter<AdapterSelectedPostAttribute.MyViewHolder> {


    private List<SelectedAttr> SelectedAttrList;
    private LayoutInflater layoutInflater;
    private OnItemClickListener listener;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_attr_title)
        AppCompatTextView tvAttrTitle;
        @BindView(R.id.tv_attr_value)
        AppCompatTextView tvAttrValue;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


    public AdapterSelectedPostAttribute(Context context, List<SelectedAttr> SelectedAttrList) {
        this.SelectedAttrList = SelectedAttrList;
        this.context = context;
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_selected_post_attr_layout, parent, false);
        return new MyViewHolder(v);


    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.tvAttrTitle.setText(SelectedAttrList.get(position).getKey());
        holder.tvAttrValue.setText(SelectedAttrList.get(position).getValue());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return SelectedAttrList.size();
    }
}