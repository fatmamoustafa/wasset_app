package com.wasset.utility;

/**
 * Created by AHMED AHEL on 27-Apr-16.
 */
public class QuickstartPreferences {

    public final static String MAIN_SCREEN_STATE = "main";
    public final static String SELECTED_MENU_ITEM = "selected_menu_item";
    public final static String NOTIFICATION_COUNT = "notification";


    public static final int OK_STATUS_CODE = 200;
    public static final int UNAUTHORIZED_STATUS_CODE = 401;
    public static final int NOT_FOUND_CODE = 404;
    public static final int VALIDATION_ERROR_CODE = 422;


    public static final String USER_INFO = "USER_INFO";
    public static final String CONFIG = "CONFIG";
    public static final String IS_NOT_FIRST_LUNCH = "isFirstLunch";
    public static final String BADGE_COUNT = "BADGE_COUNT";

}