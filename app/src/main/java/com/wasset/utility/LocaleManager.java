package com.wasset.utility;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;

import java.util.Locale;

/**
 * Manage app locale change and persistence
 *
 * @author Omar Albelbaisy
 */
public class LocaleManager {

    /**
     *
     * @param context application context
     * @return
     */
    public static Context prepareContext(Context context) {
        return prepareContext(context, getLocaleCode(context));
    }

    /**
     *
     * @param context application context
     * @param language
     * @return
     */
    public static Context prepareContext(Context context, String language) {
        persistLocaleCode(context, language);
        return wrap(context, language);
    }

    /**
     * Get saved locale code
     * @param context application context
     * @return
     */
    private static String getLocaleCode(Context context) {
        String localeCode = Locale.getDefault().getLanguage();
        SharedPreferences preferences = context.getSharedPreferences("config", Context.MODE_PRIVATE);
        localeCode = preferences.getString("locale_code", localeCode);
        return localeCode;
    }

    /**
     * Save chosen locale code
     * @param context application context
     * @param localeCode
     */
    private static void persistLocaleCode(Context context, String localeCode) {
        SharedPreferences preferences = context.getSharedPreferences("config", Context.MODE_PRIVATE);
        preferences.edit().putString("locale_code", localeCode).apply();
    }

    /**
     * Update context locale to match the chosen locale
     * @param context application context
     * @param localeCode
     * @return
     */
    private static Context wrap(Context context, String localeCode) {
        Locale locale = new Locale(localeCode);
        Locale.setDefault(locale);
        Resources res = context.getResources();
        Configuration config = res.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            setSystemLocale(config, locale);
        } else {
            setSystemLocaleLegacy(config, locale);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            context = context.createConfigurationContext(config);
        } else {
            res.updateConfiguration(config, res.getDisplayMetrics());
        }
        return context;
    }

    @SuppressWarnings("deprecation")
    private static void setSystemLocaleLegacy(Configuration config, Locale locale) {
        config.locale = locale;
    }

    @TargetApi(Build.VERSION_CODES.N)
    private static void setSystemLocale(Configuration config, Locale locale) {
        config.setLocale(locale);
    }
}