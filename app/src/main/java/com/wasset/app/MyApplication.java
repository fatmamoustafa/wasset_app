package com.wasset.app;

import android.content.res.Configuration;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;
import com.twitter.sdk.android.core.Twitter;
import com.wasset.utility.LocaleManager;

import io.fabric.sdk.android.Fabric;

public class MyApplication extends MultiDexApplication {

    private static MyApplication instance;

   /* static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }*/

    public static MyApplication getInstance() {
        return MyApplication.instance;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        /*Add Crashlytics: */
        Fabric.with(this, new Crashlytics());

        instance = this;
        Twitter.initialize(this);
        MultiDex.install(this);


    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Update context locale to match chosen locale after configuration change
        LocaleManager.prepareContext(this);
    }

}
