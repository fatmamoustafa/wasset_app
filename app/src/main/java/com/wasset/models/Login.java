package com.wasset.models;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.joooonho.SelectableRoundedImageView;
import com.wasset.BR;
import com.wasset.R;

/**
 * Created by macbook on 7/14/18.
 */

public class Login {

    private boolean status;
    private int code;
    private String message;
    private DataBean data;
    private ValidationBean validation;

    public boolean isStatus() {
        return status;
    }

    public ValidationBean getValidation() {
        return validation;
    }

    public void setValidation(ValidationBean validation) {
        this.validation = validation;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean extends BaseObservable {
        /**
         * id : 5
         * full_name :
         * username : ahmedahel
         * email : ahmedahel92@gmail.com
         * password : $1$7.5K4nCX$HkYgfR5ripKnZzG1Ehckm1
         * account_status : 2
         * mobile_code :
         * mobile_number : 972599781377
         * picture : null
         * device_id : 32434234
         * fcm : asdasdsdssd
         * blocked : 1
         * device_type : android
         * city_id : 0
         * last_login : null
         * added_date : 2018-07-14 17:01:39
         * token : $1$RHEXd15z$FhKfq.c/IEJckRyvHtprq0
         */

        private String id;
        private String full_name;
        private String username;
        private String email;
        private String password;
        private String account_status;
        private String mobile_code;
        private String mobile_number;
        private String picture;
        private String device_id;
        private String fcm;
        private String blocked;
        private String device_type;
        private String city_id;
        private Object last_login;
        private String added_date;
        private String token;
        private String currency;
        private Boolean user_special;

        public Boolean getUser_special() {
            return user_special;
        }

        public void setUser_special(Boolean user_special) {
            this.user_special = user_special;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        @Bindable
        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
            notifyPropertyChanged(BR.id);
        }

        @Bindable
        public String getFull_name() {
            return full_name;
        }

        public void setFull_name(String full_name) {
            this.full_name = full_name;
            notifyPropertyChanged(BR.full_name);
        }

        @Bindable
        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
            notifyPropertyChanged(BR.username);
        }

        @Bindable
        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
            notifyPropertyChanged(BR.email);
        }

        @Bindable
        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
            notifyPropertyChanged(BR.password);
        }

        @Bindable
        public String getAccount_status() {
            return account_status;
        }

        public void setAccount_status(String account_status) {
            this.account_status = account_status;
            notifyPropertyChanged(BR.account_status);
        }

        @Bindable
        public String getMobile_code() {
            return mobile_code;
        }

        public void setMobile_code(String mobile_code) {
            this.mobile_code = mobile_code;
            notifyPropertyChanged(BR.mobile_code);
        }

        @Bindable
        public String getMobile_number() {
            return mobile_number;
        }

        public void setMobile_number(String mobile_number) {
            this.mobile_number = mobile_number;
            notifyPropertyChanged(BR.mobile_number);
        }

        @Bindable
        public String getPicture() {
            return picture;
        }

        public void setPicture(String picture) {
            this.picture = picture;
            notifyPropertyChanged(BR.picture);
        }

        @BindingAdapter("imageUrl")
        public static void loadImage(SelectableRoundedImageView view, String imageUrl) {
            if (imageUrl != null && !imageUrl.isEmpty()) {
                Glide.with(view.getContext())
                        .load(imageUrl)
                        .into(view);
            } else {
                view.setImageResource(R.drawable.ic_placeholder);
            }
        }


        @Bindable
        public String getDevice_id() {
            return device_id;
        }

        public void setDevice_id(String device_id) {
            this.device_id = device_id;
            notifyPropertyChanged(BR.device_id);
        }

        @Bindable
        public String getFcm() {
            return fcm;
        }

        public void setFcm(String fcm) {
            this.fcm = fcm;
            notifyPropertyChanged(BR.fcm);
        }

        @Bindable
        public String getBlocked() {
            return blocked;
        }

        public void setBlocked(String blocked) {
            this.blocked = blocked;
            notifyPropertyChanged(BR.blocked);
        }

        @Bindable
        public String getDevice_type() {
            return device_type;
        }

        public void setDevice_type(String device_type) {
            this.device_type = device_type;
            notifyPropertyChanged(BR.device_type);
        }

        @Bindable
        public String getCity_id() {
            return city_id;
        }

        public void setCity_id(String city_id) {
            this.city_id = city_id;
            notifyPropertyChanged(BR.city_id);
        }

        @Bindable
        public Object getLast_login() {
            return last_login;
        }

        public void setLast_login(Object last_login) {
            this.last_login = last_login;
            notifyPropertyChanged(BR.last_login);
        }

        @Bindable
        public String getAdded_date() {
            return added_date;
        }

        public void setAdded_date(String added_date) {
            this.added_date = added_date;
            notifyPropertyChanged(BR.added_date);
        }

        @Bindable
        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
            notifyPropertyChanged(BR.token);
        }
    }


}
