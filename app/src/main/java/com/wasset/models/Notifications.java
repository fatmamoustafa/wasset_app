package com.wasset.models;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.joooonho.SelectableRoundedImageView;
import com.wasset.R;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by macbook on 9/1/18.
 */

public class Notifications {


    /**
     * status : true
     * code : 200
     * data : [{"user_id":"1","title":"تعليق جديد","post_id":"48","content":"قام العميل حازم أسامة الغلاييني بالتعليق على إعلان لك","type":"comment","added_date":"منذ 20 ساعات","picture":"http://hwaseet.com.sa/uploads/users/9b9b736c8075e25239cd2bbc935c0006.jpg"},{"user_id":"1","title":"إضافة للمفضلة","post_id":"105","content":" قام العميل حازم أسامة الغلاييني بإضافة إعلانك لمفضلته","type":"favorite","added_date":"منذ 0 دقائق","picture":"http://hwaseet.com.sa/uploads/users/9b9b736c8075e25239cd2bbc935c0006.jpg"},{"user_id":"1","title":"إضافة للمفضلة","post_id":"104","content":"قام العميل  حازم أسامة الغلاييني بإضافة إعلانك لمفضلته","type":"favorite","added_date":"منذ 0 دقائق","picture":"http://hwaseet.com.sa/uploads/users/9b9b736c8075e25239cd2bbc935c0006.jpg"},{"user_id":"1","title":"إضافة للمفضلة","post_id":"50","content":"قام العميل  حازم أسامة الغلاييني بإضافة إعلانك لمفضلته","type":"favorite","added_date":"منذ 0 دقائق","picture":"http://hwaseet.com.sa/uploads/users/9b9b736c8075e25239cd2bbc935c0006.jpg"}]
     */

    private boolean status;
    private int code;
    private List<DataBean> data;



    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<DataBean> getData() {
        if(data == null){
            data =  new ArrayList<>();
        }
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }


    public static class DataBean {
        /**
         * user_id : 1
         * title : تعليق جديد
         * post_id : 48
         * content : قام العميل حازم أسامة الغلاييني بالتعليق على إعلان لك
         * type : comment
         * added_date : منذ 20 ساعات
         * picture : http://hwaseet.com.sa/uploads/users/9b9b736c8075e25239cd2bbc935c0006.jpg
         */

        private String user_id;
        private String title;
        private String post_id;
        private String content;
        private String type;
        private String added_date;
        private String picture;

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getPost_id() {
            return post_id;
        }

        public void setPost_id(String post_id) {
            this.post_id = post_id;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getAdded_date() {
            return added_date;
        }

        public void setAdded_date(String added_date) {
            this.added_date = added_date;
        }

        public String getPicture() {
            return picture;
        }

        public void setPicture(String picture) {
            this.picture = picture;
        }

        @BindingAdapter("imageUrl")
        public static void loadImage(SelectableRoundedImageView view, String imageUrl) {
            if(imageUrl!= null && !imageUrl.isEmpty()) {
                Glide.with(view.getContext())
                        .load(imageUrl)
                        .into(view);
            }else {
                view.setImageResource(R.drawable.ic_placeholder);
            }
        }
    }
}
