package com.wasset.models;

import java.util.List;

/**
 * Created by macbook on 8/18/18.
 */

public class steps4details {


    /**
     * status : true
     * code : 200
     * data : [{"title":"سيارة حديثة للبيع"},{"details":"السيارة للبيع لاعلى سعر"},{"price":"1000"},{"mobile_number":"972599781377"},{"category_id":"11"},{"title":"النوع","field":{"attribute":"brand_id","type":"ListView","input_type":null,"depend_on":null,"values":[{"id":1,"name":"هيونداي","selected":true},{"id":2,"name":"تويوتا","selected":false},{"id":3,"name":"كيا","selected":false},{"id":4,"name":"بي ام دبليو","selected":false},{"id":5,"name":"مرسيدس بنز","selected":false},{"id":6,"name":"استون مارتن","selected":false},{"id":7,"name":"الفا روميو","selected":false},{"id":8,"name":"أودي","selected":false},{"id":9,"name":"ايسوزو","selected":false},{"id":10,"name":"إنفينيتي","selected":false},{"id":11,"name":"أوبل","selected":false},{"id":12,"name":"بنتلي","selected":false},{"id":13,"name":"بورش","selected":false},{"id":14,"name":"بونتياك","selected":false},{"id":15,"name":"بيجو","selected":false},{"id":16,"name":"جاغوار","selected":false},{"id":17,"name":"جريت وول","selected":false},{"id":18,"name":"جي إم سي","selected":false},{"id":19,"name":"جيب","selected":false},{"id":20,"name":"دايهاتسو","selected":false},{"id":21,"name":"دايو","selected":false},{"id":22,"name":"دودج","selected":false},{"id":23,"name":"روفر","selected":false},{"id":24,"name":"رولز رویس","selected":false},{"id":25,"name":"رينو","selected":false}],"alt_attribute":null,"value":null}},{"title":"الفئة","field":{"attribute":"model_id","type":"ListView","input_type":null,"depend_on":"brand_id","values":null,"alt_attribute":null,"value":"2"}},{"title":"سنة الصنع","field":{"attribute":"year_id","type":"ListView","input_type":null,"depend_on":null,"values":[{"id":1,"name":"2010","selected":true},{"id":2,"name":"2011","selected":false},{"id":3,"name":"2012","selected":false},{"id":4,"name":"2013","selected":false},{"id":5,"name":"2014","selected":false},{"id":6,"name":"2015","selected":false},{"id":7,"name":"2016","selected":false},{"id":8,"name":"2017","selected":false},{"id":9,"name":"2018","selected":false}],"alt_attribute":null,"value":null}},{"title":"الحالة","field":{"attribute":"used_type","type":"ListView","input_type":null,"depend_on":null,"values":[{"id":1,"name":"مستخدم","selected":true},{"id":2,"name":"جديد","selected":false}],"alt_attribute":null,"value":null}},{"title":"ناقل الحركة","field":{"attribute":"transmission_type","type":"ListView","input_type":null,"depend_on":null,"values":[{"id":1,"name":"عادي","selected":true},{"id":2,"name":"أوتوماتيك","selected":false},{"id":3,"name":"غير ذلك","selected":false}],"alt_attribute":null,"value":null}},{"title":"عدد الكيلومترات","field":{"attribute":"kilometers_id","type":"ListView","input_type":null,"depend_on":null,"values":[{"id":1,"name":"0","selected":false},{"id":2,"name":"1 - 999","selected":false},{"id":3,"name":"1000 - 10000","selected":false},{"id":4,"name":"10000 - 100000","selected":false},{"id":5,"name":"Other","selected":false}],"alt_attribute":null,"value":null}},{"title":"نوع الوقود","field":{"attribute":"fuel_type","type":"ListView","input_type":null,"depend_on":null,"values":[{"id":1,"name":"بنزين","selected":true},{"id":2,"name":"ديزل","selected":false},{"id":3,"name":"هايبرد","selected":false},{"id":4,"name":"كهربائي","selected":false}],"alt_attribute":null,"value":null}},{"title":"اللون","field":{"attribute":"color_id","type":"ListView","input_type":null,"depend_on":null,"values":[{"id":1,"name":"أحمر","selected":false},{"id":2,"name":"أزرق","selected":false}],"alt_attribute":null,"value":null}},{"title":"المدينة","field":{"attribute":"city_id","type":"ListView","input_type":null,"depend_on":null,"values":[{"id":1,"name":"أبو ظبي","selected":true},{"id":2,"name":"أم القيوين","selected":false},{"id":3,"name":"عجمان","selected":false},{"id":4,"name":"دبي","selected":false},{"id":5,"name":"الفجيرة","selected":false},{"id":6,"name":"رأس الخيمة","selected":false},{"id":7,"name":"الشارقة","selected":false},{"id":8,"name":"العين","selected":false}],"alt_attribute":null,"value":null}}]
     */

    private boolean status;
    private int code;
    private List<DataBean> data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * title : سيارة حديثة للبيع
         * details : السيارة للبيع لاعلى سعر
         * price : 1000
         * mobile_number : 972599781377
         * category_id : 11
         * field : {"attribute":"brand_id","type":"ListView","input_type":null,"depend_on":null,"values":[{"id":1,"name":"هيونداي","selected":true},{"id":2,"name":"تويوتا","selected":false},{"id":3,"name":"كيا","selected":false},{"id":4,"name":"بي ام دبليو","selected":false},{"id":5,"name":"مرسيدس بنز","selected":false},{"id":6,"name":"استون مارتن","selected":false},{"id":7,"name":"الفا روميو","selected":false},{"id":8,"name":"أودي","selected":false},{"id":9,"name":"ايسوزو","selected":false},{"id":10,"name":"إنفينيتي","selected":false},{"id":11,"name":"أوبل","selected":false},{"id":12,"name":"بنتلي","selected":false},{"id":13,"name":"بورش","selected":false},{"id":14,"name":"بونتياك","selected":false},{"id":15,"name":"بيجو","selected":false},{"id":16,"name":"جاغوار","selected":false},{"id":17,"name":"جريت وول","selected":false},{"id":18,"name":"جي إم سي","selected":false},{"id":19,"name":"جيب","selected":false},{"id":20,"name":"دايهاتسو","selected":false},{"id":21,"name":"دايو","selected":false},{"id":22,"name":"دودج","selected":false},{"id":23,"name":"روفر","selected":false},{"id":24,"name":"رولز رویس","selected":false},{"id":25,"name":"رينو","selected":false}],"alt_attribute":null,"value":null}
         */

        private String title;
        private String details;
        private String price;
        private String mobile_number;
        private String category_id;
        private FieldBean field;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDetails() {
            return details;
        }

        public void setDetails(String details) {
            this.details = details;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getMobile_number() {
            return mobile_number;
        }

        public void setMobile_number(String mobile_number) {
            this.mobile_number = mobile_number;
        }

        public String getCategory_id() {
            return category_id;
        }

        public void setCategory_id(String category_id) {
            this.category_id = category_id;
        }

        public FieldBean getField() {
            return field;
        }

        public void setField(FieldBean field) {
            this.field = field;
        }

        public static class FieldBean {
            /**
             * attribute : brand_id
             * type : ListView
             * input_type : null
             * depend_on : null
             * values : [{"id":1,"name":"هيونداي","selected":true},{"id":2,"name":"تويوتا","selected":false},{"id":3,"name":"كيا","selected":false},{"id":4,"name":"بي ام دبليو","selected":false},{"id":5,"name":"مرسيدس بنز","selected":false},{"id":6,"name":"استون مارتن","selected":false},{"id":7,"name":"الفا روميو","selected":false},{"id":8,"name":"أودي","selected":false},{"id":9,"name":"ايسوزو","selected":false},{"id":10,"name":"إنفينيتي","selected":false},{"id":11,"name":"أوبل","selected":false},{"id":12,"name":"بنتلي","selected":false},{"id":13,"name":"بورش","selected":false},{"id":14,"name":"بونتياك","selected":false},{"id":15,"name":"بيجو","selected":false},{"id":16,"name":"جاغوار","selected":false},{"id":17,"name":"جريت وول","selected":false},{"id":18,"name":"جي إم سي","selected":false},{"id":19,"name":"جيب","selected":false},{"id":20,"name":"دايهاتسو","selected":false},{"id":21,"name":"دايو","selected":false},{"id":22,"name":"دودج","selected":false},{"id":23,"name":"روفر","selected":false},{"id":24,"name":"رولز رویس","selected":false},{"id":25,"name":"رينو","selected":false}]
             * alt_attribute : null
             * value : null
             */

            private String attribute;
            private String type;
            private Object input_type;
            private Object depend_on;
            private Object alt_attribute;
            private Object value;
            private List<ValuesBean> values;

            public String getAttribute() {
                return attribute;
            }

            public void setAttribute(String attribute) {
                this.attribute = attribute;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public Object getInput_type() {
                return input_type;
            }

            public void setInput_type(Object input_type) {
                this.input_type = input_type;
            }

            public Object getDepend_on() {
                return depend_on;
            }

            public void setDepend_on(Object depend_on) {
                this.depend_on = depend_on;
            }

            public Object getAlt_attribute() {
                return alt_attribute;
            }

            public void setAlt_attribute(Object alt_attribute) {
                this.alt_attribute = alt_attribute;
            }

            public Object getValue() {
                return value;
            }

            public void setValue(Object value) {
                this.value = value;
            }

            public List<ValuesBean> getValues() {
                return values;
            }

            public void setValues(List<ValuesBean> values) {
                this.values = values;
            }

            public static class ValuesBean {
                /**
                 * id : 1
                 * name : هيونداي
                 * selected : true
                 */

                private int id;
                private String name;
                private boolean selected;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public boolean isSelected() {
                    return selected;
                }

                public void setSelected(boolean selected) {
                    this.selected = selected;
                }
            }
        }
    }
}
