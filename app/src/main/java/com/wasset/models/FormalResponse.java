package com.wasset.models;

public class FormalResponse {

    private String message;
    private boolean status;
    private int code;
    private boolean favorite_status;
    private ValidationBean validation;


    public ValidationBean getValidation() {
        return validation;
    }

    public void setValidation(ValidationBean validation) {
        this.validation = validation;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isFavorite_status() {
        return favorite_status;
    }

    public void setFavorite_status(boolean favorite_status) {
        this.favorite_status = favorite_status;
    }
}
