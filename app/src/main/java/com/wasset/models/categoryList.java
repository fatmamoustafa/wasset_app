package com.wasset.models;

import androidx.databinding.BindingAdapter;

import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by macbook on 7/31/18.
 */

public class categoryList implements Serializable{


    /**
     * status : true
     * code : 200
     * data : [{"category_id":"1","name":"سيارات ومركبات","photo":"http://hwaseet.com.sa/uploads/categories/cars.jpg","sub_categories":[{"category_id":"11","name":"سيارات للبيع","photo":"http://hwaseet.com.sa/uploads/categories/buildings.jpg"},{"category_id":"12","name":"سيارات للإيجار","photo":"http://hwaseet.com.sa/uploads/categories/buildings.jpg"},{"category_id":"13","name":"دراجات نارية","photo":"http://hwaseet.com.sa/uploads/categories/buildings.jpg"},{"category_id":"14","name":"لوحة مميزة","photo":"http://hwaseet.com.sa/uploads/categories/buildings.jpg"},{"category_id":"15","name":"قطع غيار السيارات واكسسواراتها","photo":"http://hwaseet.com.sa/uploads/categories/buildings.jpg"},{"category_id":"27","name":"جنوط - اطارات","photo":"http://hwaseet.com.sa/uploads/categories/buildings.jpg"},{"category_id":"28","name":"شاحنات - اليات ثقيلة","photo":"http://hwaseet.com.sa/uploads/categories/buildings.jpg"},{"category_id":"29","name":"سيارات ومركبات اخرى","photo":"http://hwaseet.com.sa/uploads/categories/buildings.jpg"}]},{"category_id":"2","name":"موبايل - تابلت","photo":"http://hwaseet.com.sa/uploads/categories/mobiles.jpg","sub_categories":[{"category_id":"16","name":"موبايل","photo":"http://hwaseet.com.sa/uploads/categories/buildings.jpg"},{"category_id":"17","name":"تابلت","photo":"http://hwaseet.com.sa/uploads/categories/buildings.jpg"},{"category_id":"18","name":"أرقام هواتف مميزة","photo":"http://hwaseet.com.sa/uploads/categories/buildings.jpg"},{"category_id":"19","name":"اكسسوارات موبايل تابلت","photo":"http://hwaseet.com.sa/uploads/categories/buildings.jpg"},{"category_id":"20","name":"قطع غيار موبايل تابلت","photo":"http://hwaseet.com.sa/uploads/categories/buildings.jpg"}]},{"category_id":"3","name":"اجهزة - الكترونيات","photo":"http://hwaseet.com.sa/uploads/categories/electronic.jpg","sub_categories":[]},{"category_id":"4","name":"أثاث - ديكور","photo":"http://hwaseet.com.sa/uploads/categories/buildings.jpg","sub_categories":[]},{"category_id":"5","name":"عقارات للبيع","photo":"http://hwaseet.com.sa/uploads/categories/buildings.jpg","sub_categories":[{"category_id":"21","name":"فلل - قصور للبيع","photo":"http://hwaseet.com.sa/uploads/categories/buildings.jpg"},{"category_id":"22","name":"تجاري للبيع","photo":"http://hwaseet.com.sa/uploads/categories/buildings.jpg"},{"category_id":"23","name":"عمارات للبيع","photo":"http://hwaseet.com.sa/uploads/categories/buildings.jpg"},{"category_id":"24","name":"اراضي - مزارع للبيع","photo":"http://hwaseet.com.sa/uploads/categories/buildings.jpg"}]},{"category_id":"6","name":"عقارات للايجار","photo":"http://hwaseet.com.sa/uploads/categories/buildings.jpg","sub_categories":[]},{"category_id":"7","name":"ازياء - موضة نسائية","photo":"http://hwaseet.com.sa/uploads/categories/fashion.jpg","sub_categories":[]},{"category_id":"8","name":"ازياء - موضة رجالي","photo":"http://hwaseet.com.sa/uploads/categories/fashion.jpg","sub_categories":[]},{"category_id":"9","name":"الخدمات","photo":"http://hwaseet.com.sa/uploads/categories/services.jpg","sub_categories":[]},{"category_id":"10","name":"وظائف وتوظيف","photo":"http://hwaseet.com.sa/uploads/categories/buildings.jpg","sub_categories":[{"category_id":"25","name":"وظائف شاغرة","photo":"http://hwaseet.com.sa/uploads/categories/buildings.jpg"},{"category_id":"26","name":"باحثين عن عمل","photo":"http://hwaseet.com.sa/uploads/categories/jobs.jpg"}]}]
     */

    private boolean status;
    private int code;
    private List<DataBean> data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<DataBean> getData() {
        if(data == null){
            data = new ArrayList<>();
        }
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable{
        /**
         * category_id : 1
         * name : سيارات ومركبات
         * photo : http://hwaseet.com.sa/uploads/categories/cars.jpg
         * sub_categories : [{"category_id":"11","name":"سيارات للبيع","photo":"http://hwaseet.com.sa/uploads/categories/buildings.jpg"},{"category_id":"12","name":"سيارات للإيجار","photo":"http://hwaseet.com.sa/uploads/categories/buildings.jpg"},{"category_id":"13","name":"دراجات نارية","photo":"http://hwaseet.com.sa/uploads/categories/buildings.jpg"},{"category_id":"14","name":"لوحة مميزة","photo":"http://hwaseet.com.sa/uploads/categories/buildings.jpg"},{"category_id":"15","name":"قطع غيار السيارات واكسسواراتها","photo":"http://hwaseet.com.sa/uploads/categories/buildings.jpg"},{"category_id":"27","name":"جنوط - اطارات","photo":"http://hwaseet.com.sa/uploads/categories/buildings.jpg"},{"category_id":"28","name":"شاحنات - اليات ثقيلة","photo":"http://hwaseet.com.sa/uploads/categories/buildings.jpg"},{"category_id":"29","name":"سيارات ومركبات اخرى","photo":"http://hwaseet.com.sa/uploads/categories/buildings.jpg"}]
         */

        private boolean selected ;
        private String category_id;
        private String name;
        private String photo;
        private List<SubCategoriesBean> sub_categories;

        public boolean isSelected() {
            return selected;
        }

        public void setSelected(boolean selected) {
            this.selected = selected;
        }

        public String getCategory_id() {
            return category_id;
        }

        public void setCategory_id(String category_id) {
            this.category_id = category_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        @BindingAdapter("imageUrl")
        public static void loadImage(ImageView view, String imageUrl) {
            Glide.with(view.getContext())
                    .load(imageUrl)
                    .into(view);
        }

        public List<SubCategoriesBean> getSub_categories() {
            if(sub_categories == null){
                sub_categories = new ArrayList<>();
            }
            return sub_categories;
        }

        public void setSub_categories(List<SubCategoriesBean> sub_categories) {
            this.sub_categories = sub_categories;
        }

        public static class SubCategoriesBean implements Parcelable {
            /**
             * category_id : 11
             * name : سيارات للبيع
             * photo : http://hwaseet.com.sa/uploads/categories/buildings.jpg
             */
            private boolean selected ;
            private String category_id;
            private String name;
            private String photo;

            protected SubCategoriesBean(Parcel in) {
                selected = in.readByte() != 0;
                category_id = in.readString();
                name = in.readString();
                photo = in.readString();
            }

            public static final Creator<SubCategoriesBean> CREATOR = new Creator<SubCategoriesBean>() {
                @Override
                public SubCategoriesBean createFromParcel(Parcel in) {
                    return new SubCategoriesBean(in);
                }

                @Override
                public SubCategoriesBean[] newArray(int size) {
                    return new SubCategoriesBean[size];
                }
            };

            public boolean isSelected() {
                return selected;
            }

            public void setSelected(boolean selected) {
                this.selected = selected;
            }

            public String getCategory_id() {
                return category_id;
            }

            public void setCategory_id(String category_id) {
                this.category_id = category_id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPhoto() {
                return photo;
            }

            public void setPhoto(String photo) {
                this.photo = photo;
            }

            @BindingAdapter("imageUrl")
            public static void loadImage(ImageView view, String imageUrl) {
                Glide.with(view.getContext())
                        .load(imageUrl)
                        .into(view);
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel parcel, int i) {
                parcel.writeByte((byte) (selected ? 1 : 0));
                parcel.writeString(category_id);
                parcel.writeString(name);
                parcel.writeString(photo);
            }
        }
    }
}
