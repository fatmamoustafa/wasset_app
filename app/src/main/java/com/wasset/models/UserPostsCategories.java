package com.wasset.models;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

/**
 * Created by Ahmed Adel on 25/05/2019.
 * email : a3adel@hotmail.com
 */
public class UserPostsCategories extends ExpandableGroup<ExpandableHolder> {
    private String category_id;
    private String name;
    private String userUrl;

    public String getUserUrl() {
        return userUrl;
    }

    public void setUserUrl(String userUrl) {
        this.userUrl = userUrl;
    }

    private List<ExpandableHolder> posts;

    public UserPostsCategories(String title, List<ExpandableHolder> items) {
        super(title, items);
    }


    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ExpandableHolder> getPosts() {
        return posts;
    }

    public void setPosts(List<ExpandableHolder> posts) {
        this.posts = posts;
    }
}
