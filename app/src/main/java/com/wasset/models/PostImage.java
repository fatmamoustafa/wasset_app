package com.wasset.models;

import android.graphics.Bitmap;

import java.io.File;

/**
 * Created by macbook on 8/17/18.
 */

public class PostImage {

    private String id ;
    private int type ;
    private String Url ;
    private File ImageFile ;
    private Bitmap imageBitmap ;


    public PostImage() {
    }

    public Bitmap getImageBitmap() {
        return imageBitmap;
    }

    public void setImageBitmap(Bitmap imageBitmap) {
        this.imageBitmap = imageBitmap;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public File getImageFile() {
        return ImageFile;
    }

    public void setImageFile(File imageFile) {
        ImageFile = imageFile;
    }
}
