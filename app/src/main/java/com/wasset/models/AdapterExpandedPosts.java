package com.wasset.models;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wasset.R;
import com.wasset.ui.CategoryPostsActivity;
import com.wasset.ui.HomeActivity;
import com.wasset.ui.PostDetailsActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ahmed Adel on 25/05/2019.
 * email : a3adel@hotmail.com
 */
public class AdapterExpandedPosts extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<UserPosts> userPosts;
    Context context;
    int MORE_VIEW_TYPE = 2;
    int ITEM_VIEW_TYPE = 1;
    String name;
    String categoryId;

    public AdapterExpandedPosts(ArrayList<UserPosts> userPosts, String categoryId, String name) {
        this.userPosts = userPosts;
        this.name = name;
        this.categoryId = categoryId;
    }

    @Override
    public int getItemViewType(int position) {
        if (userPosts.get(position) == null)
            return MORE_VIEW_TYPE;
        else
            return ITEM_VIEW_TYPE;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view;
        context = viewGroup.getContext();
        if (viewType == ITEM_VIEW_TYPE) {
            view = LayoutInflater.from(context).inflate(R.layout.row_post_image_item, viewGroup, false);
            return new ExpandedViewHolder(view);
        } else {
            view = LayoutInflater.from(context).inflate(R.layout.row_user_profile_more_posts, viewGroup, false);
            return new MoreItemsViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (getItemViewType(position) == ITEM_VIEW_TYPE) {
            ExpandedViewHolder expandableViewHolder = (ExpandedViewHolder) viewHolder;
            if (userPosts.get(position).getPhoto() != null)

                Glide.with(context)
                        .load(userPosts.get(position).getPhoto())
                        .into(expandableViewHolder.postImageView);
            if (userPosts.get(position).getTitle() != null)
                expandableViewHolder.categoryNameAppCompatTextView.setText(userPosts.get(position).getTitle());
            expandableViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, PostDetailsActivity.class);
                    intent.putExtra("Id", userPosts.get(position).getId());
                    intent.putExtra("SHOULD_SCROLL", false);
                    context.startActivity(intent);
                }
            });
        } else {
            MoreItemsViewHolder moreItemsViewHolder = (MoreItemsViewHolder) viewHolder;
            moreItemsViewHolder.viewMoreTextView.setText("مشاهدة الكل في " + name);
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        ((HomeActivity) (context)).getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, CategoryPostsActivity.newInstance(categoryId, name,"home")).
                                setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out).commit();
                    } catch (Exception e) {

                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return userPosts.size();
    }

    class ExpandedViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_category_image)
        AppCompatImageView postImageView;
        @BindView(R.id.iv_category_name)
        AppCompatTextView categoryNameAppCompatTextView;

        public ExpandedViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class MoreItemsViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_view_more)
        TextView viewMoreTextView;

        MoreItemsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
