package com.wasset.models;

/**
 * Created by macbook on 7/16/18.
 */

public class postDetails {


    /**
     * status : true
     * code : 200
     * data : {"id":"2","title":"منزل للبيع","details":"منزل للبيع","added_date":"2018-06-25","price":"1000","category_id":"21","is_favorite":"false","city":"عجمان","user":"test103","user_id":"1","mobile_number":"","attributes":[{"key":"الطابق","value":"طابق التسوية"},{"key":"عدد الحمامات","value":"2"},{"key":"عدد الغرف","value":"5"},{"key":"مساحة البناء","value":"33 متر مربع"},{"key":"مساحة الأرض","value":"250 متر مربع"},{"key":"مفروش","value":"نعم-مفروش"}],"photos":[{"url":"http://hwaseet.com.sa/uploads/posts/cars.jpg"},{"url":"http://hwaseet.com.sa/uploads/posts/cars.jpg"},{"url":"http://hwaseet.com.sa/uploads/posts/cars.jpg"},{"url":"http://hwaseet.com.sa/uploads/posts/cars.jpg"}],"related":[{"id":"1","title":"سيارة حديثة للبيع","photo":"http://hwaseet.com.sa/uploads/posts/cars.jpg"},{"id":"2","title":"منزل للبيع","photo":"http://hwaseet.com.sa/uploads/posts/cars.jpg"},{"id":"3","title":"وظيفة محاسب","photo":"http://hwaseet.com.sa/uploads/posts/29e7c2aea0378de6dd953a38c6c6c61a.jpg"},{"id":"7","title":"سيارة هونداي للبيع","photo":"http://hwaseet.com.sa/uploads/posts/8258893d9327dde68bd7bd209a7f11a9.PNG"},{"id":"35","title":"متوفر لدينا للبيع نقدا او للبدل / هونداي اي 30 ، موديل الـ 2008 ","photo":"http://hwaseet.com.sa/uploads/posts/029938b890de5ae652985f178981af43.jpg"},{"id":"36","title":"للبيع هيونداى هونداي توسان","photo":"http://hwaseet.com.sa/uploads/posts/46ee6a1ea639044fef4278a1cfb16bd3.jpg"}]}
     */

    private boolean status;
    private int code;
    private PostDataBean data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public PostDataBean getData() {
        return data;
    }

    public void setData(PostDataBean data) {
        this.data = data;
    }

 }
