package com.wasset.models;

/**
 * Created by Ahmed Adel on 14/06/2019.
 * email : a3adel@hotmail.com
 */
public class CategoryTitle {
    String title;
    String id;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
