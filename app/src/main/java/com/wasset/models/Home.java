package com.wasset.models;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by macbook on 6/25/18.
 */

public class Home {



    private boolean status;
    private int code;
    private List<DataBean> data;



    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<DataBean> getData() {
        if (data == null){
            data =  new ArrayList<>();
        }
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * category_id : 1
         * name : سيارات ومركبات
         * posts : [{"id":"40","title":"بيجو 308","details":"نص تجريبي، وتحرص السعودية على تطوير صناعة السيارات في إطار جهودها الرامية لتنويع موارد الاقتصاد وتقليص الاعتماد على النفط، وشكلت فريقا مع شركة تويوتا موتور لإجراء الدراسات اللازمة لذلك.","added_date":"أمس","price":"30000","is_favorite":false,"city":"أبو ظبي","user":"test103","user_id":"1","photo":"http://hwaseet.com.sa/uploads/posts/4600ac3349172a039efc43736c52cdba.jpg"},{"id":"39","title":"2014 Mercedes-Benz C-Class 200","details":"نص تجريبي، وتحرص السعودية على تطوير صناعة السيارات في إطار جهودها الرامية لتنويع موارد الاقتصاد وتقليص الاعتماد على النفط، وشكلت فريقا مع شركة تويوتا موتور لإجراء الدراسات اللازمة لذلك.","added_date":"2018-07-28","price":"2500","is_favorite":false,"city":"أبو ظبي","user":"test103","user_id":"1","photo":"http://hwaseet.com.sa/uploads/posts/7c6c0b73f1ac6c7a631d079aac15df2f.jpg"},{"id":"38","title":"رينو فلوانس","details":"نص تجريبي، وتحرص السعودية على تطوير صناعة السيارات في إطار جهودها الرامية لتنويع موارد الاقتصاد وتقليص الاعتماد على النفط، وشكلت فريقا مع شركة تويوتا موتور لإجراء الدراسات اللازمة لذلك.","added_date":"2018-07-28","price":"10000","is_favorite":false,"city":"أبو ظبي","user":"test103","user_id":"1","photo":"http://hwaseet.com.sa/uploads/posts/1c23f7d958f9dd15e4fc355e0fcf72a9.jpg"},{"id":"37","title":"هونداي اكسنت 2010","details":"نص تجريبي، وتحرص السعودية على تطوير صناعة السيارات في إطار جهودها الرامية لتنويع موارد الاقتصاد وتقليص الاعتماد على النفط، وشكلت فريقا مع شركة تويوتا موتور لإجراء الدراسات اللازمة لذلك.","added_date":"2018-07-18","price":"0","is_favorite":false,"city":"أبو ظبي","user":"test103","user_id":"1","photo":"http://hwaseet.com.sa/uploads/posts/c6f93bad49777b4892d0ca9e25d4a19d.jpg"},{"id":"36","title":"للبيع هيونداى هونداي توسان","details":"نص تجريبي، وتحرص السعودية على تطوير صناعة السيارات في إطار جهودها الرامية لتنويع موارد الاقتصاد وتقليص الاعتماد على النفط، وشكلت فريقا مع شركة تويوتا موتور لإجراء الدراسات اللازمة لذلك.","added_date":"2018-07-18","price":"5000","is_favorite":false,"city":"أبو ظبي","user":"test103","user_id":"1","photo":"http://hwaseet.com.sa/uploads/posts/46ee6a1ea639044fef4278a1cfb16bd3.jpg"},{"id":"35","title":"متوفر لدينا للبيع نقدا او للبدل / هونداي اي 30 ، موديل الـ 2008 ","details":"نص تجريبي، وتحرص السعودية على تطوير صناعة السيارات في إطار جهودها الرامية لتنويع موارد الاقتصاد وتقليص الاعتماد على النفط، وشكلت فريقا مع شركة تويوتا موتور لإجراء الدراسات اللازمة لذلك.","added_date":"2018-07-18","price":"5000","is_favorite":false,"city":"أبو ظبي","user":"test103","user_id":"1","photo":"http://hwaseet.com.sa/uploads/posts/029938b890de5ae652985f178981af43.jpg"}]
         */

        private String category_id;
        private String name;
        private List<PostDataBean> posts;

        public String getCategory_id() {
            return category_id;
        }

        public void setCategory_id(String category_id) {
            this.category_id = category_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<PostDataBean> getPosts() {
            if(posts == null){
                posts = new ArrayList<>();
            }
            return posts;
        }

        public void setPosts(List<PostDataBean> posts) {
            this.posts = posts;
        }

     }
}
