package com.wasset.models;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.joooonho.SelectableRoundedImageView;
import com.wasset.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by macbook on 8/17/18.
 */

public class Comments {


    /**
     * status : true
     * code : 200
     * comments : [{"added_date":"2018-06-25","comment_text":"طبعا ياشباب هذة لازم يكون مسجل دخول عشان يقدر يعلق","user":"test103"},{"added_date":"2018-06-25","comment_text":"طبعا ياشباب هذة لازم يكون مسجل دخول عشان يقدر يعلق","user":"test103"},{"added_date":"2018-06-25","comment_text":"طبعا ياشباب هذة لازم يكون مسجل دخول عشان يقدر يعلق","user":"test103"},{"added_date":"2018-06-25","comment_text":"تجريب واحد ","user":"test103"}]
     */

    private boolean status;
    private int code;
    private String message ;
    private List<CommentsBean> comments;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<CommentsBean> getComments() {
        if (comments == null){
            comments =  new ArrayList<>();
        }
        return comments;
    }

    public void setComments(List<CommentsBean> comments) {
        this.comments = comments;
    }

    public static class CommentsBean {
        /**
         * added_date : 2018-06-25
         * comment_text : طبعا ياشباب هذة لازم يكون مسجل دخول عشان يقدر يعلق
         * user : test103
         */

        private String added_date;
        private String comment_text;
        private String user;
        private String picture ;

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getComment_id() {
            return comment_id;
        }

        public void setComment_id(String comment_id) {
            this.comment_id = comment_id;
        }

        private String user_id ;
        private String comment_id ;

        public String getPicture() {
            return picture;
        }

        public void setPicture(String picture) {
            this.picture = picture;
        }

        @BindingAdapter("imageUrl")
        public static void loadImage(SelectableRoundedImageView view, String imageUrl) {
            if(imageUrl!= null && !imageUrl.isEmpty()) {
                Glide.with(view.getContext())
                        .load(imageUrl)
                        .into(view);
            }else {
                view.setImageResource(R.drawable.ic_placeholder);
            }
        }


        public String getAdded_date() {
            return added_date;
        }

        public void setAdded_date(String added_date) {
            this.added_date = added_date;
        }

        public String getComment_text() {
            return comment_text;
        }

        public void setComment_text(String comment_text) {
            this.comment_text = comment_text;
        }

        public String getUser() {
            return user;
        }

        public void setUser(String user) {
            this.user = user;
        }
    }
}
