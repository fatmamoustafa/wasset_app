package com.wasset.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ahmed Adel on 25/05/2019.
 * email : a3adel@hotmail.com
 */
public class UserPosts implements Parcelable {
    private String id;
    private String title;
    private String details;
    private String added_date;
    private String price;
    private String category_id;
    private boolean is_favorite;
    private String city;
    private String user;
    private String user_id;
    private String mobile_number;
    private String post_url;
    private List<PostDataBean.AttributesBean> attributes;
    private List<PostDataBean.PhotosBean> photos;
    private List<PostDataBean.RelatedBean> related;
    private String photo;
    private boolean is_hidden;

    protected UserPosts(Parcel in) {
        id = in.readString();
        title = in.readString();
        details = in.readString();
        added_date = in.readString();
        price = in.readString();
        category_id = in.readString();
        is_favorite = in.readByte() != 0;
        city = in.readString();
        user = in.readString();
        user_id = in.readString();
        mobile_number = in.readString();
        post_url = in.readString();
        photo = in.readString();
        is_hidden = in.readByte() != 0;
        user_special = in.readByte() != 0;
        views_count = in.readInt();
        comments_count = in.readInt();
    }

    public static final Creator<UserPosts> CREATOR = new Creator<UserPosts>() {
        @Override
        public UserPosts createFromParcel(Parcel in) {
            return new UserPosts(in);
        }

        @Override
        public UserPosts[] newArray(int size) {
            return new UserPosts[size];
        }
    };

    public boolean isUser_special() {
        return user_special;
    }

    public void setUser_special(boolean user_special) {
        this.user_special = user_special;
    }

    boolean user_special;
    private int views_count;
    private int comments_count;

    public UserPosts() {

    }

    public String getPost_url() {
        return post_url;
    }

    public void setPost_url(String post_url) {
        this.post_url = post_url;
    }

    public boolean isIs_hidden() {
        return is_hidden;
    }

    public void setIs_hidden(boolean is_hidden) {
        this.is_hidden = is_hidden;
    }

    public int getViews_count() {
        return views_count;
    }

    public void setViews_count(int views_count) {
        this.views_count = views_count;
    }

    public int getComments_count() {
        return comments_count;
    }

    public void setComments_count(int comments_count) {
        this.comments_count = comments_count;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;

    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getAdded_date() {
        return added_date;
    }

    public void setAdded_date(String added_date) {
        this.added_date = added_date;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public boolean getIs_favorite() {
        return is_favorite;
    }

    public void setIs_favorite(boolean is_favorite) {
        this.is_favorite = is_favorite;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public List<PostDataBean.AttributesBean> getAttributes() {
        if (attributes == null) {
            attributes = new ArrayList<>();
        }
        return attributes;
    }

    public void setAttributes(List<PostDataBean.AttributesBean> attributes) {
        this.attributes = attributes;
    }

    public List<PostDataBean.PhotosBean> getPhotos() {
        if (photos == null) {
            photos = new ArrayList<>();
        }
        return photos;
    }

    public void setPhotos(List<PostDataBean.PhotosBean> photos) {
        this.photos = photos;
    }

    public List<PostDataBean.RelatedBean> getRelated() {

        if (related == null) {
            related = new ArrayList<>();
        }
        return related;
    }

    public void setRelated(List<PostDataBean.RelatedBean> related) {
        this.related = related;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(details);
        dest.writeString(added_date);
        dest.writeString(price);
        dest.writeString(category_id);
        dest.writeByte((byte) (is_favorite ? 1 : 0));
        dest.writeString(city);
        dest.writeString(user);
        dest.writeString(user_id);
        dest.writeString(mobile_number);
        dest.writeString(post_url);
        dest.writeString(photo);
        dest.writeByte((byte) (is_hidden ? 1 : 0));
        dest.writeByte((byte) (user_special ? 1 : 0));
        dest.writeInt(views_count);
        dest.writeInt(comments_count);
    }


    public static class AttributesBean {
        /**
         * key : الطابق
         * value : طابق التسوية
         */

        private String key;
        private String value;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    public static class PhotosBean {
        /**
         * url : http://hwaseet.com.sa/uploads/posts/cars.jpg
         */

        private String url;
        private String id;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }


    }

    public static class RelatedBean {
        /**
         * id : 1
         * title : سيارة حديثة للبيع
         * photo : http://hwaseet.com.sa/uploads/posts/cars.jpg
         */

        private String id;
        private String title;
        private String photo;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

    }

}
