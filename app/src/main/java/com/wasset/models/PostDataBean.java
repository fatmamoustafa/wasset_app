package com.wasset.models;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.wasset.BR;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by macbook on 7/23/18.
 */

public class PostDataBean extends BaseObservable {


    private String id;
    private String title;
    private String details;
    private String added_date;
    private String price;
    private String category_id;
    private boolean is_favorite;
    private String city;

    @Bindable
    public String getLocation() {
        return country + "," + city;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    private String location;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    private String country = "";
    private String user;
    private String user_id;
    private String mobile_number;
    private String post_url;
    private List<AttributesBean> attributes;
    private List<PhotosBean> photos;
    private List<RelatedBean> related;
    private String photo;
    private boolean is_hidden;
    private boolean comment_enable;
    private boolean call_enable;

    public boolean isComment_enable() {
        return comment_enable;
    }

    public void setComment_enable(boolean comment_enable) {
        this.comment_enable = comment_enable;
    }

    public boolean isCall_enable() {
        return call_enable;
    }

    public void setCall_enable(boolean call_enable) {
        this.call_enable = call_enable;
    }

    public boolean isUser_special() {
        return user_special;
    }

    public void setUser_special(boolean user_special) {
        this.user_special = user_special;
    }

    boolean user_special;
    private int views_count;
    private int comments_count;

    public PostDataBean(String id) {
        this.id = id;
    }

    public String getPost_url() {
        return post_url;
    }

    public void setPost_url(String post_url) {
        this.post_url = post_url;
    }

    public boolean isIs_hidden() {
        return is_hidden;
    }

    public void setIs_hidden(boolean is_hidden) {
        this.is_hidden = is_hidden;
    }

    public int getViews_count() {
        return views_count;
    }

    public void setViews_count(int views_count) {
        this.views_count = views_count;
    }

    public int getComments_count() {
        return comments_count;
    }

    public void setComments_count(int comments_count) {
        this.comments_count = comments_count;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @BindingAdapter("imageUrl")
    public static void loadImage(ImageView view, String imageUrl) {
        Glide.with(view.getContext())
                .load(imageUrl)
                .into(view);
    }

    @Bindable
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
        notifyPropertyChanged(BR.id);

    }

    @Bindable
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        notifyPropertyChanged(BR.title);
    }

    @Bindable
    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    @Bindable
    public String getAdded_date() {
        return added_date;
    }

    public void setAdded_date(String added_date) {
        this.added_date = added_date;
    }

    @Bindable
    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Bindable
    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    @Bindable
    public boolean getIs_favorite() {
        return is_favorite;
    }

    public void setIs_favorite(boolean is_favorite) {
        this.is_favorite = is_favorite;
        notifyPropertyChanged(BR.is_favorite);
    }

    @Bindable
    public String getCity() {
        return city;
    }


    public void setCity(String city) {
        this.city = city;
    }

    @Bindable
    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @Bindable
    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    @Bindable
    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    @Bindable
    public List<AttributesBean> getAttributes() {
        if (attributes == null) {
            attributes = new ArrayList<>();
        }
        return attributes;
    }

    public void setAttributes(List<AttributesBean> attributes) {
        this.attributes = attributes;
    }

    @Bindable
    public List<PhotosBean> getPhotos() {
        if (photos == null) {
            photos = new ArrayList<>();
        }
        return photos;
    }

    public void setPhotos(List<PhotosBean> photos) {
        this.photos = photos;
    }

    @Bindable
    public List<RelatedBean> getRelated() {

        if (related == null) {
            related = new ArrayList<>();
        }
        return related;
    }

    public void setRelated(List<RelatedBean> related) {
        this.related = related;
    }


    public static class AttributesBean {
        /**
         * key : الطابق
         * value : طابق التسوية
         */

        private String key;
        private String value;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    public static class PhotosBean {
        /**
         * url : http://hwaseet.com.sa/uploads/posts/cars.jpg
         */

        private String url;
        private String id;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }


        @BindingAdapter("imageUrl")
        public static void loadImage(ImageView view, String imageUrl) {
            Glide.with(view.getContext())
                    .load(imageUrl)
                    .into(view);
        }
    }

    public static class RelatedBean {
        /**
         * id : 1
         * title : سيارة حديثة للبيع
         * photo : http://hwaseet.com.sa/uploads/posts/cars.jpg
         */

        private String id;
        private String title;
        private String photo;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }


        @BindingAdapter("imageUrl")
        public static void loadImage(ImageView view, String imageUrl) {
            Glide.with(view.getContext())
                    .load(imageUrl)
                    .into(view);
        }


    }
}

