package com.wasset.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by macbook on 8/13/18.
 */

public class Models {


    /**
     * status : true
     * code : 200
     * data : [{"id":"1","name":"Avante"},{"id":"2","name":"XG"},{"id":"3","name":"ان اف"},{"id":"4","name":"ايكوس"},{"id":"5","name":"H-1"}]
     */

    private boolean status;
    private int code;
    private List<PostSteps.DataBean.FieldBean.ValuesBean> data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<PostSteps.DataBean.FieldBean.ValuesBean> getData() {
        if (data == null) {
            data = new ArrayList<>();
        }
        return data;
    }

    public void setData(List<PostSteps.DataBean.FieldBean.ValuesBean> data) {
        this.data = data;
    }

 }
