package com.wasset.models;

import androidx.databinding.BindingAdapter;

import java.util.List;

/**
 * Created by macbook on 6/27/18.
 */

public class favorite {


    /**
     * status : true
     * code : 200
     * posts : [{"id":"1","title":"سيارة حديثة للبيع","details":"السيارة للبيع لاعلى سعر","added_date":"2018-06-25","price":"1000","is_favorite":"true","city":"أبو ظبي","user":"test103","photos":[{"url":"http://hwaseet.com.sa/uploads/posts/cars.jpg"},{"url":"http://hwaseet.com.sa/uploads/posts/cars.jpg"}]},{"id":"2","title":"منزل للبيع","details":"منزل للبيع","added_date":"2018-06-25","price":"1000","is_favorite":"true","city":"عجمان","user":"test103","photos":[{"url":"http://hwaseet.com.sa/uploads/posts/cars.jpg"},{"url":"http://hwaseet.com.sa/uploads/posts/cars.jpg"},{"url":"http://hwaseet.com.sa/uploads/posts/cars.jpg"},{"url":"http://hwaseet.com.sa/uploads/posts/cars.jpg"}]}]
     */

    private boolean status;
    private int code;
    private List<PostDataBean> posts;


    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<PostDataBean> getPosts() {
        return posts;
    }

    public void setPosts(List<PostDataBean> posts) {
        this.posts = posts;
    }


}
