package com.wasset.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by macbook on 8/1/18.
 */

public class publisherProfile {


    /**
     * status : true
     * code : 200
     * data : {"user_info":{"id":"1","full_name":"حازم أسامة الغلاييني","username":"test103","email":"test@customer.com","mobile_code":"+972","mobile_number":"123456897","city_id":"0","picture":"http://hwaseet.com.sa/uploads/users/49c6887f94f42c05782f611469e1cf76.jpg","token":"#$5p05.g05zEU"},"posts":[{"id":"43","title":"موظف\\ة مبيعات","details":"موظف\\ة مبيعات في محل مفروشات اثاث في رام الله ","added_date":"2018-07-28","price":null,"is_favorite":false,"city":"أبو ظبي","user":"test103","user_id":"1","photo":"http://hwaseet.com.sa/uploads/posts/9883e66e6ba66b73ea0bc62a83954b30.jpg"},{"id":"42","title":"شيف - طباخة","details":"علن مطبخ جنان عن حاجته لسيدات وفتيات للعمل في مجال إعداد الطعام العربي وخاصة المعجنات والحلويات الشرقية. كما ويعلن عن حاجته أيضاً إلى شبان وفتيات للعمل كسفرجية. يفضل ممن لديه الخبرة في هذا المجال.\r\n","added_date":"2018-07-28","price":null,"is_favorite":false,"city":"أبو ظبي","user":"test103","user_id":"1","photo":"http://hwaseet.com.sa/uploads/posts/f7975ce2dd2bc1a82377d0ea04c18836.jpg"},{"id":"41","title":"مهندس/ة ميكانيك","details":"مهندس/ة ميكانيك خبرة لا تقل عن 3 سنوات في مجال التصميم والاشراف في المكاتب الهندسية، للعمل في شركة السوائد للتعهدات العامة الكائنة في جوريش ","added_date":"2018-07-28","price":null,"is_favorite":false,"city":"أبو ظبي","user":"test103","user_id":"1","photo":"http://hwaseet.com.sa/uploads/posts/6d50934dd2af15cb6590c619ee059911.jpg"},{"id":"40","title":"بيجو 308","details":"نص تجريبي، وتحرص السعودية على تطوير صناعة السيارات في إطار جهودها الرامية لتنويع موارد الاقتصاد وتقليص الاعتماد على النفط، وشكلت فريقا مع شركة تويوتا موتور لإجراء الدراسات اللازمة لذلك.","added_date":"2018-07-28","price":"30000","is_favorite":false,"city":"أبو ظبي","user":"test103","user_id":"1","photo":"http://hwaseet.com.sa/uploads/posts/4600ac3349172a039efc43736c52cdba.jpg"},{"id":"39","title":"2014 Mercedes-Benz C-Class 200","details":"نص تجريبي، وتحرص السعودية على تطوير صناعة السيارات في إطار جهودها الرامية لتنويع موارد الاقتصاد وتقليص الاعتماد على النفط، وشكلت فريقا مع شركة تويوتا موتور لإجراء الدراسات اللازمة لذلك.","added_date":"2018-07-28","price":"2500","is_favorite":false,"city":"أبو ظبي","user":"test103","user_id":"1","photo":"http://hwaseet.com.sa/uploads/posts/7c6c0b73f1ac6c7a631d079aac15df2f.jpg"},{"id":"38","title":"رينو فلوانس","details":"نص تجريبي، وتحرص السعودية على تطوير صناعة السيارات في إطار جهودها الرامية لتنويع موارد الاقتصاد وتقليص الاعتماد على النفط، وشكلت فريقا مع شركة تويوتا موتور لإجراء الدراسات اللازمة لذلك.","added_date":"2018-07-28","price":"10000","is_favorite":false,"city":"أبو ظبي","user":"test103","user_id":"1","photo":"http://hwaseet.com.sa/uploads/posts/1c23f7d958f9dd15e4fc355e0fcf72a9.jpg"},{"id":"37","title":"هونداي اكسنت 2010","details":"نص تجريبي، وتحرص السعودية على تطوير صناعة السيارات في إطار جهودها الرامية لتنويع موارد الاقتصاد وتقليص الاعتماد على النفط، وشكلت فريقا مع شركة تويوتا موتور لإجراء الدراسات اللازمة لذلك.","added_date":"2018-07-18","price":"0","is_favorite":false,"city":"أبو ظبي","user":"test103","user_id":"1","photo":"http://hwaseet.com.sa/uploads/posts/c6f93bad49777b4892d0ca9e25d4a19d.jpg"},{"id":"36","title":"للبيع هيونداى هونداي توسان","details":"نص تجريبي، وتحرص السعودية على تطوير صناعة السيارات في إطار جهودها الرامية لتنويع موارد الاقتصاد وتقليص الاعتماد على النفط، وشكلت فريقا مع شركة تويوتا موتور لإجراء الدراسات اللازمة لذلك.","added_date":"2018-07-18","price":"5000","is_favorite":false,"city":"أبو ظبي","user":"test103","user_id":"1","photo":"http://hwaseet.com.sa/uploads/posts/46ee6a1ea639044fef4278a1cfb16bd3.jpg"},{"id":"35","title":"متوفر لدينا للبيع نقدا او للبدل / هونداي اي 30 ، موديل الـ 2008 ","details":"نص تجريبي، وتحرص السعودية على تطوير صناعة السيارات في إطار جهودها الرامية لتنويع موارد الاقتصاد وتقليص الاعتماد على النفط، وشكلت فريقا مع شركة تويوتا موتور لإجراء الدراسات اللازمة لذلك.","added_date":"2018-07-18","price":"5000","is_favorite":false,"city":"أبو ظبي","user":"test103","user_id":"1","photo":"http://hwaseet.com.sa/uploads/posts/029938b890de5ae652985f178981af43.jpg"},{"id":"7","title":"سيارة هونداي للبيع","details":"سيارة حديثة موديل 2015لا للبيع","added_date":"2018-07-03","price":"40","is_favorite":false,"city":"أبو ظبي","user":"test103","user_id":"1","photo":"http://hwaseet.com.sa/uploads/posts/8258893d9327dde68bd7bd209a7f11a9.PNG"},{"id":"3","title":"وظيفة محاسب","details":"اعلان وظيفة محاسب بدوام كامل","added_date":"2018-06-29","price":"0","is_favorite":false,"city":"أم القيوين","user":"test103","user_id":"1","photo":"http://hwaseet.com.sa/uploads/posts/29e7c2aea0378de6dd953a38c6c6c61a.jpg"},{"id":"1","title":"سيارة حديثة للبيع","details":"السيارة للبيع لاعلى سعر","added_date":"2018-06-25","price":"1000","is_favorite":false,"city":"أبو ظبي","user":"test103","user_id":"1","photo":"http://hwaseet.com.sa/uploads/posts/cars.jpg"},{"id":"2","title":"منزل للبيع","details":"منزل للبيع","added_date":"2018-06-25","price":"1000","is_favorite":false,"city":"عجمان","user":"test103","user_id":"1","photo":"http://hwaseet.com.sa/uploads/posts/cars.jpg"}]}
     */

    private boolean status;
    private int code;
    private DataBean data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {


        private Login.DataBean user_info;
        private List<PostDataBean> posts;
        private String user_url;

        public String getUser_url() {
            return user_url;
        }

        public void setUser_url(String user_url) {
            this.user_url = user_url;
        }

        public List<Home.DataBean> getUser_categories() {
            return user_categories;
        }

        public void setUser_categories(List<Home.DataBean> user_categories) {
            this.user_categories = user_categories;
        }

        private List<Home.DataBean> user_categories;

        public Login.DataBean getUser_info() {
            return user_info;
        }

        public void setUser_info(Login.DataBean user_info) {
            this.user_info = user_info;
        }

        public List<PostDataBean> getPosts() {

            if(posts == null){
                posts = new ArrayList<>();
            }
            return posts;
        }

        public void setPosts(List<PostDataBean> posts) {
            this.posts = posts;
        }


     }
}
