package com.wasset.models;

import java.util.ArrayList;
import java.util.List;


public class PostSteps {


    /**
     * status : true
     * code : 200
     * data : [{"title":"النوع","field":{"attribute":"brand_id","type":"ListView","input_type":null,"depend_on":null,"values":[{"id":1,"name":"هيونداي"},{"id":2,"name":"تويوتا"},{"id":3,"name":"كيا"},{"id":4,"name":"بي ام دبليو"},{"id":5,"name":"مرسيدس بنز"},{"id":6,"name":"استون مارتن"},{"id":7,"name":"الفا روميو"},{"id":8,"name":"أودي"},{"id":9,"name":"ايسوزو"},{"id":10,"name":"إنفينيتي"},{"id":11,"name":"أوبل"},{"id":12,"name":"بنتلي"},{"id":13,"name":"بورش"},{"id":14,"name":"بونتياك"},{"id":15,"name":"بيجو"},{"id":16,"name":"جاغوار"},{"id":17,"name":"جريت وول"},{"id":18,"name":"جي إم سي"},{"id":19,"name":"جيب"},{"id":20,"name":"دايهاتسو"},{"id":21,"name":"دايو"},{"id":22,"name":"دودج"},{"id":23,"name":"روفر"},{"id":24,"name":"رولز رویس"},{"id":25,"name":"رينو"}],"alt_attribute":null}},{"title":"الفئة","field":{"attribute":"model_id","type":"ListView","input_type":null,"depend_on":"brand_id","values":null,"alt_attribute":null}},{"title":"سنة الصنع","field":{"attribute":"year_id","type":"ListView","input_type":null,"depend_on":null,"values":[{"id":1,"name":"2010"},{"id":2,"name":"2011"},{"id":3,"name":"2012"},{"id":4,"name":"2013"},{"id":5,"name":"2014"},{"id":6,"name":"2015"},{"id":7,"name":"2016"},{"id":8,"name":"2017"},{"id":9,"name":"2018"}],"alt_attribute":null}},{"title":"الحالة","field":{"attribute":"used_type","type":"ListView","input_type":null,"depend_on":null,"values":[{"id":1,"name":"مستخدم"},{"id":2,"name":"جديد"}],"alt_attribute":null}},{"title":"ناقل الحركة","field":{"attribute":"transmission_type","type":"ListView","input_type":null,"depend_on":null,"values":[{"id":1,"name":"عادي"},{"id":1,"name":"أوتوماتيك"},{"id":1,"name":"غير ذلك"}],"alt_attribute":null}},{"title":"عدد الكيلومترات","field":{"attribute":"kilometers_id","type":"ListView","input_type":null,"depend_on":null,"values":[{"id":1,"name":"0"},{"id":2,"name":"1 - 999"},{"id":3,"name":"1000 - 10000"},{"id":4,"name":"10000 - 100000"},{"id":5,"name":"Other"}],"alt_attribute":null}},{"title":"نوع الوقود","field":{"attribute":"fuel_type","type":"ListView","input_type":null,"depend_on":null,"values":[{"id":1,"name":"بنزين"},{"id":2,"name":"ديزل"},{"id":3,"name":"هايبرد"},{"id":4,"name":"كهربائي"}],"alt_attribute":null}},{"title":"اللون","field":{"attribute":"color_id","type":"ListView","input_type":null,"depend_on":null,"values":[{"id":1,"name":"أحمر"},{"id":2,"name":"أزرق"}],"alt_attribute":null}}]
     */

    private boolean status;
    private int code;
    private String currency;
    private List<DataBean> data;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<DataBean> getData() {
        if (data == null) {
            data = new ArrayList<>();
        }
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * title : النوع
         * field : {"attribute":"brand_id","type":"ListView","input_type":null,"depend_on":null,"values":[{"id":1,"name":"هيونداي"},{"id":2,"name":"تويوتا"},{"id":3,"name":"كيا"},{"id":4,"name":"بي ام دبليو"},{"id":5,"name":"مرسيدس بنز"},{"id":6,"name":"استون مارتن"},{"id":7,"name":"الفا روميو"},{"id":8,"name":"أودي"},{"id":9,"name":"ايسوزو"},{"id":10,"name":"إنفينيتي"},{"id":11,"name":"أوبل"},{"id":12,"name":"بنتلي"},{"id":13,"name":"بورش"},{"id":14,"name":"بونتياك"},{"id":15,"name":"بيجو"},{"id":16,"name":"جاغوار"},{"id":17,"name":"جريت وول"},{"id":18,"name":"جي إم سي"},{"id":19,"name":"جيب"},{"id":20,"name":"دايهاتسو"},{"id":21,"name":"دايو"},{"id":22,"name":"دودج"},{"id":23,"name":"روفر"},{"id":24,"name":"رولز رویس"},{"id":25,"name":"رينو"}],"alt_attribute":null}
         */

        String title;
        FieldBean field;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public FieldBean getField() {
            return field;
        }

        public void setField(FieldBean field) {
            this.field = field;
        }

        public static class FieldBean {
            /**
             * attribute : brand_id
             * type : ListView
             * input_type : null
             * depend_on : null
             * values : [{"id":1,"name":"هيونداي"},{"id":2,"name":"تويوتا"},{"id":3,"name":"كيا"},{"id":4,"name":"بي ام دبليو"},{"id":5,"name":"مرسيدس بنز"},{"id":6,"name":"استون مارتن"},{"id":7,"name":"الفا روميو"},{"id":8,"name":"أودي"},{"id":9,"name":"ايسوزو"},{"id":10,"name":"إنفينيتي"},{"id":11,"name":"أوبل"},{"id":12,"name":"بنتلي"},{"id":13,"name":"بورش"},{"id":14,"name":"بونتياك"},{"id":15,"name":"بيجو"},{"id":16,"name":"جاغوار"},{"id":17,"name":"جريت وول"},{"id":18,"name":"جي إم سي"},{"id":19,"name":"جيب"},{"id":20,"name":"دايهاتسو"},{"id":21,"name":"دايو"},{"id":22,"name":"دودج"},{"id":23,"name":"روفر"},{"id":24,"name":"رولز رویس"},{"id":25,"name":"رينو"}]
             * alt_attribute : null
             */

            private String attribute;
            private String type;
            private String input_type;
            private String depend_on;
            private String alt_attribute;
            private List<ValuesBean> values;
            private String value;

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public String getAttribute() {
                return attribute;
            }

            public void setAttribute(String attribute) {
                this.attribute = attribute;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public Object getInput_type() {
                return input_type;
            }

            public void setInput_type(String input_type) {
                this.input_type = input_type;
            }

            public String getDepend_on() {
                return depend_on;
            }

            public void setDepend_on(String depend_on) {
                this.depend_on = depend_on;
            }

            public String getAlt_attribute() {
                return alt_attribute;
            }

            public void setAlt_attribute(String alt_attribute) {
                this.alt_attribute = alt_attribute;
            }

            public List<ValuesBean> getValues() {
                if (values == null) {
                    values = new ArrayList<>();
                }
                return values;
            }

            public void setValues(List<ValuesBean> values) {
                this.values = values;
            }

            public static class ValuesBean {
                /**
                 * id : 1
                 * name : هيونداي
                 */

                private int id;
                private String name;
                private boolean selected;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public boolean isSelected() {
                    return selected;
                }

                public void setSelected(boolean selected) {
                    this.selected = selected;
                }
            }
        }
    }
}
