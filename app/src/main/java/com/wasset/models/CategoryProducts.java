package com.wasset.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by macbook on 6/25/18.
 */

public class CategoryProducts {


    /**
     * status : true
     * code : 200
     * data : [{"id":"37","title":"هونداي اكسنت 2010","details":"نص تجريبي، وتحرص السعودية على تطوير صناعة السيارات في إطار جهودها الرامية لتنويع موارد الاقتصاد وتقليص الاعتماد على النفط، وشكلت فريقا مع شركة تويوتا موتور لإجراء الدراسات اللازمة لذلك.","added_date":"2018-07-18","price":"0","is_favorite":"true","city":"أبو ظبي","user":"test103","photos":[{"url":"http://hwaseet.com.sa/uploads/posts/c6f93bad49777b4892d0ca9e25d4a19d.jpg"},{"url":"http://hwaseet.com.sa/uploads/posts/f53c3caf70afdcbd56c2b6d2ca238695.jpg"},{"url":"http://hwaseet.com.sa/uploads/posts/a79bbe5fce19afa0ff384eb37f813cc2.jpg"}]},{"id":"36","title":"للبيع هيونداى هونداي توسان","details":"نص تجريبي، وتحرص السعودية على تطوير صناعة السيارات في إطار جهودها الرامية لتنويع موارد الاقتصاد وتقليص الاعتماد على النفط، وشكلت فريقا مع شركة تويوتا موتور لإجراء الدراسات اللازمة لذلك.","added_date":"2018-07-18","price":"5000","is_favorite":"false","city":"أبو ظبي","user":"test103","photos":[{"url":"http://hwaseet.com.sa/uploads/posts/46ee6a1ea639044fef4278a1cfb16bd3.jpg"},{"url":"http://hwaseet.com.sa/uploads/posts/3fb74db12aa8a2f5f24d3e31e41609a3.jpg"}]},{"id":"35","title":"متوفر لدينا للبيع نقدا او للبدل / هونداي اي 30 ، موديل الـ 2008 ","details":"نص تجريبي، وتحرص السعودية على تطوير صناعة السيارات في إطار جهودها الرامية لتنويع موارد الاقتصاد وتقليص الاعتماد على النفط، وشكلت فريقا مع شركة تويوتا موتور لإجراء الدراسات اللازمة لذلك.","added_date":"2018-07-18","price":"5000","is_favorite":"false","city":"أبو ظبي","user":"test103","photos":[{"url":"http://hwaseet.com.sa/uploads/posts/029938b890de5ae652985f178981af43.jpg"},{"url":"http://hwaseet.com.sa/uploads/posts/e4d6f4850be7a7c78f51ae9c31f76268.jpg"}]},{"id":"1","title":"سيارة حديثة للبيع","details":"السيارة للبيع لاعلى سعر","added_date":"2018-06-25","price":"1000","is_favorite":"true","city":"أبو ظبي","user":"test103","photos":[{"url":"http://hwaseet.com.sa/uploads/posts/cars.jpg"},{"url":"http://hwaseet.com.sa/uploads/posts/cars.jpg"}]}]
     */

    private boolean status;
    private int code;
    private List<PostDataBean> data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<PostDataBean> getData() {
        if(data == null){
            data = new ArrayList<>();
        }
        return data;
    }

    public void setData(List<PostDataBean> data) {
        this.data = data;
    }

 }
