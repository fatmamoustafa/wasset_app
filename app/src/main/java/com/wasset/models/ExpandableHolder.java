package com.wasset.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Ahmed Adel on 25/05/2019.
 * email : a3adel@hotmail.com
 */
public class ExpandableHolder implements Parcelable {
    ArrayList<UserPosts> userPosts;
public ExpandableHolder(){}
    protected ExpandableHolder(Parcel in) {
        userPosts = in.createTypedArrayList(UserPosts.CREATOR);
    }

    public static final Creator<ExpandableHolder> CREATOR = new Creator<ExpandableHolder>() {
        @Override
        public ExpandableHolder createFromParcel(Parcel in) {
            return new ExpandableHolder(in);
        }

        @Override
        public ExpandableHolder[] newArray(int size) {
            return new ExpandableHolder[size];
        }
    };

    public ArrayList<UserPosts> getUserPosts() {
        return userPosts;
    }

    public void setUserPosts(ArrayList<UserPosts> userPosts) {
        this.userPosts = userPosts;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(userPosts);
    }
}
