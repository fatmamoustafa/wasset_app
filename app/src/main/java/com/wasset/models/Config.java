package com.wasset.models;

import java.util.ArrayList;
import java.util.List;

public class Config {

    private boolean status;
    private int code;
    private DataBean data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {

        private String mobile_number;
        private String phone_number;
        private String email;
        private String facebook;
        private String twitter;
        private String about_us;
        private String discount_system;
        private String commission_system;
        private String privacy_policy;
        private String rules ;
        private String agreement ;
        private List<CountriesBean> countries;


        public String getAgreement() {
            return agreement;
        }

        public void setAgreement(String agreement) {
            this.agreement = agreement;
        }

        public String getRules() {
            return rules;
        }

        public void setRules(String rules) {
            this.rules = rules;
        }

        public String getMobile_number() {
            return mobile_number;
        }

        public void setMobile_number(String mobile_number) {
            this.mobile_number = mobile_number;
        }

        public String getPhone_number() {
            return phone_number;
        }

        public void setPhone_number(String phone_number) {
            this.phone_number = phone_number;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getFacebook() {
            return facebook;
        }

        public void setFacebook(String facebook) {
            this.facebook = facebook;
        }

        public String getTwitter() {
            return twitter;
        }

        public void setTwitter(String twitter) {
            this.twitter = twitter;
        }

        public String getAbout_us() {
            return about_us;
        }

        public void setAbout_us(String about_us) {
            this.about_us = about_us;
        }

        public String getDiscount_system() {
            return discount_system;
        }

        public void setDiscount_system(String discount_system) {
            this.discount_system = discount_system;
        }

        public String getCommission_system() {
            return commission_system;
        }

        public void setCommission_system(String commission_system) {
            this.commission_system = commission_system;
        }

        public String getPrivacy_policy() {
            return privacy_policy;
        }

        public void setPrivacy_policy(String privacy_policy) {
            this.privacy_policy = privacy_policy;
        }

        public List<CountriesBean> getCountries() {
            if(countries == null){
                countries = new ArrayList<>();
            }
            return countries;
        }

        public void setCountries(List<CountriesBean> countries) {
            this.countries = countries;
        }

        public static class CountriesBean {
            /**
             * id : 0
             * name : الكل
             * flag : http://hwaseet.com.sa/uploads/flags/ps_16.png
             * mobile_code : null
             * cities : []
             */

            private String id;
            private String name;
            private String flag;
            private Object mobile_code;
            private List<CitiesBean> cities;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getFlag() {
                return flag;
            }

            public void setFlag(String flag) {
                this.flag = flag;
            }

            public Object getMobile_code() {
                return mobile_code;
            }

            public void setMobile_code(Object mobile_code) {
                this.mobile_code = mobile_code;
            }

            public List<CitiesBean> getCities() {
                if(cities == null){
                    cities = new ArrayList<>();
                }
                return cities;
            }

            public void setCities(List<CitiesBean> cities) {
                this.cities = cities;
            }
            public static class CitiesBean {
                /**
                 * id : 1
                 * name : أبو ظبي
                 */

                private String id;
                private String name;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }
            }

        }

    }
}
