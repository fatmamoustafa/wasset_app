package com.wasset.models;

/**
 * Created by macbook on 8/17/18.
 */

public class SelectedAttr {

    String Key ;
    String Value ;


    public SelectedAttr(String key, String value) {
        Key = key;
        Value = value;
    }

    public String getKey() {
        return Key;
    }

    public void setKey(String key) {
        Key = key;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }
}
